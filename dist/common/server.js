"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var path = _interopRequireWildcard(require("path"));

var bodyParser = _interopRequireWildcard(require("body-parser"));

var http = _interopRequireWildcard(require("http"));

var os = _interopRequireWildcard(require("os"));

var _cookieParser = _interopRequireDefault(require("cookie-parser"));

var _swagger = _interopRequireDefault(require("./swagger"));

var _logger = _interopRequireDefault(require("./logger"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const hlf = require('../blockchain/hlf-client');

require('./hlf/config.js');

const app = new _express.default();

class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.set('appPath', `${root}client`);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
      extended: true
    }));
    app.use((0, _cookieParser.default)(process.env.SESSION_SECRET));
    app.use(_express.default.static(`${root}/public`));
    hlf.setClient();
  }

  router(routes) {
    (0, _swagger.default)(app, routes);
    return this;
  }

  listen(port = process.env.PORT) {
    const welcome = p => () => _logger.default.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname()} on port: ${p}}`);

    http.createServer(app).listen(port, welcome(port));
    return app;
  }

}

exports.default = ExpressServer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9jb21tb24vc2VydmVyLmpzIl0sIm5hbWVzIjpbImhsZiIsInJlcXVpcmUiLCJhcHAiLCJFeHByZXNzIiwiRXhwcmVzc1NlcnZlciIsImNvbnN0cnVjdG9yIiwicm9vdCIsInBhdGgiLCJub3JtYWxpemUiLCJfX2Rpcm5hbWUiLCJzZXQiLCJ1c2UiLCJib2R5UGFyc2VyIiwianNvbiIsInVybGVuY29kZWQiLCJleHRlbmRlZCIsInByb2Nlc3MiLCJlbnYiLCJTRVNTSU9OX1NFQ1JFVCIsInN0YXRpYyIsInNldENsaWVudCIsInJvdXRlciIsInJvdXRlcyIsImxpc3RlbiIsInBvcnQiLCJQT1JUIiwid2VsY29tZSIsInAiLCJsb2dnZXIiLCJpbmZvIiwiTk9ERV9FTlYiLCJvcyIsImhvc3RuYW1lIiwiaHR0cCIsImNyZWF0ZVNlcnZlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxNQUFNQSxHQUFHLEdBQUdDLE9BQU8sQ0FBQywwQkFBRCxDQUFuQjs7QUFDQUEsT0FBTyxDQUFDLGlCQUFELENBQVA7O0FBRUEsTUFBTUMsR0FBRyxHQUFHLElBQUlDLGdCQUFKLEVBQVo7O0FBRWUsTUFBTUMsYUFBTixDQUFvQjtBQUNqQ0MsRUFBQUEsV0FBVyxHQUFHO0FBQ1osVUFBTUMsSUFBSSxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZ0IsR0FBRUMsU0FBVSxRQUE1QixDQUFiO0FBQ0FQLElBQUFBLEdBQUcsQ0FBQ1EsR0FBSixDQUFRLFNBQVIsRUFBb0IsR0FBRUosSUFBSyxRQUEzQjtBQUNBSixJQUFBQSxHQUFHLENBQUNTLEdBQUosQ0FBUUMsVUFBVSxDQUFDQyxJQUFYLEVBQVI7QUFDQVgsSUFBQUEsR0FBRyxDQUFDUyxHQUFKLENBQVFDLFVBQVUsQ0FBQ0UsVUFBWCxDQUFzQjtBQUFFQyxNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUF0QixDQUFSO0FBQ0FiLElBQUFBLEdBQUcsQ0FBQ1MsR0FBSixDQUFRLDJCQUFhSyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsY0FBekIsQ0FBUjtBQUNBaEIsSUFBQUEsR0FBRyxDQUFDUyxHQUFKLENBQVFSLGlCQUFRZ0IsTUFBUixDQUFnQixHQUFFYixJQUFLLFNBQXZCLENBQVI7QUFFQU4sSUFBQUEsR0FBRyxDQUFDb0IsU0FBSjtBQUNEOztBQUVEQyxFQUFBQSxNQUFNLENBQUNDLE1BQUQsRUFBUztBQUNiLDBCQUFXcEIsR0FBWCxFQUFnQm9CLE1BQWhCO0FBQ0EsV0FBTyxJQUFQO0FBQ0Q7O0FBRURDLEVBQUFBLE1BQU0sQ0FBQ0MsSUFBSSxHQUFHUixPQUFPLENBQUNDLEdBQVIsQ0FBWVEsSUFBcEIsRUFBMEI7QUFDOUIsVUFBTUMsT0FBTyxHQUFHQyxDQUFDLElBQUksTUFBTUMsZ0JBQU9DLElBQVAsQ0FBYSxxQkFBb0JiLE9BQU8sQ0FBQ0MsR0FBUixDQUFZYSxRQUFaLElBQXdCLGFBQWMsT0FBTUMsRUFBRSxDQUFDQyxRQUFILEVBQWMsYUFBWUwsQ0FBRSxHQUF6RyxDQUEzQjs7QUFDQU0sSUFBQUEsSUFBSSxDQUFDQyxZQUFMLENBQWtCaEMsR0FBbEIsRUFBdUJxQixNQUF2QixDQUE4QkMsSUFBOUIsRUFBb0NFLE9BQU8sQ0FBQ0YsSUFBRCxDQUEzQztBQUNBLFdBQU90QixHQUFQO0FBQ0Q7O0FBckJnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBFeHByZXNzIGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0ICogYXMgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCAqIGFzIGJvZHlQYXJzZXIgZnJvbSAnYm9keS1wYXJzZXInO1xuaW1wb3J0ICogYXMgaHR0cCBmcm9tICdodHRwJztcbmltcG9ydCAqIGFzIG9zIGZyb20gJ29zJztcbmltcG9ydCBjb29raWVQYXJzZXIgZnJvbSAnY29va2llLXBhcnNlcic7XG5pbXBvcnQgc3dhZ2dlcmlmeSBmcm9tICcuL3N3YWdnZXInO1xuaW1wb3J0IGxvZ2dlciBmcm9tICcuL2xvZ2dlcic7XG5cbmNvbnN0IGhsZiA9IHJlcXVpcmUoJy4uL2Jsb2NrY2hhaW4vaGxmLWNsaWVudCcpO1xucmVxdWlyZSgnLi9obGYvY29uZmlnLmpzJyk7XG5cbmNvbnN0IGFwcCA9IG5ldyBFeHByZXNzKCk7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV4cHJlc3NTZXJ2ZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBjb25zdCByb290ID0gcGF0aC5ub3JtYWxpemUoYCR7X19kaXJuYW1lfS8uLi8uLmApO1xuICAgIGFwcC5zZXQoJ2FwcFBhdGgnLCBgJHtyb290fWNsaWVudGApO1xuICAgIGFwcC51c2UoYm9keVBhcnNlci5qc29uKCkpO1xuICAgIGFwcC51c2UoYm9keVBhcnNlci51cmxlbmNvZGVkKHsgZXh0ZW5kZWQ6IHRydWUgfSkpO1xuICAgIGFwcC51c2UoY29va2llUGFyc2VyKHByb2Nlc3MuZW52LlNFU1NJT05fU0VDUkVUKSk7XG4gICAgYXBwLnVzZShFeHByZXNzLnN0YXRpYyhgJHtyb290fS9wdWJsaWNgKSk7XG5cbiAgICBobGYuc2V0Q2xpZW50KCk7XG4gIH1cblxuICByb3V0ZXIocm91dGVzKSB7XG4gICAgc3dhZ2dlcmlmeShhcHAsIHJvdXRlcyk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBsaXN0ZW4ocG9ydCA9IHByb2Nlc3MuZW52LlBPUlQpIHtcbiAgICBjb25zdCB3ZWxjb21lID0gcCA9PiAoKSA9PiBsb2dnZXIuaW5mbyhgdXAgYW5kIHJ1bm5pbmcgaW4gJHtwcm9jZXNzLmVudi5OT0RFX0VOViB8fCAnZGV2ZWxvcG1lbnQnfSBAOiAke29zLmhvc3RuYW1lKCl9IG9uIHBvcnQ6ICR7cH19YCk7XG4gICAgaHR0cC5jcmVhdGVTZXJ2ZXIoYXBwKS5saXN0ZW4ocG9ydCwgd2VsY29tZShwb3J0KSk7XG4gICAgcmV0dXJuIGFwcDtcbiAgfVxufVxuIl19