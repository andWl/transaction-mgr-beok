"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _pino = _interopRequireDefault(require("pino"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const logger = (0, _pino.default)({
  name: process.env.APP_ID,
  level: process.env.LOG_LEVEL
});
var _default = logger;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NlcnZlci9jb21tb24vbG9nZ2VyLmpzIl0sIm5hbWVzIjpbImxvZ2dlciIsIm5hbWUiLCJwcm9jZXNzIiwiZW52IiwiQVBQX0lEIiwibGV2ZWwiLCJMT0dfTEVWRUwiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztBQUVBLE1BQU1BLE1BQU0sR0FBRyxtQkFBSztBQUNsQkMsRUFBQUEsSUFBSSxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsTUFEQTtBQUVsQkMsRUFBQUEsS0FBSyxFQUFFSCxPQUFPLENBQUNDLEdBQVIsQ0FBWUc7QUFGRCxDQUFMLENBQWY7ZUFLZU4sTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBwaW5vIGZyb20gJ3Bpbm8nO1xuXG5jb25zdCBsb2dnZXIgPSBwaW5vKHtcbiAgbmFtZTogcHJvY2Vzcy5lbnYuQVBQX0lELFxuICBsZXZlbDogcHJvY2Vzcy5lbnYuTE9HX0xFVkVMLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGxvZ2dlcjtcbiJdfQ==