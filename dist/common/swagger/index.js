"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _swaggerExpressMiddleware = _interopRequireDefault(require("swagger-express-middleware"));

var path = _interopRequireWildcard(require("path"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _default(app, routes) {
  (0, _swaggerExpressMiddleware.default)(path.join(__dirname, 'Api.yaml'), app, (err, mw) => {
    // Enable Express' case-sensitive and strict options
    // (so "/entities", "/Entities", and "/Entities/" are all different)
    app.enable('case sensitive routing');
    app.enable('strict routing');
    app.use(mw.metadata());
    app.use(mw.files({
      // Override the Express App's case-sensitive
      // and strict-routing settings for the Files middleware.
      caseSensitive: false,
      strict: false
    }, {
      useBasePath: true,
      apiPath: process.env.SWAGGER_API_SPEC // Disable serving the "Api.yaml" file
      // rawFilesPath: false

    }));
    app.use(mw.parseRequest({
      // Configure the cookie parser to use secure cookies
      cookie: {
        secret: process.env.SESSION_SECRET
      },
      // Don't allow JSON content over 100kb (default is 1mb)
      json: {
        limit: process.env.REQUEST_LIMIT
      }
    })); // These two middleware don't have any options (yet)

    app.use(mw.CORS(), mw.validateRequest()); // Error handler to display the validation error as HTML
    // eslint-disable-next-line no-unused-vars, no-shadow

    app.use((err, req, res, next) => {
      res.status(err.status || 500);
      res.send(`<h1>${err.status || 500} Error</h1>` + `<pre>${err.message}</pre>`);
    });
    routes(app);
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NlcnZlci9jb21tb24vc3dhZ2dlci9pbmRleC5qcyJdLCJuYW1lcyI6WyJhcHAiLCJyb3V0ZXMiLCJwYXRoIiwiam9pbiIsIl9fZGlybmFtZSIsImVyciIsIm13IiwiZW5hYmxlIiwidXNlIiwibWV0YWRhdGEiLCJmaWxlcyIsImNhc2VTZW5zaXRpdmUiLCJzdHJpY3QiLCJ1c2VCYXNlUGF0aCIsImFwaVBhdGgiLCJwcm9jZXNzIiwiZW52IiwiU1dBR0dFUl9BUElfU1BFQyIsInBhcnNlUmVxdWVzdCIsImNvb2tpZSIsInNlY3JldCIsIlNFU1NJT05fU0VDUkVUIiwianNvbiIsImxpbWl0IiwiUkVRVUVTVF9MSU1JVCIsIkNPUlMiLCJ2YWxpZGF0ZVJlcXVlc3QiLCJyZXEiLCJyZXMiLCJuZXh0Iiwic3RhdHVzIiwic2VuZCIsIm1lc3NhZ2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7Ozs7O0FBRWUsa0JBQVVBLEdBQVYsRUFBZUMsTUFBZixFQUF1QjtBQUNwQyx5Q0FBV0MsSUFBSSxDQUFDQyxJQUFMLENBQVVDLFNBQVYsRUFBcUIsVUFBckIsQ0FBWCxFQUE2Q0osR0FBN0MsRUFBa0QsQ0FBQ0ssR0FBRCxFQUFNQyxFQUFOLEtBQWE7QUFDN0Q7QUFDQTtBQUNBTixJQUFBQSxHQUFHLENBQUNPLE1BQUosQ0FBVyx3QkFBWDtBQUNBUCxJQUFBQSxHQUFHLENBQUNPLE1BQUosQ0FBVyxnQkFBWDtBQUVBUCxJQUFBQSxHQUFHLENBQUNRLEdBQUosQ0FBUUYsRUFBRSxDQUFDRyxRQUFILEVBQVI7QUFDQVQsSUFBQUEsR0FBRyxDQUFDUSxHQUFKLENBQVFGLEVBQUUsQ0FBQ0ksS0FBSCxDQUFTO0FBQ2Y7QUFDQTtBQUNBQyxNQUFBQSxhQUFhLEVBQUUsS0FIQTtBQUlmQyxNQUFBQSxNQUFNLEVBQUU7QUFKTyxLQUFULEVBS0w7QUFDREMsTUFBQUEsV0FBVyxFQUFFLElBRFo7QUFFREMsTUFBQUEsT0FBTyxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsZ0JBRnBCLENBR0Q7QUFDQTs7QUFKQyxLQUxLLENBQVI7QUFZQWpCLElBQUFBLEdBQUcsQ0FBQ1EsR0FBSixDQUFRRixFQUFFLENBQUNZLFlBQUgsQ0FBZ0I7QUFDdEI7QUFDQUMsTUFBQUEsTUFBTSxFQUFFO0FBQ05DLFFBQUFBLE1BQU0sRUFBRUwsT0FBTyxDQUFDQyxHQUFSLENBQVlLO0FBRGQsT0FGYztBQUt0QjtBQUNBQyxNQUFBQSxJQUFJLEVBQUU7QUFDSkMsUUFBQUEsS0FBSyxFQUFFUixPQUFPLENBQUNDLEdBQVIsQ0FBWVE7QUFEZjtBQU5nQixLQUFoQixDQUFSLEVBbkI2RCxDQThCN0Q7O0FBQ0F4QixJQUFBQSxHQUFHLENBQUNRLEdBQUosQ0FDRUYsRUFBRSxDQUFDbUIsSUFBSCxFQURGLEVBRUVuQixFQUFFLENBQUNvQixlQUFILEVBRkYsRUEvQjZELENBbUM3RDtBQUNBOztBQUNBMUIsSUFBQUEsR0FBRyxDQUFDUSxHQUFKLENBQVEsQ0FBQ0gsR0FBRCxFQUFNc0IsR0FBTixFQUFXQyxHQUFYLEVBQWdCQyxJQUFoQixLQUF5QjtBQUMvQkQsTUFBQUEsR0FBRyxDQUFDRSxNQUFKLENBQVd6QixHQUFHLENBQUN5QixNQUFKLElBQWMsR0FBekI7QUFDQUYsTUFBQUEsR0FBRyxDQUFDRyxJQUFKLENBQ0csT0FBTTFCLEdBQUcsQ0FBQ3lCLE1BQUosSUFBYyxHQUFJLGFBQXpCLEdBQ0MsUUFBT3pCLEdBQUcsQ0FBQzJCLE9BQVEsUUFGdEI7QUFHRCxLQUxEO0FBT0EvQixJQUFBQSxNQUFNLENBQUNELEdBQUQsQ0FBTjtBQUNELEdBN0NEO0FBOENEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1pZGRsZXdhcmUgZnJvbSAnc3dhZ2dlci1leHByZXNzLW1pZGRsZXdhcmUnO1xuaW1wb3J0ICogYXMgcGF0aCBmcm9tICdwYXRoJztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGFwcCwgcm91dGVzKSB7XG4gIG1pZGRsZXdhcmUocGF0aC5qb2luKF9fZGlybmFtZSwgJ0FwaS55YW1sJyksIGFwcCwgKGVyciwgbXcpID0+IHtcbiAgICAvLyBFbmFibGUgRXhwcmVzcycgY2FzZS1zZW5zaXRpdmUgYW5kIHN0cmljdCBvcHRpb25zXG4gICAgLy8gKHNvIFwiL2VudGl0aWVzXCIsIFwiL0VudGl0aWVzXCIsIGFuZCBcIi9FbnRpdGllcy9cIiBhcmUgYWxsIGRpZmZlcmVudClcbiAgICBhcHAuZW5hYmxlKCdjYXNlIHNlbnNpdGl2ZSByb3V0aW5nJyk7XG4gICAgYXBwLmVuYWJsZSgnc3RyaWN0IHJvdXRpbmcnKTtcblxuICAgIGFwcC51c2UobXcubWV0YWRhdGEoKSk7XG4gICAgYXBwLnVzZShtdy5maWxlcyh7XG4gICAgICAvLyBPdmVycmlkZSB0aGUgRXhwcmVzcyBBcHAncyBjYXNlLXNlbnNpdGl2ZVxuICAgICAgLy8gYW5kIHN0cmljdC1yb3V0aW5nIHNldHRpbmdzIGZvciB0aGUgRmlsZXMgbWlkZGxld2FyZS5cbiAgICAgIGNhc2VTZW5zaXRpdmU6IGZhbHNlLFxuICAgICAgc3RyaWN0OiBmYWxzZSxcbiAgICB9LCB7XG4gICAgICB1c2VCYXNlUGF0aDogdHJ1ZSxcbiAgICAgIGFwaVBhdGg6IHByb2Nlc3MuZW52LlNXQUdHRVJfQVBJX1NQRUMsXG4gICAgICAvLyBEaXNhYmxlIHNlcnZpbmcgdGhlIFwiQXBpLnlhbWxcIiBmaWxlXG4gICAgICAvLyByYXdGaWxlc1BhdGg6IGZhbHNlXG4gICAgfSkpO1xuXG4gICAgYXBwLnVzZShtdy5wYXJzZVJlcXVlc3Qoe1xuICAgICAgLy8gQ29uZmlndXJlIHRoZSBjb29raWUgcGFyc2VyIHRvIHVzZSBzZWN1cmUgY29va2llc1xuICAgICAgY29va2llOiB7XG4gICAgICAgIHNlY3JldDogcHJvY2Vzcy5lbnYuU0VTU0lPTl9TRUNSRVQsXG4gICAgICB9LFxuICAgICAgLy8gRG9uJ3QgYWxsb3cgSlNPTiBjb250ZW50IG92ZXIgMTAwa2IgKGRlZmF1bHQgaXMgMW1iKVxuICAgICAganNvbjoge1xuICAgICAgICBsaW1pdDogcHJvY2Vzcy5lbnYuUkVRVUVTVF9MSU1JVCxcbiAgICAgIH0sXG4gICAgfSkpO1xuXG4gICAgLy8gVGhlc2UgdHdvIG1pZGRsZXdhcmUgZG9uJ3QgaGF2ZSBhbnkgb3B0aW9ucyAoeWV0KVxuICAgIGFwcC51c2UoXG4gICAgICBtdy5DT1JTKCksXG4gICAgICBtdy52YWxpZGF0ZVJlcXVlc3QoKSk7XG5cbiAgICAvLyBFcnJvciBoYW5kbGVyIHRvIGRpc3BsYXkgdGhlIHZhbGlkYXRpb24gZXJyb3IgYXMgSFRNTFxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby11bnVzZWQtdmFycywgbm8tc2hhZG93XG4gICAgYXBwLnVzZSgoZXJyLCByZXEsIHJlcywgbmV4dCkgPT4ge1xuICAgICAgcmVzLnN0YXR1cyhlcnIuc3RhdHVzIHx8IDUwMCk7XG4gICAgICByZXMuc2VuZChcbiAgICAgICAgYDxoMT4ke2Vyci5zdGF0dXMgfHwgNTAwfSBFcnJvcjwvaDE+YCArXG4gICAgICAgIGA8cHJlPiR7ZXJyLm1lc3NhZ2V9PC9wcmU+YCk7XG4gICAgfSk7XG5cbiAgICByb3V0ZXMoYXBwKTtcbiAgfSk7XG59XG4iXX0=