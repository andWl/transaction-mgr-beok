"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("./common/env");

var _server = _interopRequireDefault(require("./common/server"));

var _routes = _interopRequireDefault(require("./routes"));

require("./grpc-scheduler");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = new _server.default().router(_routes.default).listen(process.env.PORT);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NlcnZlci9pbmRleC5qcyJdLCJuYW1lcyI6WyJTZXJ2ZXIiLCJyb3V0ZXIiLCJyb3V0ZXMiLCJsaXN0ZW4iLCJwcm9jZXNzIiwiZW52IiwiUE9SVCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOzs7O2VBRWUsSUFBSUEsZUFBSixHQUNaQyxNQURZLENBQ0xDLGVBREssRUFFWkMsTUFGWSxDQUVMQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsSUFGUCxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICcuL2NvbW1vbi9lbnYnO1xuaW1wb3J0IFNlcnZlciBmcm9tICcuL2NvbW1vbi9zZXJ2ZXInO1xuaW1wb3J0IHJvdXRlcyBmcm9tICcuL3JvdXRlcyc7XG5pbXBvcnQgJy4vZ3JwYy1zY2hlZHVsZXInO1xuXG5leHBvcnQgZGVmYXVsdCBuZXcgU2VydmVyKClcbiAgLnJvdXRlcihyb3V0ZXMpXG4gIC5saXN0ZW4ocHJvY2Vzcy5lbnYuUE9SVCk7XG4iXX0=