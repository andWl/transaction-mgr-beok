"use strict";

var _logger = _interopRequireDefault(require("../../common/logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 Copyright 2018 KT All Rights Reserved.
*/

/* eslint-disable */
const baseChannel = require('fabric-client/lib/Channel');

const utils = require('fabric-client/lib/utils.js');

const clientUtils = require('./ClientUtilsExtends');

const Constants = require('fabric-client/lib/Constants.js');

const grpc = require('grpc');

const Long = require('long');

const path = require('path');

const _ccProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/chaincode.proto')).protos;
const _transProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/transaction.proto')).protos;
const _proposalProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/proposal.proto')).protos;
const _commonProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/common/common.proto')).common;
const _identityProto = grpc.load(path.join(__dirname, '/protos/msp/identities.proto')).msp;
const _gossipProto = grpc.load(path.join(__dirname, '/protos/gossip/message.proto')).gossip;
const _discoveryProto = grpc.load(path.join(__dirname, '/protos/discovery/protocol.proto')).discovery;
const ChannelExtends = class extends baseChannel {
  constructor(name, clientContext) {
    super(name, clientContext);
  }

  sendTransactionProposalOB(request, timeout) {
    _logger.default.debug('sendTransactionProposal - start');

    if (!request) {
      throw new Error('Missing request object for this transaction proposal');
    }

    request.targets = this._getTargets(request.targets, Constants.NetworkConfig.ENDORSING_PEER_ROLE);
    return ChannelExtends.sendTransactionProposalOB(request, this._name, this._clientContext, timeout);
  }

  static sendTransactionProposalOB(request, channelId, clientContext, timeout) {
    // Verify that a Peer has been added
    var errorMsg = clientUtils.checkProposalRequest(request);

    if (errorMsg) {// do nothing so we skip the rest of the checks
    } else if (!request.args) {
      // args is not optional because we need for transaction to execute
      errorMsg = 'Missing "args" in Transaction proposal request';
    } else if (!request.targets || request.targets.length < 1) {
      errorMsg = 'Missing peer objects in Transaction proposal';
    }

    if (errorMsg) {
      _logger.default.error('sendTransactionProposal error ' + errorMsg);

      throw new Error(errorMsg);
    }

    var args = [];
    args.push(Buffer.from(request.fcn ? request.fcn : 'invoke', 'utf8'));

    _logger.default.debug('sendTransactionProposal - adding function arg:%s', request.fcn ? request.fcn : 'invoke');

    for (let i = 0; i < request.args.length; i++) {
      _logger.default.debug('sendTransactionProposal - adding arg:%s', request.args[i]);

      args.push(Buffer.from(request.args[i], 'utf8'));
    } //special case to support the bytes argument of the query by hash


    if (request.argbytes) {
      _logger.default.debug('sendTransactionProposal - adding the argument :: argbytes');

      args.push(request.argbytes);
    } else {
      _logger.default.debug('sendTransactionProposal - not adding the argument :: argbytes');
    }

    let invokeSpec = {
      type: _ccProto.ChaincodeSpec.Type.GOLANG,
      chaincode_id: {
        name: request.chaincodeId
      },
      input: {
        args: args
      }
    };
    var proposal, header;
    var signer = null;

    if (request.signer) {
      signer = request.signer;
    } else {
      signer = clientContext._getSigningIdentity(request.txId.isAdmin());
    }

    var channelHeader = clientUtils.buildChannelHeader(_commonProto.HeaderType.ENDORSER_TRANSACTION, channelId, request.txId.getTransactionID(), null, request.chaincodeId, clientUtils.buildCurrentTimestamp(), request.targets[0].getClientCertHash());
    header = clientUtils.buildHeaderOB(signer, channelHeader, request.txId.getNonce());
    proposal = clientUtils.buildProposal(invokeSpec, header, request.transientMap);
    let signed_proposal = clientUtils.signProposalOB(signer, proposal);
    return clientUtils.sendPeersProposal(request.targets, signed_proposal, timeout).then(function (responses) {
      return Promise.resolve([responses, proposal]);
    }).catch(function (err) {
      _logger.default.error('Failed Proposal. Error: %s', err.stack ? err.stack : err);

      return Promise.reject(err);
    });
  }

  sendTransactionOB(request) {
    _logger.default.debug('sendTransaction - start :: channel %s', this);

    var errorMsg = null;

    if (request) {
      // Verify that data is being passed in
      if (!request.proposalResponses) {
        errorMsg = 'Missing "proposalResponses" parameter in transaction request';
      }

      if (!request.proposal) {
        errorMsg = 'Missing "proposal" parameter in transaction request';
      }
    } else {
      errorMsg = 'Missing input request object on the transaction request';
    }

    if (errorMsg) {
      _logger.default.error('sendTransaction error ' + errorMsg);

      throw new Error(errorMsg);
    }

    let proposalResponses = request.proposalResponses;
    let chaincodeProposal = request.proposal;
    var endorsements = [];
    let proposalResponse = proposalResponses;

    if (Array.isArray(proposalResponses)) {
      for (let i = 0; i < proposalResponses.length; i++) {
        // make sure only take the valid responses to set on the consolidated response object
        // to use in the transaction object
        if (proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          proposalResponse = proposalResponses[i];
          endorsements.push(proposalResponse.endorsement);
        }
      }
    } else {
      if (proposalResponse && proposalResponse.response && proposalResponse.response.status === 200) {
        endorsements.push(proposalResponse.endorsement);
      }
    }

    if (endorsements.length < 1) {
      _logger.default.error('sendTransaction - no valid endorsements found');

      throw new Error('no valid endorsements found');
    } // verify that we have an orderer configured


    var orderer = this._clientContext.getTargetOrderer(request.orderer, this._orderers, this._name);

    var use_admin_signer = false;

    if (request.txId) {
      use_admin_signer = request.txId.isAdmin();
    }

    let header = _commonProto.Header.decode(chaincodeProposal.getHeader());

    var chaincodeEndorsedAction = new _transProto.ChaincodeEndorsedAction();
    chaincodeEndorsedAction.setProposalResponsePayload(proposalResponse.payload);
    chaincodeEndorsedAction.setEndorsements(endorsements);
    var chaincodeActionPayload = new _transProto.ChaincodeActionPayload();
    chaincodeActionPayload.setAction(chaincodeEndorsedAction); // the TransientMap field inside the original proposal payload is only meant for the
    // endorsers to use from inside the chaincode. This must be taken out before sending
    // to the orderer, otherwise the transaction will be rejected by the validators when
    // it compares the proposal hash calculated by the endorsers and returned in the
    // proposal response, which was calculated without the TransientMap

    var originalChaincodeProposalPayload = _proposalProto.ChaincodeProposalPayload.decode(chaincodeProposal.payload);

    var chaincodeProposalPayloadNoTrans = new _proposalProto.ChaincodeProposalPayload();
    chaincodeProposalPayloadNoTrans.setInput(originalChaincodeProposalPayload.input); // only set the input field, skipping the TransientMap

    chaincodeActionPayload.setChaincodeProposalPayload(chaincodeProposalPayloadNoTrans.toBuffer());
    var transactionAction = new _transProto.TransactionAction();
    transactionAction.setHeader(header.getSignatureHeader());
    transactionAction.setPayload(chaincodeActionPayload.toBuffer());
    var actions = [];
    actions.push(transactionAction);
    var transaction = new _transProto.Transaction();
    transaction.setActions(actions);
    var payload = new _commonProto.Payload();
    payload.setHeader(header);
    payload.setData(transaction.toBuffer());
    let payload_bytes = payload.toBuffer();

    var signer = this._clientContext._getSigningIdentity(use_admin_signer);

    let signature = "0"; // building manually or will get protobuf errors on send

    var envelope = {
      signature: signature,
      payload: payload_bytes
    };
    return orderer.sendBroadcast(envelope); //orderer.sendBroadcastOB(envelope);
  }

  async initializeOB(request) {
    const method = 'initialize';

    _logger.default.debug('%s - start', method);

    let endorsement_handler_path = null;
    let commit_handler_path = null;

    if (request) {
      if (request.configUpate) {
        _logger.default.debug('%s - have a configupdate', method);

        this.loadConfigUpdate(request.configUpate);
        return true;
      } else {
        if (typeof request.discover !== 'undefined') {
          if (typeof request.discover === 'boolean') {
            _logger.default.debug('%s - user requested discover %s', method, request.discover);

            this._use_discovery = request.discover;
          } else {
            throw new Error('Request parameter "discover" must be boolean');
          }
        }

        if (request.endorsementHandler) {
          _logger.default.debug('%s - user requested endorsementHandler %s', method, request.endorsementHandler);

          endorsement_handler_path = request.endorsementHandler;
        }

        if (request.commitHandler) {
          _logger.default.debug('%s - user requested commitHandler %s', method, request.commitHandler);

          commit_handler_path = request.commitHandler;
        }
      }
    } // setup the endorsement handler


    if (!endorsement_handler_path && this._use_discovery) {
      endorsement_handler_path = utils.getConfigSetting('endorsement-handler');

      _logger.default.debug('%s - using config setting for endorsement handler ::%s', method, endorsement_handler_path);
    }

    if (endorsement_handler_path) {
      this._endorsement_handler = require(endorsement_handler_path).create(this);
      await this._endorsement_handler.initialize();
    } // setup the commit handler


    if (!commit_handler_path) {
      commit_handler_path = utils.getConfigSetting('commit-handler');

      _logger.default.debug('%s - using config setting for commit handler ::%s', method, commit_handler_path);
    }

    if (commit_handler_path) {
      this._commit_handler = require(commit_handler_path).create(this);
      await this._commit_handler.initialize();
    }

    const results = await this._initializeOB(request);
    return results;
  }

  async _initializeOB(request) {
    const method = '_initialize';

    _logger.default.debug('%s - start', method);

    this._discovery_results = null;
    this._last_discover_timestamp = null;
    this._last_refresh_request = Object.assign({}, request);
    let target_peer = this._discovery_peer;

    if (request && request.target) {
      target_peer = request.target;
    }

    if (this._use_discovery) {
      _logger.default.debug('%s - starting discovery', method);

      target_peer = this._getTargetForDiscovery(target_peer);

      if (!target_peer) {
        throw new Error('No target provided for discovery services');
      }

      try {
        const discover_request = {
          target: target_peer,
          config: true
        };
        const discovery_results = await this._discoverOB(discover_request);

        if (discovery_results) {
          if (discovery_results.msps) {
            this._buildDiscoveryMSPs(discovery_results);
          } else {
            throw Error('No MSP information found');
          }

          if (discovery_results.orderers) {
            this._buildDiscoveryOrderers(discovery_results, discovery_results.msps, request);
          }

          if (discovery_results.peers_by_org) {
            this._buildDiscoveryPeers(discovery_results, discovery_results.msps, request);
          }
        }

        discovery_results.endorsement_plans = [];
        const interests = [];
        const plan_ids = [];

        this._discovery_interests.forEach((interest, plan_id) => {
          _logger.default.debug('%s - have interest of:%s', method, plan_id);

          plan_ids.push(plan_id);
          interests.push(interest);
        });

        for (const i in plan_ids) {
          const plan_id = plan_ids[i];
          const interest = interests[i];
          const discover_request = {
            target: target_peer,
            interests: [interest]
          };
          let discover_interest_results = null;

          try {
            discover_interest_results = await this._discoverOB(discover_request);
          } catch (error) {
            _logger.default.error('Not able to get an endorsement plan for %s', plan_id);
          }

          if (discover_interest_results && discover_interest_results.endorsement_plans && discover_interest_results.endorsement_plans[0]) {
            const plan = this._buildDiscoveryEndorsementPlan(discover_interest_results, plan_id, discovery_results.msps, request);

            discovery_results.endorsement_plans.push(plan);

            _logger.default.debug('Added an endorsement plan for %s', plan_id);
          } else {
            _logger.default.debug('Not adding an endorsement plan for %s', plan_id);
          }
        }

        discovery_results.timestamp = Date.now();
        this._discovery_results = discovery_results;
        this._discovery_peer = target_peer;
        this._last_discover_timestamp = discovery_results.timestamp;
        return discovery_results;
      } catch (error) {
        _logger.default.error(error);

        throw Error('Failed to discover ::' + error.toString());
      }
    } else {
      target_peer = this._getFirstAvailableTarget(target_peer);

      if (!target_peer) {
        throw new Error('No target provided for non-discovery initialization');
      }

      const config_envelope = await this.getChannelConfig(target_peer);

      _logger.default.debug('initialize - got config envelope from getChannelConfig :: %j', config_envelope);

      const config_items = this.loadConfigEnvelope(config_envelope);
      return config_items;
    }
  }

  async _discoverOB(request) {
    const method = 'discover';
    const self = this;

    _logger.default.debug('%s - start', method);

    const results = {};

    if (!request) {
      request = {};
    }

    let useAdmin = true; //default

    if (typeof request.useAdmin === 'boolean') {
      useAdmin = request.useAdmin;
    }

    const target_peer = this._getTargetForDiscovery(request.target);

    const signer = this._clientContext._getSigningIdentity(useAdmin); //use the admin if assigned


    const discovery_request = new _discoveryProto.Request();
    const authentication = new _discoveryProto.AuthInfo();
    authentication.setClientIdentity(signer.serialize());

    const cert_hash = this._clientContext.getClientCertHash(true);

    if (cert_hash) {
      authentication.setClientTlsCertHash(cert_hash);
    }

    discovery_request.setAuthentication(authentication); // be sure to add all entries to this array before setting into the
    // grpc object

    const queries = []; // if doing local it will be index 0 of the results

    if (request.local) {
      const query = new _discoveryProto.Query();
      queries.push(query);
      const local_peers = new _discoveryProto.LocalPeerQuery();
      query.setLocalPeers(local_peers);

      _logger.default.debug('%s - adding local peers query', method);
    }

    if (request.config) {
      let query = new _discoveryProto.Query();
      queries.push(query);
      query.setChannel(this.getName());
      const config_query = new _discoveryProto.ConfigQuery();
      query.setConfigQuery(config_query);

      _logger.default.debug('%s - adding config query', method);

      query = new _discoveryProto.Query();
      queries.push(query);
      query.setChannel(this.getName());
      const peer_query = new _discoveryProto.PeerMembershipQuery();
      query.setPeerQuery(peer_query);

      _logger.default.debug('%s - adding channel peers query', method);
    } // add a chaincode query to get endorsement plans


    if (request.interests && request.interests.length > 0) {
      const query = new _discoveryProto.Query();
      queries.push(query);
      query.setChannel(this.getName());
      const interests = [];

      for (const interest of request.interests) {
        const proto_interest = this._buildProtoChaincodeInterest(interest);

        interests.push(proto_interest);
      }

      const cc_query = new _discoveryProto.ChaincodeQuery();
      cc_query.setInterests(interests);
      query.setCcQuery(cc_query);

      _logger.default.debug('%s - adding chaincodes/collection query', method);
    } // be sure to set the array after completely building it


    discovery_request.setQueries(queries); // build up the outbound request object

    const signed_request = clientUtils.toEnvelope(clientUtils.signProposal(signer, discovery_request));
    const response = await target_peer.sendDiscovery(signed_request);

    _logger.default.debug('%s - processing discovery response', method);

    if (response && response.results) {
      let error_msg = null;

      _logger.default.debug('%s - parse discovery response', method);

      for (const index in response.results) {
        const result = response.results[index];

        if (!result) {
          error_msg = 'Discover results are missing';
          break;
        } else if (result.result === 'error') {
          _logger.default.error('Channel:%s received discovery error:%s', self.getName(), result.error.content);

          error_msg = result.error.content;
          break;
        } else {
          _logger.default.debug('%s - process results', method);

          if (result.config_result) {
            const config = self._processDiscoveryConfigResults(result.config_result);

            results.msps = config.msps;
            results.orderers = config.orderers;
          }

          if (result.members) {
            if (request.local && index === 0) {
              results.local_peers = self._processDiscoveryMembershipResultsOB(result.members);
            } else {
              results.peers_by_org = self._processDiscoveryMembershipResultsOB(result.members);
            }
          }

          if (result.cc_query_res) {
            results.endorsement_plans = self._processDiscoveryChaincodeResultsOB(result.cc_query_res);
          }

          _logger.default.debug('%s - completed processing results', method);
        }
      }

      if (error_msg) {
        throw Error('Channel:' + self.getName() + ' Discovery error:' + error_msg);
      } else {
        return results;
      }
    } else {
      throw new Error('Discovery has failed to return results');
    }
  }

  _processDiscoveryChaincodeResultsOB(q_chaincodes) {
    const method = '_processDiscoveryChaincodeResults';

    _logger.default.debug('%s - start', method);

    const endorsement_plans = [];

    if (q_chaincodes && q_chaincodes.content) {
      if (Array.isArray(q_chaincodes.content)) {
        for (const index in q_chaincodes.content) {
          const q_endors_desc = q_chaincodes.content[index];
          const endorsement_plan = {};
          endorsement_plan.chaincode = q_endors_desc.chaincode;
          endorsement_plans.push(endorsement_plan); // GROUPS

          endorsement_plan.groups = {};

          for (const group_name in q_endors_desc.endorsers_by_groups) {
            _logger.default.debug('%s - found group: %s', method, group_name);

            const group = {};
            group.peers = this._processPeersOB(q_endors_desc.endorsers_by_groups[group_name].peers); //all done with this group

            endorsement_plan.groups[group_name] = group;
          } // LAYOUTS


          endorsement_plan.layouts = [];

          for (const index in q_endors_desc.layouts) {
            const q_layout = q_endors_desc.layouts[index];
            const layout = {};

            for (const group_name in q_layout.quantities_by_group) {
              layout[group_name] = q_layout.quantities_by_group[group_name];
            }

            _logger.default.debug('%s - layout :%j', method, layout);

            endorsement_plan.layouts.push(layout);
          }
        }
      }
    }

    return endorsement_plans;
  }

  _processDiscoveryMembershipResultsOB(q_members) {
    const method = '_processDiscoveryChannelMembershipResults';

    _logger.default.debug('%s - start', method);

    const peers_by_org = {};

    if (q_members && q_members.peers_by_org) {
      for (const mspid in q_members.peers_by_org) {
        _logger.default.debug('%s - found org:%s', method, mspid);

        peers_by_org[mspid] = {};
        peers_by_org[mspid].peers = this._processPeersOB(q_members.peers_by_org[mspid].peers);
      }
    }

    return peers_by_org;
  }

  _processPeersOB(q_peers) {
    const method = '_processPeersOB';
    const peers = [];
    q_peers.forEach(q_peer => {
      const peer = {}; // IDENTITY

      const q_identity = _identityProto.SerializedIdentity.decode(q_peer.identity);

      peer.mspid = q_identity.mspid; // MEMBERSHIP

      const q_membership_message = _gossipProto.GossipMessage.decode(q_peer.membership_info.payload);

      peer.endpoint = q_membership_message.alive_msg.membership.endpoint;

      _logger.default.debug('%s - found peer :%s', method, peer.endpoint); // STATE


      if (q_peer.state_info) {
        const message_s = _gossipProto.GossipMessage.decode(q_peer.state_info.payload);

        if (message_s && message_s.state_info && message_s.state_info.properties && message_s.state_info.properties.ledger_height) {
          peer.ledger_height = Long.fromValue(message_s.state_info.properties.ledger_height);
        } else {
          _logger.default.debug('%s - did not find ledger_height', method);

          peer.ledger_height = Long.fromValue(0);
        }

        _logger.default.debug('%s - found ledger_height :%s', method, peer.ledger_height);

        if (message_s && message_s.state_info && message_s.state_info.properties && message_s.state_info.properties.load_info) {
          peer.load_info = message_s.state_info.properties.load_info;
        } else {
          _logger.default.debug('%s - did not find ledger_height', method);

          peer.load_info = 0;
        }

        _logger.default.debug('%s - found ledger_height :%s', method, peer.load_info);

        peer.chaincodes = [];

        for (const index in message_s.state_info.properties.chaincodes) {
          const q_chaincode = message_s.state_info.properties.chaincodes[index];
          const chaincode = {};
          chaincode.name = q_chaincode.getName();
          chaincode.version = q_chaincode.getVersion(); //TODO metadata ?

          _logger.default.debug('%s - found chaincode :%j', method, chaincode);

          peer.chaincodes.push(chaincode);
        }
      } //all done with this peer


      peers.push(peer);
    });
    return peers;
  }

};
module.exports = ChannelExtends;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NlcnZlci9ibG9ja2NoYWluL29iL0NoYW5uZWxFeHRlbmRzLmpzIl0sIm5hbWVzIjpbImJhc2VDaGFubmVsIiwicmVxdWlyZSIsInV0aWxzIiwiY2xpZW50VXRpbHMiLCJDb25zdGFudHMiLCJncnBjIiwiTG9uZyIsInBhdGgiLCJfY2NQcm90byIsImxvYWQiLCJqb2luIiwiX19kaXJuYW1lIiwicHJvdG9zIiwiX3RyYW5zUHJvdG8iLCJfcHJvcG9zYWxQcm90byIsIl9jb21tb25Qcm90byIsImNvbW1vbiIsIl9pZGVudGl0eVByb3RvIiwibXNwIiwiX2dvc3NpcFByb3RvIiwiZ29zc2lwIiwiX2Rpc2NvdmVyeVByb3RvIiwiZGlzY292ZXJ5IiwiQ2hhbm5lbEV4dGVuZHMiLCJjb25zdHJ1Y3RvciIsIm5hbWUiLCJjbGllbnRDb250ZXh0Iiwic2VuZFRyYW5zYWN0aW9uUHJvcG9zYWxPQiIsInJlcXVlc3QiLCJ0aW1lb3V0IiwibG9nZ2VyIiwiZGVidWciLCJFcnJvciIsInRhcmdldHMiLCJfZ2V0VGFyZ2V0cyIsIk5ldHdvcmtDb25maWciLCJFTkRPUlNJTkdfUEVFUl9ST0xFIiwiX25hbWUiLCJfY2xpZW50Q29udGV4dCIsImNoYW5uZWxJZCIsImVycm9yTXNnIiwiY2hlY2tQcm9wb3NhbFJlcXVlc3QiLCJhcmdzIiwibGVuZ3RoIiwiZXJyb3IiLCJwdXNoIiwiQnVmZmVyIiwiZnJvbSIsImZjbiIsImkiLCJhcmdieXRlcyIsImludm9rZVNwZWMiLCJ0eXBlIiwiQ2hhaW5jb2RlU3BlYyIsIlR5cGUiLCJHT0xBTkciLCJjaGFpbmNvZGVfaWQiLCJjaGFpbmNvZGVJZCIsImlucHV0IiwicHJvcG9zYWwiLCJoZWFkZXIiLCJzaWduZXIiLCJfZ2V0U2lnbmluZ0lkZW50aXR5IiwidHhJZCIsImlzQWRtaW4iLCJjaGFubmVsSGVhZGVyIiwiYnVpbGRDaGFubmVsSGVhZGVyIiwiSGVhZGVyVHlwZSIsIkVORE9SU0VSX1RSQU5TQUNUSU9OIiwiZ2V0VHJhbnNhY3Rpb25JRCIsImJ1aWxkQ3VycmVudFRpbWVzdGFtcCIsImdldENsaWVudENlcnRIYXNoIiwiYnVpbGRIZWFkZXJPQiIsImdldE5vbmNlIiwiYnVpbGRQcm9wb3NhbCIsInRyYW5zaWVudE1hcCIsInNpZ25lZF9wcm9wb3NhbCIsInNpZ25Qcm9wb3NhbE9CIiwic2VuZFBlZXJzUHJvcG9zYWwiLCJ0aGVuIiwicmVzcG9uc2VzIiwiUHJvbWlzZSIsInJlc29sdmUiLCJjYXRjaCIsImVyciIsInN0YWNrIiwicmVqZWN0Iiwic2VuZFRyYW5zYWN0aW9uT0IiLCJwcm9wb3NhbFJlc3BvbnNlcyIsImNoYWluY29kZVByb3Bvc2FsIiwiZW5kb3JzZW1lbnRzIiwicHJvcG9zYWxSZXNwb25zZSIsIkFycmF5IiwiaXNBcnJheSIsInJlc3BvbnNlIiwic3RhdHVzIiwiZW5kb3JzZW1lbnQiLCJvcmRlcmVyIiwiZ2V0VGFyZ2V0T3JkZXJlciIsIl9vcmRlcmVycyIsInVzZV9hZG1pbl9zaWduZXIiLCJIZWFkZXIiLCJkZWNvZGUiLCJnZXRIZWFkZXIiLCJjaGFpbmNvZGVFbmRvcnNlZEFjdGlvbiIsIkNoYWluY29kZUVuZG9yc2VkQWN0aW9uIiwic2V0UHJvcG9zYWxSZXNwb25zZVBheWxvYWQiLCJwYXlsb2FkIiwic2V0RW5kb3JzZW1lbnRzIiwiY2hhaW5jb2RlQWN0aW9uUGF5bG9hZCIsIkNoYWluY29kZUFjdGlvblBheWxvYWQiLCJzZXRBY3Rpb24iLCJvcmlnaW5hbENoYWluY29kZVByb3Bvc2FsUGF5bG9hZCIsIkNoYWluY29kZVByb3Bvc2FsUGF5bG9hZCIsImNoYWluY29kZVByb3Bvc2FsUGF5bG9hZE5vVHJhbnMiLCJzZXRJbnB1dCIsInNldENoYWluY29kZVByb3Bvc2FsUGF5bG9hZCIsInRvQnVmZmVyIiwidHJhbnNhY3Rpb25BY3Rpb24iLCJUcmFuc2FjdGlvbkFjdGlvbiIsInNldEhlYWRlciIsImdldFNpZ25hdHVyZUhlYWRlciIsInNldFBheWxvYWQiLCJhY3Rpb25zIiwidHJhbnNhY3Rpb24iLCJUcmFuc2FjdGlvbiIsInNldEFjdGlvbnMiLCJQYXlsb2FkIiwic2V0RGF0YSIsInBheWxvYWRfYnl0ZXMiLCJzaWduYXR1cmUiLCJlbnZlbG9wZSIsInNlbmRCcm9hZGNhc3QiLCJpbml0aWFsaXplT0IiLCJtZXRob2QiLCJlbmRvcnNlbWVudF9oYW5kbGVyX3BhdGgiLCJjb21taXRfaGFuZGxlcl9wYXRoIiwiY29uZmlnVXBhdGUiLCJsb2FkQ29uZmlnVXBkYXRlIiwiZGlzY292ZXIiLCJfdXNlX2Rpc2NvdmVyeSIsImVuZG9yc2VtZW50SGFuZGxlciIsImNvbW1pdEhhbmRsZXIiLCJnZXRDb25maWdTZXR0aW5nIiwiX2VuZG9yc2VtZW50X2hhbmRsZXIiLCJjcmVhdGUiLCJpbml0aWFsaXplIiwiX2NvbW1pdF9oYW5kbGVyIiwicmVzdWx0cyIsIl9pbml0aWFsaXplT0IiLCJfZGlzY292ZXJ5X3Jlc3VsdHMiLCJfbGFzdF9kaXNjb3Zlcl90aW1lc3RhbXAiLCJfbGFzdF9yZWZyZXNoX3JlcXVlc3QiLCJPYmplY3QiLCJhc3NpZ24iLCJ0YXJnZXRfcGVlciIsIl9kaXNjb3ZlcnlfcGVlciIsInRhcmdldCIsIl9nZXRUYXJnZXRGb3JEaXNjb3ZlcnkiLCJkaXNjb3Zlcl9yZXF1ZXN0IiwiY29uZmlnIiwiZGlzY292ZXJ5X3Jlc3VsdHMiLCJfZGlzY292ZXJPQiIsIm1zcHMiLCJfYnVpbGREaXNjb3ZlcnlNU1BzIiwib3JkZXJlcnMiLCJfYnVpbGREaXNjb3ZlcnlPcmRlcmVycyIsInBlZXJzX2J5X29yZyIsIl9idWlsZERpc2NvdmVyeVBlZXJzIiwiZW5kb3JzZW1lbnRfcGxhbnMiLCJpbnRlcmVzdHMiLCJwbGFuX2lkcyIsIl9kaXNjb3ZlcnlfaW50ZXJlc3RzIiwiZm9yRWFjaCIsImludGVyZXN0IiwicGxhbl9pZCIsImRpc2NvdmVyX2ludGVyZXN0X3Jlc3VsdHMiLCJwbGFuIiwiX2J1aWxkRGlzY292ZXJ5RW5kb3JzZW1lbnRQbGFuIiwidGltZXN0YW1wIiwiRGF0ZSIsIm5vdyIsInRvU3RyaW5nIiwiX2dldEZpcnN0QXZhaWxhYmxlVGFyZ2V0IiwiY29uZmlnX2VudmVsb3BlIiwiZ2V0Q2hhbm5lbENvbmZpZyIsImNvbmZpZ19pdGVtcyIsImxvYWRDb25maWdFbnZlbG9wZSIsInNlbGYiLCJ1c2VBZG1pbiIsImRpc2NvdmVyeV9yZXF1ZXN0IiwiUmVxdWVzdCIsImF1dGhlbnRpY2F0aW9uIiwiQXV0aEluZm8iLCJzZXRDbGllbnRJZGVudGl0eSIsInNlcmlhbGl6ZSIsImNlcnRfaGFzaCIsInNldENsaWVudFRsc0NlcnRIYXNoIiwic2V0QXV0aGVudGljYXRpb24iLCJxdWVyaWVzIiwibG9jYWwiLCJxdWVyeSIsIlF1ZXJ5IiwibG9jYWxfcGVlcnMiLCJMb2NhbFBlZXJRdWVyeSIsInNldExvY2FsUGVlcnMiLCJzZXRDaGFubmVsIiwiZ2V0TmFtZSIsImNvbmZpZ19xdWVyeSIsIkNvbmZpZ1F1ZXJ5Iiwic2V0Q29uZmlnUXVlcnkiLCJwZWVyX3F1ZXJ5IiwiUGVlck1lbWJlcnNoaXBRdWVyeSIsInNldFBlZXJRdWVyeSIsInByb3RvX2ludGVyZXN0IiwiX2J1aWxkUHJvdG9DaGFpbmNvZGVJbnRlcmVzdCIsImNjX3F1ZXJ5IiwiQ2hhaW5jb2RlUXVlcnkiLCJzZXRJbnRlcmVzdHMiLCJzZXRDY1F1ZXJ5Iiwic2V0UXVlcmllcyIsInNpZ25lZF9yZXF1ZXN0IiwidG9FbnZlbG9wZSIsInNpZ25Qcm9wb3NhbCIsInNlbmREaXNjb3ZlcnkiLCJlcnJvcl9tc2ciLCJpbmRleCIsInJlc3VsdCIsImNvbnRlbnQiLCJjb25maWdfcmVzdWx0IiwiX3Byb2Nlc3NEaXNjb3ZlcnlDb25maWdSZXN1bHRzIiwibWVtYmVycyIsIl9wcm9jZXNzRGlzY292ZXJ5TWVtYmVyc2hpcFJlc3VsdHNPQiIsImNjX3F1ZXJ5X3JlcyIsIl9wcm9jZXNzRGlzY292ZXJ5Q2hhaW5jb2RlUmVzdWx0c09CIiwicV9jaGFpbmNvZGVzIiwicV9lbmRvcnNfZGVzYyIsImVuZG9yc2VtZW50X3BsYW4iLCJjaGFpbmNvZGUiLCJncm91cHMiLCJncm91cF9uYW1lIiwiZW5kb3JzZXJzX2J5X2dyb3VwcyIsImdyb3VwIiwicGVlcnMiLCJfcHJvY2Vzc1BlZXJzT0IiLCJsYXlvdXRzIiwicV9sYXlvdXQiLCJsYXlvdXQiLCJxdWFudGl0aWVzX2J5X2dyb3VwIiwicV9tZW1iZXJzIiwibXNwaWQiLCJxX3BlZXJzIiwicV9wZWVyIiwicGVlciIsInFfaWRlbnRpdHkiLCJTZXJpYWxpemVkSWRlbnRpdHkiLCJpZGVudGl0eSIsInFfbWVtYmVyc2hpcF9tZXNzYWdlIiwiR29zc2lwTWVzc2FnZSIsIm1lbWJlcnNoaXBfaW5mbyIsImVuZHBvaW50IiwiYWxpdmVfbXNnIiwibWVtYmVyc2hpcCIsInN0YXRlX2luZm8iLCJtZXNzYWdlX3MiLCJwcm9wZXJ0aWVzIiwibGVkZ2VyX2hlaWdodCIsImZyb21WYWx1ZSIsImxvYWRfaW5mbyIsImNoYWluY29kZXMiLCJxX2NoYWluY29kZSIsInZlcnNpb24iLCJnZXRWZXJzaW9uIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7QUFLQTs7OztBQUxBOzs7O0FBSUE7QUFHQSxNQUFNQSxXQUFXLEdBQUdDLE9BQU8sQ0FBQywyQkFBRCxDQUEzQjs7QUFFQSxNQUFNQyxLQUFLLEdBQUdELE9BQU8sQ0FBQyw0QkFBRCxDQUFyQjs7QUFDQSxNQUFNRSxXQUFXLEdBQUdGLE9BQU8sQ0FBQyxzQkFBRCxDQUEzQjs7QUFDQSxNQUFNRyxTQUFTLEdBQUdILE9BQU8sQ0FBQyxnQ0FBRCxDQUF6Qjs7QUFFQSxNQUFNSSxJQUFJLEdBQUdKLE9BQU8sQ0FBQyxNQUFELENBQXBCOztBQUNBLE1BQU1LLElBQUksR0FBR0wsT0FBTyxDQUFDLE1BQUQsQ0FBcEI7O0FBQ0EsTUFBTU0sSUFBSSxHQUFHTixPQUFPLENBQUMsTUFBRCxDQUFwQjs7QUFFQSxNQUFNTyxRQUFRLEdBQUdILElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQixVQUFyQixFQUFpQyw0REFBakMsQ0FBVixFQUEwR0MsTUFBM0g7QUFDQSxNQUFNQyxXQUFXLEdBQUdSLElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQixVQUFyQixFQUFpQyw4REFBakMsQ0FBVixFQUE0R0MsTUFBaEk7QUFDQSxNQUFNRSxjQUFjLEdBQUdULElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQixVQUFyQixFQUFpQywyREFBakMsQ0FBVixFQUF5R0MsTUFBaEk7QUFDQSxNQUFNRyxZQUFZLEdBQUdWLElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQixVQUFyQixFQUFpQywyREFBakMsQ0FBVixFQUF5R0ssTUFBOUg7QUFFQSxNQUFNQyxjQUFjLEdBQUdaLElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQiw4QkFBckIsQ0FBVixFQUFnRU8sR0FBdkY7QUFDQSxNQUFNQyxZQUFZLEdBQUdkLElBQUksQ0FBQ0ksSUFBTCxDQUFVRixJQUFJLENBQUNHLElBQUwsQ0FBVUMsU0FBVixFQUFxQiw4QkFBckIsQ0FBVixFQUFnRVMsTUFBckY7QUFDQSxNQUFNQyxlQUFlLEdBQUdoQixJQUFJLENBQUNJLElBQUwsQ0FBVUYsSUFBSSxDQUFDRyxJQUFMLENBQVVDLFNBQVYsRUFBcUIsa0NBQXJCLENBQVYsRUFBb0VXLFNBQTVGO0FBRUEsTUFBTUMsY0FBYyxHQUFHLGNBQWN2QixXQUFkLENBQTBCO0FBQzdDd0IsRUFBQUEsV0FBVyxDQUFDQyxJQUFELEVBQU9DLGFBQVAsRUFBc0I7QUFDN0IsVUFBTUQsSUFBTixFQUFZQyxhQUFaO0FBQ0g7O0FBRURDLEVBQUFBLHlCQUF5QixDQUFDQyxPQUFELEVBQVVDLE9BQVYsRUFBbUI7QUFDeENDLG9CQUFPQyxLQUFQLENBQWEsaUNBQWI7O0FBRUEsUUFBSSxDQUFDSCxPQUFMLEVBQWM7QUFDVixZQUFNLElBQUlJLEtBQUosQ0FBVSxzREFBVixDQUFOO0FBQ0g7O0FBQ0RKLElBQUFBLE9BQU8sQ0FBQ0ssT0FBUixHQUFrQixLQUFLQyxXQUFMLENBQWlCTixPQUFPLENBQUNLLE9BQXpCLEVBQWtDN0IsU0FBUyxDQUFDK0IsYUFBVixDQUF3QkMsbUJBQTFELENBQWxCO0FBRUEsV0FBT2IsY0FBYyxDQUFDSSx5QkFBZixDQUF5Q0MsT0FBekMsRUFBa0QsS0FBS1MsS0FBdkQsRUFBOEQsS0FBS0MsY0FBbkUsRUFBbUZULE9BQW5GLENBQVA7QUFDSDs7QUFFRCxTQUFPRix5QkFBUCxDQUFpQ0MsT0FBakMsRUFBMENXLFNBQTFDLEVBQXFEYixhQUFyRCxFQUFvRUcsT0FBcEUsRUFBNkU7QUFDekU7QUFDQSxRQUFJVyxRQUFRLEdBQUdyQyxXQUFXLENBQUNzQyxvQkFBWixDQUFpQ2IsT0FBakMsQ0FBZjs7QUFFQSxRQUFJWSxRQUFKLEVBQWMsQ0FDVjtBQUNILEtBRkQsTUFFTyxJQUFJLENBQUNaLE9BQU8sQ0FBQ2MsSUFBYixFQUFtQjtBQUN0QjtBQUNBRixNQUFBQSxRQUFRLEdBQUcsZ0RBQVg7QUFDSCxLQUhNLE1BR0EsSUFBSSxDQUFDWixPQUFPLENBQUNLLE9BQVQsSUFBb0JMLE9BQU8sQ0FBQ0ssT0FBUixDQUFnQlUsTUFBaEIsR0FBeUIsQ0FBakQsRUFBb0Q7QUFDdkRILE1BQUFBLFFBQVEsR0FBRyw4Q0FBWDtBQUNIOztBQUVELFFBQUlBLFFBQUosRUFBYztBQUNWVixzQkFBT2MsS0FBUCxDQUFhLG1DQUFtQ0osUUFBaEQ7O0FBQ0EsWUFBTSxJQUFJUixLQUFKLENBQVVRLFFBQVYsQ0FBTjtBQUNIOztBQUVELFFBQUlFLElBQUksR0FBRyxFQUFYO0FBQ0FBLElBQUFBLElBQUksQ0FBQ0csSUFBTCxDQUFVQyxNQUFNLENBQUNDLElBQVAsQ0FBWW5CLE9BQU8sQ0FBQ29CLEdBQVIsR0FBY3BCLE9BQU8sQ0FBQ29CLEdBQXRCLEdBQTRCLFFBQXhDLEVBQWtELE1BQWxELENBQVY7O0FBQ0FsQixvQkFBT0MsS0FBUCxDQUFhLGtEQUFiLEVBQWlFSCxPQUFPLENBQUNvQixHQUFSLEdBQWNwQixPQUFPLENBQUNvQixHQUF0QixHQUE0QixRQUE3Rjs7QUFFQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdyQixPQUFPLENBQUNjLElBQVIsQ0FBYUMsTUFBakMsRUFBeUNNLENBQUMsRUFBMUMsRUFBOEM7QUFDMUNuQixzQkFBT0MsS0FBUCxDQUFhLHlDQUFiLEVBQXdESCxPQUFPLENBQUNjLElBQVIsQ0FBYU8sQ0FBYixDQUF4RDs7QUFDQVAsTUFBQUEsSUFBSSxDQUFDRyxJQUFMLENBQVVDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZbkIsT0FBTyxDQUFDYyxJQUFSLENBQWFPLENBQWIsQ0FBWixFQUE2QixNQUE3QixDQUFWO0FBQ0gsS0F6QndFLENBMEJ6RTs7O0FBQ0EsUUFBSXJCLE9BQU8sQ0FBQ3NCLFFBQVosRUFBc0I7QUFDbEJwQixzQkFBT0MsS0FBUCxDQUFhLDJEQUFiOztBQUNBVyxNQUFBQSxJQUFJLENBQUNHLElBQUwsQ0FBVWpCLE9BQU8sQ0FBQ3NCLFFBQWxCO0FBQ0gsS0FIRCxNQUdPO0FBQ0hwQixzQkFBT0MsS0FBUCxDQUFhLCtEQUFiO0FBQ0g7O0FBQ0QsUUFBSW9CLFVBQVUsR0FBRztBQUNiQyxNQUFBQSxJQUFJLEVBQUU1QyxRQUFRLENBQUM2QyxhQUFULENBQXVCQyxJQUF2QixDQUE0QkMsTUFEckI7QUFFYkMsTUFBQUEsWUFBWSxFQUFFO0FBQ1YvQixRQUFBQSxJQUFJLEVBQUVHLE9BQU8sQ0FBQzZCO0FBREosT0FGRDtBQUtiQyxNQUFBQSxLQUFLLEVBQUU7QUFDSGhCLFFBQUFBLElBQUksRUFBRUE7QUFESDtBQUxNLEtBQWpCO0FBVUEsUUFBSWlCLFFBQUosRUFBY0MsTUFBZDtBQUNBLFFBQUlDLE1BQU0sR0FBRyxJQUFiOztBQUNBLFFBQUlqQyxPQUFPLENBQUNpQyxNQUFaLEVBQW9CO0FBQ2hCQSxNQUFBQSxNQUFNLEdBQUdqQyxPQUFPLENBQUNpQyxNQUFqQjtBQUNILEtBRkQsTUFFTztBQUNIQSxNQUFBQSxNQUFNLEdBQUduQyxhQUFhLENBQUNvQyxtQkFBZCxDQUFrQ2xDLE9BQU8sQ0FBQ21DLElBQVIsQ0FBYUMsT0FBYixFQUFsQyxDQUFUO0FBQ0g7O0FBQ0QsUUFBSUMsYUFBYSxHQUFHOUQsV0FBVyxDQUFDK0Qsa0JBQVosQ0FDaEJuRCxZQUFZLENBQUNvRCxVQUFiLENBQXdCQyxvQkFEUixFQUVoQjdCLFNBRmdCLEVBR2hCWCxPQUFPLENBQUNtQyxJQUFSLENBQWFNLGdCQUFiLEVBSGdCLEVBSWhCLElBSmdCLEVBS2hCekMsT0FBTyxDQUFDNkIsV0FMUSxFQU1oQnRELFdBQVcsQ0FBQ21FLHFCQUFaLEVBTmdCLEVBT2hCMUMsT0FBTyxDQUFDSyxPQUFSLENBQWdCLENBQWhCLEVBQW1Cc0MsaUJBQW5CLEVBUGdCLENBQXBCO0FBU0FYLElBQUFBLE1BQU0sR0FBR3pELFdBQVcsQ0FBQ3FFLGFBQVosQ0FBMEJYLE1BQTFCLEVBQWtDSSxhQUFsQyxFQUFpRHJDLE9BQU8sQ0FBQ21DLElBQVIsQ0FBYVUsUUFBYixFQUFqRCxDQUFUO0FBQ0FkLElBQUFBLFFBQVEsR0FBR3hELFdBQVcsQ0FBQ3VFLGFBQVosQ0FBMEJ2QixVQUExQixFQUFzQ1MsTUFBdEMsRUFBOENoQyxPQUFPLENBQUMrQyxZQUF0RCxDQUFYO0FBQ0EsUUFBSUMsZUFBZSxHQUFHekUsV0FBVyxDQUFDMEUsY0FBWixDQUEyQmhCLE1BQTNCLEVBQW1DRixRQUFuQyxDQUF0QjtBQUVBLFdBQU94RCxXQUFXLENBQUMyRSxpQkFBWixDQUE4QmxELE9BQU8sQ0FBQ0ssT0FBdEMsRUFBK0MyQyxlQUEvQyxFQUFnRS9DLE9BQWhFLEVBQ0ZrRCxJQURFLENBRUMsVUFBVUMsU0FBVixFQUFxQjtBQUNqQixhQUFPQyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsQ0FBQ0YsU0FBRCxFQUFZckIsUUFBWixDQUFoQixDQUFQO0FBQ0gsS0FKRixFQUtEd0IsS0FMQyxDQU1DLFVBQVVDLEdBQVYsRUFBZTtBQUNYdEQsc0JBQU9jLEtBQVAsQ0FBYSw0QkFBYixFQUEyQ3dDLEdBQUcsQ0FBQ0MsS0FBSixHQUFZRCxHQUFHLENBQUNDLEtBQWhCLEdBQXdCRCxHQUFuRTs7QUFDQSxhQUFPSCxPQUFPLENBQUNLLE1BQVIsQ0FBZUYsR0FBZixDQUFQO0FBQ0gsS0FURixDQUFQO0FBV0g7O0FBRURHLEVBQUFBLGlCQUFpQixDQUFDM0QsT0FBRCxFQUFVO0FBQ3ZCRSxvQkFBT0MsS0FBUCxDQUFhLHVDQUFiLEVBQXNELElBQXREOztBQUNBLFFBQUlTLFFBQVEsR0FBRyxJQUFmOztBQUVBLFFBQUlaLE9BQUosRUFBYTtBQUNUO0FBQ0EsVUFBSSxDQUFDQSxPQUFPLENBQUM0RCxpQkFBYixFQUFnQztBQUM1QmhELFFBQUFBLFFBQVEsR0FBRyw4REFBWDtBQUNIOztBQUNELFVBQUksQ0FBQ1osT0FBTyxDQUFDK0IsUUFBYixFQUF1QjtBQUNuQm5CLFFBQUFBLFFBQVEsR0FBRyxxREFBWDtBQUNIO0FBQ0osS0FSRCxNQVFPO0FBQ0hBLE1BQUFBLFFBQVEsR0FBRyx5REFBWDtBQUNIOztBQUVELFFBQUlBLFFBQUosRUFBYztBQUNWVixzQkFBT2MsS0FBUCxDQUFhLDJCQUEyQkosUUFBeEM7O0FBQ0EsWUFBTSxJQUFJUixLQUFKLENBQVVRLFFBQVYsQ0FBTjtBQUNIOztBQUVELFFBQUlnRCxpQkFBaUIsR0FBRzVELE9BQU8sQ0FBQzRELGlCQUFoQztBQUNBLFFBQUlDLGlCQUFpQixHQUFHN0QsT0FBTyxDQUFDK0IsUUFBaEM7QUFFQSxRQUFJK0IsWUFBWSxHQUFHLEVBQW5CO0FBQ0EsUUFBSUMsZ0JBQWdCLEdBQUdILGlCQUF2Qjs7QUFDQSxRQUFJSSxLQUFLLENBQUNDLE9BQU4sQ0FBY0wsaUJBQWQsQ0FBSixFQUFzQztBQUNsQyxXQUFLLElBQUl2QyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHdUMsaUJBQWlCLENBQUM3QyxNQUF0QyxFQUE4Q00sQ0FBQyxFQUEvQyxFQUFtRDtBQUMvQztBQUNBO0FBQ0EsWUFBSXVDLGlCQUFpQixDQUFDdkMsQ0FBRCxDQUFqQixDQUFxQjZDLFFBQXJCLElBQWlDTixpQkFBaUIsQ0FBQ3ZDLENBQUQsQ0FBakIsQ0FBcUI2QyxRQUFyQixDQUE4QkMsTUFBOUIsS0FBeUMsR0FBOUUsRUFBbUY7QUFDL0VKLFVBQUFBLGdCQUFnQixHQUFHSCxpQkFBaUIsQ0FBQ3ZDLENBQUQsQ0FBcEM7QUFDQXlDLFVBQUFBLFlBQVksQ0FBQzdDLElBQWIsQ0FBa0I4QyxnQkFBZ0IsQ0FBQ0ssV0FBbkM7QUFDSDtBQUNKO0FBQ0osS0FURCxNQVNPO0FBQ0gsVUFBSUwsZ0JBQWdCLElBQUlBLGdCQUFnQixDQUFDRyxRQUFyQyxJQUFpREgsZ0JBQWdCLENBQUNHLFFBQWpCLENBQTBCQyxNQUExQixLQUFxQyxHQUExRixFQUErRjtBQUMzRkwsUUFBQUEsWUFBWSxDQUFDN0MsSUFBYixDQUFrQjhDLGdCQUFnQixDQUFDSyxXQUFuQztBQUNIO0FBQ0o7O0FBRUQsUUFBSU4sWUFBWSxDQUFDL0MsTUFBYixHQUFzQixDQUExQixFQUE2QjtBQUN6QmIsc0JBQU9jLEtBQVAsQ0FBYSwrQ0FBYjs7QUFDQSxZQUFNLElBQUlaLEtBQUosQ0FBVSw2QkFBVixDQUFOO0FBQ0gsS0E1Q3NCLENBOEN2Qjs7O0FBQ0EsUUFBSWlFLE9BQU8sR0FBRyxLQUFLM0QsY0FBTCxDQUFvQjRELGdCQUFwQixDQUFxQ3RFLE9BQU8sQ0FBQ3FFLE9BQTdDLEVBQXNELEtBQUtFLFNBQTNELEVBQXNFLEtBQUs5RCxLQUEzRSxDQUFkOztBQUVBLFFBQUkrRCxnQkFBZ0IsR0FBRyxLQUF2Qjs7QUFDQSxRQUFJeEUsT0FBTyxDQUFDbUMsSUFBWixFQUFrQjtBQUNkcUMsTUFBQUEsZ0JBQWdCLEdBQUd4RSxPQUFPLENBQUNtQyxJQUFSLENBQWFDLE9BQWIsRUFBbkI7QUFDSDs7QUFFRCxRQUFJSixNQUFNLEdBQUc3QyxZQUFZLENBQUNzRixNQUFiLENBQW9CQyxNQUFwQixDQUEyQmIsaUJBQWlCLENBQUNjLFNBQWxCLEVBQTNCLENBQWI7O0FBRUEsUUFBSUMsdUJBQXVCLEdBQUcsSUFBSTNGLFdBQVcsQ0FBQzRGLHVCQUFoQixFQUE5QjtBQUNBRCxJQUFBQSx1QkFBdUIsQ0FBQ0UsMEJBQXhCLENBQW1EZixnQkFBZ0IsQ0FBQ2dCLE9BQXBFO0FBQ0FILElBQUFBLHVCQUF1QixDQUFDSSxlQUF4QixDQUF3Q2xCLFlBQXhDO0FBRUEsUUFBSW1CLHNCQUFzQixHQUFHLElBQUloRyxXQUFXLENBQUNpRyxzQkFBaEIsRUFBN0I7QUFDQUQsSUFBQUEsc0JBQXNCLENBQUNFLFNBQXZCLENBQWlDUCx1QkFBakMsRUE3RHVCLENBK0R2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFFBQUlRLGdDQUFnQyxHQUFHbEcsY0FBYyxDQUFDbUcsd0JBQWYsQ0FBd0NYLE1BQXhDLENBQStDYixpQkFBaUIsQ0FBQ2tCLE9BQWpFLENBQXZDOztBQUNBLFFBQUlPLCtCQUErQixHQUFHLElBQUlwRyxjQUFjLENBQUNtRyx3QkFBbkIsRUFBdEM7QUFDQUMsSUFBQUEsK0JBQStCLENBQUNDLFFBQWhDLENBQXlDSCxnQ0FBZ0MsQ0FBQ3RELEtBQTFFLEVBdEV1QixDQXNFMkQ7O0FBQ2xGbUQsSUFBQUEsc0JBQXNCLENBQUNPLDJCQUF2QixDQUFtREYsK0JBQStCLENBQUNHLFFBQWhDLEVBQW5EO0FBRUEsUUFBSUMsaUJBQWlCLEdBQUcsSUFBSXpHLFdBQVcsQ0FBQzBHLGlCQUFoQixFQUF4QjtBQUNBRCxJQUFBQSxpQkFBaUIsQ0FBQ0UsU0FBbEIsQ0FBNEI1RCxNQUFNLENBQUM2RCxrQkFBUCxFQUE1QjtBQUNBSCxJQUFBQSxpQkFBaUIsQ0FBQ0ksVUFBbEIsQ0FBNkJiLHNCQUFzQixDQUFDUSxRQUF2QixFQUE3QjtBQUVBLFFBQUlNLE9BQU8sR0FBRyxFQUFkO0FBQ0FBLElBQUFBLE9BQU8sQ0FBQzlFLElBQVIsQ0FBYXlFLGlCQUFiO0FBRUEsUUFBSU0sV0FBVyxHQUFHLElBQUkvRyxXQUFXLENBQUNnSCxXQUFoQixFQUFsQjtBQUNBRCxJQUFBQSxXQUFXLENBQUNFLFVBQVosQ0FBdUJILE9BQXZCO0FBR0EsUUFBSWhCLE9BQU8sR0FBRyxJQUFJNUYsWUFBWSxDQUFDZ0gsT0FBakIsRUFBZDtBQUNBcEIsSUFBQUEsT0FBTyxDQUFDYSxTQUFSLENBQWtCNUQsTUFBbEI7QUFDQStDLElBQUFBLE9BQU8sQ0FBQ3FCLE9BQVIsQ0FBZ0JKLFdBQVcsQ0FBQ1AsUUFBWixFQUFoQjtBQUVBLFFBQUlZLGFBQWEsR0FBR3RCLE9BQU8sQ0FBQ1UsUUFBUixFQUFwQjs7QUFFQSxRQUFJeEQsTUFBTSxHQUFHLEtBQUt2QixjQUFMLENBQW9Cd0IsbUJBQXBCLENBQXdDc0MsZ0JBQXhDLENBQWI7O0FBRUEsUUFBSThCLFNBQVMsR0FBRyxHQUFoQixDQTVGdUIsQ0E4RnZCOztBQUNBLFFBQUlDLFFBQVEsR0FBRztBQUNYRCxNQUFBQSxTQUFTLEVBQUVBLFNBREE7QUFFWHZCLE1BQUFBLE9BQU8sRUFBRXNCO0FBRkUsS0FBZjtBQUtBLFdBQU9oQyxPQUFPLENBQUNtQyxhQUFSLENBQXNCRCxRQUF0QixDQUFQLENBcEd1QixDQXFHdkI7QUFDSDs7QUFFRCxRQUFNRSxZQUFOLENBQW1CekcsT0FBbkIsRUFBNEI7QUFDeEIsVUFBTTBHLE1BQU0sR0FBRyxZQUFmOztBQUNBeEcsb0JBQU9DLEtBQVAsQ0FBYSxZQUFiLEVBQTJCdUcsTUFBM0I7O0FBRUEsUUFBSUMsd0JBQXdCLEdBQUcsSUFBL0I7QUFDQSxRQUFJQyxtQkFBbUIsR0FBRyxJQUExQjs7QUFFQSxRQUFJNUcsT0FBSixFQUFhO0FBQ1QsVUFBSUEsT0FBTyxDQUFDNkcsV0FBWixFQUF5QjtBQUNyQjNHLHdCQUFPQyxLQUFQLENBQWEsMEJBQWIsRUFBeUN1RyxNQUF6Qzs7QUFDQSxhQUFLSSxnQkFBTCxDQUFzQjlHLE9BQU8sQ0FBQzZHLFdBQTlCO0FBRUEsZUFBTyxJQUFQO0FBQ0gsT0FMRCxNQUtPO0FBQ0gsWUFBSSxPQUFPN0csT0FBTyxDQUFDK0csUUFBZixLQUE0QixXQUFoQyxFQUE2QztBQUN6QyxjQUFJLE9BQU8vRyxPQUFPLENBQUMrRyxRQUFmLEtBQTRCLFNBQWhDLEVBQTJDO0FBQ3ZDN0csNEJBQU9DLEtBQVAsQ0FBYSxpQ0FBYixFQUFnRHVHLE1BQWhELEVBQXdEMUcsT0FBTyxDQUFDK0csUUFBaEU7O0FBQ0EsaUJBQUtDLGNBQUwsR0FBc0JoSCxPQUFPLENBQUMrRyxRQUE5QjtBQUNILFdBSEQsTUFHTztBQUNILGtCQUFNLElBQUkzRyxLQUFKLENBQVUsOENBQVYsQ0FBTjtBQUNIO0FBQ0o7O0FBQ0QsWUFBSUosT0FBTyxDQUFDaUgsa0JBQVosRUFBZ0M7QUFDNUIvRywwQkFBT0MsS0FBUCxDQUFhLDJDQUFiLEVBQTBEdUcsTUFBMUQsRUFBa0UxRyxPQUFPLENBQUNpSCxrQkFBMUU7O0FBQ0FOLFVBQUFBLHdCQUF3QixHQUFHM0csT0FBTyxDQUFDaUgsa0JBQW5DO0FBQ0g7O0FBQ0QsWUFBSWpILE9BQU8sQ0FBQ2tILGFBQVosRUFBMkI7QUFDdkJoSCwwQkFBT0MsS0FBUCxDQUFhLHNDQUFiLEVBQXFEdUcsTUFBckQsRUFBNkQxRyxPQUFPLENBQUNrSCxhQUFyRTs7QUFDQU4sVUFBQUEsbUJBQW1CLEdBQUc1RyxPQUFPLENBQUNrSCxhQUE5QjtBQUNIO0FBQ0o7QUFDSixLQS9CdUIsQ0FpQ3hCOzs7QUFDQSxRQUFJLENBQUNQLHdCQUFELElBQTZCLEtBQUtLLGNBQXRDLEVBQXNEO0FBQ2xETCxNQUFBQSx3QkFBd0IsR0FBR3JJLEtBQUssQ0FBQzZJLGdCQUFOLENBQXVCLHFCQUF2QixDQUEzQjs7QUFDQWpILHNCQUFPQyxLQUFQLENBQWEsd0RBQWIsRUFBdUV1RyxNQUF2RSxFQUErRUMsd0JBQS9FO0FBQ0g7O0FBQ0QsUUFBSUEsd0JBQUosRUFBOEI7QUFDMUIsV0FBS1Msb0JBQUwsR0FBNEIvSSxPQUFPLENBQUNzSSx3QkFBRCxDQUFQLENBQWtDVSxNQUFsQyxDQUF5QyxJQUF6QyxDQUE1QjtBQUNBLFlBQU0sS0FBS0Qsb0JBQUwsQ0FBMEJFLFVBQTFCLEVBQU47QUFDSCxLQXpDdUIsQ0EyQ3hCOzs7QUFDQSxRQUFJLENBQUNWLG1CQUFMLEVBQTBCO0FBQ3RCQSxNQUFBQSxtQkFBbUIsR0FBR3RJLEtBQUssQ0FBQzZJLGdCQUFOLENBQXVCLGdCQUF2QixDQUF0Qjs7QUFDQWpILHNCQUFPQyxLQUFQLENBQWEsbURBQWIsRUFBa0V1RyxNQUFsRSxFQUEwRUUsbUJBQTFFO0FBQ0g7O0FBQ0QsUUFBSUEsbUJBQUosRUFBeUI7QUFDckIsV0FBS1csZUFBTCxHQUF1QmxKLE9BQU8sQ0FBQ3VJLG1CQUFELENBQVAsQ0FBNkJTLE1BQTdCLENBQW9DLElBQXBDLENBQXZCO0FBQ0EsWUFBTSxLQUFLRSxlQUFMLENBQXFCRCxVQUFyQixFQUFOO0FBQ0g7O0FBRUQsVUFBTUUsT0FBTyxHQUFHLE1BQU0sS0FBS0MsYUFBTCxDQUFtQnpILE9BQW5CLENBQXRCO0FBRUEsV0FBT3dILE9BQVA7QUFDSDs7QUFFRCxRQUFNQyxhQUFOLENBQW9CekgsT0FBcEIsRUFBNkI7QUFDekIsVUFBTTBHLE1BQU0sR0FBRyxhQUFmOztBQUNBeEcsb0JBQU9DLEtBQVAsQ0FBYSxZQUFiLEVBQTJCdUcsTUFBM0I7O0FBRUEsU0FBS2dCLGtCQUFMLEdBQTBCLElBQTFCO0FBQ0EsU0FBS0Msd0JBQUwsR0FBZ0MsSUFBaEM7QUFDQSxTQUFLQyxxQkFBTCxHQUE2QkMsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQjlILE9BQWxCLENBQTdCO0FBQ0EsUUFBSStILFdBQVcsR0FBRyxLQUFLQyxlQUF2Qjs7QUFFQSxRQUFJaEksT0FBTyxJQUFJQSxPQUFPLENBQUNpSSxNQUF2QixFQUErQjtBQUMzQkYsTUFBQUEsV0FBVyxHQUFHL0gsT0FBTyxDQUFDaUksTUFBdEI7QUFDSDs7QUFFRCxRQUFJLEtBQUtqQixjQUFULEVBQXlCO0FBQ3JCOUcsc0JBQU9DLEtBQVAsQ0FBYSx5QkFBYixFQUF3Q3VHLE1BQXhDOztBQUNBcUIsTUFBQUEsV0FBVyxHQUFHLEtBQUtHLHNCQUFMLENBQTRCSCxXQUE1QixDQUFkOztBQUNBLFVBQUksQ0FBQ0EsV0FBTCxFQUFrQjtBQUNkLGNBQU0sSUFBSTNILEtBQUosQ0FBVSwyQ0FBVixDQUFOO0FBQ0g7O0FBRUQsVUFBSTtBQUNBLGNBQU0rSCxnQkFBZ0IsR0FBRztBQUNyQkYsVUFBQUEsTUFBTSxFQUFFRixXQURhO0FBRXJCSyxVQUFBQSxNQUFNLEVBQUU7QUFGYSxTQUF6QjtBQUtBLGNBQU1DLGlCQUFpQixHQUFHLE1BQU0sS0FBS0MsV0FBTCxDQUFpQkgsZ0JBQWpCLENBQWhDOztBQUNBLFlBQUlFLGlCQUFKLEVBQXVCO0FBQ25CLGNBQUlBLGlCQUFpQixDQUFDRSxJQUF0QixFQUE0QjtBQUN4QixpQkFBS0MsbUJBQUwsQ0FBeUJILGlCQUF6QjtBQUNILFdBRkQsTUFFTztBQUNILGtCQUFNakksS0FBSyxDQUFDLDBCQUFELENBQVg7QUFDSDs7QUFDRCxjQUFJaUksaUJBQWlCLENBQUNJLFFBQXRCLEVBQWdDO0FBQzVCLGlCQUFLQyx1QkFBTCxDQUE2QkwsaUJBQTdCLEVBQWdEQSxpQkFBaUIsQ0FBQ0UsSUFBbEUsRUFBd0V2SSxPQUF4RTtBQUNIOztBQUNELGNBQUlxSSxpQkFBaUIsQ0FBQ00sWUFBdEIsRUFBb0M7QUFDaEMsaUJBQUtDLG9CQUFMLENBQTBCUCxpQkFBMUIsRUFBNkNBLGlCQUFpQixDQUFDRSxJQUEvRCxFQUFxRXZJLE9BQXJFO0FBQ0g7QUFDSjs7QUFFRHFJLFFBQUFBLGlCQUFpQixDQUFDUSxpQkFBbEIsR0FBc0MsRUFBdEM7QUFFQSxjQUFNQyxTQUFTLEdBQUcsRUFBbEI7QUFDQSxjQUFNQyxRQUFRLEdBQUcsRUFBakI7O0FBQ0EsYUFBS0Msb0JBQUwsQ0FBMEJDLE9BQTFCLENBQWtDLENBQUNDLFFBQUQsRUFBV0MsT0FBWCxLQUFzQjtBQUNwRGpKLDBCQUFPQyxLQUFQLENBQWEsMEJBQWIsRUFBeUN1RyxNQUF6QyxFQUFpRHlDLE9BQWpEOztBQUNBSixVQUFBQSxRQUFRLENBQUM5SCxJQUFULENBQWNrSSxPQUFkO0FBQ0FMLFVBQUFBLFNBQVMsQ0FBQzdILElBQVYsQ0FBZWlJLFFBQWY7QUFDSCxTQUpEOztBQU1BLGFBQUksTUFBTTdILENBQVYsSUFBZTBILFFBQWYsRUFBeUI7QUFDckIsZ0JBQU1JLE9BQU8sR0FBR0osUUFBUSxDQUFDMUgsQ0FBRCxDQUF4QjtBQUNBLGdCQUFNNkgsUUFBUSxHQUFHSixTQUFTLENBQUN6SCxDQUFELENBQTFCO0FBRUEsZ0JBQU04RyxnQkFBZ0IsR0FBRztBQUNyQkYsWUFBQUEsTUFBTSxFQUFFRixXQURhO0FBRXJCZSxZQUFBQSxTQUFTLEVBQUUsQ0FBQ0ksUUFBRDtBQUZVLFdBQXpCO0FBS0EsY0FBSUUseUJBQXlCLEdBQUcsSUFBaEM7O0FBQ0EsY0FBSTtBQUNBQSxZQUFBQSx5QkFBeUIsR0FBRyxNQUFNLEtBQUtkLFdBQUwsQ0FBaUJILGdCQUFqQixDQUFsQztBQUNILFdBRkQsQ0FFRSxPQUFNbkgsS0FBTixFQUFhO0FBQ1hkLDRCQUFPYyxLQUFQLENBQWEsNENBQWIsRUFBMkRtSSxPQUEzRDtBQUNIOztBQUVELGNBQUdDLHlCQUF5QixJQUFJQSx5QkFBeUIsQ0FBQ1AsaUJBQXZELElBQTRFTyx5QkFBeUIsQ0FBQ1AsaUJBQTFCLENBQTRDLENBQTVDLENBQS9FLEVBQStIO0FBQzNILGtCQUFNUSxJQUFJLEdBQUcsS0FBS0MsOEJBQUwsQ0FBb0NGLHlCQUFwQyxFQUErREQsT0FBL0QsRUFBd0VkLGlCQUFpQixDQUFDRSxJQUExRixFQUFnR3ZJLE9BQWhHLENBQWI7O0FBQ0FxSSxZQUFBQSxpQkFBaUIsQ0FBQ1EsaUJBQWxCLENBQW9DNUgsSUFBcEMsQ0FBeUNvSSxJQUF6Qzs7QUFDQW5KLDRCQUFPQyxLQUFQLENBQWEsa0NBQWIsRUFBaURnSixPQUFqRDtBQUNILFdBSkQsTUFJTztBQUNIakosNEJBQU9DLEtBQVAsQ0FBYSx1Q0FBYixFQUFzRGdKLE9BQXREO0FBQ0g7QUFDSjs7QUFFRGQsUUFBQUEsaUJBQWlCLENBQUNrQixTQUFsQixHQUE4QkMsSUFBSSxDQUFDQyxHQUFMLEVBQTlCO0FBQ0EsYUFBSy9CLGtCQUFMLEdBQTBCVyxpQkFBMUI7QUFDQSxhQUFLTCxlQUFMLEdBQXVCRCxXQUF2QjtBQUNBLGFBQUtKLHdCQUFMLEdBQWdDVSxpQkFBaUIsQ0FBQ2tCLFNBQWxEO0FBRUEsZUFBT2xCLGlCQUFQO0FBQ0gsT0E5REQsQ0E4REUsT0FBTXJILEtBQU4sRUFBYTtBQUNYZCx3QkFBT2MsS0FBUCxDQUFhQSxLQUFiOztBQUNBLGNBQU1aLEtBQUssQ0FBQywwQkFBeUJZLEtBQUssQ0FBQzBJLFFBQU4sRUFBMUIsQ0FBWDtBQUNIO0FBQ0osS0F6RUQsTUF5RU87QUFDSDNCLE1BQUFBLFdBQVcsR0FBRyxLQUFLNEIsd0JBQUwsQ0FBOEI1QixXQUE5QixDQUFkOztBQUNBLFVBQUksQ0FBQ0EsV0FBTCxFQUFrQjtBQUNkLGNBQU0sSUFBSTNILEtBQUosQ0FBVSxxREFBVixDQUFOO0FBQ0g7O0FBQ0QsWUFBTXdKLGVBQWUsR0FBRyxNQUFNLEtBQUtDLGdCQUFMLENBQXNCOUIsV0FBdEIsQ0FBOUI7O0FBQ0E3SCxzQkFBT0MsS0FBUCxDQUFhLDhEQUFiLEVBQTZFeUosZUFBN0U7O0FBQ0EsWUFBTUUsWUFBWSxHQUFHLEtBQUtDLGtCQUFMLENBQXdCSCxlQUF4QixDQUFyQjtBQUVBLGFBQU9FLFlBQVA7QUFFSDtBQUNKOztBQUVELFFBQU14QixXQUFOLENBQWtCdEksT0FBbEIsRUFBMkI7QUFDdkIsVUFBTTBHLE1BQU0sR0FBRyxVQUFmO0FBQ0EsVUFBTXNELElBQUksR0FBRyxJQUFiOztBQUNBOUosb0JBQU9DLEtBQVAsQ0FBYSxZQUFiLEVBQTJCdUcsTUFBM0I7O0FBQ0EsVUFBTWMsT0FBTyxHQUFHLEVBQWhCOztBQUNBLFFBQUksQ0FBQ3hILE9BQUwsRUFBYztBQUNWQSxNQUFBQSxPQUFPLEdBQUcsRUFBVjtBQUNIOztBQUVELFFBQUlpSyxRQUFRLEdBQUcsSUFBZixDQVR1QixDQVNGOztBQUNyQixRQUFHLE9BQU9qSyxPQUFPLENBQUNpSyxRQUFmLEtBQTRCLFNBQS9CLEVBQTBDO0FBQ3RDQSxNQUFBQSxRQUFRLEdBQUdqSyxPQUFPLENBQUNpSyxRQUFuQjtBQUNIOztBQUNELFVBQU1sQyxXQUFXLEdBQUcsS0FBS0csc0JBQUwsQ0FBNEJsSSxPQUFPLENBQUNpSSxNQUFwQyxDQUFwQjs7QUFDQSxVQUFNaEcsTUFBTSxHQUFHLEtBQUt2QixjQUFMLENBQW9Cd0IsbUJBQXBCLENBQXdDK0gsUUFBeEMsQ0FBZixDQWR1QixDQWMyQzs7O0FBQ2xFLFVBQU1DLGlCQUFpQixHQUFHLElBQUl6SyxlQUFlLENBQUMwSyxPQUFwQixFQUExQjtBQUVBLFVBQU1DLGNBQWMsR0FBRyxJQUFJM0ssZUFBZSxDQUFDNEssUUFBcEIsRUFBdkI7QUFDQUQsSUFBQUEsY0FBYyxDQUFDRSxpQkFBZixDQUFpQ3JJLE1BQU0sQ0FBQ3NJLFNBQVAsRUFBakM7O0FBQ0EsVUFBTUMsU0FBUyxHQUFHLEtBQUs5SixjQUFMLENBQW9CaUMsaUJBQXBCLENBQXNDLElBQXRDLENBQWxCOztBQUNBLFFBQUk2SCxTQUFKLEVBQWU7QUFDWEosTUFBQUEsY0FBYyxDQUFDSyxvQkFBZixDQUFvQ0QsU0FBcEM7QUFDSDs7QUFDRE4sSUFBQUEsaUJBQWlCLENBQUNRLGlCQUFsQixDQUFvQ04sY0FBcEMsRUF2QnVCLENBeUJ2QjtBQUNBOztBQUNBLFVBQU1PLE9BQU8sR0FBRyxFQUFoQixDQTNCdUIsQ0E2QnZCOztBQUNBLFFBQUkzSyxPQUFPLENBQUM0SyxLQUFaLEVBQW1CO0FBQ2YsWUFBTUMsS0FBSyxHQUFHLElBQUlwTCxlQUFlLENBQUNxTCxLQUFwQixFQUFkO0FBQ0FILE1BQUFBLE9BQU8sQ0FBQzFKLElBQVIsQ0FBYTRKLEtBQWI7QUFFQSxZQUFNRSxXQUFXLEdBQUcsSUFBSXRMLGVBQWUsQ0FBQ3VMLGNBQXBCLEVBQXBCO0FBQ0FILE1BQUFBLEtBQUssQ0FBQ0ksYUFBTixDQUFvQkYsV0FBcEI7O0FBQ0E3SyxzQkFBT0MsS0FBUCxDQUFhLCtCQUFiLEVBQThDdUcsTUFBOUM7QUFDSDs7QUFFRCxRQUFJMUcsT0FBTyxDQUFDb0ksTUFBWixFQUFvQjtBQUNoQixVQUFJeUMsS0FBSyxHQUFHLElBQUlwTCxlQUFlLENBQUNxTCxLQUFwQixFQUFaO0FBQ0FILE1BQUFBLE9BQU8sQ0FBQzFKLElBQVIsQ0FBYTRKLEtBQWI7QUFDQUEsTUFBQUEsS0FBSyxDQUFDSyxVQUFOLENBQWlCLEtBQUtDLE9BQUwsRUFBakI7QUFFQSxZQUFNQyxZQUFZLEdBQUcsSUFBSTNMLGVBQWUsQ0FBQzRMLFdBQXBCLEVBQXJCO0FBQ0FSLE1BQUFBLEtBQUssQ0FBQ1MsY0FBTixDQUFxQkYsWUFBckI7O0FBQ0FsTCxzQkFBT0MsS0FBUCxDQUFhLDBCQUFiLEVBQXlDdUcsTUFBekM7O0FBRUFtRSxNQUFBQSxLQUFLLEdBQUcsSUFBSXBMLGVBQWUsQ0FBQ3FMLEtBQXBCLEVBQVI7QUFDQUgsTUFBQUEsT0FBTyxDQUFDMUosSUFBUixDQUFhNEosS0FBYjtBQUNBQSxNQUFBQSxLQUFLLENBQUNLLFVBQU4sQ0FBaUIsS0FBS0MsT0FBTCxFQUFqQjtBQUVBLFlBQU1JLFVBQVUsR0FBRyxJQUFJOUwsZUFBZSxDQUFDK0wsbUJBQXBCLEVBQW5CO0FBQ0FYLE1BQUFBLEtBQUssQ0FBQ1ksWUFBTixDQUFtQkYsVUFBbkI7O0FBQ0FyTCxzQkFBT0MsS0FBUCxDQUFhLGlDQUFiLEVBQWdEdUcsTUFBaEQ7QUFDSCxLQXZEc0IsQ0F5RHZCOzs7QUFDQSxRQUFJMUcsT0FBTyxDQUFDOEksU0FBUixJQUFxQjlJLE9BQU8sQ0FBQzhJLFNBQVIsQ0FBa0IvSCxNQUFsQixHQUEyQixDQUFwRCxFQUF1RDtBQUNuRCxZQUFNOEosS0FBSyxHQUFHLElBQUlwTCxlQUFlLENBQUNxTCxLQUFwQixFQUFkO0FBQ0FILE1BQUFBLE9BQU8sQ0FBQzFKLElBQVIsQ0FBYTRKLEtBQWI7QUFDQUEsTUFBQUEsS0FBSyxDQUFDSyxVQUFOLENBQWlCLEtBQUtDLE9BQUwsRUFBakI7QUFFQSxZQUFNckMsU0FBUyxHQUFHLEVBQWxCOztBQUNBLFdBQUksTUFBTUksUUFBVixJQUFzQmxKLE9BQU8sQ0FBQzhJLFNBQTlCLEVBQXlDO0FBQ3JDLGNBQU00QyxjQUFjLEdBQUcsS0FBS0MsNEJBQUwsQ0FBa0N6QyxRQUFsQyxDQUF2Qjs7QUFDQUosUUFBQUEsU0FBUyxDQUFDN0gsSUFBVixDQUFleUssY0FBZjtBQUNIOztBQUVELFlBQU1FLFFBQVEsR0FBRyxJQUFJbk0sZUFBZSxDQUFDb00sY0FBcEIsRUFBakI7QUFDQUQsTUFBQUEsUUFBUSxDQUFDRSxZQUFULENBQXNCaEQsU0FBdEI7QUFDQStCLE1BQUFBLEtBQUssQ0FBQ2tCLFVBQU4sQ0FBaUJILFFBQWpCOztBQUNBMUwsc0JBQU9DLEtBQVAsQ0FBYSx5Q0FBYixFQUF3RHVHLE1BQXhEO0FBQ0gsS0F6RXNCLENBMkV2Qjs7O0FBQ0F3RCxJQUFBQSxpQkFBaUIsQ0FBQzhCLFVBQWxCLENBQTZCckIsT0FBN0IsRUE1RXVCLENBOEV2Qjs7QUFDQSxVQUFNc0IsY0FBYyxHQUFHMU4sV0FBVyxDQUFDMk4sVUFBWixDQUF1QjNOLFdBQVcsQ0FBQzROLFlBQVosQ0FBeUJsSyxNQUF6QixFQUFpQ2lJLGlCQUFqQyxDQUF2QixDQUF2QjtBQUVBLFVBQU1oRyxRQUFRLEdBQUcsTUFBTTZELFdBQVcsQ0FBQ3FFLGFBQVosQ0FBMEJILGNBQTFCLENBQXZCOztBQUNBL0wsb0JBQU9DLEtBQVAsQ0FBYSxvQ0FBYixFQUFtRHVHLE1BQW5EOztBQUNBLFFBQUl4QyxRQUFRLElBQUlBLFFBQVEsQ0FBQ3NELE9BQXpCLEVBQWtDO0FBQzlCLFVBQUk2RSxTQUFTLEdBQUcsSUFBaEI7O0FBQ0FuTSxzQkFBT0MsS0FBUCxDQUFhLCtCQUFiLEVBQThDdUcsTUFBOUM7O0FBQ0EsV0FBSyxNQUFNNEYsS0FBWCxJQUFvQnBJLFFBQVEsQ0FBQ3NELE9BQTdCLEVBQXNDO0FBQ2xDLGNBQU0rRSxNQUFNLEdBQUdySSxRQUFRLENBQUNzRCxPQUFULENBQWlCOEUsS0FBakIsQ0FBZjs7QUFDQSxZQUFJLENBQUNDLE1BQUwsRUFBYTtBQUNURixVQUFBQSxTQUFTLEdBQUcsOEJBQVo7QUFDQTtBQUNILFNBSEQsTUFHTyxJQUFJRSxNQUFNLENBQUNBLE1BQVAsS0FBa0IsT0FBdEIsRUFBK0I7QUFDbENyTSwwQkFBT2MsS0FBUCxDQUFhLHdDQUFiLEVBQXVEZ0osSUFBSSxDQUFDbUIsT0FBTCxFQUF2RCxFQUF1RW9CLE1BQU0sQ0FBQ3ZMLEtBQVAsQ0FBYXdMLE9BQXBGOztBQUNBSCxVQUFBQSxTQUFTLEdBQUdFLE1BQU0sQ0FBQ3ZMLEtBQVAsQ0FBYXdMLE9BQXpCO0FBQ0E7QUFDSCxTQUpNLE1BSUE7QUFDSHRNLDBCQUFPQyxLQUFQLENBQWEsc0JBQWIsRUFBcUN1RyxNQUFyQzs7QUFDQSxjQUFJNkYsTUFBTSxDQUFDRSxhQUFYLEVBQTBCO0FBQ3RCLGtCQUFNckUsTUFBTSxHQUFHNEIsSUFBSSxDQUFDMEMsOEJBQUwsQ0FBb0NILE1BQU0sQ0FBQ0UsYUFBM0MsQ0FBZjs7QUFDQWpGLFlBQUFBLE9BQU8sQ0FBQ2UsSUFBUixHQUFlSCxNQUFNLENBQUNHLElBQXRCO0FBQ0FmLFlBQUFBLE9BQU8sQ0FBQ2lCLFFBQVIsR0FBbUJMLE1BQU0sQ0FBQ0ssUUFBMUI7QUFDSDs7QUFDRCxjQUFJOEQsTUFBTSxDQUFDSSxPQUFYLEVBQW9CO0FBQ2hCLGdCQUFJM00sT0FBTyxDQUFDNEssS0FBUixJQUFpQjBCLEtBQUssS0FBSyxDQUEvQixFQUFrQztBQUM5QjlFLGNBQUFBLE9BQU8sQ0FBQ3VELFdBQVIsR0FBc0JmLElBQUksQ0FBQzRDLG9DQUFMLENBQTBDTCxNQUFNLENBQUNJLE9BQWpELENBQXRCO0FBQ0gsYUFGRCxNQUVPO0FBQ0huRixjQUFBQSxPQUFPLENBQUNtQixZQUFSLEdBQXVCcUIsSUFBSSxDQUFDNEMsb0NBQUwsQ0FBMENMLE1BQU0sQ0FBQ0ksT0FBakQsQ0FBdkI7QUFDSDtBQUNKOztBQUNELGNBQUlKLE1BQU0sQ0FBQ00sWUFBWCxFQUF5QjtBQUNyQnJGLFlBQUFBLE9BQU8sQ0FBQ3FCLGlCQUFSLEdBQTRCbUIsSUFBSSxDQUFDOEMsbUNBQUwsQ0FBeUNQLE1BQU0sQ0FBQ00sWUFBaEQsQ0FBNUI7QUFDSDs7QUFDRDNNLDBCQUFPQyxLQUFQLENBQWEsbUNBQWIsRUFBa0R1RyxNQUFsRDtBQUNIO0FBQ0o7O0FBRUQsVUFBSTJGLFNBQUosRUFBZTtBQUNYLGNBQU1qTSxLQUFLLENBQUMsYUFBYTRKLElBQUksQ0FBQ21CLE9BQUwsRUFBYixHQUE4QixtQkFBOUIsR0FBb0RrQixTQUFyRCxDQUFYO0FBQ0gsT0FGRCxNQUVPO0FBRUgsZUFBTzdFLE9BQVA7QUFDSDtBQUNKLEtBdkNELE1BdUNPO0FBQ0gsWUFBTSxJQUFJcEgsS0FBSixDQUFVLHdDQUFWLENBQU47QUFDSDtBQUNKOztBQUVEME0sRUFBQUEsbUNBQW1DLENBQUNDLFlBQUQsRUFBZTtBQUM5QyxVQUFNckcsTUFBTSxHQUFHLG1DQUFmOztBQUNBeEcsb0JBQU9DLEtBQVAsQ0FBYSxZQUFiLEVBQTJCdUcsTUFBM0I7O0FBQ0EsVUFBTW1DLGlCQUFpQixHQUFHLEVBQTFCOztBQUNBLFFBQUlrRSxZQUFZLElBQUlBLFlBQVksQ0FBQ1AsT0FBakMsRUFBMEM7QUFDdEMsVUFBSXhJLEtBQUssQ0FBQ0MsT0FBTixDQUFjOEksWUFBWSxDQUFDUCxPQUEzQixDQUFKLEVBQXlDO0FBQ3JDLGFBQUssTUFBTUYsS0FBWCxJQUFvQlMsWUFBWSxDQUFDUCxPQUFqQyxFQUEwQztBQUN0QyxnQkFBTVEsYUFBYSxHQUFHRCxZQUFZLENBQUNQLE9BQWIsQ0FBcUJGLEtBQXJCLENBQXRCO0FBQ0EsZ0JBQU1XLGdCQUFnQixHQUFHLEVBQXpCO0FBQ0FBLFVBQUFBLGdCQUFnQixDQUFDQyxTQUFqQixHQUE2QkYsYUFBYSxDQUFDRSxTQUEzQztBQUNBckUsVUFBQUEsaUJBQWlCLENBQUM1SCxJQUFsQixDQUF1QmdNLGdCQUF2QixFQUpzQyxDQU10Qzs7QUFDQUEsVUFBQUEsZ0JBQWdCLENBQUNFLE1BQWpCLEdBQTBCLEVBQTFCOztBQUNBLGVBQUssTUFBTUMsVUFBWCxJQUF5QkosYUFBYSxDQUFDSyxtQkFBdkMsRUFBNEQ7QUFDeERuTiw0QkFBT0MsS0FBUCxDQUFhLHNCQUFiLEVBQXFDdUcsTUFBckMsRUFBNkMwRyxVQUE3Qzs7QUFDQSxrQkFBTUUsS0FBSyxHQUFHLEVBQWQ7QUFDQUEsWUFBQUEsS0FBSyxDQUFDQyxLQUFOLEdBQWMsS0FBS0MsZUFBTCxDQUFxQlIsYUFBYSxDQUFDSyxtQkFBZCxDQUFrQ0QsVUFBbEMsRUFBOENHLEtBQW5FLENBQWQsQ0FId0QsQ0FJeEQ7O0FBQ0FOLFlBQUFBLGdCQUFnQixDQUFDRSxNQUFqQixDQUF3QkMsVUFBeEIsSUFBc0NFLEtBQXRDO0FBQ0gsV0FkcUMsQ0FnQnRDOzs7QUFDQUwsVUFBQUEsZ0JBQWdCLENBQUNRLE9BQWpCLEdBQTJCLEVBQTNCOztBQUNBLGVBQUssTUFBTW5CLEtBQVgsSUFBb0JVLGFBQWEsQ0FBQ1MsT0FBbEMsRUFBMkM7QUFDdkMsa0JBQU1DLFFBQVEsR0FBR1YsYUFBYSxDQUFDUyxPQUFkLENBQXNCbkIsS0FBdEIsQ0FBakI7QUFDQSxrQkFBTXFCLE1BQU0sR0FBRyxFQUFmOztBQUNBLGlCQUFLLE1BQU1QLFVBQVgsSUFBeUJNLFFBQVEsQ0FBQ0UsbUJBQWxDLEVBQXVEO0FBQ25ERCxjQUFBQSxNQUFNLENBQUNQLFVBQUQsQ0FBTixHQUFxQk0sUUFBUSxDQUFDRSxtQkFBVCxDQUE2QlIsVUFBN0IsQ0FBckI7QUFDSDs7QUFDRGxOLDRCQUFPQyxLQUFQLENBQWEsaUJBQWIsRUFBZ0N1RyxNQUFoQyxFQUF3Q2lILE1BQXhDOztBQUNBVixZQUFBQSxnQkFBZ0IsQ0FBQ1EsT0FBakIsQ0FBeUJ4TSxJQUF6QixDQUE4QjBNLE1BQTlCO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7O0FBRUQsV0FBTzlFLGlCQUFQO0FBQ0g7O0FBRUQrRCxFQUFBQSxvQ0FBb0MsQ0FBQ2lCLFNBQUQsRUFBWTtBQUM1QyxVQUFNbkgsTUFBTSxHQUFHLDJDQUFmOztBQUNBeEcsb0JBQU9DLEtBQVAsQ0FBYSxZQUFiLEVBQTJCdUcsTUFBM0I7O0FBQ0EsVUFBTWlDLFlBQVksR0FBRyxFQUFyQjs7QUFDQSxRQUFJa0YsU0FBUyxJQUFJQSxTQUFTLENBQUNsRixZQUEzQixFQUF5QztBQUNyQyxXQUFLLE1BQU1tRixLQUFYLElBQW9CRCxTQUFTLENBQUNsRixZQUE5QixFQUE0QztBQUN4Q3pJLHdCQUFPQyxLQUFQLENBQWEsbUJBQWIsRUFBa0N1RyxNQUFsQyxFQUEwQ29ILEtBQTFDOztBQUNBbkYsUUFBQUEsWUFBWSxDQUFDbUYsS0FBRCxDQUFaLEdBQXNCLEVBQXRCO0FBQ0FuRixRQUFBQSxZQUFZLENBQUNtRixLQUFELENBQVosQ0FBb0JQLEtBQXBCLEdBQTRCLEtBQUtDLGVBQUwsQ0FBcUJLLFNBQVMsQ0FBQ2xGLFlBQVYsQ0FBdUJtRixLQUF2QixFQUE4QlAsS0FBbkQsQ0FBNUI7QUFDSDtBQUNKOztBQUNELFdBQU81RSxZQUFQO0FBQ0g7O0FBRUQ2RSxFQUFBQSxlQUFlLENBQUNPLE9BQUQsRUFBVTtBQUNyQixVQUFNckgsTUFBTSxHQUFHLGlCQUFmO0FBQ0EsVUFBTTZHLEtBQUssR0FBRyxFQUFkO0FBQ0FRLElBQUFBLE9BQU8sQ0FBQzlFLE9BQVIsQ0FBaUIrRSxNQUFELElBQVk7QUFDeEIsWUFBTUMsSUFBSSxHQUFHLEVBQWIsQ0FEd0IsQ0FFeEI7O0FBQ0EsWUFBTUMsVUFBVSxHQUFHN08sY0FBYyxDQUFDOE8sa0JBQWYsQ0FBa0N6SixNQUFsQyxDQUF5Q3NKLE1BQU0sQ0FBQ0ksUUFBaEQsQ0FBbkI7O0FBQ0FILE1BQUFBLElBQUksQ0FBQ0gsS0FBTCxHQUFhSSxVQUFVLENBQUNKLEtBQXhCLENBSndCLENBTXhCOztBQUNBLFlBQU1PLG9CQUFvQixHQUFHOU8sWUFBWSxDQUFDK08sYUFBYixDQUEyQjVKLE1BQTNCLENBQWtDc0osTUFBTSxDQUFDTyxlQUFQLENBQXVCeEosT0FBekQsQ0FBN0I7O0FBQ0FrSixNQUFBQSxJQUFJLENBQUNPLFFBQUwsR0FBZ0JILG9CQUFvQixDQUFDSSxTQUFyQixDQUErQkMsVUFBL0IsQ0FBMENGLFFBQTFEOztBQUNBdE8sc0JBQU9DLEtBQVAsQ0FBYSxxQkFBYixFQUFvQ3VHLE1BQXBDLEVBQTRDdUgsSUFBSSxDQUFDTyxRQUFqRCxFQVR3QixDQVd4Qjs7O0FBQ0EsVUFBSVIsTUFBTSxDQUFDVyxVQUFYLEVBQXVCO0FBQ25CLGNBQU1DLFNBQVMsR0FBR3JQLFlBQVksQ0FBQytPLGFBQWIsQ0FBMkI1SixNQUEzQixDQUFrQ3NKLE1BQU0sQ0FBQ1csVUFBUCxDQUFrQjVKLE9BQXBELENBQWxCOztBQUNBLFlBQUk2SixTQUFTLElBQUlBLFNBQVMsQ0FBQ0QsVUFBdkIsSUFBcUNDLFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBMUQsSUFBd0VELFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NDLGFBQTVHLEVBQTJIO0FBQ3ZIYixVQUFBQSxJQUFJLENBQUNhLGFBQUwsR0FBcUJwUSxJQUFJLENBQUNxUSxTQUFMLENBQWVILFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NDLGFBQS9DLENBQXJCO0FBQ0gsU0FGRCxNQUVPO0FBQ0g1TywwQkFBT0MsS0FBUCxDQUFhLGlDQUFiLEVBQWdEdUcsTUFBaEQ7O0FBQ0F1SCxVQUFBQSxJQUFJLENBQUNhLGFBQUwsR0FBcUJwUSxJQUFJLENBQUNxUSxTQUFMLENBQWUsQ0FBZixDQUFyQjtBQUNIOztBQUNEN08sd0JBQU9DLEtBQVAsQ0FBYSw4QkFBYixFQUE2Q3VHLE1BQTdDLEVBQXFEdUgsSUFBSSxDQUFDYSxhQUExRDs7QUFFQSxZQUFHRixTQUFTLElBQUlBLFNBQVMsQ0FBQ0QsVUFBdkIsSUFBcUNDLFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBMUQsSUFBd0VELFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NHLFNBQTNHLEVBQXNIO0FBQ2xIZixVQUFBQSxJQUFJLENBQUNlLFNBQUwsR0FBaUJKLFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NHLFNBQWpEO0FBQ0gsU0FGRCxNQUVPO0FBQ0g5TywwQkFBT0MsS0FBUCxDQUFhLGlDQUFiLEVBQWdEdUcsTUFBaEQ7O0FBQ0F1SCxVQUFBQSxJQUFJLENBQUNlLFNBQUwsR0FBaUIsQ0FBakI7QUFDSDs7QUFDRDlPLHdCQUFPQyxLQUFQLENBQWEsOEJBQWIsRUFBNkN1RyxNQUE3QyxFQUFxRHVILElBQUksQ0FBQ2UsU0FBMUQ7O0FBQ0FmLFFBQUFBLElBQUksQ0FBQ2dCLFVBQUwsR0FBa0IsRUFBbEI7O0FBQ0EsYUFBSyxNQUFNM0MsS0FBWCxJQUFvQnNDLFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NJLFVBQXBELEVBQWdFO0FBQzVELGdCQUFNQyxXQUFXLEdBQUdOLFNBQVMsQ0FBQ0QsVUFBVixDQUFxQkUsVUFBckIsQ0FBZ0NJLFVBQWhDLENBQTJDM0MsS0FBM0MsQ0FBcEI7QUFDQSxnQkFBTVksU0FBUyxHQUFHLEVBQWxCO0FBQ0FBLFVBQUFBLFNBQVMsQ0FBQ3JOLElBQVYsR0FBaUJxUCxXQUFXLENBQUMvRCxPQUFaLEVBQWpCO0FBQ0ErQixVQUFBQSxTQUFTLENBQUNpQyxPQUFWLEdBQW9CRCxXQUFXLENBQUNFLFVBQVosRUFBcEIsQ0FKNEQsQ0FLNUQ7O0FBQ0FsUCwwQkFBT0MsS0FBUCxDQUFhLDBCQUFiLEVBQXlDdUcsTUFBekMsRUFBaUR3RyxTQUFqRDs7QUFDQWUsVUFBQUEsSUFBSSxDQUFDZ0IsVUFBTCxDQUFnQmhPLElBQWhCLENBQXFCaU0sU0FBckI7QUFDSDtBQUNKLE9BdkN1QixDQXlDeEI7OztBQUNBSyxNQUFBQSxLQUFLLENBQUN0TSxJQUFOLENBQVdnTixJQUFYO0FBQ0gsS0EzQ0Q7QUE2Q0EsV0FBT1YsS0FBUDtBQUNIOztBQXhrQjRDLENBQWpEO0FBMmtCQThCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjNQLGNBQWpCIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiBDb3B5cmlnaHQgMjAxOCBLVCBBbGwgUmlnaHRzIFJlc2VydmVkLlxuKi9cblxuLyogZXNsaW50LWRpc2FibGUgKi9cbmltcG9ydCBsb2dnZXIgZnJvbSAnLi4vLi4vY29tbW9uL2xvZ2dlcic7XG5cbmNvbnN0IGJhc2VDaGFubmVsID0gcmVxdWlyZSgnZmFicmljLWNsaWVudC9saWIvQ2hhbm5lbCcpO1xuXG5jb25zdCB1dGlscyA9IHJlcXVpcmUoJ2ZhYnJpYy1jbGllbnQvbGliL3V0aWxzLmpzJyk7XG5jb25zdCBjbGllbnRVdGlscyA9IHJlcXVpcmUoJy4vQ2xpZW50VXRpbHNFeHRlbmRzJyk7XG5jb25zdCBDb25zdGFudHMgPSByZXF1aXJlKCdmYWJyaWMtY2xpZW50L2xpYi9Db25zdGFudHMuanMnKTtcblxuY29uc3QgZ3JwYyA9IHJlcXVpcmUoJ2dycGMnKTtcbmNvbnN0IExvbmcgPSByZXF1aXJlKCdsb25nJyk7XG5jb25zdCBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xuXG5jb25zdCBfY2NQcm90byA9IGdycGMubG9hZChwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi4vLi4vLi4nLCAnbm9kZV9tb2R1bGVzL2ZhYnJpYy1jbGllbnQvbGliL3Byb3Rvcy9wZWVyL2NoYWluY29kZS5wcm90bycpKS5wcm90b3M7XG5jb25zdCBfdHJhbnNQcm90byA9IGdycGMubG9hZChwYXRoLmpvaW4oX19kaXJuYW1lLCAnLi4vLi4vLi4nLCAnbm9kZV9tb2R1bGVzL2ZhYnJpYy1jbGllbnQvbGliL3Byb3Rvcy9wZWVyL3RyYW5zYWN0aW9uLnByb3RvJykpLnByb3RvcztcbmNvbnN0IF9wcm9wb3NhbFByb3RvID0gZ3JwYy5sb2FkKHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi8uLi8uLicsICdub2RlX21vZHVsZXMvZmFicmljLWNsaWVudC9saWIvcHJvdG9zL3BlZXIvcHJvcG9zYWwucHJvdG8nKSkucHJvdG9zO1xuY29uc3QgX2NvbW1vblByb3RvID0gZ3JwYy5sb2FkKHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi8uLi8uLicsICdub2RlX21vZHVsZXMvZmFicmljLWNsaWVudC9saWIvcHJvdG9zL2NvbW1vbi9jb21tb24ucHJvdG8nKSkuY29tbW9uO1xuXG5jb25zdCBfaWRlbnRpdHlQcm90byA9IGdycGMubG9hZChwYXRoLmpvaW4oX19kaXJuYW1lLCAnL3Byb3Rvcy9tc3AvaWRlbnRpdGllcy5wcm90bycpKS5tc3A7XG5jb25zdCBfZ29zc2lwUHJvdG8gPSBncnBjLmxvYWQocGF0aC5qb2luKF9fZGlybmFtZSwgJy9wcm90b3MvZ29zc2lwL21lc3NhZ2UucHJvdG8nKSkuZ29zc2lwO1xuY29uc3QgX2Rpc2NvdmVyeVByb3RvID0gZ3JwYy5sb2FkKHBhdGguam9pbihfX2Rpcm5hbWUsICcvcHJvdG9zL2Rpc2NvdmVyeS9wcm90b2NvbC5wcm90bycpKS5kaXNjb3Zlcnk7XG5cbmNvbnN0IENoYW5uZWxFeHRlbmRzID0gY2xhc3MgZXh0ZW5kcyBiYXNlQ2hhbm5lbCB7XG4gICAgY29uc3RydWN0b3IobmFtZSwgY2xpZW50Q29udGV4dCkge1xuICAgICAgICBzdXBlcihuYW1lLCBjbGllbnRDb250ZXh0KTtcbiAgICB9XG5cbiAgICBzZW5kVHJhbnNhY3Rpb25Qcm9wb3NhbE9CKHJlcXVlc3QsIHRpbWVvdXQpIHtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKCdzZW5kVHJhbnNhY3Rpb25Qcm9wb3NhbCAtIHN0YXJ0Jyk7XG5cbiAgICAgICAgaWYgKCFyZXF1ZXN0KSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ01pc3NpbmcgcmVxdWVzdCBvYmplY3QgZm9yIHRoaXMgdHJhbnNhY3Rpb24gcHJvcG9zYWwnKTtcbiAgICAgICAgfVxuICAgICAgICByZXF1ZXN0LnRhcmdldHMgPSB0aGlzLl9nZXRUYXJnZXRzKHJlcXVlc3QudGFyZ2V0cywgQ29uc3RhbnRzLk5ldHdvcmtDb25maWcuRU5ET1JTSU5HX1BFRVJfUk9MRSk7XG5cbiAgICAgICAgcmV0dXJuIENoYW5uZWxFeHRlbmRzLnNlbmRUcmFuc2FjdGlvblByb3Bvc2FsT0IocmVxdWVzdCwgdGhpcy5fbmFtZSwgdGhpcy5fY2xpZW50Q29udGV4dCwgdGltZW91dCk7XG4gICAgfVxuXG4gICAgc3RhdGljIHNlbmRUcmFuc2FjdGlvblByb3Bvc2FsT0IocmVxdWVzdCwgY2hhbm5lbElkLCBjbGllbnRDb250ZXh0LCB0aW1lb3V0KSB7XG4gICAgICAgIC8vIFZlcmlmeSB0aGF0IGEgUGVlciBoYXMgYmVlbiBhZGRlZFxuICAgICAgICB2YXIgZXJyb3JNc2cgPSBjbGllbnRVdGlscy5jaGVja1Byb3Bvc2FsUmVxdWVzdChyZXF1ZXN0KTtcblxuICAgICAgICBpZiAoZXJyb3JNc2cpIHtcbiAgICAgICAgICAgIC8vIGRvIG5vdGhpbmcgc28gd2Ugc2tpcCB0aGUgcmVzdCBvZiB0aGUgY2hlY2tzXG4gICAgICAgIH0gZWxzZSBpZiAoIXJlcXVlc3QuYXJncykge1xuICAgICAgICAgICAgLy8gYXJncyBpcyBub3Qgb3B0aW9uYWwgYmVjYXVzZSB3ZSBuZWVkIGZvciB0cmFuc2FjdGlvbiB0byBleGVjdXRlXG4gICAgICAgICAgICBlcnJvck1zZyA9ICdNaXNzaW5nIFwiYXJnc1wiIGluIFRyYW5zYWN0aW9uIHByb3Bvc2FsIHJlcXVlc3QnO1xuICAgICAgICB9IGVsc2UgaWYgKCFyZXF1ZXN0LnRhcmdldHMgfHwgcmVxdWVzdC50YXJnZXRzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIGVycm9yTXNnID0gJ01pc3NpbmcgcGVlciBvYmplY3RzIGluIFRyYW5zYWN0aW9uIHByb3Bvc2FsJztcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChlcnJvck1zZykge1xuICAgICAgICAgICAgbG9nZ2VyLmVycm9yKCdzZW5kVHJhbnNhY3Rpb25Qcm9wb3NhbCBlcnJvciAnICsgZXJyb3JNc2cpO1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGVycm9yTXNnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBhcmdzID0gW107XG4gICAgICAgIGFyZ3MucHVzaChCdWZmZXIuZnJvbShyZXF1ZXN0LmZjbiA/IHJlcXVlc3QuZmNuIDogJ2ludm9rZScsICd1dGY4JykpO1xuICAgICAgICBsb2dnZXIuZGVidWcoJ3NlbmRUcmFuc2FjdGlvblByb3Bvc2FsIC0gYWRkaW5nIGZ1bmN0aW9uIGFyZzolcycsIHJlcXVlc3QuZmNuID8gcmVxdWVzdC5mY24gOiAnaW52b2tlJyk7XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByZXF1ZXN0LmFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1Zygnc2VuZFRyYW5zYWN0aW9uUHJvcG9zYWwgLSBhZGRpbmcgYXJnOiVzJywgcmVxdWVzdC5hcmdzW2ldKTtcbiAgICAgICAgICAgIGFyZ3MucHVzaChCdWZmZXIuZnJvbShyZXF1ZXN0LmFyZ3NbaV0sICd1dGY4JykpO1xuICAgICAgICB9XG4gICAgICAgIC8vc3BlY2lhbCBjYXNlIHRvIHN1cHBvcnQgdGhlIGJ5dGVzIGFyZ3VtZW50IG9mIHRoZSBxdWVyeSBieSBoYXNoXG4gICAgICAgIGlmIChyZXF1ZXN0LmFyZ2J5dGVzKSB7XG4gICAgICAgICAgICBsb2dnZXIuZGVidWcoJ3NlbmRUcmFuc2FjdGlvblByb3Bvc2FsIC0gYWRkaW5nIHRoZSBhcmd1bWVudCA6OiBhcmdieXRlcycpO1xuICAgICAgICAgICAgYXJncy5wdXNoKHJlcXVlc3QuYXJnYnl0ZXMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCdzZW5kVHJhbnNhY3Rpb25Qcm9wb3NhbCAtIG5vdCBhZGRpbmcgdGhlIGFyZ3VtZW50IDo6IGFyZ2J5dGVzJyk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGludm9rZVNwZWMgPSB7XG4gICAgICAgICAgICB0eXBlOiBfY2NQcm90by5DaGFpbmNvZGVTcGVjLlR5cGUuR09MQU5HLFxuICAgICAgICAgICAgY2hhaW5jb2RlX2lkOiB7XG4gICAgICAgICAgICAgICAgbmFtZTogcmVxdWVzdC5jaGFpbmNvZGVJZFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGlucHV0OiB7XG4gICAgICAgICAgICAgICAgYXJnczogYXJnc1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBwcm9wb3NhbCwgaGVhZGVyO1xuICAgICAgICB2YXIgc2lnbmVyID0gbnVsbDtcbiAgICAgICAgaWYgKHJlcXVlc3Quc2lnbmVyKSB7XG4gICAgICAgICAgICBzaWduZXIgPSByZXF1ZXN0LnNpZ25lcjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHNpZ25lciA9IGNsaWVudENvbnRleHQuX2dldFNpZ25pbmdJZGVudGl0eShyZXF1ZXN0LnR4SWQuaXNBZG1pbigpKTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgY2hhbm5lbEhlYWRlciA9IGNsaWVudFV0aWxzLmJ1aWxkQ2hhbm5lbEhlYWRlcihcbiAgICAgICAgICAgIF9jb21tb25Qcm90by5IZWFkZXJUeXBlLkVORE9SU0VSX1RSQU5TQUNUSU9OLFxuICAgICAgICAgICAgY2hhbm5lbElkLFxuICAgICAgICAgICAgcmVxdWVzdC50eElkLmdldFRyYW5zYWN0aW9uSUQoKSxcbiAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICByZXF1ZXN0LmNoYWluY29kZUlkLFxuICAgICAgICAgICAgY2xpZW50VXRpbHMuYnVpbGRDdXJyZW50VGltZXN0YW1wKCksXG4gICAgICAgICAgICByZXF1ZXN0LnRhcmdldHNbMF0uZ2V0Q2xpZW50Q2VydEhhc2goKVxuICAgICAgICApO1xuICAgICAgICBoZWFkZXIgPSBjbGllbnRVdGlscy5idWlsZEhlYWRlck9CKHNpZ25lciwgY2hhbm5lbEhlYWRlciwgcmVxdWVzdC50eElkLmdldE5vbmNlKCkpO1xuICAgICAgICBwcm9wb3NhbCA9IGNsaWVudFV0aWxzLmJ1aWxkUHJvcG9zYWwoaW52b2tlU3BlYywgaGVhZGVyLCByZXF1ZXN0LnRyYW5zaWVudE1hcCk7XG4gICAgICAgIGxldCBzaWduZWRfcHJvcG9zYWwgPSBjbGllbnRVdGlscy5zaWduUHJvcG9zYWxPQihzaWduZXIsIHByb3Bvc2FsKTtcblxuICAgICAgICByZXR1cm4gY2xpZW50VXRpbHMuc2VuZFBlZXJzUHJvcG9zYWwocmVxdWVzdC50YXJnZXRzLCBzaWduZWRfcHJvcG9zYWwsIHRpbWVvdXQpXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAocmVzcG9uc2VzKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoW3Jlc3BvbnNlcywgcHJvcG9zYWxdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApLmNhdGNoKFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKCdGYWlsZWQgUHJvcG9zYWwuIEVycm9yOiAlcycsIGVyci5zdGFjayA/IGVyci5zdGFjayA6IGVycik7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgfVxuXG4gICAgc2VuZFRyYW5zYWN0aW9uT0IocmVxdWVzdCkge1xuICAgICAgICBsb2dnZXIuZGVidWcoJ3NlbmRUcmFuc2FjdGlvbiAtIHN0YXJ0IDo6IGNoYW5uZWwgJXMnLCB0aGlzKTtcbiAgICAgICAgdmFyIGVycm9yTXNnID0gbnVsbDtcblxuICAgICAgICBpZiAocmVxdWVzdCkge1xuICAgICAgICAgICAgLy8gVmVyaWZ5IHRoYXQgZGF0YSBpcyBiZWluZyBwYXNzZWQgaW5cbiAgICAgICAgICAgIGlmICghcmVxdWVzdC5wcm9wb3NhbFJlc3BvbnNlcykge1xuICAgICAgICAgICAgICAgIGVycm9yTXNnID0gJ01pc3NpbmcgXCJwcm9wb3NhbFJlc3BvbnNlc1wiIHBhcmFtZXRlciBpbiB0cmFuc2FjdGlvbiByZXF1ZXN0JztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghcmVxdWVzdC5wcm9wb3NhbCkge1xuICAgICAgICAgICAgICAgIGVycm9yTXNnID0gJ01pc3NpbmcgXCJwcm9wb3NhbFwiIHBhcmFtZXRlciBpbiB0cmFuc2FjdGlvbiByZXF1ZXN0JztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVycm9yTXNnID0gJ01pc3NpbmcgaW5wdXQgcmVxdWVzdCBvYmplY3Qgb24gdGhlIHRyYW5zYWN0aW9uIHJlcXVlc3QnO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGVycm9yTXNnKSB7XG4gICAgICAgICAgICBsb2dnZXIuZXJyb3IoJ3NlbmRUcmFuc2FjdGlvbiBlcnJvciAnICsgZXJyb3JNc2cpO1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGVycm9yTXNnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBwcm9wb3NhbFJlc3BvbnNlcyA9IHJlcXVlc3QucHJvcG9zYWxSZXNwb25zZXM7XG4gICAgICAgIGxldCBjaGFpbmNvZGVQcm9wb3NhbCA9IHJlcXVlc3QucHJvcG9zYWw7XG5cbiAgICAgICAgdmFyIGVuZG9yc2VtZW50cyA9IFtdO1xuICAgICAgICBsZXQgcHJvcG9zYWxSZXNwb25zZSA9IHByb3Bvc2FsUmVzcG9uc2VzO1xuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShwcm9wb3NhbFJlc3BvbnNlcykpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvcG9zYWxSZXNwb25zZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAvLyBtYWtlIHN1cmUgb25seSB0YWtlIHRoZSB2YWxpZCByZXNwb25zZXMgdG8gc2V0IG9uIHRoZSBjb25zb2xpZGF0ZWQgcmVzcG9uc2Ugb2JqZWN0XG4gICAgICAgICAgICAgICAgLy8gdG8gdXNlIGluIHRoZSB0cmFuc2FjdGlvbiBvYmplY3RcbiAgICAgICAgICAgICAgICBpZiAocHJvcG9zYWxSZXNwb25zZXNbaV0ucmVzcG9uc2UgJiYgcHJvcG9zYWxSZXNwb25zZXNbaV0ucmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxSZXNwb25zZSA9IHByb3Bvc2FsUmVzcG9uc2VzW2ldO1xuICAgICAgICAgICAgICAgICAgICBlbmRvcnNlbWVudHMucHVzaChwcm9wb3NhbFJlc3BvbnNlLmVuZG9yc2VtZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAocHJvcG9zYWxSZXNwb25zZSAmJiBwcm9wb3NhbFJlc3BvbnNlLnJlc3BvbnNlICYmIHByb3Bvc2FsUmVzcG9uc2UucmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICBlbmRvcnNlbWVudHMucHVzaChwcm9wb3NhbFJlc3BvbnNlLmVuZG9yc2VtZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChlbmRvcnNlbWVudHMubGVuZ3RoIDwgMSkge1xuICAgICAgICAgICAgbG9nZ2VyLmVycm9yKCdzZW5kVHJhbnNhY3Rpb24gLSBubyB2YWxpZCBlbmRvcnNlbWVudHMgZm91bmQnKTtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignbm8gdmFsaWQgZW5kb3JzZW1lbnRzIGZvdW5kJyk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyB2ZXJpZnkgdGhhdCB3ZSBoYXZlIGFuIG9yZGVyZXIgY29uZmlndXJlZFxuICAgICAgICB2YXIgb3JkZXJlciA9IHRoaXMuX2NsaWVudENvbnRleHQuZ2V0VGFyZ2V0T3JkZXJlcihyZXF1ZXN0Lm9yZGVyZXIsIHRoaXMuX29yZGVyZXJzLCB0aGlzLl9uYW1lKTtcblxuICAgICAgICB2YXIgdXNlX2FkbWluX3NpZ25lciA9IGZhbHNlO1xuICAgICAgICBpZiAocmVxdWVzdC50eElkKSB7XG4gICAgICAgICAgICB1c2VfYWRtaW5fc2lnbmVyID0gcmVxdWVzdC50eElkLmlzQWRtaW4oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBoZWFkZXIgPSBfY29tbW9uUHJvdG8uSGVhZGVyLmRlY29kZShjaGFpbmNvZGVQcm9wb3NhbC5nZXRIZWFkZXIoKSk7XG5cbiAgICAgICAgdmFyIGNoYWluY29kZUVuZG9yc2VkQWN0aW9uID0gbmV3IF90cmFuc1Byb3RvLkNoYWluY29kZUVuZG9yc2VkQWN0aW9uKCk7XG4gICAgICAgIGNoYWluY29kZUVuZG9yc2VkQWN0aW9uLnNldFByb3Bvc2FsUmVzcG9uc2VQYXlsb2FkKHByb3Bvc2FsUmVzcG9uc2UucGF5bG9hZCk7XG4gICAgICAgIGNoYWluY29kZUVuZG9yc2VkQWN0aW9uLnNldEVuZG9yc2VtZW50cyhlbmRvcnNlbWVudHMpO1xuXG4gICAgICAgIHZhciBjaGFpbmNvZGVBY3Rpb25QYXlsb2FkID0gbmV3IF90cmFuc1Byb3RvLkNoYWluY29kZUFjdGlvblBheWxvYWQoKTtcbiAgICAgICAgY2hhaW5jb2RlQWN0aW9uUGF5bG9hZC5zZXRBY3Rpb24oY2hhaW5jb2RlRW5kb3JzZWRBY3Rpb24pO1xuXG4gICAgICAgIC8vIHRoZSBUcmFuc2llbnRNYXAgZmllbGQgaW5zaWRlIHRoZSBvcmlnaW5hbCBwcm9wb3NhbCBwYXlsb2FkIGlzIG9ubHkgbWVhbnQgZm9yIHRoZVxuICAgICAgICAvLyBlbmRvcnNlcnMgdG8gdXNlIGZyb20gaW5zaWRlIHRoZSBjaGFpbmNvZGUuIFRoaXMgbXVzdCBiZSB0YWtlbiBvdXQgYmVmb3JlIHNlbmRpbmdcbiAgICAgICAgLy8gdG8gdGhlIG9yZGVyZXIsIG90aGVyd2lzZSB0aGUgdHJhbnNhY3Rpb24gd2lsbCBiZSByZWplY3RlZCBieSB0aGUgdmFsaWRhdG9ycyB3aGVuXG4gICAgICAgIC8vIGl0IGNvbXBhcmVzIHRoZSBwcm9wb3NhbCBoYXNoIGNhbGN1bGF0ZWQgYnkgdGhlIGVuZG9yc2VycyBhbmQgcmV0dXJuZWQgaW4gdGhlXG4gICAgICAgIC8vIHByb3Bvc2FsIHJlc3BvbnNlLCB3aGljaCB3YXMgY2FsY3VsYXRlZCB3aXRob3V0IHRoZSBUcmFuc2llbnRNYXBcbiAgICAgICAgdmFyIG9yaWdpbmFsQ2hhaW5jb2RlUHJvcG9zYWxQYXlsb2FkID0gX3Byb3Bvc2FsUHJvdG8uQ2hhaW5jb2RlUHJvcG9zYWxQYXlsb2FkLmRlY29kZShjaGFpbmNvZGVQcm9wb3NhbC5wYXlsb2FkKTtcbiAgICAgICAgdmFyIGNoYWluY29kZVByb3Bvc2FsUGF5bG9hZE5vVHJhbnMgPSBuZXcgX3Byb3Bvc2FsUHJvdG8uQ2hhaW5jb2RlUHJvcG9zYWxQYXlsb2FkKCk7XG4gICAgICAgIGNoYWluY29kZVByb3Bvc2FsUGF5bG9hZE5vVHJhbnMuc2V0SW5wdXQob3JpZ2luYWxDaGFpbmNvZGVQcm9wb3NhbFBheWxvYWQuaW5wdXQpOyAvLyBvbmx5IHNldCB0aGUgaW5wdXQgZmllbGQsIHNraXBwaW5nIHRoZSBUcmFuc2llbnRNYXBcbiAgICAgICAgY2hhaW5jb2RlQWN0aW9uUGF5bG9hZC5zZXRDaGFpbmNvZGVQcm9wb3NhbFBheWxvYWQoY2hhaW5jb2RlUHJvcG9zYWxQYXlsb2FkTm9UcmFucy50b0J1ZmZlcigpKTtcblxuICAgICAgICB2YXIgdHJhbnNhY3Rpb25BY3Rpb24gPSBuZXcgX3RyYW5zUHJvdG8uVHJhbnNhY3Rpb25BY3Rpb24oKTtcbiAgICAgICAgdHJhbnNhY3Rpb25BY3Rpb24uc2V0SGVhZGVyKGhlYWRlci5nZXRTaWduYXR1cmVIZWFkZXIoKSk7XG4gICAgICAgIHRyYW5zYWN0aW9uQWN0aW9uLnNldFBheWxvYWQoY2hhaW5jb2RlQWN0aW9uUGF5bG9hZC50b0J1ZmZlcigpKTtcblxuICAgICAgICB2YXIgYWN0aW9ucyA9IFtdO1xuICAgICAgICBhY3Rpb25zLnB1c2godHJhbnNhY3Rpb25BY3Rpb24pO1xuXG4gICAgICAgIHZhciB0cmFuc2FjdGlvbiA9IG5ldyBfdHJhbnNQcm90by5UcmFuc2FjdGlvbigpO1xuICAgICAgICB0cmFuc2FjdGlvbi5zZXRBY3Rpb25zKGFjdGlvbnMpO1xuXG5cbiAgICAgICAgdmFyIHBheWxvYWQgPSBuZXcgX2NvbW1vblByb3RvLlBheWxvYWQoKTtcbiAgICAgICAgcGF5bG9hZC5zZXRIZWFkZXIoaGVhZGVyKTtcbiAgICAgICAgcGF5bG9hZC5zZXREYXRhKHRyYW5zYWN0aW9uLnRvQnVmZmVyKCkpO1xuXG4gICAgICAgIGxldCBwYXlsb2FkX2J5dGVzID0gcGF5bG9hZC50b0J1ZmZlcigpO1xuXG4gICAgICAgIHZhciBzaWduZXIgPSB0aGlzLl9jbGllbnRDb250ZXh0Ll9nZXRTaWduaW5nSWRlbnRpdHkodXNlX2FkbWluX3NpZ25lcik7XG5cbiAgICAgICAgbGV0IHNpZ25hdHVyZSA9IFwiMFwiO1xuXG4gICAgICAgIC8vIGJ1aWxkaW5nIG1hbnVhbGx5IG9yIHdpbGwgZ2V0IHByb3RvYnVmIGVycm9ycyBvbiBzZW5kXG4gICAgICAgIHZhciBlbnZlbG9wZSA9IHtcbiAgICAgICAgICAgIHNpZ25hdHVyZTogc2lnbmF0dXJlLFxuICAgICAgICAgICAgcGF5bG9hZDogcGF5bG9hZF9ieXRlc1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBvcmRlcmVyLnNlbmRCcm9hZGNhc3QoZW52ZWxvcGUpO1xuICAgICAgICAvL29yZGVyZXIuc2VuZEJyb2FkY2FzdE9CKGVudmVsb3BlKTtcbiAgICB9XG5cbiAgICBhc3luYyBpbml0aWFsaXplT0IocmVxdWVzdCkge1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnaW5pdGlhbGl6ZSc7XG4gICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBzdGFydCcsIG1ldGhvZCk7XG5cbiAgICAgICAgbGV0IGVuZG9yc2VtZW50X2hhbmRsZXJfcGF0aCA9IG51bGw7XG4gICAgICAgIGxldCBjb21taXRfaGFuZGxlcl9wYXRoID0gbnVsbDtcblxuICAgICAgICBpZiAocmVxdWVzdCkge1xuICAgICAgICAgICAgaWYgKHJlcXVlc3QuY29uZmlnVXBhdGUpIHtcbiAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gaGF2ZSBhIGNvbmZpZ3VwZGF0ZScsIG1ldGhvZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkQ29uZmlnVXBkYXRlKHJlcXVlc3QuY29uZmlnVXBhdGUpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcmVxdWVzdC5kaXNjb3ZlciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiByZXF1ZXN0LmRpc2NvdmVyID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSB1c2VyIHJlcXVlc3RlZCBkaXNjb3ZlciAlcycsIG1ldGhvZCwgcmVxdWVzdC5kaXNjb3Zlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl91c2VfZGlzY292ZXJ5ID0gcmVxdWVzdC5kaXNjb3ZlcjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUmVxdWVzdCBwYXJhbWV0ZXIgXCJkaXNjb3ZlclwiIG11c3QgYmUgYm9vbGVhbicpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChyZXF1ZXN0LmVuZG9yc2VtZW50SGFuZGxlcikge1xuICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gdXNlciByZXF1ZXN0ZWQgZW5kb3JzZW1lbnRIYW5kbGVyICVzJywgbWV0aG9kLCByZXF1ZXN0LmVuZG9yc2VtZW50SGFuZGxlcik7XG4gICAgICAgICAgICAgICAgICAgIGVuZG9yc2VtZW50X2hhbmRsZXJfcGF0aCA9IHJlcXVlc3QuZW5kb3JzZW1lbnRIYW5kbGVyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAocmVxdWVzdC5jb21taXRIYW5kbGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSB1c2VyIHJlcXVlc3RlZCBjb21taXRIYW5kbGVyICVzJywgbWV0aG9kLCByZXF1ZXN0LmNvbW1pdEhhbmRsZXIpO1xuICAgICAgICAgICAgICAgICAgICBjb21taXRfaGFuZGxlcl9wYXRoID0gcmVxdWVzdC5jb21taXRIYW5kbGVyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIHNldHVwIHRoZSBlbmRvcnNlbWVudCBoYW5kbGVyXG4gICAgICAgIGlmICghZW5kb3JzZW1lbnRfaGFuZGxlcl9wYXRoICYmIHRoaXMuX3VzZV9kaXNjb3ZlcnkpIHtcbiAgICAgICAgICAgIGVuZG9yc2VtZW50X2hhbmRsZXJfcGF0aCA9IHV0aWxzLmdldENvbmZpZ1NldHRpbmcoJ2VuZG9yc2VtZW50LWhhbmRsZXInKTtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSB1c2luZyBjb25maWcgc2V0dGluZyBmb3IgZW5kb3JzZW1lbnQgaGFuZGxlciA6OiVzJywgbWV0aG9kLCBlbmRvcnNlbWVudF9oYW5kbGVyX3BhdGgpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbmRvcnNlbWVudF9oYW5kbGVyX3BhdGgpIHtcbiAgICAgICAgICAgIHRoaXMuX2VuZG9yc2VtZW50X2hhbmRsZXIgPSByZXF1aXJlKGVuZG9yc2VtZW50X2hhbmRsZXJfcGF0aCkuY3JlYXRlKHRoaXMpO1xuICAgICAgICAgICAgYXdhaXQgdGhpcy5fZW5kb3JzZW1lbnRfaGFuZGxlci5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBzZXR1cCB0aGUgY29tbWl0IGhhbmRsZXJcbiAgICAgICAgaWYgKCFjb21taXRfaGFuZGxlcl9wYXRoKSB7XG4gICAgICAgICAgICBjb21taXRfaGFuZGxlcl9wYXRoID0gdXRpbHMuZ2V0Q29uZmlnU2V0dGluZygnY29tbWl0LWhhbmRsZXInKTtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSB1c2luZyBjb25maWcgc2V0dGluZyBmb3IgY29tbWl0IGhhbmRsZXIgOjolcycsIG1ldGhvZCwgY29tbWl0X2hhbmRsZXJfcGF0aCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGNvbW1pdF9oYW5kbGVyX3BhdGgpIHtcbiAgICAgICAgICAgIHRoaXMuX2NvbW1pdF9oYW5kbGVyID0gcmVxdWlyZShjb21taXRfaGFuZGxlcl9wYXRoKS5jcmVhdGUodGhpcyk7XG4gICAgICAgICAgICBhd2FpdCB0aGlzLl9jb21taXRfaGFuZGxlci5pbml0aWFsaXplKCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCByZXN1bHRzID0gYXdhaXQgdGhpcy5faW5pdGlhbGl6ZU9CKHJlcXVlc3QpO1xuXG4gICAgICAgIHJldHVybiByZXN1bHRzO1xuICAgIH1cblxuICAgIGFzeW5jIF9pbml0aWFsaXplT0IocmVxdWVzdCkge1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnX2luaXRpYWxpemUnO1xuICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gc3RhcnQnLCBtZXRob2QpO1xuXG4gICAgICAgIHRoaXMuX2Rpc2NvdmVyeV9yZXN1bHRzID0gbnVsbDtcbiAgICAgICAgdGhpcy5fbGFzdF9kaXNjb3Zlcl90aW1lc3RhbXAgPSBudWxsO1xuICAgICAgICB0aGlzLl9sYXN0X3JlZnJlc2hfcmVxdWVzdCA9IE9iamVjdC5hc3NpZ24oe30sIHJlcXVlc3QpO1xuICAgICAgICBsZXQgdGFyZ2V0X3BlZXIgPSB0aGlzLl9kaXNjb3ZlcnlfcGVlcjtcblxuICAgICAgICBpZiAocmVxdWVzdCAmJiByZXF1ZXN0LnRhcmdldCkge1xuICAgICAgICAgICAgdGFyZ2V0X3BlZXIgPSByZXF1ZXN0LnRhcmdldDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLl91c2VfZGlzY292ZXJ5KSB7XG4gICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gc3RhcnRpbmcgZGlzY292ZXJ5JywgbWV0aG9kKTtcbiAgICAgICAgICAgIHRhcmdldF9wZWVyID0gdGhpcy5fZ2V0VGFyZ2V0Rm9yRGlzY292ZXJ5KHRhcmdldF9wZWVyKTtcbiAgICAgICAgICAgIGlmICghdGFyZ2V0X3BlZXIpIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIHRhcmdldCBwcm92aWRlZCBmb3IgZGlzY292ZXJ5IHNlcnZpY2VzJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgY29uc3QgZGlzY292ZXJfcmVxdWVzdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiB0YXJnZXRfcGVlcixcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnOiB0cnVlXG4gICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGRpc2NvdmVyeV9yZXN1bHRzID0gYXdhaXQgdGhpcy5fZGlzY292ZXJPQihkaXNjb3Zlcl9yZXF1ZXN0KTtcbiAgICAgICAgICAgICAgICBpZiAoZGlzY292ZXJ5X3Jlc3VsdHMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRpc2NvdmVyeV9yZXN1bHRzLm1zcHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2J1aWxkRGlzY292ZXJ5TVNQcyhkaXNjb3ZlcnlfcmVzdWx0cyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBFcnJvcignTm8gTVNQIGluZm9ybWF0aW9uIGZvdW5kJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGRpc2NvdmVyeV9yZXN1bHRzLm9yZGVyZXJzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9idWlsZERpc2NvdmVyeU9yZGVyZXJzKGRpc2NvdmVyeV9yZXN1bHRzLCBkaXNjb3ZlcnlfcmVzdWx0cy5tc3BzLCByZXF1ZXN0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAoZGlzY292ZXJ5X3Jlc3VsdHMucGVlcnNfYnlfb3JnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9idWlsZERpc2NvdmVyeVBlZXJzKGRpc2NvdmVyeV9yZXN1bHRzLCBkaXNjb3ZlcnlfcmVzdWx0cy5tc3BzLCByZXF1ZXN0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGRpc2NvdmVyeV9yZXN1bHRzLmVuZG9yc2VtZW50X3BsYW5zID0gW107XG5cbiAgICAgICAgICAgICAgICBjb25zdCBpbnRlcmVzdHMgPSBbXTtcbiAgICAgICAgICAgICAgICBjb25zdCBwbGFuX2lkcyA9IFtdO1xuICAgICAgICAgICAgICAgIHRoaXMuX2Rpc2NvdmVyeV9pbnRlcmVzdHMuZm9yRWFjaCgoaW50ZXJlc3QsIHBsYW5faWQpID0+e1xuICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gaGF2ZSBpbnRlcmVzdCBvZjolcycsIG1ldGhvZCwgcGxhbl9pZCk7XG4gICAgICAgICAgICAgICAgICAgIHBsYW5faWRzLnB1c2gocGxhbl9pZCk7XG4gICAgICAgICAgICAgICAgICAgIGludGVyZXN0cy5wdXNoKGludGVyZXN0KTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIGZvcihjb25zdCBpIGluIHBsYW5faWRzKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHBsYW5faWQgPSBwbGFuX2lkc1tpXTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaW50ZXJlc3QgPSBpbnRlcmVzdHNbaV07XG5cbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGlzY292ZXJfcmVxdWVzdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldDogdGFyZ2V0X3BlZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICBpbnRlcmVzdHM6IFtpbnRlcmVzdF1cbiAgICAgICAgICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgICAgICAgICBsZXQgZGlzY292ZXJfaW50ZXJlc3RfcmVzdWx0cyA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNjb3Zlcl9pbnRlcmVzdF9yZXN1bHRzID0gYXdhaXQgdGhpcy5fZGlzY292ZXJPQihkaXNjb3Zlcl9yZXF1ZXN0KTtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaChlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKCdOb3QgYWJsZSB0byBnZXQgYW4gZW5kb3JzZW1lbnQgcGxhbiBmb3IgJXMnLCBwbGFuX2lkKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmKGRpc2NvdmVyX2ludGVyZXN0X3Jlc3VsdHMgJiYgZGlzY292ZXJfaW50ZXJlc3RfcmVzdWx0cy5lbmRvcnNlbWVudF9wbGFucyAmJiBkaXNjb3Zlcl9pbnRlcmVzdF9yZXN1bHRzLmVuZG9yc2VtZW50X3BsYW5zWzBdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwbGFuID0gdGhpcy5fYnVpbGREaXNjb3ZlcnlFbmRvcnNlbWVudFBsYW4oZGlzY292ZXJfaW50ZXJlc3RfcmVzdWx0cywgcGxhbl9pZCwgZGlzY292ZXJ5X3Jlc3VsdHMubXNwcywgcmVxdWVzdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNjb3ZlcnlfcmVzdWx0cy5lbmRvcnNlbWVudF9wbGFucy5wdXNoKHBsYW4pO1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCdBZGRlZCBhbiBlbmRvcnNlbWVudCBwbGFuIGZvciAlcycsIHBsYW5faWQpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCdOb3QgYWRkaW5nIGFuIGVuZG9yc2VtZW50IHBsYW4gZm9yICVzJywgcGxhbl9pZCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBkaXNjb3ZlcnlfcmVzdWx0cy50aW1lc3RhbXAgPSBEYXRlLm5vdygpO1xuICAgICAgICAgICAgICAgIHRoaXMuX2Rpc2NvdmVyeV9yZXN1bHRzID0gZGlzY292ZXJ5X3Jlc3VsdHM7XG4gICAgICAgICAgICAgICAgdGhpcy5fZGlzY292ZXJ5X3BlZXIgPSB0YXJnZXRfcGVlcjtcbiAgICAgICAgICAgICAgICB0aGlzLl9sYXN0X2Rpc2NvdmVyX3RpbWVzdGFtcCA9IGRpc2NvdmVyeV9yZXN1bHRzLnRpbWVzdGFtcDtcblxuICAgICAgICAgICAgICAgIHJldHVybiBkaXNjb3ZlcnlfcmVzdWx0cztcbiAgICAgICAgICAgIH0gY2F0Y2goZXJyb3IpIHtcbiAgICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgICAgIHRocm93IEVycm9yKCdGYWlsZWQgdG8gZGlzY292ZXIgOjonKyBlcnJvci50b1N0cmluZygpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRhcmdldF9wZWVyID0gdGhpcy5fZ2V0Rmlyc3RBdmFpbGFibGVUYXJnZXQodGFyZ2V0X3BlZXIpO1xuICAgICAgICAgICAgaWYgKCF0YXJnZXRfcGVlcikge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTm8gdGFyZ2V0IHByb3ZpZGVkIGZvciBub24tZGlzY292ZXJ5IGluaXRpYWxpemF0aW9uJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBjb25maWdfZW52ZWxvcGUgPSBhd2FpdCB0aGlzLmdldENoYW5uZWxDb25maWcodGFyZ2V0X3BlZXIpO1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCdpbml0aWFsaXplIC0gZ290IGNvbmZpZyBlbnZlbG9wZSBmcm9tIGdldENoYW5uZWxDb25maWcgOjogJWonLCBjb25maWdfZW52ZWxvcGUpO1xuICAgICAgICAgICAgY29uc3QgY29uZmlnX2l0ZW1zID0gdGhpcy5sb2FkQ29uZmlnRW52ZWxvcGUoY29uZmlnX2VudmVsb3BlKTtcblxuICAgICAgICAgICAgcmV0dXJuIGNvbmZpZ19pdGVtcztcblxuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXN5bmMgX2Rpc2NvdmVyT0IocmVxdWVzdCkge1xuICAgICAgICBjb25zdCBtZXRob2QgPSAnZGlzY292ZXInO1xuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAgICAgbG9nZ2VyLmRlYnVnKCclcyAtIHN0YXJ0JywgbWV0aG9kKTtcbiAgICAgICAgY29uc3QgcmVzdWx0cyA9IHt9O1xuICAgICAgICBpZiAoIXJlcXVlc3QpIHtcbiAgICAgICAgICAgIHJlcXVlc3QgPSB7fTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCB1c2VBZG1pbiA9IHRydWU7IC8vZGVmYXVsdFxuICAgICAgICBpZih0eXBlb2YgcmVxdWVzdC51c2VBZG1pbiA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgICAgICB1c2VBZG1pbiA9IHJlcXVlc3QudXNlQWRtaW47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgdGFyZ2V0X3BlZXIgPSB0aGlzLl9nZXRUYXJnZXRGb3JEaXNjb3ZlcnkocmVxdWVzdC50YXJnZXQpO1xuICAgICAgICBjb25zdCBzaWduZXIgPSB0aGlzLl9jbGllbnRDb250ZXh0Ll9nZXRTaWduaW5nSWRlbnRpdHkodXNlQWRtaW4pOyAvL3VzZSB0aGUgYWRtaW4gaWYgYXNzaWduZWRcbiAgICAgICAgY29uc3QgZGlzY292ZXJ5X3JlcXVlc3QgPSBuZXcgX2Rpc2NvdmVyeVByb3RvLlJlcXVlc3QoKTtcblxuICAgICAgICBjb25zdCBhdXRoZW50aWNhdGlvbiA9IG5ldyBfZGlzY292ZXJ5UHJvdG8uQXV0aEluZm8oKTtcbiAgICAgICAgYXV0aGVudGljYXRpb24uc2V0Q2xpZW50SWRlbnRpdHkoc2lnbmVyLnNlcmlhbGl6ZSgpKTtcbiAgICAgICAgY29uc3QgY2VydF9oYXNoID0gdGhpcy5fY2xpZW50Q29udGV4dC5nZXRDbGllbnRDZXJ0SGFzaCh0cnVlKTtcbiAgICAgICAgaWYgKGNlcnRfaGFzaCkge1xuICAgICAgICAgICAgYXV0aGVudGljYXRpb24uc2V0Q2xpZW50VGxzQ2VydEhhc2goY2VydF9oYXNoKTtcbiAgICAgICAgfVxuICAgICAgICBkaXNjb3ZlcnlfcmVxdWVzdC5zZXRBdXRoZW50aWNhdGlvbihhdXRoZW50aWNhdGlvbik7XG5cbiAgICAgICAgLy8gYmUgc3VyZSB0byBhZGQgYWxsIGVudHJpZXMgdG8gdGhpcyBhcnJheSBiZWZvcmUgc2V0dGluZyBpbnRvIHRoZVxuICAgICAgICAvLyBncnBjIG9iamVjdFxuICAgICAgICBjb25zdCBxdWVyaWVzID0gW107XG5cbiAgICAgICAgLy8gaWYgZG9pbmcgbG9jYWwgaXQgd2lsbCBiZSBpbmRleCAwIG9mIHRoZSByZXN1bHRzXG4gICAgICAgIGlmIChyZXF1ZXN0LmxvY2FsKSB7XG4gICAgICAgICAgICBjb25zdCBxdWVyeSA9IG5ldyBfZGlzY292ZXJ5UHJvdG8uUXVlcnkoKTtcbiAgICAgICAgICAgIHF1ZXJpZXMucHVzaChxdWVyeSk7XG5cbiAgICAgICAgICAgIGNvbnN0IGxvY2FsX3BlZXJzID0gbmV3IF9kaXNjb3ZlcnlQcm90by5Mb2NhbFBlZXJRdWVyeSgpO1xuICAgICAgICAgICAgcXVlcnkuc2V0TG9jYWxQZWVycyhsb2NhbF9wZWVycyk7XG4gICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gYWRkaW5nIGxvY2FsIHBlZXJzIHF1ZXJ5JywgbWV0aG9kKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChyZXF1ZXN0LmNvbmZpZykge1xuICAgICAgICAgICAgbGV0IHF1ZXJ5ID0gbmV3IF9kaXNjb3ZlcnlQcm90by5RdWVyeSgpO1xuICAgICAgICAgICAgcXVlcmllcy5wdXNoKHF1ZXJ5KTtcbiAgICAgICAgICAgIHF1ZXJ5LnNldENoYW5uZWwodGhpcy5nZXROYW1lKCkpO1xuXG4gICAgICAgICAgICBjb25zdCBjb25maWdfcXVlcnkgPSBuZXcgX2Rpc2NvdmVyeVByb3RvLkNvbmZpZ1F1ZXJ5KCk7XG4gICAgICAgICAgICBxdWVyeS5zZXRDb25maWdRdWVyeShjb25maWdfcXVlcnkpO1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCclcyAtIGFkZGluZyBjb25maWcgcXVlcnknLCBtZXRob2QpO1xuXG4gICAgICAgICAgICBxdWVyeSA9IG5ldyBfZGlzY292ZXJ5UHJvdG8uUXVlcnkoKTtcbiAgICAgICAgICAgIHF1ZXJpZXMucHVzaChxdWVyeSk7XG4gICAgICAgICAgICBxdWVyeS5zZXRDaGFubmVsKHRoaXMuZ2V0TmFtZSgpKTtcblxuICAgICAgICAgICAgY29uc3QgcGVlcl9xdWVyeSA9IG5ldyBfZGlzY292ZXJ5UHJvdG8uUGVlck1lbWJlcnNoaXBRdWVyeSgpO1xuICAgICAgICAgICAgcXVlcnkuc2V0UGVlclF1ZXJ5KHBlZXJfcXVlcnkpO1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCclcyAtIGFkZGluZyBjaGFubmVsIHBlZXJzIHF1ZXJ5JywgbWV0aG9kKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGFkZCBhIGNoYWluY29kZSBxdWVyeSB0byBnZXQgZW5kb3JzZW1lbnQgcGxhbnNcbiAgICAgICAgaWYgKHJlcXVlc3QuaW50ZXJlc3RzICYmIHJlcXVlc3QuaW50ZXJlc3RzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGNvbnN0IHF1ZXJ5ID0gbmV3IF9kaXNjb3ZlcnlQcm90by5RdWVyeSgpO1xuICAgICAgICAgICAgcXVlcmllcy5wdXNoKHF1ZXJ5KTtcbiAgICAgICAgICAgIHF1ZXJ5LnNldENoYW5uZWwodGhpcy5nZXROYW1lKCkpO1xuXG4gICAgICAgICAgICBjb25zdCBpbnRlcmVzdHMgPSBbXTtcbiAgICAgICAgICAgIGZvcihjb25zdCBpbnRlcmVzdCBvZiByZXF1ZXN0LmludGVyZXN0cykge1xuICAgICAgICAgICAgICAgIGNvbnN0IHByb3RvX2ludGVyZXN0ID0gdGhpcy5fYnVpbGRQcm90b0NoYWluY29kZUludGVyZXN0KGludGVyZXN0KTtcbiAgICAgICAgICAgICAgICBpbnRlcmVzdHMucHVzaChwcm90b19pbnRlcmVzdCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbnN0IGNjX3F1ZXJ5ID0gbmV3IF9kaXNjb3ZlcnlQcm90by5DaGFpbmNvZGVRdWVyeSgpO1xuICAgICAgICAgICAgY2NfcXVlcnkuc2V0SW50ZXJlc3RzKGludGVyZXN0cyk7XG4gICAgICAgICAgICBxdWVyeS5zZXRDY1F1ZXJ5KGNjX3F1ZXJ5KTtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBhZGRpbmcgY2hhaW5jb2Rlcy9jb2xsZWN0aW9uIHF1ZXJ5JywgbWV0aG9kKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGJlIHN1cmUgdG8gc2V0IHRoZSBhcnJheSBhZnRlciBjb21wbGV0ZWx5IGJ1aWxkaW5nIGl0XG4gICAgICAgIGRpc2NvdmVyeV9yZXF1ZXN0LnNldFF1ZXJpZXMocXVlcmllcyk7XG5cbiAgICAgICAgLy8gYnVpbGQgdXAgdGhlIG91dGJvdW5kIHJlcXVlc3Qgb2JqZWN0XG4gICAgICAgIGNvbnN0IHNpZ25lZF9yZXF1ZXN0ID0gY2xpZW50VXRpbHMudG9FbnZlbG9wZShjbGllbnRVdGlscy5zaWduUHJvcG9zYWwoc2lnbmVyLCBkaXNjb3ZlcnlfcmVxdWVzdCkpO1xuXG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgdGFyZ2V0X3BlZXIuc2VuZERpc2NvdmVyeShzaWduZWRfcmVxdWVzdCk7XG4gICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBwcm9jZXNzaW5nIGRpc2NvdmVyeSByZXNwb25zZScsIG1ldGhvZCk7XG4gICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5yZXN1bHRzKSB7XG4gICAgICAgICAgICBsZXQgZXJyb3JfbXNnID0gbnVsbDtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBwYXJzZSBkaXNjb3ZlcnkgcmVzcG9uc2UnLCBtZXRob2QpO1xuICAgICAgICAgICAgZm9yIChjb25zdCBpbmRleCBpbiByZXNwb25zZS5yZXN1bHRzKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gcmVzcG9uc2UucmVzdWx0c1tpbmRleF07XG4gICAgICAgICAgICAgICAgaWYgKCFyZXN1bHQpIHtcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JfbXNnID0gJ0Rpc2NvdmVyIHJlc3VsdHMgYXJlIG1pc3NpbmcnO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdC5yZXN1bHQgPT09ICdlcnJvcicpIHtcbiAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKCdDaGFubmVsOiVzIHJlY2VpdmVkIGRpc2NvdmVyeSBlcnJvcjolcycsIHNlbGYuZ2V0TmFtZSgpLCByZXN1bHQuZXJyb3IuY29udGVudCk7XG4gICAgICAgICAgICAgICAgICAgIGVycm9yX21zZyA9IHJlc3VsdC5lcnJvci5jb250ZW50O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gcHJvY2VzcyByZXN1bHRzJywgbWV0aG9kKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5jb25maWdfcmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjb25maWcgPSBzZWxmLl9wcm9jZXNzRGlzY292ZXJ5Q29uZmlnUmVzdWx0cyhyZXN1bHQuY29uZmlnX3Jlc3VsdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHRzLm1zcHMgPSBjb25maWcubXNwcztcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMub3JkZXJlcnMgPSBjb25maWcub3JkZXJlcnM7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5tZW1iZXJzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVxdWVzdC5sb2NhbCAmJiBpbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMubG9jYWxfcGVlcnMgPSBzZWxmLl9wcm9jZXNzRGlzY292ZXJ5TWVtYmVyc2hpcFJlc3VsdHNPQihyZXN1bHQubWVtYmVycyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMucGVlcnNfYnlfb3JnID0gc2VsZi5fcHJvY2Vzc0Rpc2NvdmVyeU1lbWJlcnNoaXBSZXN1bHRzT0IocmVzdWx0Lm1lbWJlcnMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuY2NfcXVlcnlfcmVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHRzLmVuZG9yc2VtZW50X3BsYW5zID0gc2VsZi5fcHJvY2Vzc0Rpc2NvdmVyeUNoYWluY29kZVJlc3VsdHNPQihyZXN1bHQuY2NfcXVlcnlfcmVzKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gY29tcGxldGVkIHByb2Nlc3NpbmcgcmVzdWx0cycsIG1ldGhvZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoZXJyb3JfbXNnKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgRXJyb3IoJ0NoYW5uZWw6JyArIHNlbGYuZ2V0TmFtZSgpICsgJyBEaXNjb3ZlcnkgZXJyb3I6JyArIGVycm9yX21zZyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Rpc2NvdmVyeSBoYXMgZmFpbGVkIHRvIHJldHVybiByZXN1bHRzJyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBfcHJvY2Vzc0Rpc2NvdmVyeUNoYWluY29kZVJlc3VsdHNPQihxX2NoYWluY29kZXMpIHtcbiAgICAgICAgY29uc3QgbWV0aG9kID0gJ19wcm9jZXNzRGlzY292ZXJ5Q2hhaW5jb2RlUmVzdWx0cyc7XG4gICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBzdGFydCcsIG1ldGhvZCk7XG4gICAgICAgIGNvbnN0IGVuZG9yc2VtZW50X3BsYW5zID0gW107XG4gICAgICAgIGlmIChxX2NoYWluY29kZXMgJiYgcV9jaGFpbmNvZGVzLmNvbnRlbnQpIHtcbiAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHFfY2hhaW5jb2Rlcy5jb250ZW50KSkge1xuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgaW5kZXggaW4gcV9jaGFpbmNvZGVzLmNvbnRlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcV9lbmRvcnNfZGVzYyA9IHFfY2hhaW5jb2Rlcy5jb250ZW50W2luZGV4XTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZW5kb3JzZW1lbnRfcGxhbiA9IHt9O1xuICAgICAgICAgICAgICAgICAgICBlbmRvcnNlbWVudF9wbGFuLmNoYWluY29kZSA9IHFfZW5kb3JzX2Rlc2MuY2hhaW5jb2RlO1xuICAgICAgICAgICAgICAgICAgICBlbmRvcnNlbWVudF9wbGFucy5wdXNoKGVuZG9yc2VtZW50X3BsYW4pO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIEdST1VQU1xuICAgICAgICAgICAgICAgICAgICBlbmRvcnNlbWVudF9wbGFuLmdyb3VwcyA9IHt9O1xuICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IGdyb3VwX25hbWUgaW4gcV9lbmRvcnNfZGVzYy5lbmRvcnNlcnNfYnlfZ3JvdXBzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gZm91bmQgZ3JvdXA6ICVzJywgbWV0aG9kLCBncm91cF9uYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGdyb3VwID0ge307XG4gICAgICAgICAgICAgICAgICAgICAgICBncm91cC5wZWVycyA9IHRoaXMuX3Byb2Nlc3NQZWVyc09CKHFfZW5kb3JzX2Rlc2MuZW5kb3JzZXJzX2J5X2dyb3Vwc1tncm91cF9uYW1lXS5wZWVycyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL2FsbCBkb25lIHdpdGggdGhpcyBncm91cFxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kb3JzZW1lbnRfcGxhbi5ncm91cHNbZ3JvdXBfbmFtZV0gPSBncm91cDtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIC8vIExBWU9VVFNcbiAgICAgICAgICAgICAgICAgICAgZW5kb3JzZW1lbnRfcGxhbi5sYXlvdXRzID0gW107XG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgaW5kZXggaW4gcV9lbmRvcnNfZGVzYy5sYXlvdXRzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBxX2xheW91dCA9IHFfZW5kb3JzX2Rlc2MubGF5b3V0c1tpbmRleF07XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBsYXlvdXQgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgZ3JvdXBfbmFtZSBpbiBxX2xheW91dC5xdWFudGl0aWVzX2J5X2dyb3VwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0W2dyb3VwX25hbWVdID0gcV9sYXlvdXQucXVhbnRpdGllc19ieV9ncm91cFtncm91cF9uYW1lXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBsYXlvdXQgOiVqJywgbWV0aG9kLCBsYXlvdXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZW5kb3JzZW1lbnRfcGxhbi5sYXlvdXRzLnB1c2gobGF5b3V0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBlbmRvcnNlbWVudF9wbGFucztcbiAgICB9XG5cbiAgICBfcHJvY2Vzc0Rpc2NvdmVyeU1lbWJlcnNoaXBSZXN1bHRzT0IocV9tZW1iZXJzKSB7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdfcHJvY2Vzc0Rpc2NvdmVyeUNoYW5uZWxNZW1iZXJzaGlwUmVzdWx0cyc7XG4gICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBzdGFydCcsIG1ldGhvZCk7XG4gICAgICAgIGNvbnN0IHBlZXJzX2J5X29yZyA9IHt9O1xuICAgICAgICBpZiAocV9tZW1iZXJzICYmIHFfbWVtYmVycy5wZWVyc19ieV9vcmcpIHtcbiAgICAgICAgICAgIGZvciAoY29uc3QgbXNwaWQgaW4gcV9tZW1iZXJzLnBlZXJzX2J5X29yZykge1xuICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBmb3VuZCBvcmc6JXMnLCBtZXRob2QsIG1zcGlkKTtcbiAgICAgICAgICAgICAgICBwZWVyc19ieV9vcmdbbXNwaWRdID0ge307XG4gICAgICAgICAgICAgICAgcGVlcnNfYnlfb3JnW21zcGlkXS5wZWVycyA9IHRoaXMuX3Byb2Nlc3NQZWVyc09CKHFfbWVtYmVycy5wZWVyc19ieV9vcmdbbXNwaWRdLnBlZXJzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcGVlcnNfYnlfb3JnO1xuICAgIH1cblxuICAgIF9wcm9jZXNzUGVlcnNPQihxX3BlZXJzKSB7XG4gICAgICAgIGNvbnN0IG1ldGhvZCA9ICdfcHJvY2Vzc1BlZXJzT0InO1xuICAgICAgICBjb25zdCBwZWVycyA9IFtdO1xuICAgICAgICBxX3BlZXJzLmZvckVhY2goKHFfcGVlcikgPT4ge1xuICAgICAgICAgICAgY29uc3QgcGVlciA9IHt9O1xuICAgICAgICAgICAgLy8gSURFTlRJVFlcbiAgICAgICAgICAgIGNvbnN0IHFfaWRlbnRpdHkgPSBfaWRlbnRpdHlQcm90by5TZXJpYWxpemVkSWRlbnRpdHkuZGVjb2RlKHFfcGVlci5pZGVudGl0eSk7XG4gICAgICAgICAgICBwZWVyLm1zcGlkID0gcV9pZGVudGl0eS5tc3BpZDtcblxuICAgICAgICAgICAgLy8gTUVNQkVSU0hJUFxuICAgICAgICAgICAgY29uc3QgcV9tZW1iZXJzaGlwX21lc3NhZ2UgPSBfZ29zc2lwUHJvdG8uR29zc2lwTWVzc2FnZS5kZWNvZGUocV9wZWVyLm1lbWJlcnNoaXBfaW5mby5wYXlsb2FkKTtcbiAgICAgICAgICAgIHBlZXIuZW5kcG9pbnQgPSBxX21lbWJlcnNoaXBfbWVzc2FnZS5hbGl2ZV9tc2cubWVtYmVyc2hpcC5lbmRwb2ludDtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBmb3VuZCBwZWVyIDolcycsIG1ldGhvZCwgcGVlci5lbmRwb2ludCk7XG5cbiAgICAgICAgICAgIC8vIFNUQVRFXG4gICAgICAgICAgICBpZiAocV9wZWVyLnN0YXRlX2luZm8pIHtcbiAgICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlX3MgPSBfZ29zc2lwUHJvdG8uR29zc2lwTWVzc2FnZS5kZWNvZGUocV9wZWVyLnN0YXRlX2luZm8ucGF5bG9hZCk7XG4gICAgICAgICAgICAgICAgaWYgKG1lc3NhZ2VfcyAmJiBtZXNzYWdlX3Muc3RhdGVfaW5mbyAmJiBtZXNzYWdlX3Muc3RhdGVfaW5mby5wcm9wZXJ0aWVzICYmIG1lc3NhZ2Vfcy5zdGF0ZV9pbmZvLnByb3BlcnRpZXMubGVkZ2VyX2hlaWdodCkge1xuICAgICAgICAgICAgICAgICAgICBwZWVyLmxlZGdlcl9oZWlnaHQgPSBMb25nLmZyb21WYWx1ZShtZXNzYWdlX3Muc3RhdGVfaW5mby5wcm9wZXJ0aWVzLmxlZGdlcl9oZWlnaHQpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBkaWQgbm90IGZpbmQgbGVkZ2VyX2hlaWdodCcsIG1ldGhvZCk7XG4gICAgICAgICAgICAgICAgICAgIHBlZXIubGVkZ2VyX2hlaWdodCA9IExvbmcuZnJvbVZhbHVlKDApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gZm91bmQgbGVkZ2VyX2hlaWdodCA6JXMnLCBtZXRob2QsIHBlZXIubGVkZ2VyX2hlaWdodCk7XG5cbiAgICAgICAgICAgICAgICBpZihtZXNzYWdlX3MgJiYgbWVzc2FnZV9zLnN0YXRlX2luZm8gJiYgbWVzc2FnZV9zLnN0YXRlX2luZm8ucHJvcGVydGllcyAmJiBtZXNzYWdlX3Muc3RhdGVfaW5mby5wcm9wZXJ0aWVzLmxvYWRfaW5mbykge1xuICAgICAgICAgICAgICAgICAgICBwZWVyLmxvYWRfaW5mbyA9IG1lc3NhZ2Vfcy5zdGF0ZV9pbmZvLnByb3BlcnRpZXMubG9hZF9pbmZvO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnJXMgLSBkaWQgbm90IGZpbmQgbGVkZ2VyX2hlaWdodCcsIG1ldGhvZCk7XG4gICAgICAgICAgICAgICAgICAgIHBlZXIubG9hZF9pbmZvID0gMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKCclcyAtIGZvdW5kIGxlZGdlcl9oZWlnaHQgOiVzJywgbWV0aG9kLCBwZWVyLmxvYWRfaW5mbyk7XG4gICAgICAgICAgICAgICAgcGVlci5jaGFpbmNvZGVzID0gW107XG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBpbmRleCBpbiBtZXNzYWdlX3Muc3RhdGVfaW5mby5wcm9wZXJ0aWVzLmNoYWluY29kZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcV9jaGFpbmNvZGUgPSBtZXNzYWdlX3Muc3RhdGVfaW5mby5wcm9wZXJ0aWVzLmNoYWluY29kZXNbaW5kZXhdO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBjaGFpbmNvZGUgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgY2hhaW5jb2RlLm5hbWUgPSBxX2NoYWluY29kZS5nZXROYW1lKCk7XG4gICAgICAgICAgICAgICAgICAgIGNoYWluY29kZS52ZXJzaW9uID0gcV9jaGFpbmNvZGUuZ2V0VmVyc2lvbigpO1xuICAgICAgICAgICAgICAgICAgICAvL1RPRE8gbWV0YWRhdGEgP1xuICAgICAgICAgICAgICAgICAgICBsb2dnZXIuZGVidWcoJyVzIC0gZm91bmQgY2hhaW5jb2RlIDolaicsIG1ldGhvZCwgY2hhaW5jb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgcGVlci5jaGFpbmNvZGVzLnB1c2goY2hhaW5jb2RlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vYWxsIGRvbmUgd2l0aCB0aGlzIHBlZXJcbiAgICAgICAgICAgIHBlZXJzLnB1c2gocGVlcik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBwZWVycztcbiAgICB9XG5cbn07XG5tb2R1bGUuZXhwb3J0cyA9IENoYW5uZWxFeHRlbmRzO1xuIl19