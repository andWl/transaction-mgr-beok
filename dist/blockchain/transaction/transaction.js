"use strict";

var _logger = _interopRequireDefault(require("../../common/logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable prefer-destructuring */

/**
 *
 * Copyright 2018 KT All Rights Reserved
 *
 */
const Shared = require('mmap-object');

const path = require('path');

const util = require('util');

const hlf = require('../hlf-client');

class TrxClient {
  async queryChainCode(peer, channelName, chaincodeName, fcn, args, username, orgName) {
    try {
      const client = await hlf.getAdminClientForOrg(orgName);

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      } // send query


      const request = {
        // targets: [peer], // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args
      };
      const responsePayLoads = await channel.queryByChaincode(request);

      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          _logger.default.info(`${args[0]} now has ${responsePayLoads[i].toString('utf8')} after the move`);
        }

        const txInfo = JSON.stringify({
          Tx_msg_query: 'QUERY TRANSACTION',
          Tx_fcn: fcn
        });

        _logger.default.debug(txInfo); // return `${args[0]} now has ${responsePayLoads[0].toString('utf8')}`;


        const responseValue = responsePayLoads[0].toString('utf8');
        return {
          value: responseValue
        };
      }

      _logger.default.error('responsePayLoads is null');

      return 'responsePayLoads is null';
    } catch (error) {
      _logger.default.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);

      return error.toString();
    }
  }

  async queryChainCodeForAnchor(channelName, chaincodeName, fcn, args, username) {
    let peer = null; // location.peer;
    // let orderer = null;// location.orderer;

    let orgName = null; // // location.orgName;

    try {
      const read_only_shared_object = new Shared.Open(path.join(__dirname, '../../app/', 'peer-orderer-default')); // orderer = read_only_shared_object.checked_orderer;

      peer = read_only_shared_object.checked_peer;
      orgName = read_only_shared_object.checked_org;
      read_only_shared_object.close();
    } catch (err) {
      _logger.default.debug(err);
    }

    try {
      const client = await hlf.getAdminClientForOrg(orgName);

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      } // send query


      const request = {
        targets: peer,
        // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args
      };

      if (peer) {
        request.targets = [peer];
      }

      const responsePayLoads = await channel.queryByChaincode(request);

      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          if (responsePayLoads[i][0]) {
            _logger.default.info(`value of ${args[0]} is ${responsePayLoads[i].toString('utf8')} `);

            return `${responsePayLoads[i].toString('utf8')}`;
          }
        }
      }

      _logger.default.error('responsePayLoads is null');

      return 'responsePayLoads is null';
    } catch (error) {
      _logger.default.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);

      return error.toString();
    }
  }

  async queryChainCodeForMessage(peer, channelName, chaincodeName, fcn, args, username, orgName) {
    try {
      const client = await hlf.getAdminClientForOrg(orgName);

      _logger.default.debug(client);

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      } // send query


      const request = {
        // targets: peer, // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args
      };

      if (peer) {
        request.targets = [peer];
      }

      const responsePayLoads = await channel.queryByChaincode(request);

      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          if (responsePayLoads[i][0]) {
            _logger.default.info(`value of ${args[0]} is ${responsePayLoads[i].toString('utf8')} `);

            return `${responsePayLoads[i].toString('utf8')}`;
          }
        }
      }

      _logger.default.error('responsePayLoads is null');

      return 'responsePayLoads is null';
    } catch (error) {
      _logger.default.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);

      return error.toString();
    }
  }

  async invokeChainCode(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    _logger.default.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));

    let errorMsg = null;
    let txIdString = null;
    let blockHeight = null;
    let responsePayload = null;
    let responseStatus = null;
    let responseMessage = null;

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      }

      const txId = client.newTransactionID(); // will need the transaction ID string for the event registration later

      txIdString = txId.getTransactionID(); // send proposal to endorser

      const request = {
        targets: 'peer0.org1.example.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId
      };

      _logger.default.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request); // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer

      const proposalResponses = results[0];
      const proposal = results[1]; // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed

      let allGood = true;

      for (const i in proposalResponses) {
        let oneGood = false;

        if (proposalResponses && proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          oneGood = true;

          _logger.default.info('invoke chaincode proposal was good');
        } else {
          responseStatus = proposalResponses[i].status;
          responseMessage = proposalResponses[i].message;

          _logger.default.error(JSON.stringify(proposalResponses));

          _logger.default.error('invoke chaincode proposal was bad');
        }

        allGood &= oneGood;
      }

      if (allGood) {
        responseStatus = proposalResponses[0].response.status;
        responsePayload = proposalResponses[0].response.payload;

        _logger.default.info(util.format('Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s', proposalResponses[0].response.status, proposalResponses[0].response.message, proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature)); // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization


        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          _logger.default.debug('invokeEventPromise - setting up event');

          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;

              _logger.default.error(message);

              eh.disconnect();
            }, 3000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              _logger.default.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());

              _logger.default.info('Transaction %s has status of %s in block %s', tx, code, blockNum);

              clearTimeout(ehTimeOut);
              blockHeight = blockNum;

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);

                _logger.default.error(message);

                reject(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';

                _logger.default.info(message);

                resolve(message);
              }
            }, err => {
              clearTimeout(ehTimeOut);

              _logger.default.error(err);

              reject(err);
            }, // the default for 'unregister' is true for transaction listeners
            // so no real need to set here, however for 'disconnect'
            // the default is false as most event hubs are long running
            // in this use case we are using it only once
            {
              unregister: true,
              disconnect: true
            });
            eh.connect();
          });
          promises.push(invokeEventPromise);
        });
        const ordererReq = {
          txId,
          proposalResponses,
          proposal
        };
        const sendPromise = channel.sendTransaction(ordererReq); // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing

        promises.push(sendPromise);
        const results = await Promise.all(promises);

        _logger.default.debug(util.format('------->>> R E S P O N S E : %j', results));

        const response = results.pop(); //  orderer results are last in the results

        if (response.status === 'SUCCESS') {
          _logger.default.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);

          _logger.default.debug(errorMsg);
        } // now see what each of the event hubs reported


        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];

          _logger.default.debug('Event results for event hub :%s', eh.getPeerAddr());

          if (typeof ehResult === 'string') {
            _logger.default.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();

            _logger.default.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');

        _logger.default.debug(errorMsg);
      }
    } catch (error) {
      _logger.default.error(`
              }Failed to invoke due to error: ${error.stack}` ? error.stack : error);

      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format('Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s', orgName, channelName, txIdString);

      _logger.default.info(message);

      const txInfo = JSON.stringify({
        Tx_msg_invoke: 'INVOKE TRANSACTION',
        Tx_ID: txIdString,
        Tx_blockNum: blockHeight,
        Tx_channel: channelName,
        Tx_org: orgName,
        Tx_fcn: fcn
      });

      _logger.default.debug(txInfo);

      _logger.default.debug('responsePayload is :%s', responsePayload);

      const payloadString = responsePayload.toString();
      return {
        status: responseStatus,
        txId: txIdString,
        message: payloadString
      };
    }

    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);

    _logger.default.error(message); // throw new Error(message);


    return {
      status: responseStatus,
      message: responseMessage
    };
  }

  async invokeChainCodeForAnchor(channelName, chaincodeName, fcn, value, username) {
    _logger.default.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));

    let errorMsg = null;
    let txIdString = null;
    const args = [];
    let peer = null; // location.peer;

    let orderer = null; // location.orderer;

    let orgName = null; // // location.orgName;

    try {
      const read_only_shared_object = new Shared.Open(path.join(__dirname, '../../app/', 'peer-orderer-default'));
      orderer = read_only_shared_object.checked_orderer;
      peer = read_only_shared_object.checked_peer;
      orgName = read_only_shared_object.checked_org;
      read_only_shared_object.close();
    } catch (err) {
      _logger.default.debug(err);
    } //orderer='orderer1.gr.kt.com'
    //peer='peer0.gr.kt.com'
    //orgName='Org1'


    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(orgName);

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      }

      const txId = client.newTransactionID(); // will need the transaction ID string for the event registration later

      txIdString = txId.getTransactionID(); // input txId to key

      args.push(txIdString);
      args.push(value);

      _logger.default.debug(`peer_name!!!: ${peer}`);

      _logger.default.debug(`orderer_name!!!: ${orderer}`);

      _logger.default.debug(`orgName!!!: ${orgName}`); // send proposal to endorser


      const request = {
        targets: peer,
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId
      };

      _logger.default.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request); // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer

      const proposalResponses = results[0];
      const proposal = results[1]; // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed

      let allGood = true;

      for (const i in proposalResponses) {
        let oneGood = false;

        if (proposalResponses && proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          oneGood = true;

          _logger.default.info('invoke chaincode proposal was good');
        } else {
          _logger.default.error(JSON.stringify(proposalResponses));

          _logger.default.error('invoke chaincode proposal was bad');
        }

        allGood &= oneGood;
      }

      if (allGood) {
        _logger.default.info(util.format('Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s', proposalResponses[0].response.status, proposalResponses[0].response.message, proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature)); // eventhub logic (promise 비동기)
        // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization


        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg(); // var check_promise = False ;

        eventHubs.forEach(eh => {
          _logger.default.debug('invokeEventPromise - setting up event'); // logger.debug(`${eh.getPeerAddr()}`)


          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;

              _logger.default.error(message);

              eh.disconnect();
            }, 3000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              _logger.default.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());

              _logger.default.info('Transaction %s has status of %s in block %s', tx, code, blockNum);

              _logger.default.info("txid return!!!!!");

              clearTimeout(ehTimeOut); // return txIdString;

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);

                _logger.default.error(message);

                resolve(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';

                _logger.default.info(message);

                resolve(message); // eh.disconnect()
              }
            }, err => {
              clearTimeout(ehTimeOut);

              _logger.default.debug("여긴가!!!!!!!!!!!");

              _logger.default.error(err); // return False;


              resolve(err);
            }, // the default for 'unregister' is true for transaction listeners
            // so no real need to set here, however for 'disconnect'
            // the default is false as most event hubs are long running
            // in this use case we are using it only once
            {
              unregister: true,
              disconnect: true
            });
            eh.connect();
          });

          _logger.default.info("promises.push(invokeEventPromise) !!!! ");

          promises.push(invokeEventPromise);
        }); // ~eventhub logic

        const ordererReq = {
          orderer,
          txId,
          proposalResponses,
          proposal
        }; //
        // const results22 = await Promise.all(promises);
        // logger.debug("results22: !!!!" )
        // logger.debug(results22)

        const sendPromise = channel.sendTransaction(ordererReq); // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing

        promises.push(sendPromise);
        const results = await Promise.all(promises);

        _logger.default.debug("results: !!!!");

        _logger.default.debug(results);

        _logger.default.debug(util.format('------->>> R E S P O N S E : %j', results));

        const response = results.pop(); //  orderer results are last in the results

        if (response.status === 'SUCCESS') {
          _logger.default.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);

          _logger.default.debug(errorMsg);
        } // now see what each of the event hubs reported (단순 로그 및 errorMsg 대입)


        for (const i in results) {
          _logger.default.debug("pop 이후 results!!");

          _logger.default.debug(results);

          const ehResult = results[i];
          const eh = eventHubs[i];

          _logger.default.debug('Event results for event hub :%s', eh.getPeerAddr());

          if (typeof ehResult === 'string') {
            _logger.default.debug(ehResult);

            const message = util.format('Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s', orgName, channelName, txIdString);

            _logger.default.info(message);

            return txIdString;
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();

            _logger.default.debug("여긴머여???");

            _logger.default.debug(ehResult.toString());
          }
        } // beok
        // const message = util.format(
        //   'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        //   orgName, channelName, txIdString);
        // logger.info(message);
        //
        // return txIdString;
        // ~beok

      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');

        _logger.default.debug(errorMsg);
      }
    } catch (error) {
      _logger.default.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);

      errorMsg = error.toString();
    }

    _logger.default.debug("errorMsg!!!!");

    _logger.default.debug(errorMsg); // logger.debug("allGood!!!!")
    // logger.debug(allGood)
    // if (!errorMsg) {
    //   const message = util.format(
    //   'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
    //   orgName, channelName, txIdString);
    //   logger.info(message);
    //
    //   return txIdString;
    // }


    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);

    _logger.default.error(message);

    throw new Error(message);
  }

  async invokeChainCodeForMessage(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    _logger.default.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));

    let errorMsg = null;
    let txIdString = null;

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(orgName);

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      }

      const txId = client.newTransactionID(); // will need the transaction ID string for the event registration later

      txIdString = txId.getTransactionID(); // const response = await channel.initialize({
      //   discover: true,
      //   asLocalhost: true,
      // });
      // logger.debug('Service discovery init : ');
      // logger.debug(response);
      // send proposal to endorser

      const request = {
        targets: 'peer0.gr.kt.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId
      };

      if (peerNames) {
        request.targets = peerNames;
      }

      _logger.default.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request); // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer

      const proposalResponses = results[0];
      const proposal = results[1]; // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed

      let allGood = true;

      for (const i in proposalResponses) {
        let oneGood = false;

        if (proposalResponses && proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          oneGood = true;

          _logger.default.info('invoke chaincode proposal was good');
        } else {
          _logger.default.error(JSON.stringify(proposalResponses));

          _logger.default.error('invoke chaincode proposal was bad');
        }

        allGood &= oneGood;
      }

      if (allGood) {
        _logger.default.info(util.format('Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s', proposalResponses[0].response.status, proposalResponses[0].response.message, proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature)); // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization


        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          _logger.default.debug('invokeEventPromise - setting up event');

          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;

              _logger.default.error(message);

              eh.disconnect();
            }, 30000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              _logger.default.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());

              _logger.default.info('Transaction %s has status of %s in block %s', tx, code, blockNum);

              clearTimeout(ehTimeOut);

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);

                _logger.default.error(message);

                reject(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';

                _logger.default.info(message);

                resolve(message);
              }
            }, err => {
              clearTimeout(ehTimeOut);

              _logger.default.error(err);

              reject(err);
            }, // the default for 'unregister' is true for transaction listeners
            // so no real need to set here, however for 'disconnect'
            // the default is false as most event hubs are long running
            // in this use case we are using it only once
            {
              unregister: true,
              disconnect: true
            });
            eh.connect();
          });
          promises.push(invokeEventPromise);
        });
        const ordererReq = {
          txId,
          proposalResponses,
          proposal // orderer: 'orderer5.example.com',

        };
        const sendPromise = channel.sendTransaction(ordererReq); // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing

        promises.push(sendPromise);
        const results = await Promise.all(promises);

        _logger.default.debug(util.format('------->>> R E S P O N S E : %j', results));

        const response = results.pop(); //  orderer results are last in the results

        if (response.status === 'SUCCESS') {
          _logger.default.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);

          _logger.default.debug(errorMsg);
        } // now see what each of the event hubs reported


        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];

          _logger.default.debug('Event results for event hub :%s', eh.getPeerAddr());

          if (typeof ehResult === 'string') {
            _logger.default.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();

            _logger.default.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');

        _logger.default.debug(errorMsg);
      }
    } catch (error) {
      _logger.default.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);

      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format('Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s', orgName, channelName, txIdString);

      _logger.default.info(message); // return txIdString;

    }

    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);

    _logger.default.error(message);

    throw new Error(message);
  }

  async invokeChainCodeDiscover(peerNames, channelName, chaincodeName, fcn, args, username, orgName, invokeOption) {
    _logger.default.debug(util.format('\n============ invokeChainCodeDiscover : invoke transaction on channel %s ============\n', channelName));

    let errorMsg = null;
    let txIdString = null;

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getOBChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      }

      const txId = client.newTransactionID(); // will need the transaction ID string for the event registration later

      txIdString = txId.getTransactionID(); // send proposal to endorser

      const request = {
        // targets: 'peer0.org2.example.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId
      };

      if (invokeOption === 'load_info') {
        // endorsing role peers
        const peersObj = channel._getTargets('', 'endorsingPeer');

        this._endorsingPeers = [];

        if (peersObj) {
          _logger.default.debug('Successfully got peer list');

          for (const i in peersObj) {
            this._endorsingPeers.push(peersObj[i]._name);
          }
        } // initialize service discovery to get load_info


        const response = await channel.initializeOB({
          discover: true,
          asLocalhost: true
        });

        if (response.peers_by_org) {
          _logger.default.debug('Successfully got discovery info');

          const orgMSPName = Object.keys(response.peers_by_org);
          const targetPeer = {
            name: null,
            load_info: 100
          };
          const discoveryPeer = {};

          for (const i in orgMSPName) {
            _logger.default.debug(orgMSPName[i]);

            for (const j in response.peers_by_org[orgMSPName[i]].peers) {
              discoveryPeer.name = response.peers_by_org[orgMSPName[i]].peers[j].endpoint.split(':')[0];
              discoveryPeer.load_info = response.peers_by_org[orgMSPName[i]].peers[j].load_info;

              _logger.default.debug('========= discovery info');

              _logger.default.debug('name : %s, load_info : %s', discoveryPeer.name, discoveryPeer.load_info);

              this._endorsingPeers.forEach(peerName => {
                if (peerName === discoveryPeer.name && targetPeer.load_info > discoveryPeer.load_info) {
                  targetPeer.name = discoveryPeer.name;
                  targetPeer.load_info = discoveryPeer.load_info;
                }
              });
            }
          }

          _logger.default.debug('target peer:%s, load_info:%s', targetPeer.name, targetPeer.load_info);

          request.targets = targetPeer.name;
        } else {
          const message = util.format('Failed to get load_info');

          _logger.default.error(message);
        }
      } else if (invokeOption === 'discover') {
        await channel.initializeOB({
          discover: true,
          asLocalhost: true
        });
      } // else if (invokeOption === 'targeted') {
      //   request.targets = 'peer0.org1.example.com';
      // } else if (invokeOption == '' || 'non_target')


      _logger.default.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request); // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer

      const proposalResponses = results[0];
      const proposal = results[1]; // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed

      let allGood = true;

      for (const i in proposalResponses) {
        let oneGood = false;

        if (proposalResponses && proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          oneGood = true;

          _logger.default.info('invoke chaincode proposal was good');
        } else {
          _logger.default.error(JSON.stringify(proposalResponses));

          _logger.default.error('invoke chaincode proposal was bad');
        }

        allGood &= oneGood;
      }

      if (allGood) {
        _logger.default.info(util.format('Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s', proposalResponses[0].response.status, proposalResponses[0].response.message, proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature)); // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization


        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          _logger.default.debug('invokeEventPromise - setting up event');

          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;

              _logger.default.error(message);

              eh.disconnect();
            }, 30000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              _logger.default.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());

              _logger.default.info('Transaction %s has status of %s in blocl %s', tx, code, blockNum);

              clearTimeout(ehTimeOut);

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);

                _logger.default.error(message);

                reject(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';

                _logger.default.info(message);

                resolve(message);
              }
            }, err => {
              clearTimeout(ehTimeOut);

              _logger.default.error(err);

              reject(err);
            }, // the default for 'unregister' is true for transaction listeners
            // so no real need to set here, however for 'disconnect'
            // the default is false as most event hubs are long running
            // in this use case we are using it only once
            {
              unregister: true,
              disconnect: true
            });
            eh.connect();
          });
          promises.push(invokeEventPromise);
        });
        const ordererReq = {
          txId,
          proposalResponses,
          proposal
        };
        const sendPromise = channel.sendTransaction(ordererReq); // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing

        promises.push(sendPromise);
        const results = await Promise.all(promises);

        _logger.default.debug(util.format('------->>> R E S P O N S E : %j', results));

        const response = results.pop(); //  orderer results are last in the results

        if (response.status === 'SUCCESS') {
          _logger.default.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);

          _logger.default.debug(errorMsg);
        } // now see what each of the event hubs reported


        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];

          _logger.default.debug('Event results for event hub :%s', eh.getPeerAddr());

          if (typeof ehResult === 'string') {
            _logger.default.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();

            _logger.default.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');

        _logger.default.debug(errorMsg);
      }
    } catch (error) {
      _logger.default.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);

      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format('Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s', orgName, channelName, txIdString);

      _logger.default.info(message);

      return txIdString;
    }

    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);

    _logger.default.error(message);

    throw new Error(message);
  }

  async invokeChainCodeOB(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    _logger.default.debug(util.format('\n============ invoke transaction on channel %s ============\n', channelName));

    let errorMsg = null;
    let txIdString = null;

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');

      _logger.default.debug('Successfully got the fabric client for the organization "%s"', orgName);

      const channel = client.getOBChannel(channelName);

      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);

        _logger.default.error(message);

        throw new Error(message);
      }

      const txId = client.newTransactionID(); // will need the transaction ID string for the event registration later

      txIdString = txId.getTransactionID(); // send proposal to endorser

      const request = {
        targets: peerNames,
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId
      };

      _logger.default.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposalOB(request); // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer

      const proposalResponses = results[0];
      const proposal = results[1]; // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed

      let allGood = true;

      for (const i in proposalResponses) {
        let oneGood = false;

        if (proposalResponses && proposalResponses[i].response && proposalResponses[i].response.status === 200) {
          oneGood = true;

          _logger.default.info('invoke chaincode proposal was good');
        } else {
          _logger.default.error(JSON.stringify(proposalResponses));

          _logger.default.error('invoke chaincode proposal was bad');
        }

        allGood &= oneGood;
      }

      if (allGood) {
        _logger.default.info(util.format('Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s', proposalResponses[0].response.status, proposalResponses[0].response.message, proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        const ordererReq = {
          txId,
          proposalResponses,
          proposal
        };
        channel.sendTransactionOB(ordererReq);
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');

        _logger.default.debug(errorMsg);
      }
    } catch (error) {
      _logger.default.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);

      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format('Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s', orgName, channelName, txIdString);

      _logger.default.info(message);

      return txIdString;
    }

    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);

    _logger.default.error(message);

    throw new Error(message);
  }

}

const trxClient = new TrxClient();
module.exports = trxClient;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NlcnZlci9ibG9ja2NoYWluL3RyYW5zYWN0aW9uL3RyYW5zYWN0aW9uLmpzIl0sIm5hbWVzIjpbIlNoYXJlZCIsInJlcXVpcmUiLCJwYXRoIiwidXRpbCIsImhsZiIsIlRyeENsaWVudCIsInF1ZXJ5Q2hhaW5Db2RlIiwicGVlciIsImNoYW5uZWxOYW1lIiwiY2hhaW5jb2RlTmFtZSIsImZjbiIsImFyZ3MiLCJ1c2VybmFtZSIsIm9yZ05hbWUiLCJjbGllbnQiLCJnZXRBZG1pbkNsaWVudEZvck9yZyIsImxvZ2dlciIsImRlYnVnIiwiY2hhbm5lbCIsImdldENoYW5uZWwiLCJtZXNzYWdlIiwiZm9ybWF0IiwiZXJyb3IiLCJFcnJvciIsInJlcXVlc3QiLCJjaGFpbmNvZGVJZCIsInJlc3BvbnNlUGF5TG9hZHMiLCJxdWVyeUJ5Q2hhaW5jb2RlIiwiaSIsImxlbmd0aCIsImluZm8iLCJ0b1N0cmluZyIsInR4SW5mbyIsIkpTT04iLCJzdHJpbmdpZnkiLCJUeF9tc2dfcXVlcnkiLCJUeF9mY24iLCJyZXNwb25zZVZhbHVlIiwidmFsdWUiLCJzdGFjayIsInF1ZXJ5Q2hhaW5Db2RlRm9yQW5jaG9yIiwicmVhZF9vbmx5X3NoYXJlZF9vYmplY3QiLCJPcGVuIiwiam9pbiIsIl9fZGlybmFtZSIsImNoZWNrZWRfcGVlciIsImNoZWNrZWRfb3JnIiwiY2xvc2UiLCJlcnIiLCJ0YXJnZXRzIiwicXVlcnlDaGFpbkNvZGVGb3JNZXNzYWdlIiwiaW52b2tlQ2hhaW5Db2RlIiwicGVlck5hbWVzIiwiZXJyb3JNc2ciLCJ0eElkU3RyaW5nIiwiYmxvY2tIZWlnaHQiLCJyZXNwb25zZVBheWxvYWQiLCJyZXNwb25zZVN0YXR1cyIsInJlc3BvbnNlTWVzc2FnZSIsInR4SWQiLCJuZXdUcmFuc2FjdGlvbklEIiwiZ2V0VHJhbnNhY3Rpb25JRCIsImNoYWluSWQiLCJyZXN1bHRzIiwic2VuZFRyYW5zYWN0aW9uUHJvcG9zYWwiLCJwcm9wb3NhbFJlc3BvbnNlcyIsInByb3Bvc2FsIiwiYWxsR29vZCIsIm9uZUdvb2QiLCJyZXNwb25zZSIsInN0YXR1cyIsInBheWxvYWQiLCJlbmRvcnNlbWVudCIsInNpZ25hdHVyZSIsInByb21pc2VzIiwiZXZlbnRIdWJzIiwiZ2V0Q2hhbm5lbEV2ZW50SHVic0Zvck9yZyIsImZvckVhY2giLCJlaCIsImludm9rZUV2ZW50UHJvbWlzZSIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiZWhUaW1lT3V0Iiwic2V0VGltZW91dCIsImdldFBlZXJBZGRyIiwiZGlzY29ubmVjdCIsInJlZ2lzdGVyVHhFdmVudCIsInR4IiwiY29kZSIsImJsb2NrTnVtIiwiY2xlYXJUaW1lb3V0IiwidW5yZWdpc3RlciIsImNvbm5lY3QiLCJwdXNoIiwib3JkZXJlclJlcSIsInNlbmRQcm9taXNlIiwic2VuZFRyYW5zYWN0aW9uIiwiYWxsIiwicG9wIiwiZWhSZXN1bHQiLCJUeF9tc2dfaW52b2tlIiwiVHhfSUQiLCJUeF9ibG9ja051bSIsIlR4X2NoYW5uZWwiLCJUeF9vcmciLCJwYXlsb2FkU3RyaW5nIiwiaW52b2tlQ2hhaW5Db2RlRm9yQW5jaG9yIiwib3JkZXJlciIsImNoZWNrZWRfb3JkZXJlciIsImludm9rZUNoYWluQ29kZUZvck1lc3NhZ2UiLCJpbnZva2VDaGFpbkNvZGVEaXNjb3ZlciIsImludm9rZU9wdGlvbiIsImdldE9CQ2hhbm5lbCIsInBlZXJzT2JqIiwiX2dldFRhcmdldHMiLCJfZW5kb3JzaW5nUGVlcnMiLCJfbmFtZSIsImluaXRpYWxpemVPQiIsImRpc2NvdmVyIiwiYXNMb2NhbGhvc3QiLCJwZWVyc19ieV9vcmciLCJvcmdNU1BOYW1lIiwiT2JqZWN0Iiwia2V5cyIsInRhcmdldFBlZXIiLCJuYW1lIiwibG9hZF9pbmZvIiwiZGlzY292ZXJ5UGVlciIsImoiLCJwZWVycyIsImVuZHBvaW50Iiwic3BsaXQiLCJwZWVyTmFtZSIsImludm9rZUNoYWluQ29kZU9CIiwic2VuZFRyYW5zYWN0aW9uUHJvcG9zYWxPQiIsInNlbmRUcmFuc2FjdGlvbk9CIiwidHJ4Q2xpZW50IiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7QUFPQTs7OztBQVBBOztBQUVBOzs7OztBQU9BLE1BQU1BLE1BQU0sR0FBR0MsT0FBTyxDQUFDLGFBQUQsQ0FBdEI7O0FBQ0EsTUFBTUMsSUFBSSxHQUFHRCxPQUFPLENBQUMsTUFBRCxDQUFwQjs7QUFFQSxNQUFNRSxJQUFJLEdBQUdGLE9BQU8sQ0FBQyxNQUFELENBQXBCOztBQUNBLE1BQU1HLEdBQUcsR0FBR0gsT0FBTyxDQUFDLGVBQUQsQ0FBbkI7O0FBRUEsTUFBTUksU0FBTixDQUFnQjtBQUNkLFFBQU1DLGNBQU4sQ0FBcUJDLElBQXJCLEVBQTJCQyxXQUEzQixFQUF3Q0MsYUFBeEMsRUFBdURDLEdBQXZELEVBQTREQyxJQUE1RCxFQUFrRUMsUUFBbEUsRUFBNEVDLE9BQTVFLEVBQXFGO0FBQ25GLFFBQUk7QUFDRixZQUFNQyxNQUFNLEdBQUcsTUFBTVYsR0FBRyxDQUFDVyxvQkFBSixDQUF5QkYsT0FBekIsQ0FBckI7O0FBQ0FHLHNCQUFPQyxLQUFQLENBQWEsOERBQWIsRUFBNkVKLE9BQTdFOztBQUNBLFlBQU1LLE9BQU8sR0FBR0osTUFBTSxDQUFDSyxVQUFQLENBQWtCWCxXQUFsQixDQUFoQjs7QUFDQSxVQUFJLENBQUNVLE9BQUwsRUFBYztBQUNaLGNBQU1FLE9BQU8sR0FBR2pCLElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSxzREFBWixFQUFvRWIsV0FBcEUsQ0FBaEI7O0FBQ0FRLHdCQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EsY0FBTSxJQUFJRyxLQUFKLENBQVVILE9BQVYsQ0FBTjtBQUNELE9BUkMsQ0FVRjs7O0FBQ0EsWUFBTUksT0FBTyxHQUFHO0FBQ2Q7QUFDQUMsUUFBQUEsV0FBVyxFQUFFaEIsYUFGQztBQUdkQyxRQUFBQSxHQUhjO0FBSWRDLFFBQUFBO0FBSmMsT0FBaEI7QUFNQSxZQUFNZSxnQkFBZ0IsR0FBRyxNQUFNUixPQUFPLENBQUNTLGdCQUFSLENBQXlCSCxPQUF6QixDQUEvQjs7QUFDQSxVQUFJRSxnQkFBSixFQUFzQjtBQUNwQixhQUFLLElBQUlFLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdGLGdCQUFnQixDQUFDRyxNQUFyQyxFQUE2Q0QsQ0FBQyxFQUE5QyxFQUFrRDtBQUNoRFosMEJBQU9jLElBQVAsQ0FBYSxHQUFFbkIsSUFBSSxDQUFDLENBQUQsQ0FBSSxZQUFXZSxnQkFBZ0IsQ0FBQ0UsQ0FBRCxDQUFoQixDQUFvQkcsUUFBcEIsQ0FBNkIsTUFBN0IsQ0FBcUMsaUJBQXZFO0FBQ0Q7O0FBQ0QsY0FBTUMsTUFBTSxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUM1QkMsVUFBQUEsWUFBWSxFQUFFLG1CQURjO0FBRTVCQyxVQUFBQSxNQUFNLEVBQUUxQjtBQUZvQixTQUFmLENBQWY7O0FBSUFNLHdCQUFPQyxLQUFQLENBQWFlLE1BQWIsRUFSb0IsQ0FTcEI7OztBQUNBLGNBQU1LLGFBQWEsR0FBR1gsZ0JBQWdCLENBQUMsQ0FBRCxDQUFoQixDQUFvQkssUUFBcEIsQ0FBNkIsTUFBN0IsQ0FBdEI7QUFDQSxlQUFPO0FBQ0xPLFVBQUFBLEtBQUssRUFBRUQ7QUFERixTQUFQO0FBR0Q7O0FBQ0RyQixzQkFBT00sS0FBUCxDQUFhLDBCQUFiOztBQUNBLGFBQU8sMEJBQVA7QUFDRCxLQW5DRCxDQW1DRSxPQUFPQSxLQUFQLEVBQWM7QUFDZE4sc0JBQU9NLEtBQVAsQ0FBYyxpQ0FBZ0NBLEtBQUssQ0FBQ2lCLEtBQU0sRUFBN0MsR0FBaURqQixLQUFLLENBQUNpQixLQUF2RCxHQUErRGpCLEtBQTVFOztBQUNBLGFBQU9BLEtBQUssQ0FBQ1MsUUFBTixFQUFQO0FBQ0Q7QUFDRjs7QUFFRCxRQUFNUyx1QkFBTixDQUE4QmhDLFdBQTlCLEVBQTJDQyxhQUEzQyxFQUEwREMsR0FBMUQsRUFBK0RDLElBQS9ELEVBQXFFQyxRQUFyRSxFQUErRTtBQUM3RSxRQUFJTCxJQUFJLEdBQUcsSUFBWCxDQUQ2RSxDQUM3RDtBQUNoQjs7QUFDQSxRQUFJTSxPQUFPLEdBQUcsSUFBZCxDQUg2RSxDQUd6RDs7QUFFcEIsUUFBSTtBQUNGLFlBQU00Qix1QkFBdUIsR0FBRyxJQUFJekMsTUFBTSxDQUFDMEMsSUFBWCxDQUFnQnhDLElBQUksQ0FBQ3lDLElBQUwsQ0FBVUMsU0FBVixFQUFxQixZQUFyQixFQUFtQyxzQkFBbkMsQ0FBaEIsQ0FBaEMsQ0FERSxDQUVGOztBQUNBckMsTUFBQUEsSUFBSSxHQUFHa0MsdUJBQXVCLENBQUNJLFlBQS9CO0FBQ0FoQyxNQUFBQSxPQUFPLEdBQUc0Qix1QkFBdUIsQ0FBQ0ssV0FBbEM7QUFDQUwsTUFBQUEsdUJBQXVCLENBQUNNLEtBQXhCO0FBQ0QsS0FORCxDQU1FLE9BQU9DLEdBQVAsRUFBWTtBQUNaaEMsc0JBQU9DLEtBQVAsQ0FBYStCLEdBQWI7QUFDRDs7QUFJRCxRQUFJO0FBQ0YsWUFBTWxDLE1BQU0sR0FBRyxNQUFNVixHQUFHLENBQUNXLG9CQUFKLENBQXlCRixPQUF6QixDQUFyQjs7QUFDQUcsc0JBQU9DLEtBQVAsQ0FBYSw4REFBYixFQUE2RUosT0FBN0U7O0FBQ0EsWUFBTUssT0FBTyxHQUFHSixNQUFNLENBQUNLLFVBQVAsQ0FBa0JYLFdBQWxCLENBQWhCOztBQUNBLFVBQUksQ0FBQ1UsT0FBTCxFQUFjO0FBQ1osY0FBTUUsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNEQUFaLEVBQW9FYixXQUFwRSxDQUFoQjs7QUFDQVEsd0JBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQSxjQUFNLElBQUlHLEtBQUosQ0FBVUgsT0FBVixDQUFOO0FBQ0QsT0FSQyxDQVVGOzs7QUFDQSxZQUFNSSxPQUFPLEdBQUc7QUFDZHlCLFFBQUFBLE9BQU8sRUFBRTFDLElBREs7QUFDQztBQUNma0IsUUFBQUEsV0FBVyxFQUFFaEIsYUFGQztBQUdkQyxRQUFBQSxHQUhjO0FBSWRDLFFBQUFBO0FBSmMsT0FBaEI7O0FBTUEsVUFBSUosSUFBSixFQUFVO0FBQ1JpQixRQUFBQSxPQUFPLENBQUN5QixPQUFSLEdBQWtCLENBQUMxQyxJQUFELENBQWxCO0FBQ0Q7O0FBRUQsWUFBTW1CLGdCQUFnQixHQUFHLE1BQU1SLE9BQU8sQ0FBQ1MsZ0JBQVIsQ0FBeUJILE9BQXpCLENBQS9COztBQUNBLFVBQUlFLGdCQUFKLEVBQXNCO0FBQ3BCLGFBQUssSUFBSUUsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsZ0JBQWdCLENBQUNHLE1BQXJDLEVBQTZDRCxDQUFDLEVBQTlDLEVBQWtEO0FBQ2hELGNBQUlGLGdCQUFnQixDQUFDRSxDQUFELENBQWhCLENBQW9CLENBQXBCLENBQUosRUFBNEI7QUFDMUJaLDRCQUFPYyxJQUFQLENBQWEsWUFBV25CLElBQUksQ0FBQyxDQUFELENBQUksT0FBTWUsZ0JBQWdCLENBQUNFLENBQUQsQ0FBaEIsQ0FBb0JHLFFBQXBCLENBQTZCLE1BQTdCLENBQXFDLEdBQTNFOztBQUNBLG1CQUFRLEdBQUVMLGdCQUFnQixDQUFDRSxDQUFELENBQWhCLENBQW9CRyxRQUFwQixDQUE2QixNQUE3QixDQUFxQyxFQUEvQztBQUNEO0FBQ0Y7QUFDRjs7QUFDRGYsc0JBQU9NLEtBQVAsQ0FBYSwwQkFBYjs7QUFDQSxhQUFPLDBCQUFQO0FBQ0QsS0FoQ0QsQ0FnQ0UsT0FBT0EsS0FBUCxFQUFjO0FBQ2ROLHNCQUFPTSxLQUFQLENBQWMsaUNBQWdDQSxLQUFLLENBQUNpQixLQUFNLEVBQTdDLEdBQWlEakIsS0FBSyxDQUFDaUIsS0FBdkQsR0FBK0RqQixLQUE1RTs7QUFDQSxhQUFPQSxLQUFLLENBQUNTLFFBQU4sRUFBUDtBQUNEO0FBQ0Y7O0FBR0QsUUFBTW1CLHdCQUFOLENBQStCM0MsSUFBL0IsRUFBb0NDLFdBQXBDLEVBQWlEQyxhQUFqRCxFQUFnRUMsR0FBaEUsRUFBcUVDLElBQXJFLEVBQTJFQyxRQUEzRSxFQUFvRkMsT0FBcEYsRUFBNkY7QUFFM0YsUUFBSTtBQUNGLFlBQU1DLE1BQU0sR0FBRyxNQUFNVixHQUFHLENBQUNXLG9CQUFKLENBQXlCRixPQUF6QixDQUFyQjs7QUFDQUcsc0JBQU9DLEtBQVAsQ0FBYUgsTUFBYjs7QUFDQUUsc0JBQU9DLEtBQVAsQ0FBYSw4REFBYixFQUE2RUosT0FBN0U7O0FBQ0EsWUFBTUssT0FBTyxHQUFHSixNQUFNLENBQUNLLFVBQVAsQ0FBa0JYLFdBQWxCLENBQWhCOztBQUNBLFVBQUksQ0FBQ1UsT0FBTCxFQUFjO0FBQ1osY0FBTUUsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNEQUFaLEVBQW9FYixXQUFwRSxDQUFoQjs7QUFDQVEsd0JBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQSxjQUFNLElBQUlHLEtBQUosQ0FBVUgsT0FBVixDQUFOO0FBQ0QsT0FUQyxDQVdGOzs7QUFDQSxZQUFNSSxPQUFPLEdBQUc7QUFDZDtBQUNBQyxRQUFBQSxXQUFXLEVBQUVoQixhQUZDO0FBR2RDLFFBQUFBLEdBSGM7QUFJZEMsUUFBQUE7QUFKYyxPQUFoQjs7QUFNQSxVQUFJSixJQUFKLEVBQVU7QUFDUmlCLFFBQUFBLE9BQU8sQ0FBQ3lCLE9BQVIsR0FBa0IsQ0FBQzFDLElBQUQsQ0FBbEI7QUFDRDs7QUFFRCxZQUFNbUIsZ0JBQWdCLEdBQUcsTUFBTVIsT0FBTyxDQUFDUyxnQkFBUixDQUF5QkgsT0FBekIsQ0FBL0I7O0FBQ0EsVUFBSUUsZ0JBQUosRUFBc0I7QUFDcEIsYUFBSyxJQUFJRSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixnQkFBZ0IsQ0FBQ0csTUFBckMsRUFBNkNELENBQUMsRUFBOUMsRUFBa0Q7QUFDaEQsY0FBSUYsZ0JBQWdCLENBQUNFLENBQUQsQ0FBaEIsQ0FBb0IsQ0FBcEIsQ0FBSixFQUE0QjtBQUMxQlosNEJBQU9jLElBQVAsQ0FBYSxZQUFXbkIsSUFBSSxDQUFDLENBQUQsQ0FBSSxPQUFNZSxnQkFBZ0IsQ0FBQ0UsQ0FBRCxDQUFoQixDQUFvQkcsUUFBcEIsQ0FBNkIsTUFBN0IsQ0FBcUMsR0FBM0U7O0FBQ0EsbUJBQVEsR0FBRUwsZ0JBQWdCLENBQUNFLENBQUQsQ0FBaEIsQ0FBb0JHLFFBQXBCLENBQTZCLE1BQTdCLENBQXFDLEVBQS9DO0FBQ0Q7QUFDRjtBQUNGOztBQUNEZixzQkFBT00sS0FBUCxDQUFhLDBCQUFiOztBQUNBLGFBQU8sMEJBQVA7QUFDRCxLQWpDRCxDQWlDRSxPQUFPQSxLQUFQLEVBQWM7QUFDZE4sc0JBQU9NLEtBQVAsQ0FBYyxpQ0FBZ0NBLEtBQUssQ0FBQ2lCLEtBQU0sRUFBN0MsR0FBaURqQixLQUFLLENBQUNpQixLQUF2RCxHQUErRGpCLEtBQTVFOztBQUNBLGFBQU9BLEtBQUssQ0FBQ1MsUUFBTixFQUFQO0FBQ0Q7QUFDRjs7QUFFRCxRQUFNb0IsZUFBTixDQUFzQkMsU0FBdEIsRUFBaUM1QyxXQUFqQyxFQUE4Q0MsYUFBOUMsRUFBNkRDLEdBQTdELEVBQWtFQyxJQUFsRSxFQUF3RUMsUUFBeEUsRUFBa0ZDLE9BQWxGLEVBQTJGO0FBQ3pGRyxvQkFBT0MsS0FBUCxDQUFhZCxJQUFJLENBQUNrQixNQUFMLENBQVksa0ZBQVosRUFBZ0diLFdBQWhHLENBQWI7O0FBQ0EsUUFBSTZDLFFBQVEsR0FBRyxJQUFmO0FBQ0EsUUFBSUMsVUFBVSxHQUFHLElBQWpCO0FBQ0EsUUFBSUMsV0FBVyxHQUFHLElBQWxCO0FBQ0EsUUFBSUMsZUFBZSxHQUFHLElBQXRCO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLElBQXJCO0FBQ0EsUUFBSUMsZUFBZSxHQUFHLElBQXRCOztBQUVBLFFBQUk7QUFDRjtBQUNBLFlBQU01QyxNQUFNLEdBQUcsTUFBTVYsR0FBRyxDQUFDVyxvQkFBSixDQUF5QixNQUF6QixDQUFyQjs7QUFDQUMsc0JBQU9DLEtBQVAsQ0FBYSw4REFBYixFQUE2RUosT0FBN0U7O0FBQ0EsWUFBTUssT0FBTyxHQUFHSixNQUFNLENBQUNLLFVBQVAsQ0FBa0JYLFdBQWxCLENBQWhCOztBQUNBLFVBQUksQ0FBQ1UsT0FBTCxFQUFjO0FBQ1osY0FBTUUsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNEQUFaLEVBQW9FYixXQUFwRSxDQUFoQjs7QUFDQVEsd0JBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQSxjQUFNLElBQUlHLEtBQUosQ0FBVUgsT0FBVixDQUFOO0FBQ0Q7O0FBQ0QsWUFBTXVDLElBQUksR0FBRzdDLE1BQU0sQ0FBQzhDLGdCQUFQLEVBQWIsQ0FWRSxDQVdGOztBQUNBTixNQUFBQSxVQUFVLEdBQUdLLElBQUksQ0FBQ0UsZ0JBQUwsRUFBYixDQVpFLENBY0Y7O0FBQ0EsWUFBTXJDLE9BQU8sR0FBRztBQUNkeUIsUUFBQUEsT0FBTyxFQUFFLHdCQURLO0FBRWR4QixRQUFBQSxXQUFXLEVBQUVoQixhQUZDO0FBR2RDLFFBQUFBLEdBSGM7QUFJZEMsUUFBQUEsSUFKYztBQUtkbUQsUUFBQUEsT0FBTyxFQUFFdEQsV0FMSztBQU1kbUQsUUFBQUE7QUFOYyxPQUFoQjs7QUFRQTNDLHNCQUFPQyxLQUFQLENBQWMsV0FBVWdCLElBQUksQ0FBQ0MsU0FBTCxDQUFlVixPQUFmLENBQXdCLEVBQWhEOztBQUVBLFlBQU11QyxPQUFPLEdBQUcsTUFBTTdDLE9BQU8sQ0FBQzhDLHVCQUFSLENBQWdDeEMsT0FBaEMsQ0FBdEIsQ0F6QkUsQ0EyQkY7QUFDQTtBQUNBOztBQUNBLFlBQU15QyxpQkFBaUIsR0FBR0YsT0FBTyxDQUFDLENBQUQsQ0FBakM7QUFDQSxZQUFNRyxRQUFRLEdBQUdILE9BQU8sQ0FBQyxDQUFELENBQXhCLENBL0JFLENBaUNGO0FBQ0E7QUFDQTs7QUFDQSxVQUFJSSxPQUFPLEdBQUcsSUFBZDs7QUFDQSxXQUFLLE1BQU12QyxDQUFYLElBQWdCcUMsaUJBQWhCLEVBQW1DO0FBQ2pDLFlBQUlHLE9BQU8sR0FBRyxLQUFkOztBQUNBLFlBQUlILGlCQUFpQixJQUFJQSxpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUExQyxJQUNBSixpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUFyQixDQUE4QkMsTUFBOUIsS0FBeUMsR0FEN0MsRUFDa0Q7QUFDaERGLFVBQUFBLE9BQU8sR0FBRyxJQUFWOztBQUNBcEQsMEJBQU9jLElBQVAsQ0FBWSxvQ0FBWjtBQUNELFNBSkQsTUFJTztBQUNMMkIsVUFBQUEsY0FBYyxHQUFHUSxpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUIwQyxNQUF0QztBQUNBWixVQUFBQSxlQUFlLEdBQUdPLGlCQUFpQixDQUFDckMsQ0FBRCxDQUFqQixDQUFxQlIsT0FBdkM7O0FBQ0FKLDBCQUFPTSxLQUFQLENBQWFXLElBQUksQ0FBQ0MsU0FBTCxDQUFlK0IsaUJBQWYsQ0FBYjs7QUFDQWpELDBCQUFPTSxLQUFQLENBQWEsbUNBQWI7QUFDRDs7QUFDRDZDLFFBQUFBLE9BQU8sSUFBSUMsT0FBWDtBQUNEOztBQUVELFVBQUlELE9BQUosRUFBYTtBQUNYVixRQUFBQSxjQUFjLEdBQUdRLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJJLFFBQXJCLENBQThCQyxNQUEvQztBQUNBZCxRQUFBQSxlQUFlLEdBQUdTLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJJLFFBQXJCLENBQThCRSxPQUFoRDs7QUFDQXZELHdCQUFPYyxJQUFQLENBQVkzQixJQUFJLENBQUNrQixNQUFMLENBQ1YsbUlBRFUsRUFFVjRDLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJJLFFBQXJCLENBQThCQyxNQUZwQixFQUU0QkwsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJqRCxPQUYxRCxFQUdWNkMsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJFLE9BSHBCLEVBRzZCTixpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCTyxXQUFyQixDQUFpQ0MsU0FIOUQsQ0FBWixFQUhXLENBUVg7QUFDQTs7O0FBQ0EsY0FBTUMsUUFBUSxHQUFHLEVBQWpCO0FBQ0EsY0FBTUMsU0FBUyxHQUFHekQsT0FBTyxDQUFDMEQseUJBQVIsRUFBbEI7QUFDQUQsUUFBQUEsU0FBUyxDQUFDRSxPQUFWLENBQWtCQyxFQUFFLElBQUk7QUFDdEI5RCwwQkFBT0MsS0FBUCxDQUFhLHVDQUFiOztBQUNBLGdCQUFNOEQsa0JBQWtCLEdBQUcsSUFBSUMsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUMxRCxrQkFBTUMsU0FBUyxHQUFHQyxVQUFVLENBQUMsTUFBTTtBQUNqQyxvQkFBTWhFLE9BQU8sR0FBSSxtQkFBa0IwRCxFQUFFLENBQUNPLFdBQUgsRUFBaUIsRUFBcEQ7O0FBQ0FyRSw4QkFBT00sS0FBUCxDQUFhRixPQUFiOztBQUNBMEQsY0FBQUEsRUFBRSxDQUFDUSxVQUFIO0FBQ0QsYUFKMkIsRUFJekIsSUFKeUIsQ0FBNUI7QUFLQVIsWUFBQUEsRUFBRSxDQUFDUyxlQUFILENBQW1CakMsVUFBbkIsRUFBK0IsQ0FBQ2tDLEVBQUQsRUFBS0MsSUFBTCxFQUFXQyxRQUFYLEtBQXdCO0FBQ3JEMUUsOEJBQU9jLElBQVAsQ0FBWSwwRUFBWixFQUF3RmdELEVBQUUsQ0FBQ08sV0FBSCxFQUF4Rjs7QUFDQXJFLDhCQUFPYyxJQUFQLENBQVksNkNBQVosRUFBMkQwRCxFQUEzRCxFQUErREMsSUFBL0QsRUFBcUVDLFFBQXJFOztBQUNBQyxjQUFBQSxZQUFZLENBQUNSLFNBQUQsQ0FBWjtBQUNBNUIsY0FBQUEsV0FBVyxHQUFHbUMsUUFBZDs7QUFFQSxrQkFBSUQsSUFBSSxLQUFLLE9BQWIsRUFBc0I7QUFDcEIsc0JBQU1yRSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksdURBQVosRUFBcUVvRSxJQUFyRSxDQUFoQjs7QUFDQXpFLGdDQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0E4RCxnQkFBQUEsTUFBTSxDQUFDLElBQUkzRCxLQUFKLENBQVVILE9BQVYsQ0FBRCxDQUFOO0FBQ0QsZUFKRCxNQUlPO0FBQ0wsc0JBQU1BLE9BQU8sR0FBRyw2Q0FBaEI7O0FBQ0FKLGdDQUFPYyxJQUFQLENBQVlWLE9BQVo7O0FBQ0E2RCxnQkFBQUEsT0FBTyxDQUFDN0QsT0FBRCxDQUFQO0FBQ0Q7QUFDRixhQWZELEVBZUc0QixHQUFHLElBQUk7QUFDUjJDLGNBQUFBLFlBQVksQ0FBQ1IsU0FBRCxDQUFaOztBQUNBbkUsOEJBQU9NLEtBQVAsQ0FBYTBCLEdBQWI7O0FBQ0FrQyxjQUFBQSxNQUFNLENBQUNsQyxHQUFELENBQU47QUFDRCxhQW5CRCxFQW9CRTtBQUNBO0FBQ0E7QUFDQTtBQUNGO0FBQUU0QyxjQUFBQSxVQUFVLEVBQUUsSUFBZDtBQUFvQk4sY0FBQUEsVUFBVSxFQUFFO0FBQWhDLGFBeEJBO0FBMEJBUixZQUFBQSxFQUFFLENBQUNlLE9BQUg7QUFDRCxXQWpDMEIsQ0FBM0I7QUFrQ0FuQixVQUFBQSxRQUFRLENBQUNvQixJQUFULENBQWNmLGtCQUFkO0FBQ0QsU0FyQ0Q7QUF1Q0EsY0FBTWdCLFVBQVUsR0FBRztBQUNqQnBDLFVBQUFBLElBRGlCO0FBRWpCTSxVQUFBQSxpQkFGaUI7QUFHakJDLFVBQUFBO0FBSGlCLFNBQW5CO0FBS0EsY0FBTThCLFdBQVcsR0FBRzlFLE9BQU8sQ0FBQytFLGVBQVIsQ0FBd0JGLFVBQXhCLENBQXBCLENBeERXLENBeURYO0FBQ0E7O0FBQ0FyQixRQUFBQSxRQUFRLENBQUNvQixJQUFULENBQWNFLFdBQWQ7QUFDQSxjQUFNakMsT0FBTyxHQUFHLE1BQU1pQixPQUFPLENBQUNrQixHQUFSLENBQVl4QixRQUFaLENBQXRCOztBQUNBMUQsd0JBQU9DLEtBQVAsQ0FBYWQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGlDQUFaLEVBQStDMEMsT0FBL0MsQ0FBYjs7QUFDQSxjQUFNTSxRQUFRLEdBQUdOLE9BQU8sQ0FBQ29DLEdBQVIsRUFBakIsQ0E5RFcsQ0E4RHFCOztBQUNoQyxZQUFJOUIsUUFBUSxDQUFDQyxNQUFULEtBQW9CLFNBQXhCLEVBQW1DO0FBQ2pDdEQsMEJBQU9jLElBQVAsQ0FBWSwrQ0FBWjtBQUNELFNBRkQsTUFFTztBQUNMdUIsVUFBQUEsUUFBUSxHQUFHbEQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGlEQUFaLEVBQStEZ0QsUUFBUSxDQUFDQyxNQUF4RSxDQUFYOztBQUNBdEQsMEJBQU9DLEtBQVAsQ0FBYW9DLFFBQWI7QUFDRCxTQXBFVSxDQXNFWDs7O0FBQ0EsYUFBSyxNQUFNekIsQ0FBWCxJQUFnQm1DLE9BQWhCLEVBQXlCO0FBQ3ZCLGdCQUFNcUMsUUFBUSxHQUFHckMsT0FBTyxDQUFDbkMsQ0FBRCxDQUF4QjtBQUNBLGdCQUFNa0QsRUFBRSxHQUFHSCxTQUFTLENBQUMvQyxDQUFELENBQXBCOztBQUNBWiwwQkFBT0MsS0FBUCxDQUFhLGlDQUFiLEVBQWdENkQsRUFBRSxDQUFDTyxXQUFILEVBQWhEOztBQUNBLGNBQUksT0FBT2UsUUFBUCxLQUFvQixRQUF4QixFQUFrQztBQUNoQ3BGLDRCQUFPQyxLQUFQLENBQWFtRixRQUFiO0FBQ0QsV0FGRCxNQUVPO0FBQ0wsZ0JBQUksQ0FBQy9DLFFBQUwsRUFBZUEsUUFBUSxHQUFHK0MsUUFBUSxDQUFDckUsUUFBVCxFQUFYOztBQUNmZiw0QkFBT0MsS0FBUCxDQUFhbUYsUUFBUSxDQUFDckUsUUFBVCxFQUFiO0FBQ0Q7QUFDRjtBQUNGLE9BbEZELE1Ba0ZPO0FBQ0xzQixRQUFBQSxRQUFRLEdBQUdsRCxJQUFJLENBQUNrQixNQUFMLENBQVksK0RBQVosQ0FBWDs7QUFDQUwsd0JBQU9DLEtBQVAsQ0FBYW9DLFFBQWI7QUFDRDtBQUNGLEtBMUlELENBMElFLE9BQU8vQixLQUFQLEVBQWM7QUFDZE4sc0JBQU9NLEtBQVAsQ0FBYztnREFDNEJBLEtBQUssQ0FBQ2lCLEtBQU0sRUFEekMsR0FDNkNqQixLQUFLLENBQUNpQixLQURuRCxHQUMyRGpCLEtBRHhFOztBQUVBK0IsTUFBQUEsUUFBUSxHQUFHL0IsS0FBSyxDQUFDUyxRQUFOLEVBQVg7QUFDRDs7QUFFRCxRQUFJLENBQUNzQixRQUFMLEVBQWU7QUFDYixZQUFNakMsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUNkLG9GQURjLEVBRWRSLE9BRmMsRUFFTEwsV0FGSyxFQUVROEMsVUFGUixDQUFoQjs7QUFHQXRDLHNCQUFPYyxJQUFQLENBQVlWLE9BQVo7O0FBRUEsWUFBTVksTUFBTSxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUM1Qm1FLFFBQUFBLGFBQWEsRUFBRSxvQkFEYTtBQUU1QkMsUUFBQUEsS0FBSyxFQUFFaEQsVUFGcUI7QUFHNUJpRCxRQUFBQSxXQUFXLEVBQUVoRCxXQUhlO0FBSTVCaUQsUUFBQUEsVUFBVSxFQUFFaEcsV0FKZ0I7QUFLNUJpRyxRQUFBQSxNQUFNLEVBQUU1RixPQUxvQjtBQU01QnVCLFFBQUFBLE1BQU0sRUFBRTFCO0FBTm9CLE9BQWYsQ0FBZjs7QUFRQU0sc0JBQU9DLEtBQVAsQ0FBYWUsTUFBYjs7QUFDQWhCLHNCQUFPQyxLQUFQLENBQWEsd0JBQWIsRUFBdUN1QyxlQUF2Qzs7QUFFQSxZQUFNa0QsYUFBYSxHQUFHbEQsZUFBZSxDQUFDekIsUUFBaEIsRUFBdEI7QUFDQSxhQUFPO0FBQ0x1QyxRQUFBQSxNQUFNLEVBQUViLGNBREg7QUFFTEUsUUFBQUEsSUFBSSxFQUFFTCxVQUZEO0FBR0xsQyxRQUFBQSxPQUFPLEVBQUVzRjtBQUhKLE9BQVA7QUFLRDs7QUFDRCxVQUFNdEYsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNDQUFaLEVBQW9EZ0MsUUFBcEQsQ0FBaEI7O0FBQ0FyQyxvQkFBT00sS0FBUCxDQUFhRixPQUFiLEVBbEx5RixDQW1MekY7OztBQUNBLFdBQU87QUFDTGtELE1BQUFBLE1BQU0sRUFBRWIsY0FESDtBQUVMckMsTUFBQUEsT0FBTyxFQUFFc0M7QUFGSixLQUFQO0FBSUQ7O0FBRUQsUUFBTWlELHdCQUFOLENBQStCbkcsV0FBL0IsRUFBNENDLGFBQTVDLEVBQTJEQyxHQUEzRCxFQUFnRTRCLEtBQWhFLEVBQXVFMUIsUUFBdkUsRUFBaUY7QUFDL0VJLG9CQUFPQyxLQUFQLENBQWFkLElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSxrRkFBWixFQUFnR2IsV0FBaEcsQ0FBYjs7QUFDQSxRQUFJNkMsUUFBUSxHQUFHLElBQWY7QUFDQSxRQUFJQyxVQUFVLEdBQUcsSUFBakI7QUFDQSxVQUFNM0MsSUFBSSxHQUFHLEVBQWI7QUFHQSxRQUFJSixJQUFJLEdBQUcsSUFBWCxDQVArRSxDQU8vRDs7QUFDaEIsUUFBSXFHLE9BQU8sR0FBRyxJQUFkLENBUitFLENBUTVEOztBQUNuQixRQUFJL0YsT0FBTyxHQUFHLElBQWQsQ0FUK0UsQ0FTM0Q7O0FBRXBCLFFBQUk7QUFDRixZQUFNNEIsdUJBQXVCLEdBQUcsSUFBSXpDLE1BQU0sQ0FBQzBDLElBQVgsQ0FBZ0J4QyxJQUFJLENBQUN5QyxJQUFMLENBQVVDLFNBQVYsRUFBcUIsWUFBckIsRUFBbUMsc0JBQW5DLENBQWhCLENBQWhDO0FBQ0FnRSxNQUFBQSxPQUFPLEdBQUduRSx1QkFBdUIsQ0FBQ29FLGVBQWxDO0FBQ0F0RyxNQUFBQSxJQUFJLEdBQUdrQyx1QkFBdUIsQ0FBQ0ksWUFBL0I7QUFDQWhDLE1BQUFBLE9BQU8sR0FBRzRCLHVCQUF1QixDQUFDSyxXQUFsQztBQUVBTCxNQUFBQSx1QkFBdUIsQ0FBQ00sS0FBeEI7QUFDRCxLQVBELENBT0UsT0FBT0MsR0FBUCxFQUFZO0FBQ1poQyxzQkFBT0MsS0FBUCxDQUFhK0IsR0FBYjtBQUNELEtBcEI4RSxDQXVCL0U7QUFDQTtBQUNBOzs7QUFFQSxRQUFJO0FBQ0Y7QUFDQSxZQUFNbEMsTUFBTSxHQUFHLE1BQU1WLEdBQUcsQ0FBQ1csb0JBQUosQ0FBeUJGLE9BQXpCLENBQXJCOztBQUNBRyxzQkFBT0MsS0FBUCxDQUFhLDhEQUFiLEVBQTZFSixPQUE3RTs7QUFDQSxZQUFNSyxPQUFPLEdBQUdKLE1BQU0sQ0FBQ0ssVUFBUCxDQUFrQlgsV0FBbEIsQ0FBaEI7O0FBQ0EsVUFBSSxDQUFDVSxPQUFMLEVBQWM7QUFDWixjQUFNRSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0RBQVosRUFBb0ViLFdBQXBFLENBQWhCOztBQUNBUSx3QkFBT00sS0FBUCxDQUFhRixPQUFiOztBQUNBLGNBQU0sSUFBSUcsS0FBSixDQUFVSCxPQUFWLENBQU47QUFDRDs7QUFDRCxZQUFNdUMsSUFBSSxHQUFHN0MsTUFBTSxDQUFDOEMsZ0JBQVAsRUFBYixDQVZFLENBV0Y7O0FBQ0FOLE1BQUFBLFVBQVUsR0FBR0ssSUFBSSxDQUFDRSxnQkFBTCxFQUFiLENBWkUsQ0FjRjs7QUFDQWxELE1BQUFBLElBQUksQ0FBQ21GLElBQUwsQ0FBVXhDLFVBQVY7QUFDQTNDLE1BQUFBLElBQUksQ0FBQ21GLElBQUwsQ0FBVXhELEtBQVY7O0FBS0F0QixzQkFBT0MsS0FBUCxDQUFjLGlCQUFnQlYsSUFBSyxFQUFuQzs7QUFDQVMsc0JBQU9DLEtBQVAsQ0FBYyxvQkFBbUIyRixPQUFRLEVBQXpDOztBQUNBNUYsc0JBQU9DLEtBQVAsQ0FBYyxlQUFjSixPQUFRLEVBQXBDLEVBdkJFLENBeUJGOzs7QUFDQSxZQUFNVyxPQUFPLEdBQUc7QUFDZHlCLFFBQUFBLE9BQU8sRUFBRTFDLElBREs7QUFFZGtCLFFBQUFBLFdBQVcsRUFBRWhCLGFBRkM7QUFHZEMsUUFBQUEsR0FIYztBQUlkQyxRQUFBQSxJQUpjO0FBS2RtRCxRQUFBQSxPQUFPLEVBQUV0RCxXQUxLO0FBTWRtRCxRQUFBQTtBQU5jLE9BQWhCOztBQVFBM0Msc0JBQU9DLEtBQVAsQ0FBYyxXQUFVZ0IsSUFBSSxDQUFDQyxTQUFMLENBQWVWLE9BQWYsQ0FBd0IsRUFBaEQ7O0FBRUEsWUFBTXVDLE9BQU8sR0FBRyxNQUFNN0MsT0FBTyxDQUFDOEMsdUJBQVIsQ0FBZ0N4QyxPQUFoQyxDQUF0QixDQXBDRSxDQXNDRjtBQUNBO0FBQ0E7O0FBQ0EsWUFBTXlDLGlCQUFpQixHQUFHRixPQUFPLENBQUMsQ0FBRCxDQUFqQztBQUNBLFlBQU1HLFFBQVEsR0FBR0gsT0FBTyxDQUFDLENBQUQsQ0FBeEIsQ0ExQ0UsQ0E0Q0Y7QUFDQTtBQUNBOztBQUNBLFVBQUlJLE9BQU8sR0FBRyxJQUFkOztBQUNBLFdBQUssTUFBTXZDLENBQVgsSUFBZ0JxQyxpQkFBaEIsRUFBbUM7QUFDakMsWUFBSUcsT0FBTyxHQUFHLEtBQWQ7O0FBQ0EsWUFBSUgsaUJBQWlCLElBQUlBLGlCQUFpQixDQUFDckMsQ0FBRCxDQUFqQixDQUFxQnlDLFFBQTFDLElBQ0FKLGlCQUFpQixDQUFDckMsQ0FBRCxDQUFqQixDQUFxQnlDLFFBQXJCLENBQThCQyxNQUE5QixLQUF5QyxHQUQ3QyxFQUNrRDtBQUNoREYsVUFBQUEsT0FBTyxHQUFHLElBQVY7O0FBQ0FwRCwwQkFBT2MsSUFBUCxDQUFZLG9DQUFaO0FBQ0QsU0FKRCxNQUlPO0FBQ0xkLDBCQUFPTSxLQUFQLENBQWFXLElBQUksQ0FBQ0MsU0FBTCxDQUFlK0IsaUJBQWYsQ0FBYjs7QUFDQWpELDBCQUFPTSxLQUFQLENBQWEsbUNBQWI7QUFDRDs7QUFDRDZDLFFBQUFBLE9BQU8sSUFBSUMsT0FBWDtBQUNEOztBQUVELFVBQUlELE9BQUosRUFBYTtBQUNYbkQsd0JBQU9jLElBQVAsQ0FBWTNCLElBQUksQ0FBQ2tCLE1BQUwsQ0FDVixtSUFEVSxFQUVWNEMsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJDLE1BRnBCLEVBRTRCTCxpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCSSxRQUFyQixDQUE4QmpELE9BRjFELEVBR1Y2QyxpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCSSxRQUFyQixDQUE4QkUsT0FIcEIsRUFHNkJOLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJPLFdBQXJCLENBQWlDQyxTQUg5RCxDQUFaLEVBRFcsQ0FPWDtBQUVBO0FBQ0E7OztBQUNBLGNBQU1DLFFBQVEsR0FBRyxFQUFqQjtBQUNBLGNBQU1DLFNBQVMsR0FBR3pELE9BQU8sQ0FBQzBELHlCQUFSLEVBQWxCLENBWlcsQ0FhWDs7QUFFQUQsUUFBQUEsU0FBUyxDQUFDRSxPQUFWLENBQWtCQyxFQUFFLElBQUk7QUFDdEI5RCwwQkFBT0MsS0FBUCxDQUFhLHVDQUFiLEVBRHNCLENBRXRCOzs7QUFFQSxnQkFBTThELGtCQUFrQixHQUFHLElBQUlDLE9BQUosQ0FBWSxDQUFDQyxPQUFELEVBQVVDLE1BQVYsS0FBcUI7QUFDMUQsa0JBQU1DLFNBQVMsR0FBR0MsVUFBVSxDQUFDLE1BQU07QUFDakMsb0JBQU1oRSxPQUFPLEdBQUksbUJBQWtCMEQsRUFBRSxDQUFDTyxXQUFILEVBQWlCLEVBQXBEOztBQUNBckUsOEJBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQTBELGNBQUFBLEVBQUUsQ0FBQ1EsVUFBSDtBQUNELGFBSjJCLEVBSXpCLElBSnlCLENBQTVCO0FBS0FSLFlBQUFBLEVBQUUsQ0FBQ1MsZUFBSCxDQUFtQmpDLFVBQW5CLEVBQStCLENBQUNrQyxFQUFELEVBQUtDLElBQUwsRUFBV0MsUUFBWCxLQUF3QjtBQUNyRDFFLDhCQUFPYyxJQUFQLENBQVksMEVBQVosRUFBd0ZnRCxFQUFFLENBQUNPLFdBQUgsRUFBeEY7O0FBQ0FyRSw4QkFBT2MsSUFBUCxDQUFZLDZDQUFaLEVBQTJEMEQsRUFBM0QsRUFBK0RDLElBQS9ELEVBQXFFQyxRQUFyRTs7QUFDQTFFLDhCQUFPYyxJQUFQLENBQVksa0JBQVo7O0FBQ0E2RCxjQUFBQSxZQUFZLENBQUNSLFNBQUQsQ0FBWixDQUpxRCxDQUtyRDs7QUFJQSxrQkFBSU0sSUFBSSxLQUFLLE9BQWIsRUFBc0I7QUFDcEIsc0JBQU1yRSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksdURBQVosRUFBcUVvRSxJQUFyRSxDQUFoQjs7QUFDQXpFLGdDQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0E2RCxnQkFBQUEsT0FBTyxDQUFDLElBQUkxRCxLQUFKLENBQVVILE9BQVYsQ0FBRCxDQUFQO0FBQ0QsZUFKRCxNQUlPO0FBQ0wsc0JBQU1BLE9BQU8sR0FBRyw2Q0FBaEI7O0FBQ0FKLGdDQUFPYyxJQUFQLENBQVlWLE9BQVo7O0FBQ0E2RCxnQkFBQUEsT0FBTyxDQUFDN0QsT0FBRCxDQUFQLENBSEssQ0FJTDtBQUNEO0FBQ0YsYUFuQkQsRUFtQkc0QixHQUFHLElBQUk7QUFDUjJDLGNBQUFBLFlBQVksQ0FBQ1IsU0FBRCxDQUFaOztBQUNBbkUsOEJBQU9DLEtBQVAsQ0FBYSxnQkFBYjs7QUFDQUQsOEJBQU9NLEtBQVAsQ0FBYTBCLEdBQWIsRUFIUSxDQUlSOzs7QUFDQWlDLGNBQUFBLE9BQU8sQ0FBQ2pDLEdBQUQsQ0FBUDtBQUNELGFBekJELEVBMEJFO0FBQ0E7QUFDQTtBQUNBO0FBQ0Y7QUFBRTRDLGNBQUFBLFVBQVUsRUFBRSxJQUFkO0FBQW9CTixjQUFBQSxVQUFVLEVBQUU7QUFBaEMsYUE5QkE7QUFnQ0FSLFlBQUFBLEVBQUUsQ0FBQ2UsT0FBSDtBQUNELFdBdkMwQixDQUEzQjs7QUF3Q0E3RSwwQkFBT2MsSUFBUCxDQUFZLHlDQUFaOztBQUNBNEMsVUFBQUEsUUFBUSxDQUFDb0IsSUFBVCxDQUFjZixrQkFBZDtBQUNELFNBOUNELEVBZlcsQ0FnRVg7O0FBR0EsY0FBTWdCLFVBQVUsR0FBRztBQUNqQmEsVUFBQUEsT0FEaUI7QUFFakJqRCxVQUFBQSxJQUZpQjtBQUdqQk0sVUFBQUEsaUJBSGlCO0FBSWpCQyxVQUFBQTtBQUppQixTQUFuQixDQW5FVyxDQTBFWDtBQUNBO0FBQ0E7QUFDQTs7QUFHQSxjQUFNOEIsV0FBVyxHQUFHOUUsT0FBTyxDQUFDK0UsZUFBUixDQUF3QkYsVUFBeEIsQ0FBcEIsQ0FoRlcsQ0FpRlg7QUFDQTs7QUFDQXJCLFFBQUFBLFFBQVEsQ0FBQ29CLElBQVQsQ0FBY0UsV0FBZDtBQUVBLGNBQU1qQyxPQUFPLEdBQUcsTUFBTWlCLE9BQU8sQ0FBQ2tCLEdBQVIsQ0FBWXhCLFFBQVosQ0FBdEI7O0FBQ0ExRCx3QkFBT0MsS0FBUCxDQUFhLGVBQWI7O0FBQ0FELHdCQUFPQyxLQUFQLENBQWE4QyxPQUFiOztBQUNBL0Msd0JBQU9DLEtBQVAsQ0FBYWQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGlDQUFaLEVBQStDMEMsT0FBL0MsQ0FBYjs7QUFFQSxjQUFNTSxRQUFRLEdBQUdOLE9BQU8sQ0FBQ29DLEdBQVIsRUFBakIsQ0ExRlcsQ0EwRnFCOztBQUNoQyxZQUFJOUIsUUFBUSxDQUFDQyxNQUFULEtBQW9CLFNBQXhCLEVBQW1DO0FBQ2pDdEQsMEJBQU9jLElBQVAsQ0FBWSwrQ0FBWjtBQUNELFNBRkQsTUFFTztBQUNMdUIsVUFBQUEsUUFBUSxHQUFHbEQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGlEQUFaLEVBQStEZ0QsUUFBUSxDQUFDQyxNQUF4RSxDQUFYOztBQUNBdEQsMEJBQU9DLEtBQVAsQ0FBYW9DLFFBQWI7QUFDRCxTQWhHVSxDQW1HWDs7O0FBQ0EsYUFBSyxNQUFNekIsQ0FBWCxJQUFnQm1DLE9BQWhCLEVBQXlCO0FBQ3ZCL0MsMEJBQU9DLEtBQVAsQ0FBYSxrQkFBYjs7QUFDQUQsMEJBQU9DLEtBQVAsQ0FBYThDLE9BQWI7O0FBQ0EsZ0JBQU1xQyxRQUFRLEdBQUdyQyxPQUFPLENBQUNuQyxDQUFELENBQXhCO0FBQ0EsZ0JBQU1rRCxFQUFFLEdBQUdILFNBQVMsQ0FBQy9DLENBQUQsQ0FBcEI7O0FBQ0FaLDBCQUFPQyxLQUFQLENBQWEsaUNBQWIsRUFBZ0Q2RCxFQUFFLENBQUNPLFdBQUgsRUFBaEQ7O0FBQ0EsY0FBSSxPQUFPZSxRQUFQLEtBQW9CLFFBQXhCLEVBQWtDO0FBQ2hDcEYsNEJBQU9DLEtBQVAsQ0FBYW1GLFFBQWI7O0FBR0Esa0JBQU1oRixPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQ2Qsb0ZBRGMsRUFFZFIsT0FGYyxFQUVMTCxXQUZLLEVBRVE4QyxVQUZSLENBQWhCOztBQUdBdEMsNEJBQU9jLElBQVAsQ0FBWVYsT0FBWjs7QUFFQSxtQkFBT2tDLFVBQVA7QUFPRCxXQWhCRCxNQWdCTztBQUNMLGdCQUFJLENBQUNELFFBQUwsRUFBZUEsUUFBUSxHQUFHK0MsUUFBUSxDQUFDckUsUUFBVCxFQUFYOztBQUNmZiw0QkFBT0MsS0FBUCxDQUFhLFNBQWI7O0FBQ0FELDRCQUFPQyxLQUFQLENBQWFtRixRQUFRLENBQUNyRSxRQUFULEVBQWI7QUFDRDtBQUNGLFNBL0hVLENBaUlYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0QsT0F6SUQsTUEwSUs7QUFDSHNCLFFBQUFBLFFBQVEsR0FBR2xELElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSwrREFBWixDQUFYOztBQUNBTCx3QkFBT0MsS0FBUCxDQUFhb0MsUUFBYjtBQUNEO0FBQ0YsS0EzTUQsQ0EyTUUsT0FBTy9CLEtBQVAsRUFBYztBQUNkTixzQkFBT00sS0FBUCxDQUFjLGtDQUFpQ0EsS0FBSyxDQUFDaUIsS0FBTSxFQUE5QyxHQUFrRGpCLEtBQUssQ0FBQ2lCLEtBQXhELEdBQWdFakIsS0FBN0U7O0FBQ0ErQixNQUFBQSxRQUFRLEdBQUcvQixLQUFLLENBQUNTLFFBQU4sRUFBWDtBQUNEOztBQUNEZixvQkFBT0MsS0FBUCxDQUFhLGNBQWI7O0FBQ0FELG9CQUFPQyxLQUFQLENBQWFvQyxRQUFiLEVBM08rRSxDQTRPL0U7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFVBQU1qQyxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0NBQVosRUFBb0RnQyxRQUFwRCxDQUFoQjs7QUFDQXJDLG9CQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EsVUFBTSxJQUFJRyxLQUFKLENBQVVILE9BQVYsQ0FBTjtBQUNEOztBQUdELFFBQU0wRix5QkFBTixDQUFnQzFELFNBQWhDLEVBQTJDNUMsV0FBM0MsRUFBd0RDLGFBQXhELEVBQXVFQyxHQUF2RSxFQUE0RUMsSUFBNUUsRUFBa0ZDLFFBQWxGLEVBQTRGQyxPQUE1RixFQUFxRztBQUNuR0csb0JBQU9DLEtBQVAsQ0FBYWQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGtGQUFaLEVBQWdHYixXQUFoRyxDQUFiOztBQUNBLFFBQUk2QyxRQUFRLEdBQUcsSUFBZjtBQUNBLFFBQUlDLFVBQVUsR0FBRyxJQUFqQjs7QUFDQSxRQUFJO0FBQ0Y7QUFDQSxZQUFNeEMsTUFBTSxHQUFHLE1BQU1WLEdBQUcsQ0FBQ1csb0JBQUosQ0FBeUJGLE9BQXpCLENBQXJCOztBQUNBRyxzQkFBT0MsS0FBUCxDQUFhLDhEQUFiLEVBQTZFSixPQUE3RTs7QUFDQSxZQUFNSyxPQUFPLEdBQUdKLE1BQU0sQ0FBQ0ssVUFBUCxDQUFrQlgsV0FBbEIsQ0FBaEI7O0FBQ0EsVUFBSSxDQUFDVSxPQUFMLEVBQWM7QUFDWixjQUFNRSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0RBQVosRUFBb0ViLFdBQXBFLENBQWhCOztBQUNBUSx3QkFBT00sS0FBUCxDQUFhRixPQUFiOztBQUNBLGNBQU0sSUFBSUcsS0FBSixDQUFVSCxPQUFWLENBQU47QUFDRDs7QUFDRCxZQUFNdUMsSUFBSSxHQUFHN0MsTUFBTSxDQUFDOEMsZ0JBQVAsRUFBYixDQVZFLENBV0Y7O0FBQ0FOLE1BQUFBLFVBQVUsR0FBR0ssSUFBSSxDQUFDRSxnQkFBTCxFQUFiLENBWkUsQ0FjRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFDQSxZQUFNckMsT0FBTyxHQUFHO0FBQ2R5QixRQUFBQSxPQUFPLEVBQUUsaUJBREs7QUFFZHhCLFFBQUFBLFdBQVcsRUFBRWhCLGFBRkM7QUFHZEMsUUFBQUEsR0FIYztBQUlkQyxRQUFBQSxJQUpjO0FBS2RtRCxRQUFBQSxPQUFPLEVBQUV0RCxXQUxLO0FBTWRtRCxRQUFBQTtBQU5jLE9BQWhCOztBQVFBLFVBQUlQLFNBQUosRUFBZTtBQUNiNUIsUUFBQUEsT0FBTyxDQUFDeUIsT0FBUixHQUFrQkcsU0FBbEI7QUFDRDs7QUFFRHBDLHNCQUFPQyxLQUFQLENBQWMsV0FBVWdCLElBQUksQ0FBQ0MsU0FBTCxDQUFlVixPQUFmLENBQXdCLEVBQWhEOztBQUVBLFlBQU11QyxPQUFPLEdBQUcsTUFBTTdDLE9BQU8sQ0FBQzhDLHVCQUFSLENBQWdDeEMsT0FBaEMsQ0FBdEIsQ0FwQ0UsQ0FzQ0Y7QUFDQTtBQUNBOztBQUNBLFlBQU15QyxpQkFBaUIsR0FBR0YsT0FBTyxDQUFDLENBQUQsQ0FBakM7QUFDQSxZQUFNRyxRQUFRLEdBQUdILE9BQU8sQ0FBQyxDQUFELENBQXhCLENBMUNFLENBNENGO0FBQ0E7QUFDQTs7QUFDQSxVQUFJSSxPQUFPLEdBQUcsSUFBZDs7QUFDQSxXQUFLLE1BQU12QyxDQUFYLElBQWdCcUMsaUJBQWhCLEVBQW1DO0FBQ2pDLFlBQUlHLE9BQU8sR0FBRyxLQUFkOztBQUNBLFlBQUlILGlCQUFpQixJQUFJQSxpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUExQyxJQUNBSixpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUFyQixDQUE4QkMsTUFBOUIsS0FBeUMsR0FEN0MsRUFDa0Q7QUFDaERGLFVBQUFBLE9BQU8sR0FBRyxJQUFWOztBQUNBcEQsMEJBQU9jLElBQVAsQ0FBWSxvQ0FBWjtBQUNELFNBSkQsTUFJTztBQUNMZCwwQkFBT00sS0FBUCxDQUFhVyxJQUFJLENBQUNDLFNBQUwsQ0FBZStCLGlCQUFmLENBQWI7O0FBQ0FqRCwwQkFBT00sS0FBUCxDQUFhLG1DQUFiO0FBQ0Q7O0FBQ0Q2QyxRQUFBQSxPQUFPLElBQUlDLE9BQVg7QUFDRDs7QUFFRCxVQUFJRCxPQUFKLEVBQWE7QUFDWG5ELHdCQUFPYyxJQUFQLENBQVkzQixJQUFJLENBQUNrQixNQUFMLENBQ1YsbUlBRFUsRUFFVjRDLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJJLFFBQXJCLENBQThCQyxNQUZwQixFQUU0QkwsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJqRCxPQUYxRCxFQUdWNkMsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJFLE9BSHBCLEVBRzZCTixpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCTyxXQUFyQixDQUFpQ0MsU0FIOUQsQ0FBWixFQURXLENBT1g7QUFDQTs7O0FBQ0EsY0FBTUMsUUFBUSxHQUFHLEVBQWpCO0FBQ0EsY0FBTUMsU0FBUyxHQUFHekQsT0FBTyxDQUFDMEQseUJBQVIsRUFBbEI7QUFDQUQsUUFBQUEsU0FBUyxDQUFDRSxPQUFWLENBQWtCQyxFQUFFLElBQUk7QUFDdEI5RCwwQkFBT0MsS0FBUCxDQUFhLHVDQUFiOztBQUVBLGdCQUFNOEQsa0JBQWtCLEdBQUcsSUFBSUMsT0FBSixDQUFZLENBQUNDLE9BQUQsRUFBVUMsTUFBVixLQUFxQjtBQUMxRCxrQkFBTUMsU0FBUyxHQUFHQyxVQUFVLENBQUMsTUFBTTtBQUNqQyxvQkFBTWhFLE9BQU8sR0FBSSxtQkFBa0IwRCxFQUFFLENBQUNPLFdBQUgsRUFBaUIsRUFBcEQ7O0FBQ0FyRSw4QkFBT00sS0FBUCxDQUFhRixPQUFiOztBQUNBMEQsY0FBQUEsRUFBRSxDQUFDUSxVQUFIO0FBQ0QsYUFKMkIsRUFJekIsS0FKeUIsQ0FBNUI7QUFLQVIsWUFBQUEsRUFBRSxDQUFDUyxlQUFILENBQW1CakMsVUFBbkIsRUFBK0IsQ0FBQ2tDLEVBQUQsRUFBS0MsSUFBTCxFQUFXQyxRQUFYLEtBQXdCO0FBQ3JEMUUsOEJBQU9jLElBQVAsQ0FBWSwwRUFBWixFQUF3RmdELEVBQUUsQ0FBQ08sV0FBSCxFQUF4Rjs7QUFDQXJFLDhCQUFPYyxJQUFQLENBQVksNkNBQVosRUFBMkQwRCxFQUEzRCxFQUErREMsSUFBL0QsRUFBcUVDLFFBQXJFOztBQUNBQyxjQUFBQSxZQUFZLENBQUNSLFNBQUQsQ0FBWjs7QUFFQSxrQkFBSU0sSUFBSSxLQUFLLE9BQWIsRUFBc0I7QUFDcEIsc0JBQU1yRSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksdURBQVosRUFBcUVvRSxJQUFyRSxDQUFoQjs7QUFDQXpFLGdDQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0E4RCxnQkFBQUEsTUFBTSxDQUFDLElBQUkzRCxLQUFKLENBQVVILE9BQVYsQ0FBRCxDQUFOO0FBQ0QsZUFKRCxNQUlPO0FBQ0wsc0JBQU1BLE9BQU8sR0FBRyw2Q0FBaEI7O0FBQ0FKLGdDQUFPYyxJQUFQLENBQVlWLE9BQVo7O0FBQ0E2RCxnQkFBQUEsT0FBTyxDQUFDN0QsT0FBRCxDQUFQO0FBQ0Q7QUFDRixhQWRELEVBY0c0QixHQUFHLElBQUk7QUFDUjJDLGNBQUFBLFlBQVksQ0FBQ1IsU0FBRCxDQUFaOztBQUNBbkUsOEJBQU9NLEtBQVAsQ0FBYTBCLEdBQWI7O0FBQ0FrQyxjQUFBQSxNQUFNLENBQUNsQyxHQUFELENBQU47QUFDRCxhQWxCRCxFQW1CRTtBQUNBO0FBQ0E7QUFDQTtBQUNGO0FBQUU0QyxjQUFBQSxVQUFVLEVBQUUsSUFBZDtBQUFvQk4sY0FBQUEsVUFBVSxFQUFFO0FBQWhDLGFBdkJBO0FBeUJBUixZQUFBQSxFQUFFLENBQUNlLE9BQUg7QUFDRCxXQWhDMEIsQ0FBM0I7QUFrQ0FuQixVQUFBQSxRQUFRLENBQUNvQixJQUFULENBQWNmLGtCQUFkO0FBQ0QsU0F0Q0Q7QUF5Q0EsY0FBTWdCLFVBQVUsR0FBRztBQUNqQnBDLFVBQUFBLElBRGlCO0FBRWpCTSxVQUFBQSxpQkFGaUI7QUFHakJDLFVBQUFBLFFBSGlCLENBSWpCOztBQUppQixTQUFuQjtBQU1BLGNBQU04QixXQUFXLEdBQUc5RSxPQUFPLENBQUMrRSxlQUFSLENBQXdCRixVQUF4QixDQUFwQixDQTFEVyxDQTJEWDtBQUNBOztBQUNBckIsUUFBQUEsUUFBUSxDQUFDb0IsSUFBVCxDQUFjRSxXQUFkO0FBQ0EsY0FBTWpDLE9BQU8sR0FBRyxNQUFNaUIsT0FBTyxDQUFDa0IsR0FBUixDQUFZeEIsUUFBWixDQUF0Qjs7QUFDQTFELHdCQUFPQyxLQUFQLENBQWFkLElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSxpQ0FBWixFQUErQzBDLE9BQS9DLENBQWI7O0FBQ0EsY0FBTU0sUUFBUSxHQUFHTixPQUFPLENBQUNvQyxHQUFSLEVBQWpCLENBaEVXLENBZ0VxQjs7QUFDaEMsWUFBSTlCLFFBQVEsQ0FBQ0MsTUFBVCxLQUFvQixTQUF4QixFQUFtQztBQUNqQ3RELDBCQUFPYyxJQUFQLENBQVksK0NBQVo7QUFDRCxTQUZELE1BRU87QUFDTHVCLFVBQUFBLFFBQVEsR0FBR2xELElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSxpREFBWixFQUErRGdELFFBQVEsQ0FBQ0MsTUFBeEUsQ0FBWDs7QUFDQXRELDBCQUFPQyxLQUFQLENBQWFvQyxRQUFiO0FBQ0QsU0F0RVUsQ0F3RVg7OztBQUNBLGFBQUssTUFBTXpCLENBQVgsSUFBZ0JtQyxPQUFoQixFQUF5QjtBQUN2QixnQkFBTXFDLFFBQVEsR0FBR3JDLE9BQU8sQ0FBQ25DLENBQUQsQ0FBeEI7QUFDQSxnQkFBTWtELEVBQUUsR0FBR0gsU0FBUyxDQUFDL0MsQ0FBRCxDQUFwQjs7QUFDQVosMEJBQU9DLEtBQVAsQ0FBYSxpQ0FBYixFQUFnRDZELEVBQUUsQ0FBQ08sV0FBSCxFQUFoRDs7QUFDQSxjQUFJLE9BQU9lLFFBQVAsS0FBb0IsUUFBeEIsRUFBa0M7QUFDaENwRiw0QkFBT0MsS0FBUCxDQUFhbUYsUUFBYjtBQUNELFdBRkQsTUFFTztBQUNMLGdCQUFJLENBQUMvQyxRQUFMLEVBQWVBLFFBQVEsR0FBRytDLFFBQVEsQ0FBQ3JFLFFBQVQsRUFBWDs7QUFDZmYsNEJBQU9DLEtBQVAsQ0FBYW1GLFFBQVEsQ0FBQ3JFLFFBQVQsRUFBYjtBQUNEO0FBQ0Y7QUFDRixPQXBGRCxNQW9GTztBQUNMc0IsUUFBQUEsUUFBUSxHQUFHbEQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLCtEQUFaLENBQVg7O0FBQ0FMLHdCQUFPQyxLQUFQLENBQWFvQyxRQUFiO0FBQ0Q7QUFDRixLQXJKRCxDQXFKRSxPQUFPL0IsS0FBUCxFQUFjO0FBQ2ROLHNCQUFPTSxLQUFQLENBQWMsa0NBQWlDQSxLQUFLLENBQUNpQixLQUFNLEVBQTlDLEdBQWtEakIsS0FBSyxDQUFDaUIsS0FBeEQsR0FBZ0VqQixLQUE3RTs7QUFDQStCLE1BQUFBLFFBQVEsR0FBRy9CLEtBQUssQ0FBQ1MsUUFBTixFQUFYO0FBQ0Q7O0FBRUQsUUFBSSxDQUFDc0IsUUFBTCxFQUFlO0FBQ2IsWUFBTWpDLE9BQU8sR0FBR2pCLElBQUksQ0FBQ2tCLE1BQUwsQ0FDZCxvRkFEYyxFQUVkUixPQUZjLEVBRUxMLFdBRkssRUFFUThDLFVBRlIsQ0FBaEI7O0FBR0F0QyxzQkFBT2MsSUFBUCxDQUFZVixPQUFaLEVBSmEsQ0FNYjs7QUFDRDs7QUFDRCxVQUFNQSxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0NBQVosRUFBb0RnQyxRQUFwRCxDQUFoQjs7QUFDQXJDLG9CQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EsVUFBTSxJQUFJRyxLQUFKLENBQVVILE9BQVYsQ0FBTjtBQUNEOztBQUVELFFBQU0yRix1QkFBTixDQUE4QjNELFNBQTlCLEVBQXlDNUMsV0FBekMsRUFBc0RDLGFBQXRELEVBQXFFQyxHQUFyRSxFQUEwRUMsSUFBMUUsRUFBZ0ZDLFFBQWhGLEVBQTBGQyxPQUExRixFQUFtR21HLFlBQW5HLEVBQWlIO0FBQy9HaEcsb0JBQU9DLEtBQVAsQ0FBYWQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLDBGQUFaLEVBQXdHYixXQUF4RyxDQUFiOztBQUNBLFFBQUk2QyxRQUFRLEdBQUcsSUFBZjtBQUNBLFFBQUlDLFVBQVUsR0FBRyxJQUFqQjs7QUFDQSxRQUFJO0FBQ0Y7QUFDQSxZQUFNeEMsTUFBTSxHQUFHLE1BQU1WLEdBQUcsQ0FBQ1csb0JBQUosQ0FBeUIsTUFBekIsQ0FBckI7O0FBQ0FDLHNCQUFPQyxLQUFQLENBQWEsOERBQWIsRUFBNkVKLE9BQTdFOztBQUNBLFlBQU1LLE9BQU8sR0FBR0osTUFBTSxDQUFDbUcsWUFBUCxDQUFvQnpHLFdBQXBCLENBQWhCOztBQUNBLFVBQUksQ0FBQ1UsT0FBTCxFQUFjO0FBQ1osY0FBTUUsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNEQUFaLEVBQW9FYixXQUFwRSxDQUFoQjs7QUFDQVEsd0JBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQSxjQUFNLElBQUlHLEtBQUosQ0FBVUgsT0FBVixDQUFOO0FBQ0Q7O0FBQ0QsWUFBTXVDLElBQUksR0FBRzdDLE1BQU0sQ0FBQzhDLGdCQUFQLEVBQWIsQ0FWRSxDQVdGOztBQUNBTixNQUFBQSxVQUFVLEdBQUdLLElBQUksQ0FBQ0UsZ0JBQUwsRUFBYixDQVpFLENBY0Y7O0FBQ0EsWUFBTXJDLE9BQU8sR0FBRztBQUNkO0FBQ0FDLFFBQUFBLFdBQVcsRUFBRWhCLGFBRkM7QUFHZEMsUUFBQUEsR0FIYztBQUlkQyxRQUFBQSxJQUpjO0FBS2RtRCxRQUFBQSxPQUFPLEVBQUV0RCxXQUxLO0FBTWRtRCxRQUFBQTtBQU5jLE9BQWhCOztBQVNBLFVBQUlxRCxZQUFZLEtBQUssV0FBckIsRUFBa0M7QUFDaEM7QUFDQSxjQUFNRSxRQUFRLEdBQUdoRyxPQUFPLENBQUNpRyxXQUFSLENBQW9CLEVBQXBCLEVBQXdCLGVBQXhCLENBQWpCOztBQUNBLGFBQUtDLGVBQUwsR0FBdUIsRUFBdkI7O0FBQ0EsWUFBSUYsUUFBSixFQUFjO0FBQ1psRywwQkFBT0MsS0FBUCxDQUFhLDRCQUFiOztBQUNBLGVBQUssTUFBTVcsQ0FBWCxJQUFnQnNGLFFBQWhCLEVBQTBCO0FBQ3hCLGlCQUFLRSxlQUFMLENBQXFCdEIsSUFBckIsQ0FBMEJvQixRQUFRLENBQUN0RixDQUFELENBQVIsQ0FBWXlGLEtBQXRDO0FBQ0Q7QUFDRixTQVQrQixDQVdoQzs7O0FBQ0EsY0FBTWhELFFBQVEsR0FBRyxNQUFNbkQsT0FBTyxDQUFDb0csWUFBUixDQUFxQjtBQUFFQyxVQUFBQSxRQUFRLEVBQUUsSUFBWjtBQUFrQkMsVUFBQUEsV0FBVyxFQUFFO0FBQS9CLFNBQXJCLENBQXZCOztBQUNBLFlBQUluRCxRQUFRLENBQUNvRCxZQUFiLEVBQTJCO0FBQ3pCekcsMEJBQU9DLEtBQVAsQ0FBYSxpQ0FBYjs7QUFDQSxnQkFBTXlHLFVBQVUsR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVl2RCxRQUFRLENBQUNvRCxZQUFyQixDQUFuQjtBQUNBLGdCQUFNSSxVQUFVLEdBQUc7QUFBRUMsWUFBQUEsSUFBSSxFQUFFLElBQVI7QUFBY0MsWUFBQUEsU0FBUyxFQUFFO0FBQXpCLFdBQW5CO0FBQ0EsZ0JBQU1DLGFBQWEsR0FBRyxFQUF0Qjs7QUFDQSxlQUFLLE1BQU1wRyxDQUFYLElBQWdCOEYsVUFBaEIsRUFBNEI7QUFDMUIxRyw0QkFBT0MsS0FBUCxDQUFheUcsVUFBVSxDQUFDOUYsQ0FBRCxDQUF2Qjs7QUFDQSxpQkFBSyxNQUFNcUcsQ0FBWCxJQUFnQjVELFFBQVEsQ0FBQ29ELFlBQVQsQ0FBc0JDLFVBQVUsQ0FBQzlGLENBQUQsQ0FBaEMsRUFBcUNzRyxLQUFyRCxFQUE0RDtBQUMxREYsY0FBQUEsYUFBYSxDQUFDRixJQUFkLEdBQXFCekQsUUFBUSxDQUFDb0QsWUFBVCxDQUFzQkMsVUFBVSxDQUFDOUYsQ0FBRCxDQUFoQyxFQUFxQ3NHLEtBQXJDLENBQTJDRCxDQUEzQyxFQUE4Q0UsUUFBOUMsQ0FBdURDLEtBQXZELENBQTZELEdBQTdELEVBQWtFLENBQWxFLENBQXJCO0FBQ0FKLGNBQUFBLGFBQWEsQ0FBQ0QsU0FBZCxHQUEwQjFELFFBQVEsQ0FBQ29ELFlBQVQsQ0FBc0JDLFVBQVUsQ0FBQzlGLENBQUQsQ0FBaEMsRUFBcUNzRyxLQUFyQyxDQUEyQ0QsQ0FBM0MsRUFBOENGLFNBQXhFOztBQUNBL0csOEJBQU9DLEtBQVAsQ0FBYSwwQkFBYjs7QUFDQUQsOEJBQU9DLEtBQVAsQ0FBYSwyQkFBYixFQUEwQytHLGFBQWEsQ0FBQ0YsSUFBeEQsRUFBOERFLGFBQWEsQ0FBQ0QsU0FBNUU7O0FBRUEsbUJBQUtYLGVBQUwsQ0FBcUJ2QyxPQUFyQixDQUE2QndELFFBQVEsSUFBSTtBQUN2QyxvQkFBSUEsUUFBUSxLQUFLTCxhQUFhLENBQUNGLElBQTNCLElBQW1DRCxVQUFVLENBQUNFLFNBQVgsR0FBdUJDLGFBQWEsQ0FBQ0QsU0FBNUUsRUFBdUY7QUFDckZGLGtCQUFBQSxVQUFVLENBQUNDLElBQVgsR0FBa0JFLGFBQWEsQ0FBQ0YsSUFBaEM7QUFDQUQsa0JBQUFBLFVBQVUsQ0FBQ0UsU0FBWCxHQUF1QkMsYUFBYSxDQUFDRCxTQUFyQztBQUNEO0FBQ0YsZUFMRDtBQU1EO0FBQ0Y7O0FBQ0QvRywwQkFBT0MsS0FBUCxDQUFhLDhCQUFiLEVBQTZDNEcsVUFBVSxDQUFDQyxJQUF4RCxFQUE4REQsVUFBVSxDQUFDRSxTQUF6RTs7QUFDQXZHLFVBQUFBLE9BQU8sQ0FBQ3lCLE9BQVIsR0FBa0I0RSxVQUFVLENBQUNDLElBQTdCO0FBQ0QsU0F2QkQsTUF1Qk87QUFDTCxnQkFBTTFHLE9BQU8sR0FBR2pCLElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSx5QkFBWixDQUFoQjs7QUFDQUwsMEJBQU9NLEtBQVAsQ0FBYUYsT0FBYjtBQUNEO0FBQ0YsT0F4Q0QsTUF3Q08sSUFBSTRGLFlBQVksS0FBSyxVQUFyQixFQUFpQztBQUN0QyxjQUFNOUYsT0FBTyxDQUFDb0csWUFBUixDQUFxQjtBQUFFQyxVQUFBQSxRQUFRLEVBQUUsSUFBWjtBQUFrQkMsVUFBQUEsV0FBVyxFQUFFO0FBQS9CLFNBQXJCLENBQU47QUFDRCxPQWxFQyxDQW1FRjtBQUNBO0FBQ0E7OztBQUVBeEcsc0JBQU9DLEtBQVAsQ0FBYyxXQUFVZ0IsSUFBSSxDQUFDQyxTQUFMLENBQWVWLE9BQWYsQ0FBd0IsRUFBaEQ7O0FBRUEsWUFBTXVDLE9BQU8sR0FBRyxNQUFNN0MsT0FBTyxDQUFDOEMsdUJBQVIsQ0FBZ0N4QyxPQUFoQyxDQUF0QixDQXpFRSxDQTJFRjtBQUNBO0FBQ0E7O0FBQ0EsWUFBTXlDLGlCQUFpQixHQUFHRixPQUFPLENBQUMsQ0FBRCxDQUFqQztBQUNBLFlBQU1HLFFBQVEsR0FBR0gsT0FBTyxDQUFDLENBQUQsQ0FBeEIsQ0EvRUUsQ0FpRkY7QUFDQTtBQUNBOztBQUNBLFVBQUlJLE9BQU8sR0FBRyxJQUFkOztBQUNBLFdBQUssTUFBTXZDLENBQVgsSUFBZ0JxQyxpQkFBaEIsRUFBbUM7QUFDakMsWUFBSUcsT0FBTyxHQUFHLEtBQWQ7O0FBQ0EsWUFBSUgsaUJBQWlCLElBQUlBLGlCQUFpQixDQUFDckMsQ0FBRCxDQUFqQixDQUFxQnlDLFFBQTFDLElBQ0ZKLGlCQUFpQixDQUFDckMsQ0FBRCxDQUFqQixDQUFxQnlDLFFBQXJCLENBQThCQyxNQUE5QixLQUF5QyxHQUQzQyxFQUNnRDtBQUM5Q0YsVUFBQUEsT0FBTyxHQUFHLElBQVY7O0FBQ0FwRCwwQkFBT2MsSUFBUCxDQUFZLG9DQUFaO0FBQ0QsU0FKRCxNQUlPO0FBQ0xkLDBCQUFPTSxLQUFQLENBQWFXLElBQUksQ0FBQ0MsU0FBTCxDQUFlK0IsaUJBQWYsQ0FBYjs7QUFDQWpELDBCQUFPTSxLQUFQLENBQWEsbUNBQWI7QUFDRDs7QUFDRDZDLFFBQUFBLE9BQU8sSUFBSUMsT0FBWDtBQUNEOztBQUVELFVBQUlELE9BQUosRUFBYTtBQUNYbkQsd0JBQU9jLElBQVAsQ0FBWTNCLElBQUksQ0FBQ2tCLE1BQUwsQ0FDVixtSUFEVSxFQUVWNEMsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJDLE1BRnBCLEVBRTRCTCxpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCSSxRQUFyQixDQUE4QmpELE9BRjFELEVBR1Y2QyxpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCSSxRQUFyQixDQUE4QkUsT0FIcEIsRUFHNkJOLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJPLFdBQXJCLENBQWlDQyxTQUg5RCxDQUFaLEVBRFcsQ0FNWDtBQUNBOzs7QUFDQSxjQUFNQyxRQUFRLEdBQUcsRUFBakI7QUFDQSxjQUFNQyxTQUFTLEdBQUd6RCxPQUFPLENBQUMwRCx5QkFBUixFQUFsQjtBQUNBRCxRQUFBQSxTQUFTLENBQUNFLE9BQVYsQ0FBa0JDLEVBQUUsSUFBSTtBQUN0QjlELDBCQUFPQyxLQUFQLENBQWEsdUNBQWI7O0FBQ0EsZ0JBQU04RCxrQkFBa0IsR0FBRyxJQUFJQyxPQUFKLENBQVksQ0FBQ0MsT0FBRCxFQUFVQyxNQUFWLEtBQXFCO0FBQzFELGtCQUFNQyxTQUFTLEdBQUdDLFVBQVUsQ0FBQyxNQUFNO0FBQ2pDLG9CQUFNaEUsT0FBTyxHQUFJLG1CQUFrQjBELEVBQUUsQ0FBQ08sV0FBSCxFQUFpQixFQUFwRDs7QUFDQXJFLDhCQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EwRCxjQUFBQSxFQUFFLENBQUNRLFVBQUg7QUFDRCxhQUoyQixFQUl6QixLQUp5QixDQUE1QjtBQUtBUixZQUFBQSxFQUFFLENBQUNTLGVBQUgsQ0FBbUJqQyxVQUFuQixFQUErQixDQUFDa0MsRUFBRCxFQUFLQyxJQUFMLEVBQVdDLFFBQVgsS0FBd0I7QUFDckQxRSw4QkFBT2MsSUFBUCxDQUFZLDBFQUFaLEVBQXdGZ0QsRUFBRSxDQUFDTyxXQUFILEVBQXhGOztBQUNBckUsOEJBQU9jLElBQVAsQ0FBWSw2Q0FBWixFQUEyRDBELEVBQTNELEVBQStEQyxJQUEvRCxFQUFxRUMsUUFBckU7O0FBQ0FDLGNBQUFBLFlBQVksQ0FBQ1IsU0FBRCxDQUFaOztBQUdBLGtCQUFJTSxJQUFJLEtBQUssT0FBYixFQUFzQjtBQUNwQixzQkFBTXJFLE9BQU8sR0FBR2pCLElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSx1REFBWixFQUFxRW9FLElBQXJFLENBQWhCOztBQUNBekUsZ0NBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQThELGdCQUFBQSxNQUFNLENBQUMsSUFBSTNELEtBQUosQ0FBVUgsT0FBVixDQUFELENBQU47QUFDRCxlQUpELE1BSU87QUFDTCxzQkFBTUEsT0FBTyxHQUFHLDZDQUFoQjs7QUFDQUosZ0NBQU9jLElBQVAsQ0FBWVYsT0FBWjs7QUFDQTZELGdCQUFBQSxPQUFPLENBQUM3RCxPQUFELENBQVA7QUFDRDtBQUNGLGFBZkQsRUFlRzRCLEdBQUcsSUFBSTtBQUNSMkMsY0FBQUEsWUFBWSxDQUFDUixTQUFELENBQVo7O0FBQ0FuRSw4QkFBT00sS0FBUCxDQUFhMEIsR0FBYjs7QUFDQWtDLGNBQUFBLE1BQU0sQ0FBQ2xDLEdBQUQsQ0FBTjtBQUNELGFBbkJELEVBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBRTRDLGNBQUFBLFVBQVUsRUFBRSxJQUFkO0FBQW9CTixjQUFBQSxVQUFVLEVBQUU7QUFBaEMsYUF4QkE7QUEwQkFSLFlBQUFBLEVBQUUsQ0FBQ2UsT0FBSDtBQUNELFdBakMwQixDQUEzQjtBQWtDQW5CLFVBQUFBLFFBQVEsQ0FBQ29CLElBQVQsQ0FBY2Ysa0JBQWQ7QUFDRCxTQXJDRDtBQXVDQSxjQUFNZ0IsVUFBVSxHQUFHO0FBQ2pCcEMsVUFBQUEsSUFEaUI7QUFFakJNLFVBQUFBLGlCQUZpQjtBQUdqQkMsVUFBQUE7QUFIaUIsU0FBbkI7QUFLQSxjQUFNOEIsV0FBVyxHQUFHOUUsT0FBTyxDQUFDK0UsZUFBUixDQUF3QkYsVUFBeEIsQ0FBcEIsQ0F0RFcsQ0F1RFg7QUFDQTs7QUFDQXJCLFFBQUFBLFFBQVEsQ0FBQ29CLElBQVQsQ0FBY0UsV0FBZDtBQUNBLGNBQU1qQyxPQUFPLEdBQUcsTUFBTWlCLE9BQU8sQ0FBQ2tCLEdBQVIsQ0FBWXhCLFFBQVosQ0FBdEI7O0FBQ0ExRCx3QkFBT0MsS0FBUCxDQUFhZCxJQUFJLENBQUNrQixNQUFMLENBQVksaUNBQVosRUFBK0MwQyxPQUEvQyxDQUFiOztBQUNBLGNBQU1NLFFBQVEsR0FBR04sT0FBTyxDQUFDb0MsR0FBUixFQUFqQixDQTVEVyxDQTREcUI7O0FBQ2hDLFlBQUk5QixRQUFRLENBQUNDLE1BQVQsS0FBb0IsU0FBeEIsRUFBbUM7QUFDakN0RCwwQkFBT2MsSUFBUCxDQUFZLCtDQUFaO0FBQ0QsU0FGRCxNQUVPO0FBQ0x1QixVQUFBQSxRQUFRLEdBQUdsRCxJQUFJLENBQUNrQixNQUFMLENBQVksaURBQVosRUFBK0RnRCxRQUFRLENBQUNDLE1BQXhFLENBQVg7O0FBQ0F0RCwwQkFBT0MsS0FBUCxDQUFhb0MsUUFBYjtBQUNELFNBbEVVLENBb0VYOzs7QUFDQSxhQUFLLE1BQU16QixDQUFYLElBQWdCbUMsT0FBaEIsRUFBeUI7QUFDdkIsZ0JBQU1xQyxRQUFRLEdBQUdyQyxPQUFPLENBQUNuQyxDQUFELENBQXhCO0FBQ0EsZ0JBQU1rRCxFQUFFLEdBQUdILFNBQVMsQ0FBQy9DLENBQUQsQ0FBcEI7O0FBQ0FaLDBCQUFPQyxLQUFQLENBQWEsaUNBQWIsRUFBZ0Q2RCxFQUFFLENBQUNPLFdBQUgsRUFBaEQ7O0FBQ0EsY0FBSSxPQUFPZSxRQUFQLEtBQW9CLFFBQXhCLEVBQWtDO0FBQ2hDcEYsNEJBQU9DLEtBQVAsQ0FBYW1GLFFBQWI7QUFDRCxXQUZELE1BRU87QUFDTCxnQkFBSSxDQUFDL0MsUUFBTCxFQUFlQSxRQUFRLEdBQUcrQyxRQUFRLENBQUNyRSxRQUFULEVBQVg7O0FBQ2ZmLDRCQUFPQyxLQUFQLENBQWFtRixRQUFRLENBQUNyRSxRQUFULEVBQWI7QUFDRDtBQUNGO0FBQ0YsT0FoRkQsTUFnRk87QUFDTHNCLFFBQUFBLFFBQVEsR0FBR2xELElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSwrREFBWixDQUFYOztBQUNBTCx3QkFBT0MsS0FBUCxDQUFhb0MsUUFBYjtBQUNEO0FBQ0YsS0F0TEQsQ0FzTEUsT0FBTy9CLEtBQVAsRUFBYztBQUNkTixzQkFBT00sS0FBUCxDQUFjLGtDQUFpQ0EsS0FBSyxDQUFDaUIsS0FBTSxFQUE5QyxHQUFrRGpCLEtBQUssQ0FBQ2lCLEtBQXhELEdBQWdFakIsS0FBN0U7O0FBQ0ErQixNQUFBQSxRQUFRLEdBQUcvQixLQUFLLENBQUNTLFFBQU4sRUFBWDtBQUNEOztBQUVELFFBQUksQ0FBQ3NCLFFBQUwsRUFBZTtBQUNiLFlBQU1qQyxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQ2Qsb0ZBRGMsRUFFZFIsT0FGYyxFQUVMTCxXQUZLLEVBRVE4QyxVQUZSLENBQWhCOztBQUdBdEMsc0JBQU9jLElBQVAsQ0FBWVYsT0FBWjs7QUFFQSxhQUFPa0MsVUFBUDtBQUNEOztBQUNELFVBQU1sQyxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0NBQVosRUFBb0RnQyxRQUFwRCxDQUFoQjs7QUFDQXJDLG9CQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EsVUFBTSxJQUFJRyxLQUFKLENBQVVILE9BQVYsQ0FBTjtBQUNEOztBQUVELFFBQU1rSCxpQkFBTixDQUF3QmxGLFNBQXhCLEVBQW1DNUMsV0FBbkMsRUFBZ0RDLGFBQWhELEVBQStEQyxHQUEvRCxFQUFvRUMsSUFBcEUsRUFBMEVDLFFBQTFFLEVBQW9GQyxPQUFwRixFQUE2RjtBQUMzRkcsb0JBQU9DLEtBQVAsQ0FBYWQsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLGdFQUFaLEVBQThFYixXQUE5RSxDQUFiOztBQUNBLFFBQUk2QyxRQUFRLEdBQUcsSUFBZjtBQUNBLFFBQUlDLFVBQVUsR0FBRyxJQUFqQjs7QUFDQSxRQUFJO0FBQ0Y7QUFDQSxZQUFNeEMsTUFBTSxHQUFHLE1BQU1WLEdBQUcsQ0FBQ1csb0JBQUosQ0FBeUIsTUFBekIsQ0FBckI7O0FBQ0FDLHNCQUFPQyxLQUFQLENBQWEsOERBQWIsRUFBNkVKLE9BQTdFOztBQUNBLFlBQU1LLE9BQU8sR0FBR0osTUFBTSxDQUFDbUcsWUFBUCxDQUFvQnpHLFdBQXBCLENBQWhCOztBQUNBLFVBQUksQ0FBQ1UsT0FBTCxFQUFjO0FBQ1osY0FBTUUsT0FBTyxHQUFHakIsSUFBSSxDQUFDa0IsTUFBTCxDQUFZLHNEQUFaLEVBQW9FYixXQUFwRSxDQUFoQjs7QUFDQVEsd0JBQU9NLEtBQVAsQ0FBYUYsT0FBYjs7QUFDQSxjQUFNLElBQUlHLEtBQUosQ0FBVUgsT0FBVixDQUFOO0FBQ0Q7O0FBQ0QsWUFBTXVDLElBQUksR0FBRzdDLE1BQU0sQ0FBQzhDLGdCQUFQLEVBQWIsQ0FWRSxDQVdGOztBQUNBTixNQUFBQSxVQUFVLEdBQUdLLElBQUksQ0FBQ0UsZ0JBQUwsRUFBYixDQVpFLENBY0Y7O0FBQ0EsWUFBTXJDLE9BQU8sR0FBRztBQUNkeUIsUUFBQUEsT0FBTyxFQUFFRyxTQURLO0FBRWQzQixRQUFBQSxXQUFXLEVBQUVoQixhQUZDO0FBR2RDLFFBQUFBLEdBSGM7QUFJZEMsUUFBQUEsSUFKYztBQUtkbUQsUUFBQUEsT0FBTyxFQUFFdEQsV0FMSztBQU1kbUQsUUFBQUE7QUFOYyxPQUFoQjs7QUFRQTNDLHNCQUFPQyxLQUFQLENBQWMsV0FBVWdCLElBQUksQ0FBQ0MsU0FBTCxDQUFlVixPQUFmLENBQXdCLEVBQWhEOztBQUVBLFlBQU11QyxPQUFPLEdBQUcsTUFBTTdDLE9BQU8sQ0FBQ3FILHlCQUFSLENBQWtDL0csT0FBbEMsQ0FBdEIsQ0F6QkUsQ0EyQkY7QUFDQTtBQUNBOztBQUNBLFlBQU15QyxpQkFBaUIsR0FBR0YsT0FBTyxDQUFDLENBQUQsQ0FBakM7QUFDQSxZQUFNRyxRQUFRLEdBQUdILE9BQU8sQ0FBQyxDQUFELENBQXhCLENBL0JFLENBaUNGO0FBQ0E7QUFDQTs7QUFDQSxVQUFJSSxPQUFPLEdBQUcsSUFBZDs7QUFDQSxXQUFLLE1BQU12QyxDQUFYLElBQWdCcUMsaUJBQWhCLEVBQW1DO0FBQ2pDLFlBQUlHLE9BQU8sR0FBRyxLQUFkOztBQUNBLFlBQUlILGlCQUFpQixJQUFJQSxpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUExQyxJQUNGSixpQkFBaUIsQ0FBQ3JDLENBQUQsQ0FBakIsQ0FBcUJ5QyxRQUFyQixDQUE4QkMsTUFBOUIsS0FBeUMsR0FEM0MsRUFDZ0Q7QUFDOUNGLFVBQUFBLE9BQU8sR0FBRyxJQUFWOztBQUNBcEQsMEJBQU9jLElBQVAsQ0FBWSxvQ0FBWjtBQUNELFNBSkQsTUFJTztBQUNMZCwwQkFBT00sS0FBUCxDQUFhVyxJQUFJLENBQUNDLFNBQUwsQ0FBZStCLGlCQUFmLENBQWI7O0FBQ0FqRCwwQkFBT00sS0FBUCxDQUFhLG1DQUFiO0FBQ0Q7O0FBQ0Q2QyxRQUFBQSxPQUFPLElBQUlDLE9BQVg7QUFDRDs7QUFFRCxVQUFJRCxPQUFKLEVBQWE7QUFDWG5ELHdCQUFPYyxJQUFQLENBQVkzQixJQUFJLENBQUNrQixNQUFMLENBQ1YsbUlBRFUsRUFFVjRDLGlCQUFpQixDQUFDLENBQUQsQ0FBakIsQ0FBcUJJLFFBQXJCLENBQThCQyxNQUZwQixFQUU0QkwsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJqRCxPQUYxRCxFQUdWNkMsaUJBQWlCLENBQUMsQ0FBRCxDQUFqQixDQUFxQkksUUFBckIsQ0FBOEJFLE9BSHBCLEVBRzZCTixpQkFBaUIsQ0FBQyxDQUFELENBQWpCLENBQXFCTyxXQUFyQixDQUFpQ0MsU0FIOUQsQ0FBWjs7QUFLQSxjQUFNc0IsVUFBVSxHQUFHO0FBQ2pCcEMsVUFBQUEsSUFEaUI7QUFFakJNLFVBQUFBLGlCQUZpQjtBQUdqQkMsVUFBQUE7QUFIaUIsU0FBbkI7QUFNQWhELFFBQUFBLE9BQU8sQ0FBQ3NILGlCQUFSLENBQTBCekMsVUFBMUI7QUFDRCxPQWJELE1BYU87QUFDTDFDLFFBQUFBLFFBQVEsR0FBR2xELElBQUksQ0FBQ2tCLE1BQUwsQ0FBWSwrREFBWixDQUFYOztBQUNBTCx3QkFBT0MsS0FBUCxDQUFhb0MsUUFBYjtBQUNEO0FBQ0YsS0FuRUQsQ0FtRUUsT0FBTy9CLEtBQVAsRUFBYztBQUNkTixzQkFBT00sS0FBUCxDQUFjLGtDQUFpQ0EsS0FBSyxDQUFDaUIsS0FBTSxFQUE5QyxHQUFrRGpCLEtBQUssQ0FBQ2lCLEtBQXhELEdBQWdFakIsS0FBN0U7O0FBQ0ErQixNQUFBQSxRQUFRLEdBQUcvQixLQUFLLENBQUNTLFFBQU4sRUFBWDtBQUNEOztBQUVELFFBQUksQ0FBQ3NCLFFBQUwsRUFBZTtBQUNiLFlBQU1qQyxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQ2Qsb0ZBRGMsRUFFZFIsT0FGYyxFQUVMTCxXQUZLLEVBRVE4QyxVQUZSLENBQWhCOztBQUdBdEMsc0JBQU9jLElBQVAsQ0FBWVYsT0FBWjs7QUFFQSxhQUFPa0MsVUFBUDtBQUNEOztBQUNELFVBQU1sQyxPQUFPLEdBQUdqQixJQUFJLENBQUNrQixNQUFMLENBQVksc0NBQVosRUFBb0RnQyxRQUFwRCxDQUFoQjs7QUFDQXJDLG9CQUFPTSxLQUFQLENBQWFGLE9BQWI7O0FBQ0EsVUFBTSxJQUFJRyxLQUFKLENBQVVILE9BQVYsQ0FBTjtBQUNEOztBQW5oQ2E7O0FBc2hDaEIsTUFBTXFILFNBQVMsR0FBRyxJQUFJcEksU0FBSixFQUFsQjtBQUNBcUksTUFBTSxDQUFDQyxPQUFQLEdBQWlCRixTQUFqQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIHByZWZlci1kZXN0cnVjdHVyaW5nICovXG5cbi8qKlxuICpcbiAqIENvcHlyaWdodCAyMDE4IEtUIEFsbCBSaWdodHMgUmVzZXJ2ZWRcbiAqXG4gKi9cbmltcG9ydCBsb2dnZXIgZnJvbSAnLi4vLi4vY29tbW9uL2xvZ2dlcic7XG5cbmNvbnN0IFNoYXJlZCA9IHJlcXVpcmUoJ21tYXAtb2JqZWN0Jyk7XG5jb25zdCBwYXRoID0gcmVxdWlyZSgncGF0aCcpO1xuXG5jb25zdCB1dGlsID0gcmVxdWlyZSgndXRpbCcpO1xuY29uc3QgaGxmID0gcmVxdWlyZSgnLi4vaGxmLWNsaWVudCcpO1xuXG5jbGFzcyBUcnhDbGllbnQge1xuICBhc3luYyBxdWVyeUNoYWluQ29kZShwZWVyLCBjaGFubmVsTmFtZSwgY2hhaW5jb2RlTmFtZSwgZmNuLCBhcmdzLCB1c2VybmFtZSwgb3JnTmFtZSkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBobGYuZ2V0QWRtaW5DbGllbnRGb3JPcmcob3JnTmFtZSk7XG4gICAgICBsb2dnZXIuZGVidWcoJ1N1Y2Nlc3NmdWxseSBnb3QgdGhlIGZhYnJpYyBjbGllbnQgZm9yIHRoZSBvcmdhbml6YXRpb24gXCIlc1wiJywgb3JnTmFtZSk7XG4gICAgICBjb25zdCBjaGFubmVsID0gY2xpZW50LmdldENoYW5uZWwoY2hhbm5lbE5hbWUpO1xuICAgICAgaWYgKCFjaGFubmVsKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnQ2hhbm5lbCAlcyB3YXMgbm90IGRlZmluZWQgaW4gdGhlIGNvbm5lY3Rpb24gcHJvZmlsZScsIGNoYW5uZWxOYW1lKTtcbiAgICAgICAgbG9nZ2VyLmVycm9yKG1lc3NhZ2UpO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgICB9XG5cbiAgICAgIC8vIHNlbmQgcXVlcnlcbiAgICAgIGNvbnN0IHJlcXVlc3QgPSB7XG4gICAgICAgIC8vIHRhcmdldHM6IFtwZWVyXSwgLy8gcXVlcnlCeUNoYWluY29kZSBhbGxvd3MgZm9yIG11bHRpcGxlIHRhcmdldHNcbiAgICAgICAgY2hhaW5jb2RlSWQ6IGNoYWluY29kZU5hbWUsXG4gICAgICAgIGZjbixcbiAgICAgICAgYXJncyxcbiAgICAgIH07XG4gICAgICBjb25zdCByZXNwb25zZVBheUxvYWRzID0gYXdhaXQgY2hhbm5lbC5xdWVyeUJ5Q2hhaW5jb2RlKHJlcXVlc3QpO1xuICAgICAgaWYgKHJlc3BvbnNlUGF5TG9hZHMpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByZXNwb25zZVBheUxvYWRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oYCR7YXJnc1swXX0gbm93IGhhcyAke3Jlc3BvbnNlUGF5TG9hZHNbaV0udG9TdHJpbmcoJ3V0ZjgnKX0gYWZ0ZXIgdGhlIG1vdmVgKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCB0eEluZm8gPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgVHhfbXNnX3F1ZXJ5OiAnUVVFUlkgVFJBTlNBQ1RJT04nLFxuICAgICAgICAgIFR4X2ZjbjogZmNuLFxuICAgICAgICB9KTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKHR4SW5mbyk7XG4gICAgICAgIC8vIHJldHVybiBgJHthcmdzWzBdfSBub3cgaGFzICR7cmVzcG9uc2VQYXlMb2Fkc1swXS50b1N0cmluZygndXRmOCcpfWA7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlVmFsdWUgPSByZXNwb25zZVBheUxvYWRzWzBdLnRvU3RyaW5nKCd1dGY4Jyk7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgdmFsdWU6IHJlc3BvbnNlVmFsdWUsXG4gICAgICAgIH07XG4gICAgICB9XG4gICAgICBsb2dnZXIuZXJyb3IoJ3Jlc3BvbnNlUGF5TG9hZHMgaXMgbnVsbCcpO1xuICAgICAgcmV0dXJuICdyZXNwb25zZVBheUxvYWRzIGlzIG51bGwnO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBsb2dnZXIuZXJyb3IoYEZhaWxlZCB0byBxdWVyeSBkdWUgdG8gZXJyb3I6ICR7ZXJyb3Iuc3RhY2t9YCA/IGVycm9yLnN0YWNrIDogZXJyb3IpO1xuICAgICAgcmV0dXJuIGVycm9yLnRvU3RyaW5nKCk7XG4gICAgfVxuICB9XG5cbiAgYXN5bmMgcXVlcnlDaGFpbkNvZGVGb3JBbmNob3IoY2hhbm5lbE5hbWUsIGNoYWluY29kZU5hbWUsIGZjbiwgYXJncywgdXNlcm5hbWUpIHtcbiAgICBsZXQgcGVlciA9IG51bGw7Ly8gbG9jYXRpb24ucGVlcjtcbiAgICAvLyBsZXQgb3JkZXJlciA9IG51bGw7Ly8gbG9jYXRpb24ub3JkZXJlcjtcbiAgICBsZXQgb3JnTmFtZSA9IG51bGw7IC8vIC8vIGxvY2F0aW9uLm9yZ05hbWU7XG5cbiAgICB0cnkge1xuICAgICAgY29uc3QgcmVhZF9vbmx5X3NoYXJlZF9vYmplY3QgPSBuZXcgU2hhcmVkLk9wZW4ocGF0aC5qb2luKF9fZGlybmFtZSwgJy4uLy4uL2FwcC8nLCAncGVlci1vcmRlcmVyLWRlZmF1bHQnKSk7XG4gICAgICAvLyBvcmRlcmVyID0gcmVhZF9vbmx5X3NoYXJlZF9vYmplY3QuY2hlY2tlZF9vcmRlcmVyO1xuICAgICAgcGVlciA9IHJlYWRfb25seV9zaGFyZWRfb2JqZWN0LmNoZWNrZWRfcGVlcjtcbiAgICAgIG9yZ05hbWUgPSByZWFkX29ubHlfc2hhcmVkX29iamVjdC5jaGVja2VkX29yZztcbiAgICAgIHJlYWRfb25seV9zaGFyZWRfb2JqZWN0LmNsb3NlKCk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBsb2dnZXIuZGVidWcoZXJyKTtcbiAgICB9XG5cblxuXG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IGNsaWVudCA9IGF3YWl0IGhsZi5nZXRBZG1pbkNsaWVudEZvck9yZyhvcmdOYW1lKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCB0aGUgZmFicmljIGNsaWVudCBmb3IgdGhlIG9yZ2FuaXphdGlvbiBcIiVzXCInLCBvcmdOYW1lKTtcbiAgICAgIGNvbnN0IGNoYW5uZWwgPSBjbGllbnQuZ2V0Q2hhbm5lbChjaGFubmVsTmFtZSk7XG4gICAgICBpZiAoIWNoYW5uZWwpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdDaGFubmVsICVzIHdhcyBub3QgZGVmaW5lZCBpbiB0aGUgY29ubmVjdGlvbiBwcm9maWxlJywgY2hhbm5lbE5hbWUpO1xuICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgIH1cblxuICAgICAgLy8gc2VuZCBxdWVyeVxuICAgICAgY29uc3QgcmVxdWVzdCA9IHtcbiAgICAgICAgdGFyZ2V0czogcGVlciwgLy8gcXVlcnlCeUNoYWluY29kZSBhbGxvd3MgZm9yIG11bHRpcGxlIHRhcmdldHNcbiAgICAgICAgY2hhaW5jb2RlSWQ6IGNoYWluY29kZU5hbWUsXG4gICAgICAgIGZjbixcbiAgICAgICAgYXJncyxcbiAgICAgIH07XG4gICAgICBpZiAocGVlcikge1xuICAgICAgICByZXF1ZXN0LnRhcmdldHMgPSBbcGVlcl07XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHJlc3BvbnNlUGF5TG9hZHMgPSBhd2FpdCBjaGFubmVsLnF1ZXJ5QnlDaGFpbmNvZGUocmVxdWVzdCk7XG4gICAgICBpZiAocmVzcG9uc2VQYXlMb2Fkcykge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3BvbnNlUGF5TG9hZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBpZiAocmVzcG9uc2VQYXlMb2Fkc1tpXVswXSkge1xuICAgICAgICAgICAgbG9nZ2VyLmluZm8oYHZhbHVlIG9mICR7YXJnc1swXX0gaXMgJHtyZXNwb25zZVBheUxvYWRzW2ldLnRvU3RyaW5nKCd1dGY4Jyl9IGApO1xuICAgICAgICAgICAgcmV0dXJuIGAke3Jlc3BvbnNlUGF5TG9hZHNbaV0udG9TdHJpbmcoJ3V0ZjgnKX1gO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgbG9nZ2VyLmVycm9yKCdyZXNwb25zZVBheUxvYWRzIGlzIG51bGwnKTtcbiAgICAgIHJldHVybiAncmVzcG9uc2VQYXlMb2FkcyBpcyBudWxsJztcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgbG9nZ2VyLmVycm9yKGBGYWlsZWQgdG8gcXVlcnkgZHVlIHRvIGVycm9yOiAke2Vycm9yLnN0YWNrfWAgPyBlcnJvci5zdGFjayA6IGVycm9yKTtcbiAgICAgIHJldHVybiBlcnJvci50b1N0cmluZygpO1xuICAgIH1cbiAgfVxuXG5cbiAgYXN5bmMgcXVlcnlDaGFpbkNvZGVGb3JNZXNzYWdlKHBlZXIsY2hhbm5lbE5hbWUsIGNoYWluY29kZU5hbWUsIGZjbiwgYXJncywgdXNlcm5hbWUsb3JnTmFtZSkge1xuXG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IGNsaWVudCA9IGF3YWl0IGhsZi5nZXRBZG1pbkNsaWVudEZvck9yZyhvcmdOYW1lKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZyhjbGllbnQpXG4gICAgICBsb2dnZXIuZGVidWcoJ1N1Y2Nlc3NmdWxseSBnb3QgdGhlIGZhYnJpYyBjbGllbnQgZm9yIHRoZSBvcmdhbml6YXRpb24gXCIlc1wiJywgb3JnTmFtZSk7XG4gICAgICBjb25zdCBjaGFubmVsID0gY2xpZW50LmdldENoYW5uZWwoY2hhbm5lbE5hbWUpO1xuICAgICAgaWYgKCFjaGFubmVsKSB7XG4gICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnQ2hhbm5lbCAlcyB3YXMgbm90IGRlZmluZWQgaW4gdGhlIGNvbm5lY3Rpb24gcHJvZmlsZScsIGNoYW5uZWxOYW1lKTtcbiAgICAgICAgbG9nZ2VyLmVycm9yKG1lc3NhZ2UpO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgICB9XG5cbiAgICAgIC8vIHNlbmQgcXVlcnlcbiAgICAgIGNvbnN0IHJlcXVlc3QgPSB7XG4gICAgICAgIC8vIHRhcmdldHM6IHBlZXIsIC8vIHF1ZXJ5QnlDaGFpbmNvZGUgYWxsb3dzIGZvciBtdWx0aXBsZSB0YXJnZXRzXG4gICAgICAgIGNoYWluY29kZUlkOiBjaGFpbmNvZGVOYW1lLFxuICAgICAgICBmY24sXG4gICAgICAgIGFyZ3MsXG4gICAgICB9O1xuICAgICAgaWYgKHBlZXIpIHtcbiAgICAgICAgcmVxdWVzdC50YXJnZXRzID0gW3BlZXJdO1xuICAgICAgfVxuXG4gICAgICBjb25zdCByZXNwb25zZVBheUxvYWRzID0gYXdhaXQgY2hhbm5lbC5xdWVyeUJ5Q2hhaW5jb2RlKHJlcXVlc3QpO1xuICAgICAgaWYgKHJlc3BvbnNlUGF5TG9hZHMpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByZXNwb25zZVBheUxvYWRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgaWYgKHJlc3BvbnNlUGF5TG9hZHNbaV1bMF0pIHtcbiAgICAgICAgICAgIGxvZ2dlci5pbmZvKGB2YWx1ZSBvZiAke2FyZ3NbMF19IGlzICR7cmVzcG9uc2VQYXlMb2Fkc1tpXS50b1N0cmluZygndXRmOCcpfSBgKTtcbiAgICAgICAgICAgIHJldHVybiBgJHtyZXNwb25zZVBheUxvYWRzW2ldLnRvU3RyaW5nKCd1dGY4Jyl9YDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGxvZ2dlci5lcnJvcigncmVzcG9uc2VQYXlMb2FkcyBpcyBudWxsJyk7XG4gICAgICByZXR1cm4gJ3Jlc3BvbnNlUGF5TG9hZHMgaXMgbnVsbCc7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGxvZ2dlci5lcnJvcihgRmFpbGVkIHRvIHF1ZXJ5IGR1ZSB0byBlcnJvcjogJHtlcnJvci5zdGFja31gID8gZXJyb3Iuc3RhY2sgOiBlcnJvcik7XG4gICAgICByZXR1cm4gZXJyb3IudG9TdHJpbmcoKTtcbiAgICB9XG4gIH1cblxuICBhc3luYyBpbnZva2VDaGFpbkNvZGUocGVlck5hbWVzLCBjaGFubmVsTmFtZSwgY2hhaW5jb2RlTmFtZSwgZmNuLCBhcmdzLCB1c2VybmFtZSwgb3JnTmFtZSkge1xuICAgIGxvZ2dlci5kZWJ1Zyh1dGlsLmZvcm1hdCgnXFxuPT09PT09PT09PT09IGludm9rZUNoYWluQ29kZSA6IGludm9rZSB0cmFuc2FjdGlvbiBvbiBjaGFubmVsICVzID09PT09PT09PT09PVxcbicsIGNoYW5uZWxOYW1lKSk7XG4gICAgbGV0IGVycm9yTXNnID0gbnVsbDtcbiAgICBsZXQgdHhJZFN0cmluZyA9IG51bGw7XG4gICAgbGV0IGJsb2NrSGVpZ2h0ID0gbnVsbDtcbiAgICBsZXQgcmVzcG9uc2VQYXlsb2FkID0gbnVsbDtcbiAgICBsZXQgcmVzcG9uc2VTdGF0dXMgPSBudWxsO1xuICAgIGxldCByZXNwb25zZU1lc3NhZ2UgPSBudWxsO1xuXG4gICAgdHJ5IHtcbiAgICAgIC8vIGZpcnN0IHNldHVwIHRoZSBjbGllbnQgZm9yIHRoaXMgb3JnXG4gICAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBobGYuZ2V0QWRtaW5DbGllbnRGb3JPcmcoJ09yZzEnKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCB0aGUgZmFicmljIGNsaWVudCBmb3IgdGhlIG9yZ2FuaXphdGlvbiBcIiVzXCInLCBvcmdOYW1lKTtcbiAgICAgIGNvbnN0IGNoYW5uZWwgPSBjbGllbnQuZ2V0Q2hhbm5lbChjaGFubmVsTmFtZSk7XG4gICAgICBpZiAoIWNoYW5uZWwpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdDaGFubmVsICVzIHdhcyBub3QgZGVmaW5lZCBpbiB0aGUgY29ubmVjdGlvbiBwcm9maWxlJywgY2hhbm5lbE5hbWUpO1xuICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHR4SWQgPSBjbGllbnQubmV3VHJhbnNhY3Rpb25JRCgpO1xuICAgICAgLy8gd2lsbCBuZWVkIHRoZSB0cmFuc2FjdGlvbiBJRCBzdHJpbmcgZm9yIHRoZSBldmVudCByZWdpc3RyYXRpb24gbGF0ZXJcbiAgICAgIHR4SWRTdHJpbmcgPSB0eElkLmdldFRyYW5zYWN0aW9uSUQoKTtcblxuICAgICAgLy8gc2VuZCBwcm9wb3NhbCB0byBlbmRvcnNlclxuICAgICAgY29uc3QgcmVxdWVzdCA9IHtcbiAgICAgICAgdGFyZ2V0czogJ3BlZXIwLm9yZzEuZXhhbXBsZS5jb20nLFxuICAgICAgICBjaGFpbmNvZGVJZDogY2hhaW5jb2RlTmFtZSxcbiAgICAgICAgZmNuLFxuICAgICAgICBhcmdzLFxuICAgICAgICBjaGFpbklkOiBjaGFubmVsTmFtZSxcbiAgICAgICAgdHhJZCxcbiAgICAgIH07XG4gICAgICBsb2dnZXIuZGVidWcoYHJlcXVlc3Q6JHtKU09OLnN0cmluZ2lmeShyZXF1ZXN0KX1gKTtcblxuICAgICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IGNoYW5uZWwuc2VuZFRyYW5zYWN0aW9uUHJvcG9zYWwocmVxdWVzdCk7XG5cbiAgICAgIC8vIHRoZSByZXR1cm5lZCBvYmplY3QgaGFzIGJvdGggdGhlIGVuZG9yc2VtZW50IHJlc3VsdHNcbiAgICAgIC8vIGFuZCB0aGUgYWN0dWFsIHByb3Bvc2FsLCB0aGUgcHJvcG9zYWwgd2lsbCBiZSBuZWVkZWRcbiAgICAgIC8vIGxhdGVyIHdoZW4gd2Ugc2VuZCBhIHRyYW5zYWN0aW9uIHRvIHRoZSBvcmRlcmVyXG4gICAgICBjb25zdCBwcm9wb3NhbFJlc3BvbnNlcyA9IHJlc3VsdHNbMF07XG4gICAgICBjb25zdCBwcm9wb3NhbCA9IHJlc3VsdHNbMV07XG5cbiAgICAgIC8vIGxldHMgaGF2ZSBhIGxvb2sgYXQgdGhlIHJlc3BvbnNlcyB0byBzZWUgaWYgdGhleSBhcmVcbiAgICAgIC8vIGFsbCBnb29kLCBpZiBnb29kIHRoZXkgd2lsbCBhbHNvIGluY2x1ZGUgc2lnbmF0dXJlc1xuICAgICAgLy8gcmVxdWlyZWQgdG8gYmUgY29tbWl0dGVkXG4gICAgICBsZXQgYWxsR29vZCA9IHRydWU7XG4gICAgICBmb3IgKGNvbnN0IGkgaW4gcHJvcG9zYWxSZXNwb25zZXMpIHtcbiAgICAgICAgbGV0IG9uZUdvb2QgPSBmYWxzZTtcbiAgICAgICAgaWYgKHByb3Bvc2FsUmVzcG9uc2VzICYmIHByb3Bvc2FsUmVzcG9uc2VzW2ldLnJlc3BvbnNlICYmXG4gICAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1tpXS5yZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgIG9uZUdvb2QgPSB0cnVlO1xuICAgICAgICAgIGxvZ2dlci5pbmZvKCdpbnZva2UgY2hhaW5jb2RlIHByb3Bvc2FsIHdhcyBnb29kJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVzcG9uc2VTdGF0dXMgPSBwcm9wb3NhbFJlc3BvbnNlc1tpXS5zdGF0dXM7XG4gICAgICAgICAgcmVzcG9uc2VNZXNzYWdlID0gcHJvcG9zYWxSZXNwb25zZXNbaV0ubWVzc2FnZTtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoSlNPTi5zdHJpbmdpZnkocHJvcG9zYWxSZXNwb25zZXMpKTtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoJ2ludm9rZSBjaGFpbmNvZGUgcHJvcG9zYWwgd2FzIGJhZCcpO1xuICAgICAgICB9XG4gICAgICAgIGFsbEdvb2QgJj0gb25lR29vZDtcbiAgICAgIH1cblxuICAgICAgaWYgKGFsbEdvb2QpIHtcbiAgICAgICAgcmVzcG9uc2VTdGF0dXMgPSBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5zdGF0dXM7XG4gICAgICAgIHJlc3BvbnNlUGF5bG9hZCA9IHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLnBheWxvYWQ7XG4gICAgICAgIGxvZ2dlci5pbmZvKHV0aWwuZm9ybWF0KFxuICAgICAgICAgICdTdWNjZXNzZnVsbHkgc2VudCBQcm9wb3NhbCBhbmQgcmVjZWl2ZWQgUHJvcG9zYWxSZXNwb25zZTogU3RhdHVzIC0gJXMsIG1lc3NhZ2UgLSBcIiVzXCIsIG1ldGFkYXRhIC0gXCIlc1wiLCBlbmRvcnNlbWVudCBzaWduYXR1cmU6ICVzJyxcbiAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5zdGF0dXMsIHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLm1lc3NhZ2UsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXNbMF0ucmVzcG9uc2UucGF5bG9hZCwgcHJvcG9zYWxSZXNwb25zZXNbMF0uZW5kb3JzZW1lbnQuc2lnbmF0dXJlKSk7XG5cbiAgICAgICAgLy8gd2FpdCBmb3IgdGhlIGNoYW5uZWwtYmFzZWQgZXZlbnQgaHViIHRvIHRlbGwgdXNcbiAgICAgICAgLy8gdGhhdCB0aGUgY29tbWl0IHdhcyBnb29kIG9yIGJhZCBvbiBlYWNoIHBlZXIgaW4gb3VyIG9yZ2FuaXphdGlvblxuICAgICAgICBjb25zdCBwcm9taXNlcyA9IFtdO1xuICAgICAgICBjb25zdCBldmVudEh1YnMgPSBjaGFubmVsLmdldENoYW5uZWxFdmVudEh1YnNGb3JPcmcoKTtcbiAgICAgICAgZXZlbnRIdWJzLmZvckVhY2goZWggPT4ge1xuICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnaW52b2tlRXZlbnRQcm9taXNlIC0gc2V0dGluZyB1cCBldmVudCcpO1xuICAgICAgICAgIGNvbnN0IGludm9rZUV2ZW50UHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGVoVGltZU91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlID0gYFJFUVVFU1RfVElNRU9VVDoke2VoLmdldFBlZXJBZGRyKCl9YDtcbiAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKG1lc3NhZ2UpO1xuICAgICAgICAgICAgICBlaC5kaXNjb25uZWN0KCk7XG4gICAgICAgICAgICB9LCAzMDAwKTtcbiAgICAgICAgICAgIGVoLnJlZ2lzdGVyVHhFdmVudCh0eElkU3RyaW5nLCAodHgsIGNvZGUsIGJsb2NrTnVtKSA9PiB7XG4gICAgICAgICAgICAgIGxvZ2dlci5pbmZvKCdUaGUgY2hhaW5jb2RlIGludm9rZSBjaGFpbmNvZGUgdHJhbnNhY3Rpb24gaGFzIGJlZW4gY29tbWl0dGVkIG9uIHBlZXIgJXMnLCBlaC5nZXRQZWVyQWRkcigpKTtcbiAgICAgICAgICAgICAgbG9nZ2VyLmluZm8oJ1RyYW5zYWN0aW9uICVzIGhhcyBzdGF0dXMgb2YgJXMgaW4gYmxvY2sgJXMnLCB0eCwgY29kZSwgYmxvY2tOdW0pO1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZWhUaW1lT3V0KTtcbiAgICAgICAgICAgICAgYmxvY2tIZWlnaHQgPSBibG9ja051bTtcblxuICAgICAgICAgICAgICBpZiAoY29kZSAhPT0gJ1ZBTElEJykge1xuICAgICAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnVGhlIGludm9rZSBjaGFpbmNvZGUgdHJhbnNhY3Rpb24gd2FzIGludmFsaWQsIGNvZGU6JXMnLCBjb2RlKTtcbiAgICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcihtZXNzYWdlKSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9ICdUaGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiB3YXMgdmFsaWQuJztcbiAgICAgICAgICAgICAgICBsb2dnZXIuaW5mbyhtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZXNvbHZlKG1lc3NhZ2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCBlcnIgPT4ge1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZWhUaW1lT3V0KTtcbiAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKGVycik7XG4gICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgLy8gdGhlIGRlZmF1bHQgZm9yICd1bnJlZ2lzdGVyJyBpcyB0cnVlIGZvciB0cmFuc2FjdGlvbiBsaXN0ZW5lcnNcbiAgICAgICAgICAgICAgLy8gc28gbm8gcmVhbCBuZWVkIHRvIHNldCBoZXJlLCBob3dldmVyIGZvciAnZGlzY29ubmVjdCdcbiAgICAgICAgICAgICAgLy8gdGhlIGRlZmF1bHQgaXMgZmFsc2UgYXMgbW9zdCBldmVudCBodWJzIGFyZSBsb25nIHJ1bm5pbmdcbiAgICAgICAgICAgICAgLy8gaW4gdGhpcyB1c2UgY2FzZSB3ZSBhcmUgdXNpbmcgaXQgb25seSBvbmNlXG4gICAgICAgICAgICB7IHVucmVnaXN0ZXI6IHRydWUsIGRpc2Nvbm5lY3Q6IHRydWUgfSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBlaC5jb25uZWN0KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcHJvbWlzZXMucHVzaChpbnZva2VFdmVudFByb21pc2UpO1xuICAgICAgICB9KTtcblxuICAgICAgICBjb25zdCBvcmRlcmVyUmVxID0ge1xuICAgICAgICAgIHR4SWQsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXMsXG4gICAgICAgICAgcHJvcG9zYWwsXG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHNlbmRQcm9taXNlID0gY2hhbm5lbC5zZW5kVHJhbnNhY3Rpb24ob3JkZXJlclJlcSk7XG4gICAgICAgIC8vIHB1dCB0aGUgc2VuZCB0byB0aGUgb3JkZXJlciBsYXN0IHNvIHRoYXQgdGhlIGV2ZW50cyBnZXQgcmVnaXN0ZXJlZCBhbmRcbiAgICAgICAgLy8gYXJlIHJlYWR5IGZvciB0aGUgb3JkZXJlcmluZyBhbmQgY29tbWl0dGluZ1xuICAgICAgICBwcm9taXNlcy5wdXNoKHNlbmRQcm9taXNlKTtcbiAgICAgICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IFByb21pc2UuYWxsKHByb21pc2VzKTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKHV0aWwuZm9ybWF0KCctLS0tLS0tPj4+IFIgRSBTIFAgTyBOIFMgRSA6ICVqJywgcmVzdWx0cykpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IHJlc3VsdHMucG9wKCk7IC8vICBvcmRlcmVyIHJlc3VsdHMgYXJlIGxhc3QgaW4gdGhlIHJlc3VsdHNcbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gJ1NVQ0NFU1MnKSB7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oJ1N1Y2Nlc3NmdWxseSBzZW50IHRyYW5zYWN0aW9uIHRvIHRoZSBvcmRlcmVyLicpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGVycm9yTXNnID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBvcmRlciB0aGUgdHJhbnNhY3Rpb24uIEVycm9yIGNvZGU6ICVzJywgcmVzcG9uc2Uuc3RhdHVzKTtcbiAgICAgICAgICBsb2dnZXIuZGVidWcoZXJyb3JNc2cpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gbm93IHNlZSB3aGF0IGVhY2ggb2YgdGhlIGV2ZW50IGh1YnMgcmVwb3J0ZWRcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHJlc3VsdHMpIHtcbiAgICAgICAgICBjb25zdCBlaFJlc3VsdCA9IHJlc3VsdHNbaV07XG4gICAgICAgICAgY29uc3QgZWggPSBldmVudEh1YnNbaV07XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCdFdmVudCByZXN1bHRzIGZvciBldmVudCBodWIgOiVzJywgZWguZ2V0UGVlckFkZHIoKSk7XG4gICAgICAgICAgaWYgKHR5cGVvZiBlaFJlc3VsdCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmICghZXJyb3JNc2cpIGVycm9yTXNnID0gZWhSZXN1bHQudG9TdHJpbmcoKTtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdC50b1N0cmluZygpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGVycm9yTXNnID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBzZW5kIFByb3Bvc2FsIGFuZCByZWNlaXZlIGFsbCBnb29kIFByb3Bvc2FsUmVzcG9uc2UnKTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKGVycm9yTXNnKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgbG9nZ2VyLmVycm9yKGBcbiAgICAgICAgICAgICAgfUZhaWxlZCB0byBpbnZva2UgZHVlIHRvIGVycm9yOiAke2Vycm9yLnN0YWNrfWAgPyBlcnJvci5zdGFjayA6IGVycm9yKTtcbiAgICAgIGVycm9yTXNnID0gZXJyb3IudG9TdHJpbmcoKTtcbiAgICB9XG5cbiAgICBpZiAoIWVycm9yTXNnKSB7XG4gICAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoXG4gICAgICAgICdTdWNjZXNzZnVsbHkgaW52b2tlZCB0aGUgY2hhaW5jb2RlICVzIHRvIHRoZSBjaGFubmVsIFxcJyVzXFwnIGZvciB0cmFuc2FjdGlvbiBJRDogJXMnLFxuICAgICAgICBvcmdOYW1lLCBjaGFubmVsTmFtZSwgdHhJZFN0cmluZyk7XG4gICAgICBsb2dnZXIuaW5mbyhtZXNzYWdlKTtcblxuICAgICAgY29uc3QgdHhJbmZvID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICBUeF9tc2dfaW52b2tlOiAnSU5WT0tFIFRSQU5TQUNUSU9OJyxcbiAgICAgICAgVHhfSUQ6IHR4SWRTdHJpbmcsXG4gICAgICAgIFR4X2Jsb2NrTnVtOiBibG9ja0hlaWdodCxcbiAgICAgICAgVHhfY2hhbm5lbDogY2hhbm5lbE5hbWUsXG4gICAgICAgIFR4X29yZzogb3JnTmFtZSxcbiAgICAgICAgVHhfZmNuOiBmY24sXG4gICAgICB9KTtcbiAgICAgIGxvZ2dlci5kZWJ1Zyh0eEluZm8pO1xuICAgICAgbG9nZ2VyLmRlYnVnKCdyZXNwb25zZVBheWxvYWQgaXMgOiVzJywgcmVzcG9uc2VQYXlsb2FkKTtcblxuICAgICAgY29uc3QgcGF5bG9hZFN0cmluZyA9IHJlc3BvbnNlUGF5bG9hZC50b1N0cmluZygpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzOiByZXNwb25zZVN0YXR1cyxcbiAgICAgICAgdHhJZDogdHhJZFN0cmluZyxcbiAgICAgICAgbWVzc2FnZTogcGF5bG9hZFN0cmluZyxcbiAgICAgIH07XG4gICAgfVxuICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIGludm9rZSBjaGFpbmNvZGUuIGNhdXNlOiVzJywgZXJyb3JNc2cpO1xuICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAvLyB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1czogcmVzcG9uc2VTdGF0dXMsXG4gICAgICBtZXNzYWdlOiByZXNwb25zZU1lc3NhZ2UsXG4gICAgfTtcbiAgfVxuXG4gIGFzeW5jIGludm9rZUNoYWluQ29kZUZvckFuY2hvcihjaGFubmVsTmFtZSwgY2hhaW5jb2RlTmFtZSwgZmNuLCB2YWx1ZSwgdXNlcm5hbWUpIHtcbiAgICBsb2dnZXIuZGVidWcodXRpbC5mb3JtYXQoJ1xcbj09PT09PT09PT09PSBpbnZva2VDaGFpbkNvZGUgOiBpbnZva2UgdHJhbnNhY3Rpb24gb24gY2hhbm5lbCAlcyA9PT09PT09PT09PT1cXG4nLCBjaGFubmVsTmFtZSkpO1xuICAgIGxldCBlcnJvck1zZyA9IG51bGw7XG4gICAgbGV0IHR4SWRTdHJpbmcgPSBudWxsO1xuICAgIGNvbnN0IGFyZ3MgPSBbXTtcblxuXG4gICAgbGV0IHBlZXIgPSBudWxsOy8vIGxvY2F0aW9uLnBlZXI7XG4gICAgbGV0IG9yZGVyZXIgPSBudWxsOy8vIGxvY2F0aW9uLm9yZGVyZXI7XG4gICAgbGV0IG9yZ05hbWUgPSBudWxsOyAvLyAvLyBsb2NhdGlvbi5vcmdOYW1lO1xuXG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHJlYWRfb25seV9zaGFyZWRfb2JqZWN0ID0gbmV3IFNoYXJlZC5PcGVuKHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi8uLi9hcHAvJywgJ3BlZXItb3JkZXJlci1kZWZhdWx0JykpO1xuICAgICAgb3JkZXJlciA9IHJlYWRfb25seV9zaGFyZWRfb2JqZWN0LmNoZWNrZWRfb3JkZXJlcjtcbiAgICAgIHBlZXIgPSByZWFkX29ubHlfc2hhcmVkX29iamVjdC5jaGVja2VkX3BlZXI7XG4gICAgICBvcmdOYW1lID0gcmVhZF9vbmx5X3NoYXJlZF9vYmplY3QuY2hlY2tlZF9vcmc7XG5cbiAgICAgIHJlYWRfb25seV9zaGFyZWRfb2JqZWN0LmNsb3NlKCk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBsb2dnZXIuZGVidWcoZXJyKTtcbiAgICB9XG5cblxuICAgIC8vb3JkZXJlcj0nb3JkZXJlcjEuZ3Iua3QuY29tJ1xuICAgIC8vcGVlcj0ncGVlcjAuZ3Iua3QuY29tJ1xuICAgIC8vb3JnTmFtZT0nT3JnMSdcblxuICAgIHRyeSB7XG4gICAgICAvLyBmaXJzdCBzZXR1cCB0aGUgY2xpZW50IGZvciB0aGlzIG9yZ1xuICAgICAgY29uc3QgY2xpZW50ID0gYXdhaXQgaGxmLmdldEFkbWluQ2xpZW50Rm9yT3JnKG9yZ05hbWUpO1xuICAgICAgbG9nZ2VyLmRlYnVnKCdTdWNjZXNzZnVsbHkgZ290IHRoZSBmYWJyaWMgY2xpZW50IGZvciB0aGUgb3JnYW5pemF0aW9uIFwiJXNcIicsIG9yZ05hbWUpO1xuICAgICAgY29uc3QgY2hhbm5lbCA9IGNsaWVudC5nZXRDaGFubmVsKGNoYW5uZWxOYW1lKTtcbiAgICAgIGlmICghY2hhbm5lbCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoJ0NoYW5uZWwgJXMgd2FzIG5vdCBkZWZpbmVkIGluIHRoZSBjb25uZWN0aW9uIHByb2ZpbGUnLCBjaGFubmVsTmFtZSk7XG4gICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKG1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgY29uc3QgdHhJZCA9IGNsaWVudC5uZXdUcmFuc2FjdGlvbklEKCk7XG4gICAgICAvLyB3aWxsIG5lZWQgdGhlIHRyYW5zYWN0aW9uIElEIHN0cmluZyBmb3IgdGhlIGV2ZW50IHJlZ2lzdHJhdGlvbiBsYXRlclxuICAgICAgdHhJZFN0cmluZyA9IHR4SWQuZ2V0VHJhbnNhY3Rpb25JRCgpO1xuXG4gICAgICAvLyBpbnB1dCB0eElkIHRvIGtleVxuICAgICAgYXJncy5wdXNoKHR4SWRTdHJpbmcpO1xuICAgICAgYXJncy5wdXNoKHZhbHVlKTtcblxuXG5cblxuICAgICAgbG9nZ2VyLmRlYnVnKGBwZWVyX25hbWUhISE6ICR7cGVlcn1gKTtcbiAgICAgIGxvZ2dlci5kZWJ1Zyhgb3JkZXJlcl9uYW1lISEhOiAke29yZGVyZXJ9YCk7XG4gICAgICBsb2dnZXIuZGVidWcoYG9yZ05hbWUhISE6ICR7b3JnTmFtZX1gKTtcblxuICAgICAgLy8gc2VuZCBwcm9wb3NhbCB0byBlbmRvcnNlclxuICAgICAgY29uc3QgcmVxdWVzdCA9IHtcbiAgICAgICAgdGFyZ2V0czogcGVlcixcbiAgICAgICAgY2hhaW5jb2RlSWQ6IGNoYWluY29kZU5hbWUsXG4gICAgICAgIGZjbixcbiAgICAgICAgYXJncyxcbiAgICAgICAgY2hhaW5JZDogY2hhbm5lbE5hbWUsXG4gICAgICAgIHR4SWQsXG4gICAgICB9O1xuICAgICAgbG9nZ2VyLmRlYnVnKGByZXF1ZXN0OiR7SlNPTi5zdHJpbmdpZnkocmVxdWVzdCl9YCk7XG5cbiAgICAgIGNvbnN0IHJlc3VsdHMgPSBhd2FpdCBjaGFubmVsLnNlbmRUcmFuc2FjdGlvblByb3Bvc2FsKHJlcXVlc3QpO1xuXG4gICAgICAvLyB0aGUgcmV0dXJuZWQgb2JqZWN0IGhhcyBib3RoIHRoZSBlbmRvcnNlbWVudCByZXN1bHRzXG4gICAgICAvLyBhbmQgdGhlIGFjdHVhbCBwcm9wb3NhbCwgdGhlIHByb3Bvc2FsIHdpbGwgYmUgbmVlZGVkXG4gICAgICAvLyBsYXRlciB3aGVuIHdlIHNlbmQgYSB0cmFuc2FjdGlvbiB0byB0aGUgb3JkZXJlclxuICAgICAgY29uc3QgcHJvcG9zYWxSZXNwb25zZXMgPSByZXN1bHRzWzBdO1xuICAgICAgY29uc3QgcHJvcG9zYWwgPSByZXN1bHRzWzFdO1xuXG4gICAgICAvLyBsZXRzIGhhdmUgYSBsb29rIGF0IHRoZSByZXNwb25zZXMgdG8gc2VlIGlmIHRoZXkgYXJlXG4gICAgICAvLyBhbGwgZ29vZCwgaWYgZ29vZCB0aGV5IHdpbGwgYWxzbyBpbmNsdWRlIHNpZ25hdHVyZXNcbiAgICAgIC8vIHJlcXVpcmVkIHRvIGJlIGNvbW1pdHRlZFxuICAgICAgbGV0IGFsbEdvb2QgPSB0cnVlO1xuICAgICAgZm9yIChjb25zdCBpIGluIHByb3Bvc2FsUmVzcG9uc2VzKSB7XG4gICAgICAgIGxldCBvbmVHb29kID0gZmFsc2U7XG4gICAgICAgIGlmIChwcm9wb3NhbFJlc3BvbnNlcyAmJiBwcm9wb3NhbFJlc3BvbnNlc1tpXS5yZXNwb25zZSAmJlxuICAgICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXNbaV0ucmVzcG9uc2Uuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICBvbmVHb29kID0gdHJ1ZTtcbiAgICAgICAgICBsb2dnZXIuaW5mbygnaW52b2tlIGNoYWluY29kZSBwcm9wb3NhbCB3YXMgZ29vZCcpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGxvZ2dlci5lcnJvcihKU09OLnN0cmluZ2lmeShwcm9wb3NhbFJlc3BvbnNlcykpO1xuICAgICAgICAgIGxvZ2dlci5lcnJvcignaW52b2tlIGNoYWluY29kZSBwcm9wb3NhbCB3YXMgYmFkJyk7XG4gICAgICAgIH1cbiAgICAgICAgYWxsR29vZCAmPSBvbmVHb29kO1xuICAgICAgfVxuXG4gICAgICBpZiAoYWxsR29vZCkge1xuICAgICAgICBsb2dnZXIuaW5mbyh1dGlsLmZvcm1hdChcbiAgICAgICAgICAnU3VjY2Vzc2Z1bGx5IHNlbnQgUHJvcG9zYWwgYW5kIHJlY2VpdmVkIFByb3Bvc2FsUmVzcG9uc2U6IFN0YXR1cyAtICVzLCBtZXNzYWdlIC0gXCIlc1wiLCBtZXRhZGF0YSAtIFwiJXNcIiwgZW5kb3JzZW1lbnQgc2lnbmF0dXJlOiAlcycsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXNbMF0ucmVzcG9uc2Uuc3RhdHVzLCBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5tZXNzYWdlLFxuICAgICAgICAgIHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLnBheWxvYWQsIHByb3Bvc2FsUmVzcG9uc2VzWzBdLmVuZG9yc2VtZW50LnNpZ25hdHVyZSkpO1xuXG5cbiAgICAgICAgLy8gZXZlbnRodWIgbG9naWMgKHByb21pc2Ug67mE64+Z6riwKVxuXG4gICAgICAgIC8vIHdhaXQgZm9yIHRoZSBjaGFubmVsLWJhc2VkIGV2ZW50IGh1YiB0byB0ZWxsIHVzXG4gICAgICAgIC8vIHRoYXQgdGhlIGNvbW1pdCB3YXMgZ29vZCBvciBiYWQgb24gZWFjaCBwZWVyIGluIG91ciBvcmdhbml6YXRpb25cbiAgICAgICAgY29uc3QgcHJvbWlzZXMgPSBbXTtcbiAgICAgICAgY29uc3QgZXZlbnRIdWJzID0gY2hhbm5lbC5nZXRDaGFubmVsRXZlbnRIdWJzRm9yT3JnKCk7XG4gICAgICAgIC8vIHZhciBjaGVja19wcm9taXNlID0gRmFsc2UgO1xuXG4gICAgICAgIGV2ZW50SHVicy5mb3JFYWNoKGVoID0+IHtcbiAgICAgICAgICBsb2dnZXIuZGVidWcoJ2ludm9rZUV2ZW50UHJvbWlzZSAtIHNldHRpbmcgdXAgZXZlbnQnKTtcbiAgICAgICAgICAvLyBsb2dnZXIuZGVidWcoYCR7ZWguZ2V0UGVlckFkZHIoKX1gKVxuXG4gICAgICAgICAgY29uc3QgaW52b2tlRXZlbnRQcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgZWhUaW1lT3V0ID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSBgUkVRVUVTVF9USU1FT1VUOiR7ZWguZ2V0UGVlckFkZHIoKX1gO1xuICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgICAgICAgIGVoLmRpc2Nvbm5lY3QoKTtcbiAgICAgICAgICAgIH0sIDMwMDApO1xuICAgICAgICAgICAgZWgucmVnaXN0ZXJUeEV2ZW50KHR4SWRTdHJpbmcsICh0eCwgY29kZSwgYmxvY2tOdW0pID0+IHtcbiAgICAgICAgICAgICAgbG9nZ2VyLmluZm8oJ1RoZSBjaGFpbmNvZGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiBoYXMgYmVlbiBjb21taXR0ZWQgb24gcGVlciAlcycsIGVoLmdldFBlZXJBZGRyKCkpO1xuICAgICAgICAgICAgICBsb2dnZXIuaW5mbygnVHJhbnNhY3Rpb24gJXMgaGFzIHN0YXR1cyBvZiAlcyBpbiBibG9jayAlcycsIHR4LCBjb2RlLCBibG9ja051bSk7XG4gICAgICAgICAgICAgIGxvZ2dlci5pbmZvKFwidHhpZCByZXR1cm4hISEhIVwiKVxuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZWhUaW1lT3V0KTtcbiAgICAgICAgICAgICAgLy8gcmV0dXJuIHR4SWRTdHJpbmc7XG5cblxuXG4gICAgICAgICAgICAgIGlmIChjb2RlICE9PSAnVkFMSUQnKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdUaGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiB3YXMgaW52YWxpZCwgY29kZTolcycsIGNvZGUpO1xuICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZXNvbHZlKG5ldyBFcnJvcihtZXNzYWdlKSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9ICdUaGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiB3YXMgdmFsaWQuJztcbiAgICAgICAgICAgICAgICBsb2dnZXIuaW5mbyhtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZXNvbHZlKG1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIC8vIGVoLmRpc2Nvbm5lY3QoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCBlcnIgPT4ge1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZWhUaW1lT3V0KTtcbiAgICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKFwi7Jes6ri06rCAISEhISEhISEhISFcIilcbiAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKGVycik7XG4gICAgICAgICAgICAgIC8vIHJldHVybiBGYWxzZTtcbiAgICAgICAgICAgICAgcmVzb2x2ZShlcnIpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgLy8gdGhlIGRlZmF1bHQgZm9yICd1bnJlZ2lzdGVyJyBpcyB0cnVlIGZvciB0cmFuc2FjdGlvbiBsaXN0ZW5lcnNcbiAgICAgICAgICAgICAgLy8gc28gbm8gcmVhbCBuZWVkIHRvIHNldCBoZXJlLCBob3dldmVyIGZvciAnZGlzY29ubmVjdCdcbiAgICAgICAgICAgICAgLy8gdGhlIGRlZmF1bHQgaXMgZmFsc2UgYXMgbW9zdCBldmVudCBodWJzIGFyZSBsb25nIHJ1bm5pbmdcbiAgICAgICAgICAgICAgLy8gaW4gdGhpcyB1c2UgY2FzZSB3ZSBhcmUgdXNpbmcgaXQgb25seSBvbmNlXG4gICAgICAgICAgICB7IHVucmVnaXN0ZXI6IHRydWUsIGRpc2Nvbm5lY3Q6IHRydWUgfSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBlaC5jb25uZWN0KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oXCJwcm9taXNlcy5wdXNoKGludm9rZUV2ZW50UHJvbWlzZSkgISEhISBcIilcbiAgICAgICAgICBwcm9taXNlcy5wdXNoKGludm9rZUV2ZW50UHJvbWlzZSk7XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgLy8gfmV2ZW50aHViIGxvZ2ljXG5cblxuICAgICAgICBjb25zdCBvcmRlcmVyUmVxID0ge1xuICAgICAgICAgIG9yZGVyZXIsXG4gICAgICAgICAgdHhJZCxcbiAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlcyxcbiAgICAgICAgICBwcm9wb3NhbCxcbiAgICAgICAgfTtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBjb25zdCByZXN1bHRzMjIgPSBhd2FpdCBQcm9taXNlLmFsbChwcm9taXNlcyk7XG4gICAgICAgIC8vIGxvZ2dlci5kZWJ1ZyhcInJlc3VsdHMyMjogISEhIVwiIClcbiAgICAgICAgLy8gbG9nZ2VyLmRlYnVnKHJlc3VsdHMyMilcblxuXG4gICAgICAgIGNvbnN0IHNlbmRQcm9taXNlID0gY2hhbm5lbC5zZW5kVHJhbnNhY3Rpb24ob3JkZXJlclJlcSk7XG4gICAgICAgIC8vIHB1dCB0aGUgc2VuZCB0byB0aGUgb3JkZXJlciBsYXN0IHNvIHRoYXQgdGhlIGV2ZW50cyBnZXQgcmVnaXN0ZXJlZCBhbmRcbiAgICAgICAgLy8gYXJlIHJlYWR5IGZvciB0aGUgb3JkZXJlcmluZyBhbmQgY29tbWl0dGluZ1xuICAgICAgICBwcm9taXNlcy5wdXNoKHNlbmRQcm9taXNlKTtcblxuICAgICAgICBjb25zdCByZXN1bHRzID0gYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xuICAgICAgICBsb2dnZXIuZGVidWcoXCJyZXN1bHRzOiAhISEhXCIgKVxuICAgICAgICBsb2dnZXIuZGVidWcocmVzdWx0cylcbiAgICAgICAgbG9nZ2VyLmRlYnVnKHV0aWwuZm9ybWF0KCctLS0tLS0tPj4+IFIgRSBTIFAgTyBOIFMgRSA6ICVqJywgcmVzdWx0cykpO1xuXG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gcmVzdWx0cy5wb3AoKTsgLy8gIG9yZGVyZXIgcmVzdWx0cyBhcmUgbGFzdCBpbiB0aGUgcmVzdWx0c1xuICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAnU1VDQ0VTUycpIHtcbiAgICAgICAgICBsb2dnZXIuaW5mbygnU3VjY2Vzc2Z1bGx5IHNlbnQgdHJhbnNhY3Rpb24gdG8gdGhlIG9yZGVyZXIuJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZXJyb3JNc2cgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIG9yZGVyIHRoZSB0cmFuc2FjdGlvbi4gRXJyb3IgY29kZTogJXMnLCByZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlcnJvck1zZyk7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8vIG5vdyBzZWUgd2hhdCBlYWNoIG9mIHRoZSBldmVudCBodWJzIHJlcG9ydGVkICjri6jsiJwg66Gc6re4IOuwjyBlcnJvck1zZyDrjIDsnoUpXG4gICAgICAgIGZvciAoY29uc3QgaSBpbiByZXN1bHRzKSB7XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKFwicG9wIOydtO2bhCByZXN1bHRzISFcIilcbiAgICAgICAgICBsb2dnZXIuZGVidWcocmVzdWx0cylcbiAgICAgICAgICBjb25zdCBlaFJlc3VsdCA9IHJlc3VsdHNbaV07XG4gICAgICAgICAgY29uc3QgZWggPSBldmVudEh1YnNbaV07XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCdFdmVudCByZXN1bHRzIGZvciBldmVudCBodWIgOiVzJywgZWguZ2V0UGVlckFkZHIoKSk7XG4gICAgICAgICAgaWYgKHR5cGVvZiBlaFJlc3VsdCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdCk7XG5cblxuICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KFxuICAgICAgICAgICAgICAnU3VjY2Vzc2Z1bGx5IGludm9rZWQgdGhlIGNoYWluY29kZSAlcyB0byB0aGUgY2hhbm5lbCBcXCclc1xcJyBmb3IgdHJhbnNhY3Rpb24gSUQ6ICVzJyxcbiAgICAgICAgICAgICAgb3JnTmFtZSwgY2hhbm5lbE5hbWUsIHR4SWRTdHJpbmcpO1xuICAgICAgICAgICAgbG9nZ2VyLmluZm8obWVzc2FnZSk7XG5cbiAgICAgICAgICAgIHJldHVybiB0eElkU3RyaW5nO1xuXG5cblxuXG5cblxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoIWVycm9yTXNnKSBlcnJvck1zZyA9IGVoUmVzdWx0LnRvU3RyaW5nKCk7XG4gICAgICAgICAgICBsb2dnZXIuZGVidWcoXCLsl6zquLTrqLjsl6w/Pz9cIilcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdC50b1N0cmluZygpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBiZW9rXG4gICAgICAgIC8vIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdChcbiAgICAgICAgLy8gICAnU3VjY2Vzc2Z1bGx5IGludm9rZWQgdGhlIGNoYWluY29kZSAlcyB0byB0aGUgY2hhbm5lbCBcXCclc1xcJyBmb3IgdHJhbnNhY3Rpb24gSUQ6ICVzJyxcbiAgICAgICAgLy8gICBvcmdOYW1lLCBjaGFubmVsTmFtZSwgdHhJZFN0cmluZyk7XG4gICAgICAgIC8vIGxvZ2dlci5pbmZvKG1lc3NhZ2UpO1xuICAgICAgICAvL1xuICAgICAgICAvLyByZXR1cm4gdHhJZFN0cmluZztcbiAgICAgICAgLy8gfmJlb2tcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBlcnJvck1zZyA9IHV0aWwuZm9ybWF0KCdGYWlsZWQgdG8gc2VuZCBQcm9wb3NhbCBhbmQgcmVjZWl2ZSBhbGwgZ29vZCBQcm9wb3NhbFJlc3BvbnNlJyk7XG4gICAgICAgIGxvZ2dlci5kZWJ1ZyhlcnJvck1zZyk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGxvZ2dlci5lcnJvcihgRmFpbGVkIHRvIGludm9rZSBkdWUgdG8gZXJyb3I6ICR7ZXJyb3Iuc3RhY2t9YCA/IGVycm9yLnN0YWNrIDogZXJyb3IpO1xuICAgICAgZXJyb3JNc2cgPSBlcnJvci50b1N0cmluZygpO1xuICAgIH1cbiAgICBsb2dnZXIuZGVidWcoXCJlcnJvck1zZyEhISFcIilcbiAgICBsb2dnZXIuZGVidWcoZXJyb3JNc2cpXG4gICAgLy8gbG9nZ2VyLmRlYnVnKFwiYWxsR29vZCEhISFcIilcbiAgICAvLyBsb2dnZXIuZGVidWcoYWxsR29vZClcblxuXG5cbiAgICAvLyBpZiAoIWVycm9yTXNnKSB7XG4gICAgLy8gICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoXG4gICAgLy8gICAnU3VjY2Vzc2Z1bGx5IGludm9rZWQgdGhlIGNoYWluY29kZSAlcyB0byB0aGUgY2hhbm5lbCBcXCclc1xcJyBmb3IgdHJhbnNhY3Rpb24gSUQ6ICVzJyxcbiAgICAvLyAgIG9yZ05hbWUsIGNoYW5uZWxOYW1lLCB0eElkU3RyaW5nKTtcbiAgICAvLyAgIGxvZ2dlci5pbmZvKG1lc3NhZ2UpO1xuICAgIC8vXG4gICAgLy8gICByZXR1cm4gdHhJZFN0cmluZztcbiAgICAvLyB9XG4gICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdGYWlsZWQgdG8gaW52b2tlIGNoYWluY29kZS4gY2F1c2U6JXMnLCBlcnJvck1zZyk7XG4gICAgbG9nZ2VyLmVycm9yKG1lc3NhZ2UpO1xuICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgfVxuXG5cbiAgYXN5bmMgaW52b2tlQ2hhaW5Db2RlRm9yTWVzc2FnZShwZWVyTmFtZXMsIGNoYW5uZWxOYW1lLCBjaGFpbmNvZGVOYW1lLCBmY24sIGFyZ3MsIHVzZXJuYW1lLCBvcmdOYW1lKSB7XG4gICAgbG9nZ2VyLmRlYnVnKHV0aWwuZm9ybWF0KCdcXG49PT09PT09PT09PT0gaW52b2tlQ2hhaW5Db2RlIDogaW52b2tlIHRyYW5zYWN0aW9uIG9uIGNoYW5uZWwgJXMgPT09PT09PT09PT09XFxuJywgY2hhbm5lbE5hbWUpKTtcbiAgICBsZXQgZXJyb3JNc2cgPSBudWxsO1xuICAgIGxldCB0eElkU3RyaW5nID0gbnVsbDtcbiAgICB0cnkge1xuICAgICAgLy8gZmlyc3Qgc2V0dXAgdGhlIGNsaWVudCBmb3IgdGhpcyBvcmdcbiAgICAgIGNvbnN0IGNsaWVudCA9IGF3YWl0IGhsZi5nZXRBZG1pbkNsaWVudEZvck9yZyhvcmdOYW1lKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCB0aGUgZmFicmljIGNsaWVudCBmb3IgdGhlIG9yZ2FuaXphdGlvbiBcIiVzXCInLCBvcmdOYW1lKTtcbiAgICAgIGNvbnN0IGNoYW5uZWwgPSBjbGllbnQuZ2V0Q2hhbm5lbChjaGFubmVsTmFtZSk7XG4gICAgICBpZiAoIWNoYW5uZWwpIHtcbiAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdDaGFubmVsICVzIHdhcyBub3QgZGVmaW5lZCBpbiB0aGUgY29ubmVjdGlvbiBwcm9maWxlJywgY2hhbm5lbE5hbWUpO1xuICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgIH1cbiAgICAgIGNvbnN0IHR4SWQgPSBjbGllbnQubmV3VHJhbnNhY3Rpb25JRCgpO1xuICAgICAgLy8gd2lsbCBuZWVkIHRoZSB0cmFuc2FjdGlvbiBJRCBzdHJpbmcgZm9yIHRoZSBldmVudCByZWdpc3RyYXRpb24gbGF0ZXJcbiAgICAgIHR4SWRTdHJpbmcgPSB0eElkLmdldFRyYW5zYWN0aW9uSUQoKTtcblxuICAgICAgLy8gY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBjaGFubmVsLmluaXRpYWxpemUoe1xuICAgICAgLy8gICBkaXNjb3ZlcjogdHJ1ZSxcbiAgICAgIC8vICAgYXNMb2NhbGhvc3Q6IHRydWUsXG4gICAgICAvLyB9KTtcbiAgICAgIC8vIGxvZ2dlci5kZWJ1ZygnU2VydmljZSBkaXNjb3ZlcnkgaW5pdCA6ICcpO1xuICAgICAgLy8gbG9nZ2VyLmRlYnVnKHJlc3BvbnNlKTtcblxuICAgICAgLy8gc2VuZCBwcm9wb3NhbCB0byBlbmRvcnNlclxuICAgICAgY29uc3QgcmVxdWVzdCA9IHtcbiAgICAgICAgdGFyZ2V0czogJ3BlZXIwLmdyLmt0LmNvbScsXG4gICAgICAgIGNoYWluY29kZUlkOiBjaGFpbmNvZGVOYW1lLFxuICAgICAgICBmY24sXG4gICAgICAgIGFyZ3MsXG4gICAgICAgIGNoYWluSWQ6IGNoYW5uZWxOYW1lLFxuICAgICAgICB0eElkLFxuICAgICAgfTtcbiAgICAgIGlmIChwZWVyTmFtZXMpIHtcbiAgICAgICAgcmVxdWVzdC50YXJnZXRzID0gcGVlck5hbWVzO1xuICAgICAgfVxuXG4gICAgICBsb2dnZXIuZGVidWcoYHJlcXVlc3Q6JHtKU09OLnN0cmluZ2lmeShyZXF1ZXN0KX1gKTtcblxuICAgICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IGNoYW5uZWwuc2VuZFRyYW5zYWN0aW9uUHJvcG9zYWwocmVxdWVzdCk7XG5cbiAgICAgIC8vIHRoZSByZXR1cm5lZCBvYmplY3QgaGFzIGJvdGggdGhlIGVuZG9yc2VtZW50IHJlc3VsdHNcbiAgICAgIC8vIGFuZCB0aGUgYWN0dWFsIHByb3Bvc2FsLCB0aGUgcHJvcG9zYWwgd2lsbCBiZSBuZWVkZWRcbiAgICAgIC8vIGxhdGVyIHdoZW4gd2Ugc2VuZCBhIHRyYW5zYWN0aW9uIHRvIHRoZSBvcmRlcmVyXG4gICAgICBjb25zdCBwcm9wb3NhbFJlc3BvbnNlcyA9IHJlc3VsdHNbMF07XG4gICAgICBjb25zdCBwcm9wb3NhbCA9IHJlc3VsdHNbMV07XG5cbiAgICAgIC8vIGxldHMgaGF2ZSBhIGxvb2sgYXQgdGhlIHJlc3BvbnNlcyB0byBzZWUgaWYgdGhleSBhcmVcbiAgICAgIC8vIGFsbCBnb29kLCBpZiBnb29kIHRoZXkgd2lsbCBhbHNvIGluY2x1ZGUgc2lnbmF0dXJlc1xuICAgICAgLy8gcmVxdWlyZWQgdG8gYmUgY29tbWl0dGVkXG4gICAgICBsZXQgYWxsR29vZCA9IHRydWU7XG4gICAgICBmb3IgKGNvbnN0IGkgaW4gcHJvcG9zYWxSZXNwb25zZXMpIHtcbiAgICAgICAgbGV0IG9uZUdvb2QgPSBmYWxzZTtcbiAgICAgICAgaWYgKHByb3Bvc2FsUmVzcG9uc2VzICYmIHByb3Bvc2FsUmVzcG9uc2VzW2ldLnJlc3BvbnNlICYmXG4gICAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1tpXS5yZXNwb25zZS5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgIG9uZUdvb2QgPSB0cnVlO1xuICAgICAgICAgIGxvZ2dlci5pbmZvKCdpbnZva2UgY2hhaW5jb2RlIHByb3Bvc2FsIHdhcyBnb29kJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbG9nZ2VyLmVycm9yKEpTT04uc3RyaW5naWZ5KHByb3Bvc2FsUmVzcG9uc2VzKSk7XG4gICAgICAgICAgbG9nZ2VyLmVycm9yKCdpbnZva2UgY2hhaW5jb2RlIHByb3Bvc2FsIHdhcyBiYWQnKTtcbiAgICAgICAgfVxuICAgICAgICBhbGxHb29kICY9IG9uZUdvb2Q7XG4gICAgICB9XG5cbiAgICAgIGlmIChhbGxHb29kKSB7XG4gICAgICAgIGxvZ2dlci5pbmZvKHV0aWwuZm9ybWF0KFxuICAgICAgICAgICdTdWNjZXNzZnVsbHkgc2VudCBQcm9wb3NhbCBhbmQgcmVjZWl2ZWQgUHJvcG9zYWxSZXNwb25zZTogU3RhdHVzIC0gJXMsIG1lc3NhZ2UgLSBcIiVzXCIsIG1ldGFkYXRhIC0gXCIlc1wiLCBlbmRvcnNlbWVudCBzaWduYXR1cmU6ICVzJyxcbiAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5zdGF0dXMsIHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLm1lc3NhZ2UsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXNbMF0ucmVzcG9uc2UucGF5bG9hZCwgcHJvcG9zYWxSZXNwb25zZXNbMF0uZW5kb3JzZW1lbnQuc2lnbmF0dXJlKSk7XG5cblxuICAgICAgICAvLyB3YWl0IGZvciB0aGUgY2hhbm5lbC1iYXNlZCBldmVudCBodWIgdG8gdGVsbCB1c1xuICAgICAgICAvLyB0aGF0IHRoZSBjb21taXQgd2FzIGdvb2Qgb3IgYmFkIG9uIGVhY2ggcGVlciBpbiBvdXIgb3JnYW5pemF0aW9uXG4gICAgICAgIGNvbnN0IHByb21pc2VzID0gW107XG4gICAgICAgIGNvbnN0IGV2ZW50SHVicyA9IGNoYW5uZWwuZ2V0Q2hhbm5lbEV2ZW50SHVic0Zvck9yZygpO1xuICAgICAgICBldmVudEh1YnMuZm9yRWFjaChlaCA9PiB7XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCdpbnZva2VFdmVudFByb21pc2UgLSBzZXR0aW5nIHVwIGV2ZW50Jyk7XG5cbiAgICAgICAgICBjb25zdCBpbnZva2VFdmVudFByb21pc2UgPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBlaFRpbWVPdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IGBSRVFVRVNUX1RJTUVPVVQ6JHtlaC5nZXRQZWVyQWRkcigpfWA7XG4gICAgICAgICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICAgICAgZWguZGlzY29ubmVjdCgpO1xuICAgICAgICAgICAgfSwgMzAwMDApO1xuICAgICAgICAgICAgZWgucmVnaXN0ZXJUeEV2ZW50KHR4SWRTdHJpbmcsICh0eCwgY29kZSwgYmxvY2tOdW0pID0+IHtcbiAgICAgICAgICAgICAgbG9nZ2VyLmluZm8oJ1RoZSBjaGFpbmNvZGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiBoYXMgYmVlbiBjb21taXR0ZWQgb24gcGVlciAlcycsIGVoLmdldFBlZXJBZGRyKCkpO1xuICAgICAgICAgICAgICBsb2dnZXIuaW5mbygnVHJhbnNhY3Rpb24gJXMgaGFzIHN0YXR1cyBvZiAlcyBpbiBibG9jayAlcycsIHR4LCBjb2RlLCBibG9ja051bSk7XG4gICAgICAgICAgICAgIGNsZWFyVGltZW91dChlaFRpbWVPdXQpO1xuXG4gICAgICAgICAgICAgIGlmIChjb2RlICE9PSAnVkFMSUQnKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdUaGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiB3YXMgaW52YWxpZCwgY29kZTolcycsIGNvZGUpO1xuICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZWplY3QobmV3IEVycm9yKG1lc3NhZ2UpKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlID0gJ1RoZSBpbnZva2UgY2hhaW5jb2RlIHRyYW5zYWN0aW9uIHdhcyB2YWxpZC4nO1xuICAgICAgICAgICAgICAgIGxvZ2dlci5pbmZvKG1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIHJlc29sdmUobWVzc2FnZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIGVyciA9PiB7XG4gICAgICAgICAgICAgIGNsZWFyVGltZW91dChlaFRpbWVPdXQpO1xuICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAvLyB0aGUgZGVmYXVsdCBmb3IgJ3VucmVnaXN0ZXInIGlzIHRydWUgZm9yIHRyYW5zYWN0aW9uIGxpc3RlbmVyc1xuICAgICAgICAgICAgICAvLyBzbyBubyByZWFsIG5lZWQgdG8gc2V0IGhlcmUsIGhvd2V2ZXIgZm9yICdkaXNjb25uZWN0J1xuICAgICAgICAgICAgICAvLyB0aGUgZGVmYXVsdCBpcyBmYWxzZSBhcyBtb3N0IGV2ZW50IGh1YnMgYXJlIGxvbmcgcnVubmluZ1xuICAgICAgICAgICAgICAvLyBpbiB0aGlzIHVzZSBjYXNlIHdlIGFyZSB1c2luZyBpdCBvbmx5IG9uY2VcbiAgICAgICAgICAgIHsgdW5yZWdpc3RlcjogdHJ1ZSwgZGlzY29ubmVjdDogdHJ1ZSB9LFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGVoLmNvbm5lY3QoKTtcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIHByb21pc2VzLnB1c2goaW52b2tlRXZlbnRQcm9taXNlKTtcbiAgICAgICAgfSk7XG5cblxuICAgICAgICBjb25zdCBvcmRlcmVyUmVxID0ge1xuICAgICAgICAgIHR4SWQsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXMsXG4gICAgICAgICAgcHJvcG9zYWwsXG4gICAgICAgICAgLy8gb3JkZXJlcjogJ29yZGVyZXI1LmV4YW1wbGUuY29tJyxcbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgc2VuZFByb21pc2UgPSBjaGFubmVsLnNlbmRUcmFuc2FjdGlvbihvcmRlcmVyUmVxKTtcbiAgICAgICAgLy8gcHV0IHRoZSBzZW5kIHRvIHRoZSBvcmRlcmVyIGxhc3Qgc28gdGhhdCB0aGUgZXZlbnRzIGdldCByZWdpc3RlcmVkIGFuZFxuICAgICAgICAvLyBhcmUgcmVhZHkgZm9yIHRoZSBvcmRlcmVyaW5nIGFuZCBjb21taXR0aW5nXG4gICAgICAgIHByb21pc2VzLnB1c2goc2VuZFByb21pc2UpO1xuICAgICAgICBjb25zdCByZXN1bHRzID0gYXdhaXQgUHJvbWlzZS5hbGwocHJvbWlzZXMpO1xuICAgICAgICBsb2dnZXIuZGVidWcodXRpbC5mb3JtYXQoJy0tLS0tLS0+Pj4gUiBFIFMgUCBPIE4gUyBFIDogJWonLCByZXN1bHRzKSk7XG4gICAgICAgIGNvbnN0IHJlc3BvbnNlID0gcmVzdWx0cy5wb3AoKTsgLy8gIG9yZGVyZXIgcmVzdWx0cyBhcmUgbGFzdCBpbiB0aGUgcmVzdWx0c1xuICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSAnU1VDQ0VTUycpIHtcbiAgICAgICAgICBsb2dnZXIuaW5mbygnU3VjY2Vzc2Z1bGx5IHNlbnQgdHJhbnNhY3Rpb24gdG8gdGhlIG9yZGVyZXIuJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZXJyb3JNc2cgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIG9yZGVyIHRoZSB0cmFuc2FjdGlvbi4gRXJyb3IgY29kZTogJXMnLCByZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlcnJvck1zZyk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBub3cgc2VlIHdoYXQgZWFjaCBvZiB0aGUgZXZlbnQgaHVicyByZXBvcnRlZFxuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gcmVzdWx0cykge1xuICAgICAgICAgIGNvbnN0IGVoUmVzdWx0ID0gcmVzdWx0c1tpXTtcbiAgICAgICAgICBjb25zdCBlaCA9IGV2ZW50SHVic1tpXTtcbiAgICAgICAgICBsb2dnZXIuZGVidWcoJ0V2ZW50IHJlc3VsdHMgZm9yIGV2ZW50IGh1YiA6JXMnLCBlaC5nZXRQZWVyQWRkcigpKTtcbiAgICAgICAgICBpZiAodHlwZW9mIGVoUmVzdWx0ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKGVoUmVzdWx0KTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKCFlcnJvck1zZykgZXJyb3JNc2cgPSBlaFJlc3VsdC50b1N0cmluZygpO1xuICAgICAgICAgICAgbG9nZ2VyLmRlYnVnKGVoUmVzdWx0LnRvU3RyaW5nKCkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZXJyb3JNc2cgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIHNlbmQgUHJvcG9zYWwgYW5kIHJlY2VpdmUgYWxsIGdvb2QgUHJvcG9zYWxSZXNwb25zZScpO1xuICAgICAgICBsb2dnZXIuZGVidWcoZXJyb3JNc2cpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBsb2dnZXIuZXJyb3IoYEZhaWxlZCB0byBpbnZva2UgZHVlIHRvIGVycm9yOiAke2Vycm9yLnN0YWNrfWAgPyBlcnJvci5zdGFjayA6IGVycm9yKTtcbiAgICAgIGVycm9yTXNnID0gZXJyb3IudG9TdHJpbmcoKTtcbiAgICB9XG5cbiAgICBpZiAoIWVycm9yTXNnKSB7XG4gICAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoXG4gICAgICAgICdTdWNjZXNzZnVsbHkgaW52b2tlZCB0aGUgY2hhaW5jb2RlICVzIHRvIHRoZSBjaGFubmVsIFxcJyVzXFwnIGZvciB0cmFuc2FjdGlvbiBJRDogJXMnLFxuICAgICAgICBvcmdOYW1lLCBjaGFubmVsTmFtZSwgdHhJZFN0cmluZyk7XG4gICAgICBsb2dnZXIuaW5mbyhtZXNzYWdlKTtcblxuICAgICAgLy8gcmV0dXJuIHR4SWRTdHJpbmc7XG4gICAgfVxuICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIGludm9rZSBjaGFpbmNvZGUuIGNhdXNlOiVzJywgZXJyb3JNc2cpO1xuICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gIH1cblxuICBhc3luYyBpbnZva2VDaGFpbkNvZGVEaXNjb3ZlcihwZWVyTmFtZXMsIGNoYW5uZWxOYW1lLCBjaGFpbmNvZGVOYW1lLCBmY24sIGFyZ3MsIHVzZXJuYW1lLCBvcmdOYW1lLCBpbnZva2VPcHRpb24pIHtcbiAgICBsb2dnZXIuZGVidWcodXRpbC5mb3JtYXQoJ1xcbj09PT09PT09PT09PSBpbnZva2VDaGFpbkNvZGVEaXNjb3ZlciA6IGludm9rZSB0cmFuc2FjdGlvbiBvbiBjaGFubmVsICVzID09PT09PT09PT09PVxcbicsIGNoYW5uZWxOYW1lKSk7XG4gICAgbGV0IGVycm9yTXNnID0gbnVsbDtcbiAgICBsZXQgdHhJZFN0cmluZyA9IG51bGw7XG4gICAgdHJ5IHtcbiAgICAgIC8vIGZpcnN0IHNldHVwIHRoZSBjbGllbnQgZm9yIHRoaXMgb3JnXG4gICAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBobGYuZ2V0QWRtaW5DbGllbnRGb3JPcmcoJ09yZzEnKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCB0aGUgZmFicmljIGNsaWVudCBmb3IgdGhlIG9yZ2FuaXphdGlvbiBcIiVzXCInLCBvcmdOYW1lKTtcbiAgICAgIGNvbnN0IGNoYW5uZWwgPSBjbGllbnQuZ2V0T0JDaGFubmVsKGNoYW5uZWxOYW1lKTtcbiAgICAgIGlmICghY2hhbm5lbCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoJ0NoYW5uZWwgJXMgd2FzIG5vdCBkZWZpbmVkIGluIHRoZSBjb25uZWN0aW9uIHByb2ZpbGUnLCBjaGFubmVsTmFtZSk7XG4gICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKG1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgY29uc3QgdHhJZCA9IGNsaWVudC5uZXdUcmFuc2FjdGlvbklEKCk7XG4gICAgICAvLyB3aWxsIG5lZWQgdGhlIHRyYW5zYWN0aW9uIElEIHN0cmluZyBmb3IgdGhlIGV2ZW50IHJlZ2lzdHJhdGlvbiBsYXRlclxuICAgICAgdHhJZFN0cmluZyA9IHR4SWQuZ2V0VHJhbnNhY3Rpb25JRCgpO1xuXG4gICAgICAvLyBzZW5kIHByb3Bvc2FsIHRvIGVuZG9yc2VyXG4gICAgICBjb25zdCByZXF1ZXN0ID0ge1xuICAgICAgICAvLyB0YXJnZXRzOiAncGVlcjAub3JnMi5leGFtcGxlLmNvbScsXG4gICAgICAgIGNoYWluY29kZUlkOiBjaGFpbmNvZGVOYW1lLFxuICAgICAgICBmY24sXG4gICAgICAgIGFyZ3MsXG4gICAgICAgIGNoYWluSWQ6IGNoYW5uZWxOYW1lLFxuICAgICAgICB0eElkLFxuICAgICAgfTtcblxuICAgICAgaWYgKGludm9rZU9wdGlvbiA9PT0gJ2xvYWRfaW5mbycpIHtcbiAgICAgICAgLy8gZW5kb3JzaW5nIHJvbGUgcGVlcnNcbiAgICAgICAgY29uc3QgcGVlcnNPYmogPSBjaGFubmVsLl9nZXRUYXJnZXRzKCcnLCAnZW5kb3JzaW5nUGVlcicpO1xuICAgICAgICB0aGlzLl9lbmRvcnNpbmdQZWVycyA9IFtdO1xuICAgICAgICBpZiAocGVlcnNPYmopIHtcbiAgICAgICAgICBsb2dnZXIuZGVidWcoJ1N1Y2Nlc3NmdWxseSBnb3QgcGVlciBsaXN0Jyk7XG4gICAgICAgICAgZm9yIChjb25zdCBpIGluIHBlZXJzT2JqKSB7XG4gICAgICAgICAgICB0aGlzLl9lbmRvcnNpbmdQZWVycy5wdXNoKHBlZXJzT2JqW2ldLl9uYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBpbml0aWFsaXplIHNlcnZpY2UgZGlzY292ZXJ5IHRvIGdldCBsb2FkX2luZm9cbiAgICAgICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBjaGFubmVsLmluaXRpYWxpemVPQih7IGRpc2NvdmVyOiB0cnVlLCBhc0xvY2FsaG9zdDogdHJ1ZSB9KTtcbiAgICAgICAgaWYgKHJlc3BvbnNlLnBlZXJzX2J5X29yZykge1xuICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCBkaXNjb3ZlcnkgaW5mbycpO1xuICAgICAgICAgIGNvbnN0IG9yZ01TUE5hbWUgPSBPYmplY3Qua2V5cyhyZXNwb25zZS5wZWVyc19ieV9vcmcpO1xuICAgICAgICAgIGNvbnN0IHRhcmdldFBlZXIgPSB7IG5hbWU6IG51bGwsIGxvYWRfaW5mbzogMTAwIH07XG4gICAgICAgICAgY29uc3QgZGlzY292ZXJ5UGVlciA9IHt9O1xuICAgICAgICAgIGZvciAoY29uc3QgaSBpbiBvcmdNU1BOYW1lKSB7XG4gICAgICAgICAgICBsb2dnZXIuZGVidWcob3JnTVNQTmFtZVtpXSk7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGogaW4gcmVzcG9uc2UucGVlcnNfYnlfb3JnW29yZ01TUE5hbWVbaV1dLnBlZXJzKSB7XG4gICAgICAgICAgICAgIGRpc2NvdmVyeVBlZXIubmFtZSA9IHJlc3BvbnNlLnBlZXJzX2J5X29yZ1tvcmdNU1BOYW1lW2ldXS5wZWVyc1tqXS5lbmRwb2ludC5zcGxpdCgnOicpWzBdO1xuICAgICAgICAgICAgICBkaXNjb3ZlcnlQZWVyLmxvYWRfaW5mbyA9IHJlc3BvbnNlLnBlZXJzX2J5X29yZ1tvcmdNU1BOYW1lW2ldXS5wZWVyc1tqXS5sb2FkX2luZm87XG4gICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnPT09PT09PT09IGRpc2NvdmVyeSBpbmZvJyk7XG4gICAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZygnbmFtZSA6ICVzLCBsb2FkX2luZm8gOiAlcycsIGRpc2NvdmVyeVBlZXIubmFtZSwgZGlzY292ZXJ5UGVlci5sb2FkX2luZm8pO1xuXG4gICAgICAgICAgICAgIHRoaXMuX2VuZG9yc2luZ1BlZXJzLmZvckVhY2gocGVlck5hbWUgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChwZWVyTmFtZSA9PT0gZGlzY292ZXJ5UGVlci5uYW1lICYmIHRhcmdldFBlZXIubG9hZF9pbmZvID4gZGlzY292ZXJ5UGVlci5sb2FkX2luZm8pIHtcbiAgICAgICAgICAgICAgICAgIHRhcmdldFBlZXIubmFtZSA9IGRpc2NvdmVyeVBlZXIubmFtZTtcbiAgICAgICAgICAgICAgICAgIHRhcmdldFBlZXIubG9hZF9pbmZvID0gZGlzY292ZXJ5UGVlci5sb2FkX2luZm87XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCd0YXJnZXQgcGVlcjolcywgbG9hZF9pbmZvOiVzJywgdGFyZ2V0UGVlci5uYW1lLCB0YXJnZXRQZWVyLmxvYWRfaW5mbyk7XG4gICAgICAgICAgcmVxdWVzdC50YXJnZXRzID0gdGFyZ2V0UGVlci5uYW1lO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB1dGlsLmZvcm1hdCgnRmFpbGVkIHRvIGdldCBsb2FkX2luZm8nKTtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoaW52b2tlT3B0aW9uID09PSAnZGlzY292ZXInKSB7XG4gICAgICAgIGF3YWl0IGNoYW5uZWwuaW5pdGlhbGl6ZU9CKHsgZGlzY292ZXI6IHRydWUsIGFzTG9jYWxob3N0OiB0cnVlIH0pO1xuICAgICAgfVxuICAgICAgLy8gZWxzZSBpZiAoaW52b2tlT3B0aW9uID09PSAndGFyZ2V0ZWQnKSB7XG4gICAgICAvLyAgIHJlcXVlc3QudGFyZ2V0cyA9ICdwZWVyMC5vcmcxLmV4YW1wbGUuY29tJztcbiAgICAgIC8vIH0gZWxzZSBpZiAoaW52b2tlT3B0aW9uID09ICcnIHx8ICdub25fdGFyZ2V0JylcblxuICAgICAgbG9nZ2VyLmRlYnVnKGByZXF1ZXN0OiR7SlNPTi5zdHJpbmdpZnkocmVxdWVzdCl9YCk7XG5cbiAgICAgIGNvbnN0IHJlc3VsdHMgPSBhd2FpdCBjaGFubmVsLnNlbmRUcmFuc2FjdGlvblByb3Bvc2FsKHJlcXVlc3QpO1xuXG4gICAgICAvLyB0aGUgcmV0dXJuZWQgb2JqZWN0IGhhcyBib3RoIHRoZSBlbmRvcnNlbWVudCByZXN1bHRzXG4gICAgICAvLyBhbmQgdGhlIGFjdHVhbCBwcm9wb3NhbCwgdGhlIHByb3Bvc2FsIHdpbGwgYmUgbmVlZGVkXG4gICAgICAvLyBsYXRlciB3aGVuIHdlIHNlbmQgYSB0cmFuc2FjdGlvbiB0byB0aGUgb3JkZXJlclxuICAgICAgY29uc3QgcHJvcG9zYWxSZXNwb25zZXMgPSByZXN1bHRzWzBdO1xuICAgICAgY29uc3QgcHJvcG9zYWwgPSByZXN1bHRzWzFdO1xuXG4gICAgICAvLyBsZXRzIGhhdmUgYSBsb29rIGF0IHRoZSByZXNwb25zZXMgdG8gc2VlIGlmIHRoZXkgYXJlXG4gICAgICAvLyBhbGwgZ29vZCwgaWYgZ29vZCB0aGV5IHdpbGwgYWxzbyBpbmNsdWRlIHNpZ25hdHVyZXNcbiAgICAgIC8vIHJlcXVpcmVkIHRvIGJlIGNvbW1pdHRlZFxuICAgICAgbGV0IGFsbEdvb2QgPSB0cnVlO1xuICAgICAgZm9yIChjb25zdCBpIGluIHByb3Bvc2FsUmVzcG9uc2VzKSB7XG4gICAgICAgIGxldCBvbmVHb29kID0gZmFsc2U7XG4gICAgICAgIGlmIChwcm9wb3NhbFJlc3BvbnNlcyAmJiBwcm9wb3NhbFJlc3BvbnNlc1tpXS5yZXNwb25zZSAmJlxuICAgICAgICAgIHByb3Bvc2FsUmVzcG9uc2VzW2ldLnJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgb25lR29vZCA9IHRydWU7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oJ2ludm9rZSBjaGFpbmNvZGUgcHJvcG9zYWwgd2FzIGdvb2QnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoSlNPTi5zdHJpbmdpZnkocHJvcG9zYWxSZXNwb25zZXMpKTtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoJ2ludm9rZSBjaGFpbmNvZGUgcHJvcG9zYWwgd2FzIGJhZCcpO1xuICAgICAgICB9XG4gICAgICAgIGFsbEdvb2QgJj0gb25lR29vZDtcbiAgICAgIH1cblxuICAgICAgaWYgKGFsbEdvb2QpIHtcbiAgICAgICAgbG9nZ2VyLmluZm8odXRpbC5mb3JtYXQoXG4gICAgICAgICAgJ1N1Y2Nlc3NmdWxseSBzZW50IFByb3Bvc2FsIGFuZCByZWNlaXZlZCBQcm9wb3NhbFJlc3BvbnNlOiBTdGF0dXMgLSAlcywgbWVzc2FnZSAtIFwiJXNcIiwgbWV0YWRhdGEgLSBcIiVzXCIsIGVuZG9yc2VtZW50IHNpZ25hdHVyZTogJXMnLFxuICAgICAgICAgIHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLnN0YXR1cywgcHJvcG9zYWxSZXNwb25zZXNbMF0ucmVzcG9uc2UubWVzc2FnZSxcbiAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5wYXlsb2FkLCBwcm9wb3NhbFJlc3BvbnNlc1swXS5lbmRvcnNlbWVudC5zaWduYXR1cmUpKTtcblxuICAgICAgICAvLyB3YWl0IGZvciB0aGUgY2hhbm5lbC1iYXNlZCBldmVudCBodWIgdG8gdGVsbCB1c1xuICAgICAgICAvLyB0aGF0IHRoZSBjb21taXQgd2FzIGdvb2Qgb3IgYmFkIG9uIGVhY2ggcGVlciBpbiBvdXIgb3JnYW5pemF0aW9uXG4gICAgICAgIGNvbnN0IHByb21pc2VzID0gW107XG4gICAgICAgIGNvbnN0IGV2ZW50SHVicyA9IGNoYW5uZWwuZ2V0Q2hhbm5lbEV2ZW50SHVic0Zvck9yZygpO1xuICAgICAgICBldmVudEh1YnMuZm9yRWFjaChlaCA9PiB7XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCdpbnZva2VFdmVudFByb21pc2UgLSBzZXR0aW5nIHVwIGV2ZW50Jyk7XG4gICAgICAgICAgY29uc3QgaW52b2tlRXZlbnRQcm9taXNlID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgZWhUaW1lT3V0ID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSBgUkVRVUVTVF9USU1FT1VUOiR7ZWguZ2V0UGVlckFkZHIoKX1gO1xuICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgICAgICAgICAgIGVoLmRpc2Nvbm5lY3QoKTtcbiAgICAgICAgICAgIH0sIDMwMDAwKTtcbiAgICAgICAgICAgIGVoLnJlZ2lzdGVyVHhFdmVudCh0eElkU3RyaW5nLCAodHgsIGNvZGUsIGJsb2NrTnVtKSA9PiB7XG4gICAgICAgICAgICAgIGxvZ2dlci5pbmZvKCdUaGUgY2hhaW5jb2RlIGludm9rZSBjaGFpbmNvZGUgdHJhbnNhY3Rpb24gaGFzIGJlZW4gY29tbWl0dGVkIG9uIHBlZXIgJXMnLCBlaC5nZXRQZWVyQWRkcigpKTtcbiAgICAgICAgICAgICAgbG9nZ2VyLmluZm8oJ1RyYW5zYWN0aW9uICVzIGhhcyBzdGF0dXMgb2YgJXMgaW4gYmxvY2wgJXMnLCB0eCwgY29kZSwgYmxvY2tOdW0pO1xuICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoZWhUaW1lT3V0KTtcblxuXG4gICAgICAgICAgICAgIGlmIChjb2RlICE9PSAnVkFMSUQnKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KCdUaGUgaW52b2tlIGNoYWluY29kZSB0cmFuc2FjdGlvbiB3YXMgaW52YWxpZCwgY29kZTolcycsIGNvZGUpO1xuICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICAgICAgICByZWplY3QobmV3IEVycm9yKG1lc3NhZ2UpKTtcbiAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlID0gJ1RoZSBpbnZva2UgY2hhaW5jb2RlIHRyYW5zYWN0aW9uIHdhcyB2YWxpZC4nO1xuICAgICAgICAgICAgICAgIGxvZ2dlci5pbmZvKG1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgIHJlc29sdmUobWVzc2FnZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIGVyciA9PiB7XG4gICAgICAgICAgICAgIGNsZWFyVGltZW91dChlaFRpbWVPdXQpO1xuICAgICAgICAgICAgICBsb2dnZXIuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gdGhlIGRlZmF1bHQgZm9yICd1bnJlZ2lzdGVyJyBpcyB0cnVlIGZvciB0cmFuc2FjdGlvbiBsaXN0ZW5lcnNcbiAgICAgICAgICAgIC8vIHNvIG5vIHJlYWwgbmVlZCB0byBzZXQgaGVyZSwgaG93ZXZlciBmb3IgJ2Rpc2Nvbm5lY3QnXG4gICAgICAgICAgICAvLyB0aGUgZGVmYXVsdCBpcyBmYWxzZSBhcyBtb3N0IGV2ZW50IGh1YnMgYXJlIGxvbmcgcnVubmluZ1xuICAgICAgICAgICAgLy8gaW4gdGhpcyB1c2UgY2FzZSB3ZSBhcmUgdXNpbmcgaXQgb25seSBvbmNlXG4gICAgICAgICAgICB7IHVucmVnaXN0ZXI6IHRydWUsIGRpc2Nvbm5lY3Q6IHRydWUgfSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBlaC5jb25uZWN0KCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcHJvbWlzZXMucHVzaChpbnZva2VFdmVudFByb21pc2UpO1xuICAgICAgICB9KTtcblxuICAgICAgICBjb25zdCBvcmRlcmVyUmVxID0ge1xuICAgICAgICAgIHR4SWQsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXMsXG4gICAgICAgICAgcHJvcG9zYWwsXG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHNlbmRQcm9taXNlID0gY2hhbm5lbC5zZW5kVHJhbnNhY3Rpb24ob3JkZXJlclJlcSk7XG4gICAgICAgIC8vIHB1dCB0aGUgc2VuZCB0byB0aGUgb3JkZXJlciBsYXN0IHNvIHRoYXQgdGhlIGV2ZW50cyBnZXQgcmVnaXN0ZXJlZCBhbmRcbiAgICAgICAgLy8gYXJlIHJlYWR5IGZvciB0aGUgb3JkZXJlcmluZyBhbmQgY29tbWl0dGluZ1xuICAgICAgICBwcm9taXNlcy5wdXNoKHNlbmRQcm9taXNlKTtcbiAgICAgICAgY29uc3QgcmVzdWx0cyA9IGF3YWl0IFByb21pc2UuYWxsKHByb21pc2VzKTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKHV0aWwuZm9ybWF0KCctLS0tLS0tPj4+IFIgRSBTIFAgTyBOIFMgRSA6ICVqJywgcmVzdWx0cykpO1xuICAgICAgICBjb25zdCByZXNwb25zZSA9IHJlc3VsdHMucG9wKCk7IC8vICBvcmRlcmVyIHJlc3VsdHMgYXJlIGxhc3QgaW4gdGhlIHJlc3VsdHNcbiAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gJ1NVQ0NFU1MnKSB7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oJ1N1Y2Nlc3NmdWxseSBzZW50IHRyYW5zYWN0aW9uIHRvIHRoZSBvcmRlcmVyLicpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGVycm9yTXNnID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBvcmRlciB0aGUgdHJhbnNhY3Rpb24uIEVycm9yIGNvZGU6ICVzJywgcmVzcG9uc2Uuc3RhdHVzKTtcbiAgICAgICAgICBsb2dnZXIuZGVidWcoZXJyb3JNc2cpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gbm93IHNlZSB3aGF0IGVhY2ggb2YgdGhlIGV2ZW50IGh1YnMgcmVwb3J0ZWRcbiAgICAgICAgZm9yIChjb25zdCBpIGluIHJlc3VsdHMpIHtcbiAgICAgICAgICBjb25zdCBlaFJlc3VsdCA9IHJlc3VsdHNbaV07XG4gICAgICAgICAgY29uc3QgZWggPSBldmVudEh1YnNbaV07XG4gICAgICAgICAgbG9nZ2VyLmRlYnVnKCdFdmVudCByZXN1bHRzIGZvciBldmVudCBodWIgOiVzJywgZWguZ2V0UGVlckFkZHIoKSk7XG4gICAgICAgICAgaWYgKHR5cGVvZiBlaFJlc3VsdCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmICghZXJyb3JNc2cpIGVycm9yTXNnID0gZWhSZXN1bHQudG9TdHJpbmcoKTtcbiAgICAgICAgICAgIGxvZ2dlci5kZWJ1ZyhlaFJlc3VsdC50b1N0cmluZygpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGVycm9yTXNnID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBzZW5kIFByb3Bvc2FsIGFuZCByZWNlaXZlIGFsbCBnb29kIFByb3Bvc2FsUmVzcG9uc2UnKTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKGVycm9yTXNnKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgbG9nZ2VyLmVycm9yKGBGYWlsZWQgdG8gaW52b2tlIGR1ZSB0byBlcnJvcjogJHtlcnJvci5zdGFja31gID8gZXJyb3Iuc3RhY2sgOiBlcnJvcik7XG4gICAgICBlcnJvck1zZyA9IGVycm9yLnRvU3RyaW5nKCk7XG4gICAgfVxuXG4gICAgaWYgKCFlcnJvck1zZykge1xuICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KFxuICAgICAgICAnU3VjY2Vzc2Z1bGx5IGludm9rZWQgdGhlIGNoYWluY29kZSAlcyB0byB0aGUgY2hhbm5lbCBcXCclc1xcJyBmb3IgdHJhbnNhY3Rpb24gSUQ6ICVzJyxcbiAgICAgICAgb3JnTmFtZSwgY2hhbm5lbE5hbWUsIHR4SWRTdHJpbmcpO1xuICAgICAgbG9nZ2VyLmluZm8obWVzc2FnZSk7XG5cbiAgICAgIHJldHVybiB0eElkU3RyaW5nO1xuICAgIH1cbiAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBpbnZva2UgY2hhaW5jb2RlLiBjYXVzZTolcycsIGVycm9yTXNnKTtcbiAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgdGhyb3cgbmV3IEVycm9yKG1lc3NhZ2UpO1xuICB9XG5cbiAgYXN5bmMgaW52b2tlQ2hhaW5Db2RlT0IocGVlck5hbWVzLCBjaGFubmVsTmFtZSwgY2hhaW5jb2RlTmFtZSwgZmNuLCBhcmdzLCB1c2VybmFtZSwgb3JnTmFtZSkge1xuICAgIGxvZ2dlci5kZWJ1Zyh1dGlsLmZvcm1hdCgnXFxuPT09PT09PT09PT09IGludm9rZSB0cmFuc2FjdGlvbiBvbiBjaGFubmVsICVzID09PT09PT09PT09PVxcbicsIGNoYW5uZWxOYW1lKSk7XG4gICAgbGV0IGVycm9yTXNnID0gbnVsbDtcbiAgICBsZXQgdHhJZFN0cmluZyA9IG51bGw7XG4gICAgdHJ5IHtcbiAgICAgIC8vIGZpcnN0IHNldHVwIHRoZSBjbGllbnQgZm9yIHRoaXMgb3JnXG4gICAgICBjb25zdCBjbGllbnQgPSBhd2FpdCBobGYuZ2V0QWRtaW5DbGllbnRGb3JPcmcoJ09yZzEnKTtcbiAgICAgIGxvZ2dlci5kZWJ1ZygnU3VjY2Vzc2Z1bGx5IGdvdCB0aGUgZmFicmljIGNsaWVudCBmb3IgdGhlIG9yZ2FuaXphdGlvbiBcIiVzXCInLCBvcmdOYW1lKTtcbiAgICAgIGNvbnN0IGNoYW5uZWwgPSBjbGllbnQuZ2V0T0JDaGFubmVsKGNoYW5uZWxOYW1lKTtcbiAgICAgIGlmICghY2hhbm5lbCkge1xuICAgICAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoJ0NoYW5uZWwgJXMgd2FzIG5vdCBkZWZpbmVkIGluIHRoZSBjb25uZWN0aW9uIHByb2ZpbGUnLCBjaGFubmVsTmFtZSk7XG4gICAgICAgIGxvZ2dlci5lcnJvcihtZXNzYWdlKTtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKG1lc3NhZ2UpO1xuICAgICAgfVxuICAgICAgY29uc3QgdHhJZCA9IGNsaWVudC5uZXdUcmFuc2FjdGlvbklEKCk7XG4gICAgICAvLyB3aWxsIG5lZWQgdGhlIHRyYW5zYWN0aW9uIElEIHN0cmluZyBmb3IgdGhlIGV2ZW50IHJlZ2lzdHJhdGlvbiBsYXRlclxuICAgICAgdHhJZFN0cmluZyA9IHR4SWQuZ2V0VHJhbnNhY3Rpb25JRCgpO1xuXG4gICAgICAvLyBzZW5kIHByb3Bvc2FsIHRvIGVuZG9yc2VyXG4gICAgICBjb25zdCByZXF1ZXN0ID0ge1xuICAgICAgICB0YXJnZXRzOiBwZWVyTmFtZXMsXG4gICAgICAgIGNoYWluY29kZUlkOiBjaGFpbmNvZGVOYW1lLFxuICAgICAgICBmY24sXG4gICAgICAgIGFyZ3MsXG4gICAgICAgIGNoYWluSWQ6IGNoYW5uZWxOYW1lLFxuICAgICAgICB0eElkLFxuICAgICAgfTtcbiAgICAgIGxvZ2dlci5kZWJ1ZyhgcmVxdWVzdDoke0pTT04uc3RyaW5naWZ5KHJlcXVlc3QpfWApO1xuXG4gICAgICBjb25zdCByZXN1bHRzID0gYXdhaXQgY2hhbm5lbC5zZW5kVHJhbnNhY3Rpb25Qcm9wb3NhbE9CKHJlcXVlc3QpO1xuXG4gICAgICAvLyB0aGUgcmV0dXJuZWQgb2JqZWN0IGhhcyBib3RoIHRoZSBlbmRvcnNlbWVudCByZXN1bHRzXG4gICAgICAvLyBhbmQgdGhlIGFjdHVhbCBwcm9wb3NhbCwgdGhlIHByb3Bvc2FsIHdpbGwgYmUgbmVlZGVkXG4gICAgICAvLyBsYXRlciB3aGVuIHdlIHNlbmQgYSB0cmFuc2FjdGlvbiB0byB0aGUgb3JkZXJlclxuICAgICAgY29uc3QgcHJvcG9zYWxSZXNwb25zZXMgPSByZXN1bHRzWzBdO1xuICAgICAgY29uc3QgcHJvcG9zYWwgPSByZXN1bHRzWzFdO1xuXG4gICAgICAvLyBsZXRzIGhhdmUgYSBsb29rIGF0IHRoZSByZXNwb25zZXMgdG8gc2VlIGlmIHRoZXkgYXJlXG4gICAgICAvLyBhbGwgZ29vZCwgaWYgZ29vZCB0aGV5IHdpbGwgYWxzbyBpbmNsdWRlIHNpZ25hdHVyZXNcbiAgICAgIC8vIHJlcXVpcmVkIHRvIGJlIGNvbW1pdHRlZFxuICAgICAgbGV0IGFsbEdvb2QgPSB0cnVlO1xuICAgICAgZm9yIChjb25zdCBpIGluIHByb3Bvc2FsUmVzcG9uc2VzKSB7XG4gICAgICAgIGxldCBvbmVHb29kID0gZmFsc2U7XG4gICAgICAgIGlmIChwcm9wb3NhbFJlc3BvbnNlcyAmJiBwcm9wb3NhbFJlc3BvbnNlc1tpXS5yZXNwb25zZSAmJlxuICAgICAgICAgIHByb3Bvc2FsUmVzcG9uc2VzW2ldLnJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgb25lR29vZCA9IHRydWU7XG4gICAgICAgICAgbG9nZ2VyLmluZm8oJ2ludm9rZSBjaGFpbmNvZGUgcHJvcG9zYWwgd2FzIGdvb2QnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoSlNPTi5zdHJpbmdpZnkocHJvcG9zYWxSZXNwb25zZXMpKTtcbiAgICAgICAgICBsb2dnZXIuZXJyb3IoJ2ludm9rZSBjaGFpbmNvZGUgcHJvcG9zYWwgd2FzIGJhZCcpO1xuICAgICAgICB9XG4gICAgICAgIGFsbEdvb2QgJj0gb25lR29vZDtcbiAgICAgIH1cblxuICAgICAgaWYgKGFsbEdvb2QpIHtcbiAgICAgICAgbG9nZ2VyLmluZm8odXRpbC5mb3JtYXQoXG4gICAgICAgICAgJ1N1Y2Nlc3NmdWxseSBzZW50IFByb3Bvc2FsIGFuZCByZWNlaXZlZCBQcm9wb3NhbFJlc3BvbnNlOiBTdGF0dXMgLSAlcywgbWVzc2FnZSAtIFwiJXNcIiwgbWV0YWRhdGEgLSBcIiVzXCIsIGVuZG9yc2VtZW50IHNpZ25hdHVyZTogJXMnLFxuICAgICAgICAgIHByb3Bvc2FsUmVzcG9uc2VzWzBdLnJlc3BvbnNlLnN0YXR1cywgcHJvcG9zYWxSZXNwb25zZXNbMF0ucmVzcG9uc2UubWVzc2FnZSxcbiAgICAgICAgICBwcm9wb3NhbFJlc3BvbnNlc1swXS5yZXNwb25zZS5wYXlsb2FkLCBwcm9wb3NhbFJlc3BvbnNlc1swXS5lbmRvcnNlbWVudC5zaWduYXR1cmUpKTtcblxuICAgICAgICBjb25zdCBvcmRlcmVyUmVxID0ge1xuICAgICAgICAgIHR4SWQsXG4gICAgICAgICAgcHJvcG9zYWxSZXNwb25zZXMsXG4gICAgICAgICAgcHJvcG9zYWwsXG4gICAgICAgIH07XG5cbiAgICAgICAgY2hhbm5lbC5zZW5kVHJhbnNhY3Rpb25PQihvcmRlcmVyUmVxKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGVycm9yTXNnID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBzZW5kIFByb3Bvc2FsIGFuZCByZWNlaXZlIGFsbCBnb29kIFByb3Bvc2FsUmVzcG9uc2UnKTtcbiAgICAgICAgbG9nZ2VyLmRlYnVnKGVycm9yTXNnKTtcbiAgICAgIH1cbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgbG9nZ2VyLmVycm9yKGBGYWlsZWQgdG8gaW52b2tlIGR1ZSB0byBlcnJvcjogJHtlcnJvci5zdGFja31gID8gZXJyb3Iuc3RhY2sgOiBlcnJvcik7XG4gICAgICBlcnJvck1zZyA9IGVycm9yLnRvU3RyaW5nKCk7XG4gICAgfVxuXG4gICAgaWYgKCFlcnJvck1zZykge1xuICAgICAgY29uc3QgbWVzc2FnZSA9IHV0aWwuZm9ybWF0KFxuICAgICAgICAnU3VjY2Vzc2Z1bGx5IGludm9rZWQgdGhlIGNoYWluY29kZSAlcyB0byB0aGUgY2hhbm5lbCBcXCclc1xcJyBmb3IgdHJhbnNhY3Rpb24gSUQ6ICVzJyxcbiAgICAgICAgb3JnTmFtZSwgY2hhbm5lbE5hbWUsIHR4SWRTdHJpbmcpO1xuICAgICAgbG9nZ2VyLmluZm8obWVzc2FnZSk7XG5cbiAgICAgIHJldHVybiB0eElkU3RyaW5nO1xuICAgIH1cbiAgICBjb25zdCBtZXNzYWdlID0gdXRpbC5mb3JtYXQoJ0ZhaWxlZCB0byBpbnZva2UgY2hhaW5jb2RlLiBjYXVzZTolcycsIGVycm9yTXNnKTtcbiAgICBsb2dnZXIuZXJyb3IobWVzc2FnZSk7XG4gICAgdGhyb3cgbmV3IEVycm9yKG1lc3NhZ2UpO1xuICB9XG59XG5cbmNvbnN0IHRyeENsaWVudCA9IG5ldyBUcnhDbGllbnQoKTtcbm1vZHVsZS5leHBvcnRzID0gdHJ4Q2xpZW50O1xuIl19