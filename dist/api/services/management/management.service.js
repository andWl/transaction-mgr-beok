"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _logger = _interopRequireDefault(require("../../../common/logger"));

var _management = _interopRequireDefault(require("../../../blockchain/management/management"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * Copyright 2018 KT All Rights Reserved.
 *
 */
class ManagementService {
  getNetworkInfo(req, res) {
    _logger.default.info(`${this.constructor.name}.getNetworkInfo()`);

    return Promise.resolve(_management.default.getNetworkInfo());
  }

  getChannels(req, res) {
    _logger.default.info(`${this.constructor.name}.getChannels()`);

    if (req.query.orgName && req.query.peerName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.peerName = req.query.peerName;
      return Promise.resolve(_management.default.getChannels(params));
    } else if (!req.query.orgName && !req.query.peerName) {
      return Promise.resolve(_management.default.getChannels(''));
    }

    return Promise.resolve('Error: Input both orgName and peerName!');
  }

  getChannelsInfo(req, res) {
    _logger.default.info(`${this.constructor.name}.getChannelsInfo()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    return Promise.resolve(_management.default.getChannelsInfo(params));
  }

  getBlockByNo(req, res) {
    _logger.default.info(`${this.constructor.name}.getBlockByNo()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.no = req.query.no;
    return Promise.resolve(_management.default.getBlock(params));
  }

  getTxCntByBlock(req, res) {
    _logger.default.info(`${this.constructor.name}.getTxCntByBlock()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.no = req.query.no;
    return Promise.resolve(_management.default.getTxCntbyBlock(params));
  }

  getBlockByHash(req, res) {
    _logger.default.info(`${this.constructor.name}.getBlockByHash()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.hash = req.query.hash;
    return Promise.resolve(_management.default.getBlockByHash(params));
  }

  getInstalledChaincodes(req, res) {
    _logger.default.info(`${this.constructor.name}.getInstalledChaincodes()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    return Promise.resolve(_management.default.getInstalledChaincodes(params));
  }

  getInstantiatedChaincodes(req, res) {
    _logger.default.info(`${this.constructor.name}.getInstantiatedChaincodes()`);

    const params = [];
    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    return Promise.resolve(_management.default.getInstantiatedChaincodes(params));
  }

  getQueryPeers(req, res) {
    _logger.default.info(`${this.constructor.name}.getQueryPeers()`);

    const peerRole = 'chaincodeQuery';

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(_management.default.getPeers(params, peerRole));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(_management.default.getPeers('', peerRole));
    }

    return Promise.resolve('Error: Input both orgName and channelName!');
  }

  getEndorsingPeers(req, res) {
    _logger.default.info(`${this.constructor.name}.getEndorsingPeers()`);

    const peerRole = 'endorsingPeer';

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(_management.default.getPeers(params, peerRole));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(_management.default.getPeers('', peerRole));
    }

    return Promise.resolve('Error: Input both orgName and channelName!');
  }

  getDiscoveryInfo(req, res) {
    _logger.default.info(`${this.constructor.name}.getDiscoveryInfo()`);

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(_management.default.getDiscoveryInfo(params));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(_management.default.getDiscoveryInfo(''));
    }

    return Promise.resolve('Error: Input both orgName and channelName!');
  }

}

var _default = new ManagementService();

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvc2VydmljZXMvbWFuYWdlbWVudC9tYW5hZ2VtZW50LnNlcnZpY2UuanMiXSwibmFtZXMiOlsiTWFuYWdlbWVudFNlcnZpY2UiLCJnZXROZXR3b3JrSW5mbyIsInJlcSIsInJlcyIsImxvZ2dlciIsImluZm8iLCJjb25zdHJ1Y3RvciIsIm5hbWUiLCJQcm9taXNlIiwicmVzb2x2ZSIsIm1hbmFnZW1lbnQiLCJnZXRDaGFubmVscyIsInF1ZXJ5Iiwib3JnTmFtZSIsInBlZXJOYW1lIiwicGFyYW1zIiwiZ2V0Q2hhbm5lbHNJbmZvIiwiY2hhbm5lbE5hbWUiLCJnZXRCbG9ja0J5Tm8iLCJubyIsImdldEJsb2NrIiwiZ2V0VHhDbnRCeUJsb2NrIiwiZ2V0VHhDbnRieUJsb2NrIiwiZ2V0QmxvY2tCeUhhc2giLCJoYXNoIiwiZ2V0SW5zdGFsbGVkQ2hhaW5jb2RlcyIsImdldEluc3RhbnRpYXRlZENoYWluY29kZXMiLCJnZXRRdWVyeVBlZXJzIiwicGVlclJvbGUiLCJnZXRQZWVycyIsImdldEVuZG9yc2luZ1BlZXJzIiwiZ2V0RGlzY292ZXJ5SW5mbyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQU1BOztBQUNBOzs7O0FBUEE7Ozs7O0FBU0EsTUFBTUEsaUJBQU4sQ0FBd0I7QUFDdEJDLEVBQUFBLGNBQWMsQ0FBQ0MsR0FBRCxFQUFNQyxHQUFOLEVBQVc7QUFDdkJDLG9CQUFPQyxJQUFQLENBQWEsR0FBRSxLQUFLQyxXQUFMLENBQWlCQyxJQUFLLG1CQUFyQzs7QUFDQSxXQUFPQyxPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXVCxjQUFYLEVBQWhCLENBQVA7QUFDRDs7QUFFRFUsRUFBQUEsV0FBVyxDQUFDVCxHQUFELEVBQU1DLEdBQU4sRUFBVztBQUNwQkMsb0JBQU9DLElBQVAsQ0FBYSxHQUFFLEtBQUtDLFdBQUwsQ0FBaUJDLElBQUssZ0JBQXJDOztBQUVBLFFBQUlMLEdBQUcsQ0FBQ1UsS0FBSixDQUFVQyxPQUFWLElBQXFCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBbkMsRUFBNkM7QUFDM0MsWUFBTUMsTUFBTSxHQUFHLEVBQWY7QUFDQUEsTUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsTUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFDQSxhQUFPTixPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXQyxXQUFYLENBQXVCSSxNQUF2QixDQUFoQixDQUFQO0FBQ0QsS0FMRCxNQUtPLElBQUksQ0FBQ2IsR0FBRyxDQUFDVSxLQUFKLENBQVVDLE9BQVgsSUFBc0IsQ0FBQ1gsR0FBRyxDQUFDVSxLQUFKLENBQVVFLFFBQXJDLEVBQStDO0FBQ3BELGFBQU9OLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsb0JBQVdDLFdBQVgsQ0FBdUIsRUFBdkIsQ0FBaEIsQ0FBUDtBQUNEOztBQUNELFdBQU9ILE9BQU8sQ0FBQ0MsT0FBUixDQUFnQix5Q0FBaEIsQ0FBUDtBQUNEOztBQUVETyxFQUFBQSxlQUFlLENBQUNkLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3hCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxvQkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFFQSxXQUFPTixPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXTSxlQUFYLENBQTJCRCxNQUEzQixDQUFoQixDQUFQO0FBQ0Q7O0FBRURHLEVBQUFBLFlBQVksQ0FBQ2hCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3JCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxpQkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFDQUMsSUFBQUEsTUFBTSxDQUFDSSxFQUFQLEdBQVlqQixHQUFHLENBQUNVLEtBQUosQ0FBVU8sRUFBdEI7QUFFQSxXQUFPWCxPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXVSxRQUFYLENBQW9CTCxNQUFwQixDQUFoQixDQUFQO0FBQ0Q7O0FBRURNLEVBQUFBLGVBQWUsQ0FBQ25CLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3hCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxvQkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFDQUMsSUFBQUEsTUFBTSxDQUFDSSxFQUFQLEdBQVlqQixHQUFHLENBQUNVLEtBQUosQ0FBVU8sRUFBdEI7QUFFQSxXQUFPWCxPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXWSxlQUFYLENBQTJCUCxNQUEzQixDQUFoQixDQUFQO0FBQ0Q7O0FBRURRLEVBQUFBLGNBQWMsQ0FBQ3JCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3ZCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxtQkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFDQUMsSUFBQUEsTUFBTSxDQUFDUyxJQUFQLEdBQWN0QixHQUFHLENBQUNVLEtBQUosQ0FBVVksSUFBeEI7QUFFQSxXQUFPaEIsT0FBTyxDQUFDQyxPQUFSLENBQWdCQyxvQkFBV2EsY0FBWCxDQUEwQlIsTUFBMUIsQ0FBaEIsQ0FBUDtBQUNEOztBQUVEVSxFQUFBQSxzQkFBc0IsQ0FBQ3ZCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQy9CQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSywyQkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFFQSxXQUFPTixPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXZSxzQkFBWCxDQUFrQ1YsTUFBbEMsQ0FBaEIsQ0FBUDtBQUNEOztBQUVEVyxFQUFBQSx5QkFBeUIsQ0FBQ3hCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ2xDQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyw4QkFBckM7O0FBQ0EsVUFBTVEsTUFBTSxHQUFHLEVBQWY7QUFFQUEsSUFBQUEsTUFBTSxDQUFDRixPQUFQLEdBQWlCWCxHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBM0I7QUFDQUUsSUFBQUEsTUFBTSxDQUFDRSxXQUFQLEdBQXFCZixHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBL0I7QUFDQUYsSUFBQUEsTUFBTSxDQUFDRCxRQUFQLEdBQWtCWixHQUFHLENBQUNVLEtBQUosQ0FBVUUsUUFBNUI7QUFFQSxXQUFPTixPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLG9CQUFXZ0IseUJBQVgsQ0FBcUNYLE1BQXJDLENBQWhCLENBQVA7QUFDRDs7QUFFRFksRUFBQUEsYUFBYSxDQUFDekIsR0FBRCxFQUFNQyxHQUFOLEVBQVc7QUFDdEJDLG9CQUFPQyxJQUFQLENBQWEsR0FBRSxLQUFLQyxXQUFMLENBQWlCQyxJQUFLLGtCQUFyQzs7QUFDQSxVQUFNcUIsUUFBUSxHQUFHLGdCQUFqQjs7QUFFQSxRQUFJMUIsR0FBRyxDQUFDVSxLQUFKLENBQVVDLE9BQVYsSUFBcUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUFuQyxFQUFnRDtBQUM5QyxZQUFNRixNQUFNLEdBQUcsRUFBZjtBQUNBQSxNQUFBQSxNQUFNLENBQUNGLE9BQVAsR0FBaUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVQyxPQUEzQjtBQUNBRSxNQUFBQSxNQUFNLENBQUNFLFdBQVAsR0FBcUJmLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUEvQjtBQUNBLGFBQU9ULE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsb0JBQVdtQixRQUFYLENBQW9CZCxNQUFwQixFQUE0QmEsUUFBNUIsQ0FBaEIsQ0FBUDtBQUNELEtBTEQsTUFLTyxJQUFJLENBQUMxQixHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBWCxJQUFzQixDQUFDWCxHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBckMsRUFBa0Q7QUFDdkQsYUFBT1QsT0FBTyxDQUFDQyxPQUFSLENBQWdCQyxvQkFBV21CLFFBQVgsQ0FBb0IsRUFBcEIsRUFBd0JELFFBQXhCLENBQWhCLENBQVA7QUFDRDs7QUFDRCxXQUFPcEIsT0FBTyxDQUFDQyxPQUFSLENBQWdCLDRDQUFoQixDQUFQO0FBQ0Q7O0FBRURxQixFQUFBQSxpQkFBaUIsQ0FBQzVCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQzFCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxzQkFBckM7O0FBQ0EsVUFBTXFCLFFBQVEsR0FBRyxlQUFqQjs7QUFFQSxRQUFJMUIsR0FBRyxDQUFDVSxLQUFKLENBQVVDLE9BQVYsSUFBcUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUFuQyxFQUFnRDtBQUM5QyxZQUFNRixNQUFNLEdBQUcsRUFBZjtBQUNBQSxNQUFBQSxNQUFNLENBQUNGLE9BQVAsR0FBaUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVQyxPQUEzQjtBQUNBRSxNQUFBQSxNQUFNLENBQUNFLFdBQVAsR0FBcUJmLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUEvQjtBQUNBLGFBQU9ULE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsb0JBQVdtQixRQUFYLENBQW9CZCxNQUFwQixFQUE0QmEsUUFBNUIsQ0FBaEIsQ0FBUDtBQUNELEtBTEQsTUFLTyxJQUFJLENBQUMxQixHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBWCxJQUFzQixDQUFDWCxHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBckMsRUFBa0Q7QUFDdkQsYUFBT1QsT0FBTyxDQUFDQyxPQUFSLENBQWdCQyxvQkFBV21CLFFBQVgsQ0FBb0IsRUFBcEIsRUFBd0JELFFBQXhCLENBQWhCLENBQVA7QUFDRDs7QUFDRCxXQUFPcEIsT0FBTyxDQUFDQyxPQUFSLENBQWdCLDRDQUFoQixDQUFQO0FBQ0Q7O0FBRURzQixFQUFBQSxnQkFBZ0IsQ0FBQzdCLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3pCQyxvQkFBT0MsSUFBUCxDQUFhLEdBQUUsS0FBS0MsV0FBTCxDQUFpQkMsSUFBSyxxQkFBckM7O0FBRUEsUUFBSUwsR0FBRyxDQUFDVSxLQUFKLENBQVVDLE9BQVYsSUFBcUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUFuQyxFQUFnRDtBQUM5QyxZQUFNRixNQUFNLEdBQUcsRUFBZjtBQUNBQSxNQUFBQSxNQUFNLENBQUNGLE9BQVAsR0FBaUJYLEdBQUcsQ0FBQ1UsS0FBSixDQUFVQyxPQUEzQjtBQUNBRSxNQUFBQSxNQUFNLENBQUNFLFdBQVAsR0FBcUJmLEdBQUcsQ0FBQ1UsS0FBSixDQUFVSyxXQUEvQjtBQUNBLGFBQU9ULE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsb0JBQVdxQixnQkFBWCxDQUE0QmhCLE1BQTVCLENBQWhCLENBQVA7QUFDRCxLQUxELE1BS08sSUFBSSxDQUFDYixHQUFHLENBQUNVLEtBQUosQ0FBVUMsT0FBWCxJQUFzQixDQUFDWCxHQUFHLENBQUNVLEtBQUosQ0FBVUssV0FBckMsRUFBa0Q7QUFDdkQsYUFBT1QsT0FBTyxDQUFDQyxPQUFSLENBQWdCQyxvQkFBV3FCLGdCQUFYLENBQTRCLEVBQTVCLENBQWhCLENBQVA7QUFDRDs7QUFDRCxXQUFPdkIsT0FBTyxDQUFDQyxPQUFSLENBQWdCLDRDQUFoQixDQUFQO0FBQ0Q7O0FBbklxQjs7ZUFzSVQsSUFBSVQsaUJBQUosRSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICpcbiAqIENvcHlyaWdodCAyMDE4IEtUIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKlxuICovXG5cbmltcG9ydCBsb2dnZXIgZnJvbSAnLi4vLi4vLi4vY29tbW9uL2xvZ2dlcic7XG5pbXBvcnQgbWFuYWdlbWVudCBmcm9tICcuLi8uLi8uLi9ibG9ja2NoYWluL21hbmFnZW1lbnQvbWFuYWdlbWVudCc7XG5cbmNsYXNzIE1hbmFnZW1lbnRTZXJ2aWNlIHtcbiAgZ2V0TmV0d29ya0luZm8ocmVxLCByZXMpIHtcbiAgICBsb2dnZXIuaW5mbyhgJHt0aGlzLmNvbnN0cnVjdG9yLm5hbWV9LmdldE5ldHdvcmtJbmZvKClgKTtcbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG1hbmFnZW1lbnQuZ2V0TmV0d29ya0luZm8oKSk7XG4gIH1cblxuICBnZXRDaGFubmVscyhyZXEsIHJlcykge1xuICAgIGxvZ2dlci5pbmZvKGAke3RoaXMuY29uc3RydWN0b3IubmFtZX0uZ2V0Q2hhbm5lbHMoKWApO1xuXG4gICAgaWYgKHJlcS5xdWVyeS5vcmdOYW1lICYmIHJlcS5xdWVyeS5wZWVyTmFtZSkge1xuICAgICAgY29uc3QgcGFyYW1zID0gW107XG4gICAgICBwYXJhbXMub3JnTmFtZSA9IHJlcS5xdWVyeS5vcmdOYW1lO1xuICAgICAgcGFyYW1zLnBlZXJOYW1lID0gcmVxLnF1ZXJ5LnBlZXJOYW1lO1xuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShtYW5hZ2VtZW50LmdldENoYW5uZWxzKHBhcmFtcykpO1xuICAgIH0gZWxzZSBpZiAoIXJlcS5xdWVyeS5vcmdOYW1lICYmICFyZXEucXVlcnkucGVlck5hbWUpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRDaGFubmVscygnJykpO1xuICAgIH1cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCdFcnJvcjogSW5wdXQgYm90aCBvcmdOYW1lIGFuZCBwZWVyTmFtZSEnKTtcbiAgfVxuXG4gIGdldENoYW5uZWxzSW5mbyhyZXEsIHJlcykge1xuICAgIGxvZ2dlci5pbmZvKGAke3RoaXMuY29uc3RydWN0b3IubmFtZX0uZ2V0Q2hhbm5lbHNJbmZvKClgKTtcbiAgICBjb25zdCBwYXJhbXMgPSBbXTtcblxuICAgIHBhcmFtcy5vcmdOYW1lID0gcmVxLnF1ZXJ5Lm9yZ05hbWU7XG4gICAgcGFyYW1zLmNoYW5uZWxOYW1lID0gcmVxLnF1ZXJ5LmNoYW5uZWxOYW1lO1xuICAgIHBhcmFtcy5wZWVyTmFtZSA9IHJlcS5xdWVyeS5wZWVyTmFtZTtcblxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRDaGFubmVsc0luZm8ocGFyYW1zKSk7XG4gIH1cblxuICBnZXRCbG9ja0J5Tm8ocmVxLCByZXMpIHtcbiAgICBsb2dnZXIuaW5mbyhgJHt0aGlzLmNvbnN0cnVjdG9yLm5hbWV9LmdldEJsb2NrQnlObygpYCk7XG4gICAgY29uc3QgcGFyYW1zID0gW107XG5cbiAgICBwYXJhbXMub3JnTmFtZSA9IHJlcS5xdWVyeS5vcmdOYW1lO1xuICAgIHBhcmFtcy5jaGFubmVsTmFtZSA9IHJlcS5xdWVyeS5jaGFubmVsTmFtZTtcbiAgICBwYXJhbXMucGVlck5hbWUgPSByZXEucXVlcnkucGVlck5hbWU7XG4gICAgcGFyYW1zLm5vID0gcmVxLnF1ZXJ5Lm5vO1xuXG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShtYW5hZ2VtZW50LmdldEJsb2NrKHBhcmFtcykpO1xuICB9XG5cbiAgZ2V0VHhDbnRCeUJsb2NrKHJlcSwgcmVzKSB7XG4gICAgbG9nZ2VyLmluZm8oYCR7dGhpcy5jb25zdHJ1Y3Rvci5uYW1lfS5nZXRUeENudEJ5QmxvY2soKWApO1xuICAgIGNvbnN0IHBhcmFtcyA9IFtdO1xuXG4gICAgcGFyYW1zLm9yZ05hbWUgPSByZXEucXVlcnkub3JnTmFtZTtcbiAgICBwYXJhbXMuY2hhbm5lbE5hbWUgPSByZXEucXVlcnkuY2hhbm5lbE5hbWU7XG4gICAgcGFyYW1zLnBlZXJOYW1lID0gcmVxLnF1ZXJ5LnBlZXJOYW1lO1xuICAgIHBhcmFtcy5ubyA9IHJlcS5xdWVyeS5ubztcblxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRUeENudGJ5QmxvY2socGFyYW1zKSk7XG4gIH1cblxuICBnZXRCbG9ja0J5SGFzaChyZXEsIHJlcykge1xuICAgIGxvZ2dlci5pbmZvKGAke3RoaXMuY29uc3RydWN0b3IubmFtZX0uZ2V0QmxvY2tCeUhhc2goKWApO1xuICAgIGNvbnN0IHBhcmFtcyA9IFtdO1xuXG4gICAgcGFyYW1zLm9yZ05hbWUgPSByZXEucXVlcnkub3JnTmFtZTtcbiAgICBwYXJhbXMuY2hhbm5lbE5hbWUgPSByZXEucXVlcnkuY2hhbm5lbE5hbWU7XG4gICAgcGFyYW1zLnBlZXJOYW1lID0gcmVxLnF1ZXJ5LnBlZXJOYW1lO1xuICAgIHBhcmFtcy5oYXNoID0gcmVxLnF1ZXJ5Lmhhc2g7XG5cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG1hbmFnZW1lbnQuZ2V0QmxvY2tCeUhhc2gocGFyYW1zKSk7XG4gIH1cblxuICBnZXRJbnN0YWxsZWRDaGFpbmNvZGVzKHJlcSwgcmVzKSB7XG4gICAgbG9nZ2VyLmluZm8oYCR7dGhpcy5jb25zdHJ1Y3Rvci5uYW1lfS5nZXRJbnN0YWxsZWRDaGFpbmNvZGVzKClgKTtcbiAgICBjb25zdCBwYXJhbXMgPSBbXTtcblxuICAgIHBhcmFtcy5vcmdOYW1lID0gcmVxLnF1ZXJ5Lm9yZ05hbWU7XG4gICAgcGFyYW1zLmNoYW5uZWxOYW1lID0gcmVxLnF1ZXJ5LmNoYW5uZWxOYW1lO1xuICAgIHBhcmFtcy5wZWVyTmFtZSA9IHJlcS5xdWVyeS5wZWVyTmFtZTtcblxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRJbnN0YWxsZWRDaGFpbmNvZGVzKHBhcmFtcykpO1xuICB9XG5cbiAgZ2V0SW5zdGFudGlhdGVkQ2hhaW5jb2RlcyhyZXEsIHJlcykge1xuICAgIGxvZ2dlci5pbmZvKGAke3RoaXMuY29uc3RydWN0b3IubmFtZX0uZ2V0SW5zdGFudGlhdGVkQ2hhaW5jb2RlcygpYCk7XG4gICAgY29uc3QgcGFyYW1zID0gW107XG5cbiAgICBwYXJhbXMub3JnTmFtZSA9IHJlcS5xdWVyeS5vcmdOYW1lO1xuICAgIHBhcmFtcy5jaGFubmVsTmFtZSA9IHJlcS5xdWVyeS5jaGFubmVsTmFtZTtcbiAgICBwYXJhbXMucGVlck5hbWUgPSByZXEucXVlcnkucGVlck5hbWU7XG5cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG1hbmFnZW1lbnQuZ2V0SW5zdGFudGlhdGVkQ2hhaW5jb2RlcyhwYXJhbXMpKTtcbiAgfVxuXG4gIGdldFF1ZXJ5UGVlcnMocmVxLCByZXMpIHtcbiAgICBsb2dnZXIuaW5mbyhgJHt0aGlzLmNvbnN0cnVjdG9yLm5hbWV9LmdldFF1ZXJ5UGVlcnMoKWApO1xuICAgIGNvbnN0IHBlZXJSb2xlID0gJ2NoYWluY29kZVF1ZXJ5JztcblxuICAgIGlmIChyZXEucXVlcnkub3JnTmFtZSAmJiByZXEucXVlcnkuY2hhbm5lbE5hbWUpIHtcbiAgICAgIGNvbnN0IHBhcmFtcyA9IFtdO1xuICAgICAgcGFyYW1zLm9yZ05hbWUgPSByZXEucXVlcnkub3JnTmFtZTtcbiAgICAgIHBhcmFtcy5jaGFubmVsTmFtZSA9IHJlcS5xdWVyeS5jaGFubmVsTmFtZTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRQZWVycyhwYXJhbXMsIHBlZXJSb2xlKSk7XG4gICAgfSBlbHNlIGlmICghcmVxLnF1ZXJ5Lm9yZ05hbWUgJiYgIXJlcS5xdWVyeS5jaGFubmVsTmFtZSkge1xuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShtYW5hZ2VtZW50LmdldFBlZXJzKCcnLCBwZWVyUm9sZSkpO1xuICAgIH1cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCdFcnJvcjogSW5wdXQgYm90aCBvcmdOYW1lIGFuZCBjaGFubmVsTmFtZSEnKTtcbiAgfVxuXG4gIGdldEVuZG9yc2luZ1BlZXJzKHJlcSwgcmVzKSB7XG4gICAgbG9nZ2VyLmluZm8oYCR7dGhpcy5jb25zdHJ1Y3Rvci5uYW1lfS5nZXRFbmRvcnNpbmdQZWVycygpYCk7XG4gICAgY29uc3QgcGVlclJvbGUgPSAnZW5kb3JzaW5nUGVlcic7XG5cbiAgICBpZiAocmVxLnF1ZXJ5Lm9yZ05hbWUgJiYgcmVxLnF1ZXJ5LmNoYW5uZWxOYW1lKSB7XG4gICAgICBjb25zdCBwYXJhbXMgPSBbXTtcbiAgICAgIHBhcmFtcy5vcmdOYW1lID0gcmVxLnF1ZXJ5Lm9yZ05hbWU7XG4gICAgICBwYXJhbXMuY2hhbm5lbE5hbWUgPSByZXEucXVlcnkuY2hhbm5lbE5hbWU7XG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKG1hbmFnZW1lbnQuZ2V0UGVlcnMocGFyYW1zLCBwZWVyUm9sZSkpO1xuICAgIH0gZWxzZSBpZiAoIXJlcS5xdWVyeS5vcmdOYW1lICYmICFyZXEucXVlcnkuY2hhbm5lbE5hbWUpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXRQZWVycygnJywgcGVlclJvbGUpKTtcbiAgICB9XG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgnRXJyb3I6IElucHV0IGJvdGggb3JnTmFtZSBhbmQgY2hhbm5lbE5hbWUhJyk7XG4gIH1cblxuICBnZXREaXNjb3ZlcnlJbmZvKHJlcSwgcmVzKSB7XG4gICAgbG9nZ2VyLmluZm8oYCR7dGhpcy5jb25zdHJ1Y3Rvci5uYW1lfS5nZXREaXNjb3ZlcnlJbmZvKClgKTtcblxuICAgIGlmIChyZXEucXVlcnkub3JnTmFtZSAmJiByZXEucXVlcnkuY2hhbm5lbE5hbWUpIHtcbiAgICAgIGNvbnN0IHBhcmFtcyA9IFtdO1xuICAgICAgcGFyYW1zLm9yZ05hbWUgPSByZXEucXVlcnkub3JnTmFtZTtcbiAgICAgIHBhcmFtcy5jaGFubmVsTmFtZSA9IHJlcS5xdWVyeS5jaGFubmVsTmFtZTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXREaXNjb3ZlcnlJbmZvKHBhcmFtcykpO1xuICAgIH0gZWxzZSBpZiAoIXJlcS5xdWVyeS5vcmdOYW1lICYmICFyZXEucXVlcnkuY2hhbm5lbE5hbWUpIHtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUobWFuYWdlbWVudC5nZXREaXNjb3ZlcnlJbmZvKCcnKSk7XG4gICAgfVxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoJ0Vycm9yOiBJbnB1dCBib3RoIG9yZ05hbWUgYW5kIGNoYW5uZWxOYW1lIScpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBNYW5hZ2VtZW50U2VydmljZSgpO1xuIl19