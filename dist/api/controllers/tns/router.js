"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var express = _interopRequireWildcard(require("express"));

var _controller = _interopRequireDefault(require("./controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _default = express.Router().post('/:chaincodeName/invoke/:functionName', _controller.default.invokeTns).post('/:chaincodeName/query/:functionName', _controller.default.queryTns);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvdG5zL3JvdXRlci5qcyJdLCJuYW1lcyI6WyJleHByZXNzIiwiUm91dGVyIiwicG9zdCIsImNvbnRyb2xsZXIiLCJpbnZva2VUbnMiLCJxdWVyeVRucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7ZUFFZUEsT0FBTyxDQUNuQkMsTUFEWSxHQUVaQyxJQUZZLENBRVAsc0NBRk8sRUFFaUNDLG9CQUFXQyxTQUY1QyxFQUdaRixJQUhZLENBR1AscUNBSE8sRUFHZ0NDLG9CQUFXRSxRQUgzQyxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgZXhwcmVzcyBmcm9tICdleHByZXNzJztcbmltcG9ydCBjb250cm9sbGVyIGZyb20gJy4vY29udHJvbGxlcic7XG5cbmV4cG9ydCBkZWZhdWx0IGV4cHJlc3NcbiAgLlJvdXRlcigpXG4gIC5wb3N0KCcvOmNoYWluY29kZU5hbWUvaW52b2tlLzpmdW5jdGlvbk5hbWUnLCBjb250cm9sbGVyLmludm9rZVRucylcbiAgLnBvc3QoJy86Y2hhaW5jb2RlTmFtZS9xdWVyeS86ZnVuY3Rpb25OYW1lJywgY29udHJvbGxlci5xdWVyeVRucyk7XG4iXX0=