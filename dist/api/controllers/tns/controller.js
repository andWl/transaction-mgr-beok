"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Controller = void 0;

var _tns = _interopRequireDefault(require("../../services/tns/tns.service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Controller {
  invokeTns(req, res) {
    _tns.default.invokeTns(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

  queryTns(req, res) {
    _tns.default.queryTns(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

}

exports.Controller = Controller;

var _default = new Controller();

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvdG5zL2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsiQ29udHJvbGxlciIsImludm9rZVRucyIsInJlcSIsInJlcyIsIlRuc1NlcnZpY2UiLCJ0aGVuIiwiciIsImpzb24iLCJzdGF0dXMiLCJlbmQiLCJxdWVyeVRucyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRU8sTUFBTUEsVUFBTixDQUFpQjtBQUN0QkMsRUFBQUEsU0FBUyxDQUFDQyxHQUFELEVBQU1DLEdBQU4sRUFBVztBQUNsQkMsaUJBQ0dILFNBREgsQ0FDYUMsR0FEYixFQUNrQkMsR0FEbEIsRUFFR0UsSUFGSCxDQUVRQyxDQUFDLElBQUk7QUFDVCxVQUFJQSxDQUFKLEVBQU9ILEdBQUcsQ0FBQ0ksSUFBSixDQUFTRCxDQUFULEVBQVAsS0FDS0gsR0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsR0FBaEI7QUFDTixLQUxIO0FBTUQ7O0FBRURDLEVBQUFBLFFBQVEsQ0FBQ1IsR0FBRCxFQUFNQyxHQUFOLEVBQVc7QUFDakJDLGlCQUNHTSxRQURILENBQ1lSLEdBRFosRUFDaUJDLEdBRGpCLEVBRUdFLElBRkgsQ0FFUUMsQ0FBQyxJQUFJO0FBQ1QsVUFBSUEsQ0FBSixFQUFPSCxHQUFHLENBQUNJLElBQUosQ0FBU0QsQ0FBVCxFQUFQLEtBQ0tILEdBQUcsQ0FBQ0ssTUFBSixDQUFXLEdBQVgsRUFBZ0JDLEdBQWhCO0FBQ04sS0FMSDtBQU1EOztBQWpCcUI7Ozs7ZUFvQlQsSUFBSVQsVUFBSixFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRuc1NlcnZpY2UgZnJvbSAnLi4vLi4vc2VydmljZXMvdG5zL3Rucy5zZXJ2aWNlJztcblxuZXhwb3J0IGNsYXNzIENvbnRyb2xsZXIge1xuICBpbnZva2VUbnMocmVxLCByZXMpIHtcbiAgICBUbnNTZXJ2aWNlXG4gICAgICAuaW52b2tlVG5zKHJlcSwgcmVzKVxuICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgIGlmIChyKSByZXMuanNvbihyKTtcbiAgICAgICAgZWxzZSByZXMuc3RhdHVzKDQwNCkuZW5kKCk7XG4gICAgICB9KTtcbiAgfVxuXG4gIHF1ZXJ5VG5zKHJlcSwgcmVzKSB7XG4gICAgVG5zU2VydmljZVxuICAgICAgLnF1ZXJ5VG5zKHJlcSwgcmVzKVxuICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgIGlmIChyKSByZXMuanNvbihyKTtcbiAgICAgICAgZWxzZSByZXMuc3RhdHVzKDQwNCkuZW5kKCk7XG4gICAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgQ29udHJvbGxlcigpO1xuIl19