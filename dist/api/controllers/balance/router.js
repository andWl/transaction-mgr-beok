"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var express = _interopRequireWildcard(require("express"));

var _controller = _interopRequireDefault(require("./controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _default = express.Router().get('/:id', _controller.default.getBalance).post('/', _controller.default.move).post('/user', _controller.default.addUser).post('/options', _controller.default.moveOption);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvYmFsYW5jZS9yb3V0ZXIuanMiXSwibmFtZXMiOlsiZXhwcmVzcyIsIlJvdXRlciIsImdldCIsImNvbnRyb2xsZXIiLCJnZXRCYWxhbmNlIiwicG9zdCIsIm1vdmUiLCJhZGRVc2VyIiwibW92ZU9wdGlvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7ZUFFZUEsT0FBTyxDQUNuQkMsTUFEWSxHQUVaQyxHQUZZLENBRVIsTUFGUSxFQUVBQyxvQkFBV0MsVUFGWCxFQUdaQyxJQUhZLENBR1AsR0FITyxFQUdGRixvQkFBV0csSUFIVCxFQUlaRCxJQUpZLENBSVAsT0FKTyxFQUlFRixvQkFBV0ksT0FKYixFQUtaRixJQUxZLENBS1AsVUFMTyxFQUtLRixvQkFBV0ssVUFMaEIsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGV4cHJlc3MgZnJvbSAnZXhwcmVzcyc7XG5pbXBvcnQgY29udHJvbGxlciBmcm9tICcuL2NvbnRyb2xsZXInO1xuXG5leHBvcnQgZGVmYXVsdCBleHByZXNzXG4gIC5Sb3V0ZXIoKVxuICAuZ2V0KCcvOmlkJywgY29udHJvbGxlci5nZXRCYWxhbmNlKVxuICAucG9zdCgnLycsIGNvbnRyb2xsZXIubW92ZSlcbiAgLnBvc3QoJy91c2VyJywgY29udHJvbGxlci5hZGRVc2VyKVxuICAucG9zdCgnL29wdGlvbnMnLCBjb250cm9sbGVyLm1vdmVPcHRpb24pO1xuIl19