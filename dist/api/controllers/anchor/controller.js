"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Controller = void 0;

var _anchor = _interopRequireDefault(require("../../services/anchor/anchor.service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Controller {
  setMessage(req, res) {
    _anchor.default.setmessage(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

  getMessage(req, res) {
    _anchor.default.getmessage(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

  healthCheck(req, res) {
    _anchor.default.healthcheck(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

}

exports.Controller = Controller;

var _default = new Controller();

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvYW5jaG9yL2NvbnRyb2xsZXIuanMiXSwibmFtZXMiOlsiQ29udHJvbGxlciIsInNldE1lc3NhZ2UiLCJyZXEiLCJyZXMiLCJBbmNob3JTZXJ2aWNlIiwic2V0bWVzc2FnZSIsInRoZW4iLCJyIiwianNvbiIsInN0YXR1cyIsImVuZCIsImdldE1lc3NhZ2UiLCJnZXRtZXNzYWdlIiwiaGVhbHRoQ2hlY2siLCJoZWFsdGhjaGVjayJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRU8sTUFBTUEsVUFBTixDQUFpQjtBQUN0QkMsRUFBQUEsVUFBVSxDQUFDQyxHQUFELEVBQU1DLEdBQU4sRUFBVztBQUNuQkMsb0JBQ0dDLFVBREgsQ0FDY0gsR0FEZCxFQUNtQkMsR0FEbkIsRUFFR0csSUFGSCxDQUVRQyxDQUFDLElBQUk7QUFDVCxVQUFJQSxDQUFKLEVBQU9KLEdBQUcsQ0FBQ0ssSUFBSixDQUFTRCxDQUFULEVBQVAsS0FDS0osR0FBRyxDQUFDTSxNQUFKLENBQVcsR0FBWCxFQUFnQkMsR0FBaEI7QUFDTixLQUxIO0FBTUQ7O0FBQ0RDLEVBQUFBLFVBQVUsQ0FBQ1QsR0FBRCxFQUFNQyxHQUFOLEVBQVc7QUFDbkJDLG9CQUNHUSxVQURILENBQ2NWLEdBRGQsRUFDbUJDLEdBRG5CLEVBRUdHLElBRkgsQ0FFUUMsQ0FBQyxJQUFJO0FBQ1QsVUFBSUEsQ0FBSixFQUFPSixHQUFHLENBQUNLLElBQUosQ0FBU0QsQ0FBVCxFQUFQLEtBQ0tKLEdBQUcsQ0FBQ00sTUFBSixDQUFXLEdBQVgsRUFBZ0JDLEdBQWhCO0FBQ04sS0FMSDtBQU1EOztBQUNERyxFQUFBQSxXQUFXLENBQUNYLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3BCQyxvQkFDR1UsV0FESCxDQUNlWixHQURmLEVBQ29CQyxHQURwQixFQUVHRyxJQUZILENBRVFDLENBQUMsSUFBSTtBQUNULFVBQUlBLENBQUosRUFBT0osR0FBRyxDQUFDSyxJQUFKLENBQVNELENBQVQsRUFBUCxLQUNLSixHQUFHLENBQUNNLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxHQUFoQjtBQUNOLEtBTEg7QUFNRDs7QUF4QnFCOzs7O2VBMkJULElBQUlWLFVBQUosRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBBbmNob3JTZXJ2aWNlIGZyb20gJy4uLy4uL3NlcnZpY2VzL2FuY2hvci9hbmNob3Iuc2VydmljZSc7XG5cbmV4cG9ydCBjbGFzcyBDb250cm9sbGVyIHtcbiAgc2V0TWVzc2FnZShyZXEsIHJlcykge1xuICAgIEFuY2hvclNlcnZpY2VcbiAgICAgIC5zZXRtZXNzYWdlKHJlcSwgcmVzKVxuICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgIGlmIChyKSByZXMuanNvbihyKTtcbiAgICAgICAgZWxzZSByZXMuc3RhdHVzKDQwNCkuZW5kKCk7XG4gICAgICB9KTtcbiAgfVxuICBnZXRNZXNzYWdlKHJlcSwgcmVzKSB7XG4gICAgQW5jaG9yU2VydmljZVxuICAgICAgLmdldG1lc3NhZ2UocmVxLCByZXMpXG4gICAgICAudGhlbihyID0+IHtcbiAgICAgICAgaWYgKHIpIHJlcy5qc29uKHIpO1xuICAgICAgICBlbHNlIHJlcy5zdGF0dXMoNDA0KS5lbmQoKTtcbiAgICAgIH0pO1xuICB9XG4gIGhlYWx0aENoZWNrKHJlcSwgcmVzKSB7XG4gICAgQW5jaG9yU2VydmljZVxuICAgICAgLmhlYWx0aGNoZWNrKHJlcSwgcmVzKVxuICAgICAgLnRoZW4ociA9PiB7XG4gICAgICAgIGlmIChyKSByZXMuanNvbihyKTtcbiAgICAgICAgZWxzZSByZXMuc3RhdHVzKDQwNCkuZW5kKCk7XG4gICAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgQ29udHJvbGxlcigpO1xuIl19