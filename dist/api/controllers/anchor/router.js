"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var express = _interopRequireWildcard(require("express"));

var _controller = _interopRequireDefault(require("./controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _default = express.Router().post('/oscc', _controller.default.setMessage).post('/query/oscc', _controller.default.getMessage).get('/oscc/healthcheck', _controller.default.healthCheck);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvYW5jaG9yL3JvdXRlci5qcyJdLCJuYW1lcyI6WyJleHByZXNzIiwiUm91dGVyIiwicG9zdCIsImNvbnRyb2xsZXIiLCJzZXRNZXNzYWdlIiwiZ2V0TWVzc2FnZSIsImdldCIsImhlYWx0aENoZWNrIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7OztlQUVlQSxPQUFPLENBQ25CQyxNQURZLEdBRVpDLElBRlksQ0FFUCxPQUZPLEVBRUVDLG9CQUFXQyxVQUZiLEVBR1pGLElBSFksQ0FHUCxhQUhPLEVBR1FDLG9CQUFXRSxVQUhuQixFQUlaQyxHQUpZLENBSVIsbUJBSlEsRUFJYUgsb0JBQVdJLFdBSnhCLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBleHByZXNzIGZyb20gJ2V4cHJlc3MnO1xuaW1wb3J0IGNvbnRyb2xsZXIgZnJvbSAnLi9jb250cm9sbGVyJztcblxuZXhwb3J0IGRlZmF1bHQgZXhwcmVzc1xuICAuUm91dGVyKClcbiAgLnBvc3QoJy9vc2NjJywgY29udHJvbGxlci5zZXRNZXNzYWdlKVxuICAucG9zdCgnL3F1ZXJ5L29zY2MnLCBjb250cm9sbGVyLmdldE1lc3NhZ2UpXG4gIC5nZXQoJy9vc2NjL2hlYWx0aGNoZWNrJywgY29udHJvbGxlci5oZWFsdGhDaGVjayk7XG4iXX0=