"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.Controller = void 0;

var _message = _interopRequireDefault(require("../../services/message/message.service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Controller {
  getMessage(req, res) {
    _message.default.getMessage(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

  setMessage(req, res) {
    _message.default.setMessage(req, res).then(r => {
      if (r) res.json(r);else res.status(404).end();
    });
  }

}

exports.Controller = Controller;

var _default = new Controller();

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NlcnZlci9hcGkvY29udHJvbGxlcnMvbWVzc2FnZS9jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbIkNvbnRyb2xsZXIiLCJnZXRNZXNzYWdlIiwicmVxIiwicmVzIiwiTWVzc2FnZVNlcnZpY2UiLCJ0aGVuIiwiciIsImpzb24iLCJzdGF0dXMiLCJlbmQiLCJzZXRNZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7QUFFTyxNQUFNQSxVQUFOLENBQWlCO0FBQ3RCQyxFQUFBQSxVQUFVLENBQUNDLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ25CQyxxQkFDR0gsVUFESCxDQUNjQyxHQURkLEVBQ21CQyxHQURuQixFQUVHRSxJQUZILENBRVFDLENBQUMsSUFBSTtBQUNULFVBQUlBLENBQUosRUFBT0gsR0FBRyxDQUFDSSxJQUFKLENBQVNELENBQVQsRUFBUCxLQUNLSCxHQUFHLENBQUNLLE1BQUosQ0FBVyxHQUFYLEVBQWdCQyxHQUFoQjtBQUNOLEtBTEg7QUFNRDs7QUFFREMsRUFBQUEsVUFBVSxDQUFDUixHQUFELEVBQU1DLEdBQU4sRUFBVztBQUNuQkMscUJBQ0dNLFVBREgsQ0FDY1IsR0FEZCxFQUNtQkMsR0FEbkIsRUFFR0UsSUFGSCxDQUVRQyxDQUFDLElBQUk7QUFDVCxVQUFJQSxDQUFKLEVBQU9ILEdBQUcsQ0FBQ0ksSUFBSixDQUFTRCxDQUFULEVBQVAsS0FDS0gsR0FBRyxDQUFDSyxNQUFKLENBQVcsR0FBWCxFQUFnQkMsR0FBaEI7QUFDTixLQUxIO0FBTUQ7O0FBakJxQjs7OztlQW1CVCxJQUFJVCxVQUFKLEUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgTWVzc2FnZVNlcnZpY2UgZnJvbSAnLi4vLi4vc2VydmljZXMvbWVzc2FnZS9tZXNzYWdlLnNlcnZpY2UnO1xuXG5leHBvcnQgY2xhc3MgQ29udHJvbGxlciB7XG4gIGdldE1lc3NhZ2UocmVxLCByZXMpIHtcbiAgICBNZXNzYWdlU2VydmljZVxuICAgICAgLmdldE1lc3NhZ2UocmVxLCByZXMpXG4gICAgICAudGhlbihyID0+IHtcbiAgICAgICAgaWYgKHIpIHJlcy5qc29uKHIpO1xuICAgICAgICBlbHNlIHJlcy5zdGF0dXMoNDA0KS5lbmQoKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgc2V0TWVzc2FnZShyZXEsIHJlcykge1xuICAgIE1lc3NhZ2VTZXJ2aWNlXG4gICAgICAuc2V0TWVzc2FnZShyZXEsIHJlcylcbiAgICAgIC50aGVuKHIgPT4ge1xuICAgICAgICBpZiAocikgcmVzLmpzb24ocik7XG4gICAgICAgIGVsc2UgcmVzLnN0YXR1cyg0MDQpLmVuZCgpO1xuICAgICAgfSk7XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IG5ldyBDb250cm9sbGVyKCk7XG4iXX0=