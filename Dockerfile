FROM    node:8.16.0

RUN	apt-get update
RUN	apt-get install -y vim
RUN	apt-get install -y net-tools
RUN	apt-get install -y telnet
RUN	apt-get install -y curl

RUN	mkdir -p /src
WORKDIR	/src
ADD	. /src

RUN	rm -rf node_modules
RUN	rm -rf dist

RUN	npm install
#RUN	npm rebuild
RUN	npm audit fix
RUN     npm install pm2 -g
RUN	npm run compile
#COPY    . /src

EXPOSE  8081

#CMD ["pm2-docker", "process.yml"]
#CMD ["pm2-runtime", "npm", "--", "start"]
#CMD ["pm2-docker", "npm", "--", "start"]
#CMD ["pm2-runtime", "server/grpc-scheduler.js"]
CMD ["npm", "run", "start"]
