module.exports = {
  /*
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
  */

  apps: [{
    name: 'transaction-mgr',
    script: './dist/index.js',
    instances: '-1',
    exec_mode: 'cluster',
    env: {
      NODE_ENV: 'production',
    },
    env_production: {
      NODE_ENV: 'production',
    },
    log_date_format: 'YYYYMMDD HH:mm:ss Z',
    out_file: './log/transaction-mgr.log',
    merge_logs: true,
  }],
};
