const addsrc = require('gulp-add-src');
const gulp = require('gulp');
const mocha = require('gulp-mocha');
const tape = require('gulp-tape');

const tapColorize = require('tap-colorize');

const fs = require('fs-extra');
const path = require('path');
const os = require('os');
const util = require('util');
const shell = require('gulp-shell');
const testConstants = require('../../test/unit/constants.js');

// by default for running the tests print debug to a file
const debugPath = path.join(testConstants.tempdir, 'test-log/debug.log');
process.env.HFC_LOGGING = util.format('{"debug":"%s"}', escapeWindowsPath(debugPath));

function escapeWindowsPath(p) {
  if (path.sep == '/') return p;
  return p.replace(/\\/g, '\\\\');
}

console.log('\n####################################################');
console.log(util.format('# debug log: %s', debugPath));
console.log('####################################################\n');

const arch = process.arch;
const release = require(path.join(__dirname, '../../package.json')).testFabricVersion;
const thirdpartyRelease = require(path.join(__dirname, '../../package.json')).testFabricThirdParty;
let dockerImageTag = '';
let thirdpartyImageTag = '';
let dockerArch = '';

// this is a release build, need to build the proper docker image tag
// to run the tests against the corresponding fabric released docker images
if (arch.indexOf('x64') === 0) {
  dockerArch = ':amd64';
} else if (arch.indexOf('s390') === 0) {
  dockerArch = ':s390x';
} else if (arch.indexOf('ppc64') === 0) {
  dockerArch = ':ppc64le';
} else {
  throw new Error(`Unknown architecture: ${arch}`);
}

if (!/master/.test(thirdpartyRelease)) {
  thirdpartyImageTag = `${dockerArch}-${thirdpartyRelease}`;
}
if (!/master/.test(release)) {
  dockerImageTag = `${dockerArch}-${release}`;
}

process.env.DOCKER_IMG_TAG = dockerImageTag;
process.env.THIRDPARTY_IMG_TAG = thirdpartyImageTag;

// Use nyc instead of gulp-istanbul to generate coverage report
// Cannot use gulp-istabul because it throws "unexpected identifier" for async/await functions
gulp.task('test', shell.task(
  './node_modules/nyc/bin/nyc.js gulp run-full',
));

gulp.task('clean-up', () => {
  fs.removeSync(testConstants.tempdir);
  return fs.ensureFileSync(debugPath);
});

gulp.task('docker-clean', shell.task([
  // stop and remove chaincode docker instances
  'docker kill $(docker ps | grep "dev-peer0.org[12].example.com-[en]" | awk \'{print $1}\')',
  'docker rm $(docker ps -a | grep "dev-peer0.org[12].example.com-[en]" | awk \'{print $1}\')',

  // remove chaincode images so that they get rebuilt during test
  'docker rmi $(docker images | grep "^dev-peer0.org[12].example.com-[en]" | awk \'{print $3}\')',

  'docker-compose -f test/fixtures/docker-compose.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));

gulp.task('docker-ready', ['clean-up', 'docker-clean'], shell.task([
  'docker-compose -f test/fixtures/docker-compose.yaml up -d',
]));

gulp.task('run-full', ['docker-ready'],
  () => gulp.src(shouldRunPKCS11Tests([
    'test/unit/config.js', // needs to be first
    '!test/unit/constants.js',
    '!test/unit/util.js',
    '!test/unit/logger.js',
    'test/integration/e2e.js',
  ]))
    .pipe(tape({
      reporter: tapColorize(),
    })));

// currently only the x64 CI jobs are configured with SoftHSM
// disable the pkcs11.js test for s390 or other jobs
// also skip it by default and allow it to be turned on manuall
// with an environment variable so everyone don't have to
// install SoftHsm just to run unit tests
function shouldRunPKCS11Tests(tests) {
  if (os.arch().match(/(x64|x86)/) === null ||
    !(typeof process.env.PKCS11_TESTS === 'string' && process.env.PKCS11_TESTS.toLowerCase() === 'true')) {
    tests.push('!test/unit/pkcs11.js');
    tests.push('!test/integration/javachaincode/e2e.js');
  }

  return tests;
}
