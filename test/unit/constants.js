const os = require('os');
const path = require('path');

const tempdir = path.join(os.tmpdir(), 'hfc');

module.exports = {
  tempdir,
};
