/**
 *
 * Copyright 2018 KT All Rights Reserved.
 *
 */

const path = require('path');
const fs = require('fs-extra');
const util = require('util');


const Client = require('fabric-client');
const copService = require('fabric-ca-client/lib/FabricCAServices.js');
const User = require('fabric-client/lib/User.js');
const Constants = require('./constants.js');

const logger = require('fabric-client/lib/utils.js').getLogger('TestUtil');

module.exports.NODE_END2END = {
  channel: 'mychannel',
  chaincodeId: 'e2enodecc',
  chaincodeLanguage: 'node',
  chaincodeVersion: 'v0',
};

// all temporary files and directories are created under here
const tempdir = Constants.tempdir;

module.exports.CHAINCODE_PATH = 'github.com/example_cc';
module.exports.CHAINCODE_UPGRADE_PATH = 'github.com/example_cc1';
module.exports.CHAINCODE_UPGRADE_PATH_V2 = 'github.com/example_cc2';
module.exports.CHAINCODE_PATH_PRIVATE_DATA = 'github.com/example_cc_private';

module.exports.END2END = {
  channel: 'mychannel',
  chaincodeId: 'end2endnodesdk',
  chaincodeIdPrivateData: 'end2endnodesdk_privatedata',
  chaincodeVersion: 'v0',
};

module.exports.METADATA_PATH = path.resolve(__dirname, '../fixtures/metadata');

logger.info(util.format(
  '\n\n*******************************************************************************' +
  '\n*******************************************************************************' +
  '\n*                                          ' +
  '\n* Using temp dir: %s' +
  '\n*                                          ' +
  '\n*******************************************************************************' +
  '\n*******************************************************************************\n', tempdir));

module.exports.getTempDir = function() {
  fs.ensureDirSync(tempdir);
  return tempdir;
};

// specifically set the values to defaults because they may have been overridden when
// running in the overall test bucket ('gulp test')
module.exports.resetDefaults = function() {
  global.hfc.config = undefined;
  require('nconf').reset();
};

module.exports.setupChaincodeDeploy = function() {
  process.env.GOPATH = path.join(__dirname, '../fixtures');
};

module.exports.getOrderAdminSubmitter = function (client, test) {
  return getOrdererAdmin(client, test);
};

function getOrdererAdmin(client, t) {
  const keyPath = path.join(__dirname, '../fixtures/channel/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/keystore');
  const keyPEM = Buffer.from(readAllFiles(keyPath)[0]).toString();
  const certPath = path.join(__dirname, '../fixtures/channel/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/signcerts');
  const certPEM = readAllFiles(certPath)[0];
  t.comment('getOrdererAdmin');

  return Promise.resolve(client.createUser({
    username: 'ordererAdmin',
    mspid: 'OrdererMSP',
    cryptoContent: {
      privateKeyPEM: keyPEM.toString(),
      signedCertPEM: certPEM.toString(),
    },
  }));
}

module.exports.getSubmitter = function (client, test, peerOrgAdmin, org) {
  if (arguments.length < 2) throw new Error('"client" and "test" are both required parameters');

  let peerAdmin,
    userOrg;
  if (typeof peerOrgAdmin === 'boolean') {
    peerAdmin = peerOrgAdmin;
  } else {
    peerAdmin = false;
  }

  // if the 3rd argument was skipped
  if (typeof peerOrgAdmin === 'string') {
    userOrg = peerOrgAdmin;
  } else if (typeof org === 'string') {
    userOrg = org;
  } else {
    userOrg = 'org1';
  }

  if (peerAdmin) {
    return getAdmin(client, test, userOrg);
  }
  return getMember('admin', 'adminpw', client, test, userOrg);
};

Client.addConfigFile(path.join(__dirname, '../integration/e2e/config.json'));
const ORGS = Client.getConfigSetting('test-network');

const	tlsOptions = {
  trustedRoots: [],
  verify: false,
};

function getAdmin(client, t, userOrg) {
  const keyPath = path.join(__dirname, util.format('../fixtures/channel/crypto-config/peerOrganizations/%s.example.com/users/Admin@%s.example.com/keystore', userOrg, userOrg));
  const keyPEM = Buffer.from(readAllFiles(keyPath)[0]).toString();
  const certPath = path.join(__dirname, util.format('../fixtures/channel/crypto-config/peerOrganizations/%s.example.com/users/Admin@%s.example.com/signcerts', userOrg, userOrg));
  const certPEM = readAllFiles(certPath)[0];

  const cryptoSuite = Client.newCryptoSuite();
  if (userOrg) {
    cryptoSuite.setCryptoKeyStore(Client.newCryptoKeyStore({ path: module.exports.storePathForOrg(ORGS[userOrg].name) }));
    client.setCryptoSuite(cryptoSuite);
  }

  return Promise.resolve(client.createUser({
    username: `peer${userOrg}Admin`,
    mspid: ORGS[userOrg].mspid,
    cryptoContent: {
      privateKeyPEM: keyPEM.toString(),
      signedCertPEM: certPEM.toString(),
    },
  }));
}

function getMember(username, password, client, t, userOrg) {
  const caUrl = ORGS[userOrg].ca.url;

  return client.getUserContext(username, true)
    .then(user =>
    // eslint-disable-next-line no-unused-vars
			 new Promise((resolve, reject) => {
        if (user && user.isEnrolled()) {
          t.pass('Successfully loaded member from persistence');
          return resolve(user);
        }

        const member = new User(username);
        let cryptoSuite = client.getCryptoSuite();
        if (!cryptoSuite) {
          cryptoSuite = Client.newCryptoSuite();
          if (userOrg) {
            cryptoSuite.setCryptoKeyStore(Client.newCryptoKeyStore({ path: module.exports.storePathForOrg(ORGS[userOrg].name) }));
            client.setCryptoSuite(cryptoSuite);
          }
        }
        member.setCryptoSuite(cryptoSuite);

        // need to enroll it with CA server
        const cop = new copService(caUrl, tlsOptions, ORGS[userOrg].ca.name, cryptoSuite);

        return cop.enroll({
          enrollmentID: username,
          enrollmentSecret: password,
        }).then(enrollment => {
          t.pass(`Successfully enrolled user '${username}'`);

          return member.setEnrollment(enrollment.key, enrollment.certificate, ORGS[userOrg].mspid);
        }).then(() => {
          let skipPersistence = false;
          if (!client.getStateStore()) {
            skipPersistence = true;
          }
          return client.setUserContext(member, skipPersistence);
        }).then(() => resolve(member))
          .catch(err => {
            t.fail(`Failed to enroll and persist user. Error: ${err.stack}` ? err.stack : err);
            t.end();
          });
      }),
    );
}

// directory for file based KeyValueStore
module.exports.KVS = path.join(tempdir, 'hfc-test-kvs');
module.exports.storePathForOrg = function (org) {
  return `${module.exports.KVS}_${org}`;
};

function readAllFiles(dir) {
  const files = fs.readdirSync(dir);
  const certs = [];
  files.forEach(file_name => {
    const file_path = path.join(dir, file_name);
    logger.debug(` looking at file ::${file_path}`);
    const data = fs.readFileSync(file_path);
    certs.push(data);
  });
  return certs;
}

module.exports.sleep = function (ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};
