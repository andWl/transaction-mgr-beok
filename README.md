*Ubuntu 18.04 LTS 버전을 기준으로 작성

***

# Transaction Manager

## 프로젝트 소개
**Operation Blockchain API G/W for Transaction Management**
- KT Operator Blockain 플랫폼 기반의 서비스 위한 표준 API를 제공
- 기본 프레임워크 : [generator-express-no-stress](https://www.ibm.com/developerworks/cloud/library/cl-express-no-stress-generator/index.html)
  - [npm](https://www.npmjs.com/package/generator-express-no-stress)

### Versions
- [Hyperledger Fabric SDK](https://fabric-sdk-node.github.io/release-1.3/index.html) (for node.js) v1.3 기반
  - [github](https://github.com/hyperledger/fabric-sdk-node)

***

# Prerequisites
Fabric 구동을 위해 Docker, Go Lang, Node.js, NPM, Git 등 사전 환경 구성이 필요하다.

## Docker 설치
- [Docker Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script) version 17.06.2-ce 이상

#### 설치 스크립트 다운 및 실행
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

#### 도커 그룹 유저 추가
유저 계정으로 sudo 없이 명령 위해 도커 그룹에 유저를 추가한다.
```
sudo usermod -aG docker <your-user>
```
위 명령 수행 후 터미널을 재시작 한다.

#### 도커 서비스 실행
```
sudo systemctl start docker.service
```

#### 시스템 부팅 시 자동으로 도커 실행
```
sudo systemctl enable docker
```

#### 도커 이미지 리스트 출력
```
docker images
```
에러 메세지가 출력되지 않으면 성공이다.

#### 설치된 버전 확인
```
docker --version
```

## Docker Compose 설치
- [Docker Compose](https://docs.docker.com/compose/install/#install-compose) version 1.14.0 이상

#### 도커 컴포즈 설치
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

#### 실행 권한 설정
```
sudo chmod +x /usr/local/bin/docker-compose
```

#### 설치된 버전 확인
```
docker-compose --version
```

## Go Lang 설치
- Hyperledger Fabric 은 Go 언어 기반으로 작성된 소스이기 때문에 이를 이용하기 위해 Go 언어 설치가 필요하다. 
- [Go](https://golang.org/dl/) version 1.10.x

#### 다운로드 및 압축 해제
```
wget https://dl.google.com/go/go1.10.3.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.10.3.linux-amd6.tar.gz
```

#### PATH 환경변수 추가
디렉토리를 생성한다.
```
mkdir ~/go
```

profile 파일을 수정한다.
```
vim ~/.profile
```
마지막 줄에 아래 세 라인을 추가하고 저장한다.
```
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/go
export GOROOT=/usr/local/go/bin
```
저장 후, 변경한 profile 파일이 적용되도록 한다.
```
source ~/.profile
```

#### 설치된 버전 확인
```
go version
```

## Node.js 설치
- Node.js version 8.9.x
- NPM version 5.6.0

#### Node.js 버전 관리 툴 NVM 설치
```
curl https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```
위 명령을 수행하고 나면 profile 파일에 몇 줄이 추가되는데 변경한 파일이 적용되도록 한다.
```
source ~/.profile
```

#### NVM 을 이용한 Node.js 설치
NVM 을 이용하여 특정 Node 버전을 설치할 수 있다.
설치가능한 버전을 확인하기 위해 다음의 명령을 입력한다.
```
nvm list-remote
```
본 문서에서는 현재 안정화 버전인 8.xx 버전 중 8.15.0 을 설치하도록 한다.
```
nvm install v8.15.0
```

#### 설치된 버전 확인
Node.js 와 NPM 버전을 확인힌다.
Node.js 패키지 관리 툴인 NPM 은 Node 설치 시에 함께 설치된다.
```
node --version
npm --version
```
필요 시, NPM 은 Fabric 1.3 버전에서 사용된 5.6.0 또는 최신 버전을 설치할 수 있다.
```
npm install npm@5.6.0 -g
npm install npm@latest -g
```

## Python 설치
Ubuntu 18.04 버전에서는 파이썬이 기본으로 설치되어 있으나, 필요 시에는 다음 명령으로 파이썬을 설치한다.
```
sudo apt-get install python
```
설치된 버전을 확인한다.
```
python --version
```

***

# Get Started

## Fabric 설치
별도로 설치하지 않아도, transaction-mgr 프로젝트 내에서 Fabric 네트워크 구동시 도커 이미지가 없으면 자동으로 설치를 진행하게 되므로 이 과정은 스킵해도 무방하다.

#### Fabric Docker Images
[Fabric 공식 사이트](https://hyperledger-fabric.readthedocs.io/en/release-1.3/install.html)에서 제공하는 스크립트를 이용하여, Fabric 도커 이미지를 다운로드 한다. 도커 이미지 외에도 샘플 파일과 바이너리 소스도 함께 다운 받는다. (현재 최신 버전 1.3 다운)
```
curl -sSL http://bit.ly/2ysbOFE | bash -s 1.3.0
```

설치된 이미지 목록을 확인한다.
```
docker images
```

#### Fabric Node SDK
[Fabric Node SDK](https://github.com/hyperledger/fabric-sdk-node) version 1.3
```
git clone https://github.com/hyperledger/fabric-sdk-node.git
```

## 프로젝트 다운로드
transaction-mgr 프로젝트는 Git 을 이용하여 로컬 시스템에 다운받아 사용할 수 있다. (계정 필요)

```shell
git clone http://bc.drasys.com:8282/SeoungKwangKim/transaction-mgr.git
```

## 의존 라이브러리 설치
본 프로젝트는 Node.js 언어를 기반으로 작성 되었다. 따라서 npm(Node Package Manager)를 이용하여 관련 패키지를 관리하며, npm 명령어를 통해 패키지를 설치 및 실행할 수 있다.

프로젝트 최상위 경로에서 아래 명령어를 실행하여 패키지를 설치한다.

```shell
npm install
```

프로젝트에 필요한 Node 패키지 리스트는 package.json 내 dependencies 파트에 명시되어 있다. 위 명령을 통해 패키지 설치가 진행되고, node_modules 폴더와 package-lock.json 파일이 생성된다.

***

# Running Transaction Manager : NPM

## 서버 구동

#### Run in *development* mode:
transaction-mgr 서버를 개발 모드로 구동하기 위해 다음의 명령어를 실행한다.

```shell
npm run dev
```

위 명령어를 통해 다음의 명령이 실행되도록 정의되어 있다.
```shell
"dev": "nodemon server --exec babel-node --config .nodemonrc.json | pino-pretty"
```
소스 변경 사항이 있을 시 자동으로 서버를 재시작해주는 nodemon 패키지를 이용하여 서버가 구동됨을 확인한다.
babel-node 는 ES6 로 작성한 Node 코드를 변환하여 실행할 때 사용하는 패키지이다. sourceRoot 를 server 로 지정하여 index.js 를 기본값으로 실행하게 된다.

#### Run in *development for debugging* mode:
다음 명령어는 inspector 에이전트를 활성화하는 명령을 추가시켜 디버깅 모드로 사용한다.

```shell
npm run dev:debug
```

(Node.js 디버깅 참고: https://nodejs.org/en/docs/guides/debugging-getting-started/)

#### Run in *production* mode:
다음 명령은 외부 배포 필요시 사용할 수 있는 명령으로, 난독화 처리된 소스를 제공하여 서버를 구동할 수 있다.

```shell
npm run compile
npm start
```

컴파일 완료 후 *dist* 폴더가 생성되며 이 안에 난독화된 소스가 담긴다.
`npm start` 명령으로 *dist* 폴더 내 *index.js* 파일을 실행하여 서버가 구동된다.

## 테스트

#### 모카 유닛 테스트 실행:

```shell
npm test
```

테스트 파일은 *transaction-mgr/test/examples.controller.js* 에 위치한다.

#### 모카 유닛 테스트 디버깅 모드 실행:

```shell
npm run test:debug
```

상세 명령은 package.json 내 scripts 에서 확인할 수 있다.
![](/doc_images/npm_scripts.PNG)

#### 예제 API 테스트
다음의 명령으로 기본 예제 API 를 호출해본다.
```shell
curl http://localhost:3000/api/v1/examples
```
Response 값으로 다음과 같이 출력되면 정상이다.
```
[{"id":0,"name":"example 0"},{"id":1,"name":"example 1"}]
```

## 브라우저 실행
Swagger, SwaggerUI 패키지를 통해 API 명세를 웹에서 확인할 수 있다.
- http://localhost:3000


원격 서버를 사용할 때 웹 페이지가 열리지 않는다면, 서버 시스템의 방화벽 설정을 확인한다.

```shell
systemctl status firewalld.service
systemctl disable firewalld.service
```

# Running Transaction Manager : Docker
Fabric Network 를 도커로 구동하듯이 Transaction Manager 도 도커로 구동시킬 수 있다.

#### 이미지 빌드
도커로 구동 시키기 위해서 먼저, transaction-mgr 프로젝트를 도커 이미지로 생성한다.
Dockerfile 파일에 도커 이미지 생성을 위한 명령어가 담겨 있다.

다음의 명령어를 실행하여 도커 이미지를 빌드한다.

```
npm run docker-build
```

위 명령어를 통해 다음의 명령이 실행된다. *kt/ob-transaction-mgr* 라는 이름의 도커 이미지가 생성된다.
```
"docker-build": "docker build -t kt/ob-transaction-mgr ."
```

생성된 이미지는 다음의 명령으로 확인할 수 있다.
```
docker images
```
![](/doc_images/docker_image.PNG)

#### 컨테이너 구동
[Docker Compose](https://docs.docker.com/compose/) 툴을 이용하여 도커 이미지의 컨테이너를 구동한다. Docker Compose 를 이용하면 컨테이너들을 구동시키고 정상적으로 동작하는지 확인하기 위해 매번 도커 명령어를 사용해야 하는 수고를 덜고, 다수의 컨테이너를 개별 서비스로 용이하게 관리할 수 있다. *docker-compose.yaml* 파일에 서비스와 버전, 볼륨, 네트워크를 정의한다.

도커 컨테이너를 구동하기 위해 먼저 사용할 네트워크를 추가해야 한다. 다음의 명령으로 네트워크를 생성한다.
`kt-network` 는 *docker-compose.yaml* 에서 `transaction-manager` 서비스에 정의된 네트워크 이름이다.
```
docker network create kt-network
```

생성된 네트워크 리스트와 네트워크의 상세 정보는 다음의 명령으로 확인할 수 있다.
```
docker network ls
docker network inspect kt-network
```

다음의 명령으로 컨테이너를 구동한다.
```
docker-compose -f docker-compose.yaml up
```

컨테이너 구동 상태를 확인한다.
```
docker ps
```

Transaction Manager 서버가 잘 구동 되는지 확인한다.
- 다음의 명령으로 기본 예제 API 를 호출해본다.
  ```shell
  curl http://localhost:3000/api/v1/examples
  ```
  Response 값으로 다음과 같이 출력되면 정상이다.
  ```
  [{"id":0,"name":"example 0"},{"id":1,"name":"example 1"}]
  ```
- [브라우저](http://localhost:3000) 접속이 가능한 것으로도 확인할 수 있다.

컨테이너 종료 명령은 다음과 같다.
```
docker-compose -f docker-compose.yaml down
```

***

# Use Network
앞에서는 Transaction Manager 가 잘 구동하는지만 확인해 보았다. 여기에서는 Fabric 네트워크와 연동 구동하여 Transaction Manager 를 테스트 해본다.

## 연동 방법
Fabric 네트워크와 연동 구동을 위해 Transaction Manager 에서는 해당 네트워크의 프로필과 인증서 파일이 필용하다.
- 네트워크 설정 정보가 담긴 network-config.yaml 파일과 org.yaml 파일, 인증키 정보가 담긴 crypto-config 디렉토리가 필요하다.
- 샘플 네트워크 연동에 필요한 파일은 *transaction-mgr/server/common/hlf/connection-profile* 디렉토리 하위에 위치한다.

미리 구성된 네트워크를 사용하기 위해 Transaction Manager 에서는 *transaction-mgr/.env* 환경변수 설정 파일 내용 중 `TARGET_NETWORK` 항목에 네트워크 값을 지정해야 한다. 설정 값에 따라 Transaction Manager 에서는 해당 네트워크를 이용할 수 있도록 자동으로 셋팅된다.

## 연동 테스트 : Fabric Sample Network
Fabric 샘플 네트워크를 이용하여 테스트한다. Fabric 예제에서 많이 사용되는 First-network 와 Basic-network 샘플을 지원한다.

### First-network : Basic
Fabric 샘플 중 유저 생성, 잔액 조회, 유저 간 이체 등 거래에 활용할 수 있는 대표적인 체인코드와 API 와 함께 사용되는 샘플 네트워크 이다.
- 네트워크 구성 : Org(2), Peer(4), Orderer(1), CA(2)
  - 2개의 Org 가 각각 Peer 2개씩 가진다

#### 네트워크 구동 및 기본 셋팅
네트워크 초기 구동에서는, 도커 이미지를 이용하여 블록체인 네트워크를 구동 후 채널 생성 및 조인, 체인코드 설치 및 인스턴스화 과정이 필요하다. 각 과정을 직접 명령하기가 번거롭기 때문에 한 번에 수행할 수 있는 Python 기반 Shell Script 를 제공한다. 해당 Script 를 실행하기 위해 프로젝트 최상위 디렉토리로 이동 후 다음의 명령을 수행한다.
```
cd ./tools/network/basic
python setup.py
```
위 명령 수행 후, 차레로 다음의 명령을 실행한다. 실행되는 세부 명령은 하위 cli 디렉토리의 각 Shell Script 를 참고한다.
```
 01. B/C Network Start Command
 02. Channel Create & Join Command
 03. C/C Install & Instantiate Commnad
```

도커 컨테이너 상태를 확인한다.
```
docker ps
```
![](/doc_images/docker_ps.PNG)



#### Transaction Manager 구동 및 설정
Transaction Manager 에서 환경변수 파일에 사용하고자 하는 네트워크를 설정한다. 프로젝트 최상위 디렉토리로 이동하여 다음의 명령을 실행한다.
```
vim ./.env
```

`TARGET_NETWORK` 항목에 `basic` 을 입력한다. `basic` 은 First-network 를 사용하기 위해 정의된 값이다.
```
TARGET_NETWORK=basic
```

Transaction Manager 를 구동한다.
```
npm start
```

### Basic-network : Local
위와 같은 방법으로 Fabric 기본 네트워크인 Basic-network 룰 사용할 수 있다.
- 네트워크 구성: Org, Peer, Orderer, CA 각 1개

프로젝트 최상위 디렉토리에서 *./tools/network/local* 에 위치한다.

***

# 구동 자동화 : GULP
지금까지는 Transaction Manager 를 구동하고, 블록체인 네트워크를 연동시켜 구동하는 과정을 각각 수동으로 진행하였다. 여기에서는 자동화 툴 GULP 를 이용하여, 블록체인 네트워크와 Transaction Manager 를 간편하게 구동시키는 방법을 보여준다.

#### gulp 실행
연동시키고자 하는 블록체인 네트워크(basic 또는 local)를 지정하여 gulp 명령을 실행한다.
```
gulp basic
```
```
gulp local
```

블록체인 네트워크 생성과 연동 등 구동을 위한 모든 일련의 과정을 직접 수행할 필요가 없이, 위 명령만으로 API 를 사용할 수 있는 준비가 완료된다.

#### gulp 종료
종료 시에는 다음의 명령으로 도커 컨테이너가 중지되고 도커 이미지가 삭제된다.
```
gulp docker-clean-basic
```
```
gulp docker-clean-local
```

수행되는 명령은 최상위 디렉토리의 *gulpfile.js* 에서 확인할 수 있다.

#### 


***

# API Test
위에서 설명한 방법을 통해 수동 또는 자동으로 블록체인 네트워크와 Transaction Manager 를 구동시킨 후, API 테스트를 진행한다.
Swagger 에서 제공되는 API 명세 웹 브라우저(http://localhost:3000/api-explorer/)에 접속하여 아래의 각 API 를 실행해본다.

`Try it out` 클릭 > `Execute` 클릭 후 Response 를 확인한다. Code 값이 `200` 이면 정상이다.

#### Management API
네트워크/채널/블럭 정보 조회, 체인코드 설치 및 인스턴스화 등 기본적인 관리 기능을 제공한다.

- /mgmt/network
  - Org 별로 각 mspid, peers, ca, 인증서 정보가 정상적으로 출력되는 것을 확인한다.
    
- /mgmt/channel
  - `channel_id` 값이 `mychannel` 임을 확인한다.

- /mgmt/channel/info
  - 입력 파라미터 예.
    - orgName: Org1 / channelName: mychannel / peerName: peer0.org1.example.com
  - 해당 Peer 의 height, 블록 해시 정보 등이 출력되는 것을 확인한다.

#### Balance API
Fabric 샘플에서 제공되는 Balance-transfer API 를 활용한 것으로, 유저를 등록하고 유저의 잔고 조회와 이체 기능을 제공한다.

- /balance/{id}
  - 등록된 유저의 잔고를 조회한다.
  - 체인코드 인스턴스화 시에 기본 유저 'a' 를 생성하고 '200' 을 부여하였기 때문에 'a' 의 잔고 조회시 200 이 출력된다.
  
- /balance/user
  - 새로운 유저 등록 및 금액을 부여한다.

- /balance
  - 유저 A가 유저 B에게 송금할 금액을 입력하여, 유저 간에 이체할 수 있는 기능이다.


이 외 API 는 다음의 API Lists 를 참고한다.

# API List
- Request URL 은 Base URL(http://localhost:3000/api/v1) 뒤에 각 API 별 경로가 추가된다.
- API 입출력 데이터는 JSON 포맷을 지원한다.
  - Content-type: `application/json`

| Method | API | URL | Parameters | Response | Details |
|:---:|:---:|:---:|:---:|:---:|:---:|
| get | network info | /mgmt/network | - | 200: Orgs | Org{mspid, peers, certificateAuthorities, adminPrivateKey, SignedCert} |
| get | channel name | /mgmt/channel | - | 200: channels / 404: Error | channels{channel_id} |
| get | channel info | /mgmt/channel/info | orgName(str), channelName(str), peerName(str) | 200: height, currentBlockHash, previousBlockHash / 404: Error | height{low, high, unsigned}, currentBlockHash{buffer, offset, markedOffset, limit, littleEndian, noAssert}, previousBlockHash{buffer, offset, markedOffset, limit, littleEndian, noAssert} |
| get | block info by number | /mgmt/blocks/no | orgName(str), channelName(str), peerName(str), no(int) | 200: header, data  / 404: Error | header{number, previous_hash, data_hash}, data{data[{}]} |
| get | block info by hash | /mgmt/blocks/hash | orgName(str), channelName(str), peerName(str), hash(str) | 200: header, data  / 404: Error | header{number, previous_hash, data_hash}, data{data[{}]} |
| get | chaincode install | /mgmt/chaincode/install | orgName(str), channelName(str), peerName(str) | 200: name, version, path / 404: Error | |
| get | chaincode init | /mgmt/chaincode/instantiation | orgName(str), channelName(str), peerName(str) | 200: name, version, path / 404: Error | |
| get | balance | /balance/{id} | id(str) | 200: "*user* now has *balance* after the move" | |
| post | user enroll | /balance/user | id{"name", "balance} | 200: transaction ID | |
| post | balance transfer | /balance/ | move balance request{"from", "to", "amount"} | 200: transaction ID | |


*API 명세 2019.01.08 기준 작성

***
