'use strict';
var mongoose = require('mongoose');
var log4js = require('log4js');
var logger = log4js.getLogger('DatabaseConfig');
logger.setLevel('DEBUG');
var model = require('./Model.js');

var KeyData = model.KeyData;


/**
 *@class
 * setting MongoDB Configuration to use Obchain 
 */
var Database = class {

  /**
   * @param {string} username
   * @param {string} password
   * @param {string} databaseName 
   * @param {string} ip 
   */
  
  constructor(username, password, databaseName, ip) {
    this.username = username;
    this.password = password;
    this.databaseName = databaseName;
    this.ip = ip;
  }

  /**
   * connect with MongoDB 
   */
  connect() {
    var mongoConnectURL = 'mongodb://'+this.username + ':' + this.password + '@'+this.ip+':27017/' + this.databaseName;

    mongoose.connect(mongoConnectURL, { useNewUrlParser: true });
    var db = mongoose.connection;
    
    db.on('error', function(){
      logger.error('>> MongoDB Connection Failed!');
      throw new Error('MongoDB Connection Failed!'); 
    })
    
    db.once('open', function() {
      logger.debug('>> MongoDB Connected!');
    });
  } 

  /**
   * Find data using key in MongoDB
   * @param {string} key The key to find in MongoDB
   */
  // find to mongoDB
  static findData(key) {
    logger.debug(">> find Peer :", key);
    var keyData = KeyData.findOne({key:key});
    return keyData;
  }


  /**
   * Save data with key and peer in MongoDB
   * @param {} data The data to save in MongoDB
   */
  // Save data to mongoDB
  static saveData(data) {
    var newKeyData = new KeyData({key : data.key, peer:data.peer});
    newKeyData.save().then(function(data) {
      return data;
    }).catch(function (err) {
      throw new Error(err);
    });
  };

  static getIP() {
    return this.ip;
  }

}




module.exports =Database;
