'use strict';
var mongoose = require('mongoose');
var log4js = require('log4js');
var logger = log4js.getLogger('DBConfig');
logger.setLevel('DEBUG');

/**
 *@class 
 */
var Node = class {
  /**
   * @param {string} channelName The name of the channel 
   * @param {string} peer The name of the peer
   * @param {object} channel The object of the Channel
   * @param {string} orderer The name of the Orderer
   */
  constructor(channelName, peer, channel, orderer) {
    this.channelName = channelName;
    this.peer = peer;
    this.channel = channel || undefined;
    this.orderer = orderer;
  }

  /**
   * @param {object} channel Channel Object 
   */
  setChannel(channel) {
    this.channel = channel;
  }

  /**
   * @return {string} The name of the channel
   */
  getChannelName() {
    return this.channelName;
  }

  /**
   * @return {string} The name of the peer
   */
  getPeer() {
    return this.peer;
  }

  /**
   * @return {string} The name of the orderer
   */
  getOrderer() {
    return this.orderer;
  }

   /**
   * @return {Channel} The Object of Channel
   */
  getChannel() {
    return this.channel;
  }
}

module.exports = Node;



