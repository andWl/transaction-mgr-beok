'use strict';
var model = require('./Model.js');
var log4js = require('log4js');
var logger = log4js.getLogger('OBchain');
logger.setLevel('DEBUG');
var md5 = require('md5');
var Node = require('./Node.js');
var Database = require('./Database.js');
var getAsync;
var redis;

// cache with redis

var nodeArr =[];

var Obchain = class {
  constructor () {
  }

  /**
   * @param {string} key name of key to invoke transaction
   * @returns {Node} Node Object to Invoke Transaction
   */
  static async getPeerToInvoke(key) {
    logger.debug(">> Key of getPeerToInvoke :", key);

    var data = await Database.findData(key);
    if(data.peer == undefined) {
      var hashToInteger = parseInt(md5(key).substring(0,2),16);
      var idx = hashToInteger % nodeArr.length;
      var newPeer = nodeArr[idx];
      var result = {
        key : key,
        peer : newPeer.peer
      }
      await Database.saveData(result);
      return newPeer;
    } else {
      var peer = data.peer;
      return getPeerFromArray(peer);
    }
  }  



  /**
   * get Node obejct to use in Query Transaction 
   * 
   *  @param {string} key name of key to query transaction
   *  @return {Node} Node Object to Query Transaction
   */
  // getPeer to Query Transaction
  static async getPeerToQuery(key) {
    logger.debug(">> Key of getPeerToQuery :", key);
    try {
      var result = await getAsync(key);
      if(result) {
        return getPeerFromArray(result);
      } else {
        var data = await Database.findData(key);
        
        if(data == null) {
          return ;
        } else {
          //expire set a day
          redis.setex(key,24*60*60, data.peer, function (err, reply) {
            if(err) {
              logger.debug(">> Redis Error in Save Cache : ", err);
              throw new Error(err);
            }
          });
        } 
        return getPeerFromArray(data.peer);
      }
    } catch(error) {
      logger.error(">> Redis Error : ", error);
      throw new Error(error)
    }
  }

  /**
   * create new Node object.
   * This function has argument 
   * @param {string} channelName The name of channel
   * @param {string} peer The name of peer
   * @param {string} orderer The name of orderer
   * @param {string} channel Optional. If you don't use this parameter, you must set the channel 
   * to funtion [setNodeChannel()] after you create the node object.
   * @return {Node} 
   */
  static newNode(channelName, peer, orderer, channel) {
    var tmpNode;
    if(channel) {
      tmpNode = new Node(channelName, peer, channel, orderer);
    } else {
      tmpNode = new Node(channelName, peer, undefined, orderer);
    }
    return tmpNode;
  }

  /**
   * Create new Database object
   * @param {string} username The name of user
   * @param {string} password The password of MongoDB
   * @param {string} databaseName The name of database
   * @returns {Database}
   */
  static newDatabase(username, password, databaseName, ip) {
    var db = new Database(username, password, databaseName, ip);
    var redisClient = require('redis').createClient;
    redis = redisClient(6379, ip);
    const util = require('util');
    getAsync = util.promisify(redis.get).bind(redis);
    return db;
  }

  
  /**
   * Save Node array to use Node object in Obchain 
   * @param {Node[]} array Array of Node 
   */
  static setNodeArray(array) {
    nodeArr = array;
  }

  
  /**
   * This function is used to store the channel of the node object created without channel argument.
   * @param {number} index index of Node Array
   * @param {Channel} channel channel
   */
  static setNodeChannel(index, channel) {
    nodeArr[index].setChannel(channel);
  }

  /**
   * get Node object with node array index
   * @return {Node}
   * @param {Number} index 
   */
  static getNode(index) {
    return nodeArr[index];
  }

  /**
   * Add Node object in node array 
   * @param {Node} node 
   */
  static pushNodeArray(node) {
    nodeArr.push(node);
  }

  /**
   * @return {Node[]} return Node Array 
   * @param {object} channel
   */
  static getNodeArray() {
    return nodeArr;
  }
}

/**
 * @return {Node} Node Object of 
 * @param {string} peer name of peer to find in NodeArray
 */
var getPeerFromArray = async function(peer) {
  return (nodeArr).find(function(element) {
    return element.peer === peer;
  });
}

module.exports = Obchain;
module.exports.Node = Node;
module.exports.Database = Database;