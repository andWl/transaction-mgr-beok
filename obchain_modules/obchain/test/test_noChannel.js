
var Obchain = require('../lib/OBchain.js');
var config = require('./database-config.json');
var db = Obchain.newDatabase(config.username, config.password, config.databaseName, config.ip);
db.connect();

async function main() {
	var examplePeers = [
		{"ChannelName" :"channel01", "peer" : "peer0.org1.exmple.com", "orderer" : "orderer0.example.com"},
		{"ChannelName" :"channel02", "peer" : "peer1.org1.exmple.com", "orderer" : "orderer0.example.com"},
		{"ChannelName" :"channel03", "peer" : "peer2.org1.exmple.com", "orderer" : "orderer0.example.com"},
		{"ChannelName" :"channel04", "peer" : "peer3.org1.exmple.com", "orderer" : "orderer0.example.com"},
	];
	
	var nodeArr = [];
	
	for(var idx in examplePeers) {
		var tmpNode = Obchain.newNode(examplePeers[idx].ChannelName, examplePeers[idx].peer, examplePeers[idx].orderer);
		nodeArr.push(tmpNode);
	}

	for(var idx in nodeArr) {
		nodeArr[idx].setChannel({Channel:{}});
	}
	
	global.nodeArr = nodeArr;
	Obchain.setNodeArray(nodeArr);
	
	//case 1 : Invoke Transaction
	var invokeLoc = await Obchain.getPeerToInvoke("key01");
	console.log("[Invoke Location] : ", invokeLoc);
	
	var queryLoc = await Obchain.getPeerToQuery("key01");
	console.log("[Query Location] : ", queryLoc);

	var queryLoc2 = await Obchain.getPeerToQuery("key01");
	console.log("[Query Location] : ", queryLoc2);

	var queryLoc3 = await Obchain.getPeerToQuery("key02");
	console.log("[Query Location] : ", queryLoc3);
}

main();


