#!/bin/bash

export FABRIC_CFG_PATH=$PWD

mkdir ./channel-artifacts

../bin/configtxgen -profile OneOrgOrdererGenesis -outputBlock ./channel-artifacts/genesis.block

../bin/configtxgen -profile OneOrgChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID mychannel
