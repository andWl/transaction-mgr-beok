#!/usr/bin/python

import os
import sys
import subprocess


#   define
#
MAX_ORG_COUNT  = 1
MAX_PEER_COUNT = 1
CHAINCODE_NAME = "balance_cc"
CHAINCODE_VER  = "0.1"


#   display form
#
LINE_END = "######################################################"
LINE_EMPTY="#                                                    #"
LINE_exit= "#          00. Exit Shell                            #"
LINE_1st = "#          01. B/C Network Start Command             #"
LINE_2nd = "#          02. Channel Create & Join Command         #"
LINE_3nd = "#          03. C/C Install & Instantiate Command     #"
LINE_10s = "#          10. Network Topology Change Command       #"
LINE_11s = "#          11. Setup C/C Name & Version Command      #"
LINE_12s = "#          12. View Docker Container Command         #"
LINE_PRT = "               select function number : "
LINE_DEL = "#----------------------------------------------------#"


#   func
#
def start_network_cmd():
    
        start_network_cmd = "python ./cli/0-network-start.py"
        #print( start_network_cmd )
        #cmd_result = 0
        cmd_result = subprocess.call(start_network_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
        return cmd_result

def create_channel_cmd():

        create_channel_cmd = "docker exec cli bash /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/1-1.create-channel.sh"
        #print( create_channel_cmd )
        #cmd_result = 0
        cmd_result = subprocess.call(create_channel_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
        return cmd_result

def update_channel_cmd(org_number):
        update_channel_cmd = "docker exec cli bash /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/1-2.update-channel.sh"
        #print( update_channel_cmd )
        #cmd_result = 0
        cmd_result = subprocess.call(update_channel_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
        return cmd_result

def join_channel_cmd(org_number, peer_num):

        for j in peer_num:  
            join_channel_cmd = "docker exec cli bash /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/1-3.join-channel.sh " + '{0:0d}'.format(j) + " " + '{0:0d}'.format(org_number+1)
            #print( join_channel_cmd )
            cmd_result = subprocess.call(join_channel_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
            if 0 != cmd_result:
                sys.exit(-1)

        return cmd_result

def change_org_num(max_org_num):

        try:
            max_org_num  = int(raw_input("          max org count : "))
        except ValueError, e:
            max_org_num = MAX_ORG_COUNT
        return max_org_num

def change_peer_num(max_peer_num):

        try:
            max_peer_num = int(raw_input("          max org's peer count : "))
        except ValueError, e:
            max_peer_num = MAX_PEER_COUNT
        return max_peer_num

def change_cc_name(chaincode_name):
    
        try:
            chaincode_name = raw_input("          chaincode name: ")
        except ValueError, e:
            chaincode_name = CHAINCODE_NAME
        return chaincode_name

def change_cc_ver(chaincode_ver):

        try:
            chaincode_ver  = raw_input("          chaincode ver : ")
        except ValueError, e:
            chaincode_ver = CHAINCODE_VER
        return chaincode_ver

def install_chaincode_cmd(org_number, peer_num, chaincode_name, chaincode_ver):

        for j in peer_num:  
            install_chaincode_cmd = "docker exec cli bash /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/2-1.install-chaincode.sh "  + '{0:0d}'.format(j) + " " + '{0:0d}'.format(org_number+1) + " " + chaincode_name + " " + chaincode_ver 
            #print( install_chaincode_cmd )
            #cmd_result = 0
            cmd_result = subprocess.call(install_chaincode_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
            if 0 != cmd_result:
                sys.exit(-1)
        return cmd_result

def instantiate_chaincode_cmd(chaincode_name, chaincode_ver):

        instantiate_chaincode_cmd = "docker exec cli bash /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/2-2.instantiate-chaincode.sh " + chaincode_name + " " + chaincode_ver 
        #print( instantiate_chaincode_cmd )
        #cmd_result = 0
        cmd_result = subprocess.call(instantiate_chaincode_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
        if 0 != cmd_result:
                sys.exit(-1)
        return cmd_result

def display_container_cmd():

        display_container_cmd = "docker ps -a"
        cmd_result = subprocess.call(display_container_cmd, stdin=None, stdout=None, stderr=subprocess.STDOUT, shell=True)
        if 0 != cmd_result:
                sys.exit(-1)
        print("")
        print("")
        try:
            temp = raw_input("     Press any key  to continue ... ")
        except ValueError, e:
            temp = 0

        return cmd_result

def exit_shell():

        os.system('clear') 
        print(LINE_END)
        print(LINE_EMPTY)
        print("#              Exit Fabric Setup Shell!              #")
        print(LINE_EMPTY)
        print(LINE_END)
        print(" ")
        print(" ")
        sys.exit(0)

def start_network():

        result = start_network_cmd()
        if 0 != result:
                sys.exit(-1)
        
def setup_channel(org_num, peer_num):

        os.system('clear')
        result = create_channel_cmd()
        if 0 != result:
                sys.exit(-1)

        os.system('clear')
        for i in org_num:
            result = update_channel_cmd(i)
            if 0 != result:
                    sys.exit(-1)

        os.system('clear')
        for i in org_num:
            result = join_channel_cmd(i, peer_num)
            if 0 != result:
                    sys.exit(-1)

def setup_chaincode(org_num, peer_num, chaincode_name, chaincode_ver):

        os.system('clear')
        for i in org_num:
            result = install_chaincode_cmd(i, peer_num, chaincode_name, chaincode_ver)
            if 0 != result:
                    sys.exit(-1)

        os.system('clear')
        result = instantiate_chaincode_cmd(chaincode_name, chaincode_ver)
        if 0 != result:
             sys.exit(-1)

def display_container():

        os.system('clear')
        result = display_container_cmd()
        if 0 != result:
             sys.exit(-1)



def setup_cc_config():

        os.system('clear')
        result = get_chaincode_name()
        if 0 != result:
             sys.exit(-1)


#   main
#
def main():

	select_result = 0
        max_org_num   = MAX_ORG_COUNT
        max_peer_num  = MAX_PEER_COUNT 
        org_num       = range(0, max_org_num)
        peer_num      = range(0, max_peer_num)
        chaincode_name= CHAINCODE_NAME
        chaincode_ver = CHAINCODE_VER

	while 1:
		os.system('clear') 
                print("")
		print(LINE_END)
		print(LINE_EMPTY)
		print(LINE_exit)
		print(LINE_1st)
		print(LINE_2nd)
		print(LINE_3nd)
		print(LINE_10s)
		print(LINE_11s)
		print(LINE_12s)
		print(LINE_EMPTY)
                print(LINE_DEL)
                print"#      MAX ORG COUNT : ",; print(max_org_num),; print"  MAX PEER COUNT : ",; print(max_peer_num),; print("     #")
                print(LINE_DEL)
                print"       CC NAME : ",; print(chaincode_name),; print"  C/C Ver : ",; print(chaincode_ver),; print("          ")
		print(LINE_END)
                print("")

                try:
		    select_result = int(raw_input("             select function number :  ")) 
                except ValueError, e:
                    select_result =0
                    continue

		if select_result == 0 :
                        exit_shell()
		elif select_result == 1 :
                        start_network()
		elif select_result == 2 :
			setup_channel(org_num, peer_num)
		elif select_result == 3 :
			setup_chaincode(org_num, peer_num, chaincode_name, chaincode_ver)
		elif select_result == 10:
                        max_org_num  = change_org_num(max_org_num)
                        org_num      = range(0, max_org_num)
                        max_peer_num = change_peer_num(max_peer_num)
                        peer_num     = range(0, max_peer_num)
                elif select_result == 11:
                        chaincode_name = change_cc_name(chaincode_name)
                        chaincode_ver  = change_cc_ver(chaincode_ver)
                elif select_result == 12:
                        display_container()
		else :
			print("#     function range 1 ~ 3 or Exit = 10")

		

if __name__ == '__main__':
	main()
