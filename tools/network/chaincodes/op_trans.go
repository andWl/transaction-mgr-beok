package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type OpChaincode struct {
}

func main() {
	err := shim.Start(new(OpChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}

}

// Init initializes chaincode
// ===========================
func (t *OpChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *OpChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "setMessage" {			//save
		return t.setMessage(stub, args)
	} else if function == "getMessage" {	//query
		return t.getMessage(stub, args)
	} else if function == "transfer" {	    //transfer
		return t.getMessage(stub, args)
	}


	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")

}

func (t *OpChaincode) setMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] value")
	}

	fmt.Println("-- start setMessage =>")
	fmt.Println(" key: " + args[0]+ "\n value: " + args[1])

	// === Save data set to state DB ===
	err := stub.PutState(args[0], []byte(args[1]))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("-- end setMessage")
	return shim.Success(nil)

}

func (t *OpChaincode) getMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1: [0] key")
	}

	fmt.Println("-- start getMessage")

	queryResults, err := stub.GetState(args[0])
	if err != nil {
		if err.Error() == "no such key" {
			return shim.Success(queryResults)
		}else {
			return shim.Error(err.Error())
		}
	}

	//fmt.Println(string(queryResults))

	//fmt.Println("-- end getMessage")
	return shim.Success(queryResults)

}

func (t *OpChaincode) transfer(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// args[0] = key, args[1] = delta
	var val, delta int
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] delta")
	}

	fmt.Println("-- start transfer")

	queryResults, err := stub.GetState(args[0])
	fmt.Println(" key: " + args[0]+ "\n delta: " + args[1])

	if err != nil {
		return shim.Error(err.Error())
	}
	if queryResults == nil {
		return shim.Error("Entity not found")
	}
	fmt.Println("key = " + string(queryResults))
	val, _ = strconv.Atoi(string(queryResults))

	delta, err = strconv.Atoi(args[1])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}

	val = val + delta

	//TODO : if val < 0, exception
	//       delta -> { opertaion(+/-), amount }

	err = stub.PutState(args[0], []byte(strconv.Itoa(val)))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Printf("%d = val + (%d)\n", val, delta)

	fmt.Println("-- end transfer")
	return shim.Success(nil)

}

func (t *OpChaincode) lock(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// args[0] = lockKey, args[1] = txID
	// TODO: compositeKey { locking, key, txID }
	//     : system chaincode or internal
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] locked key, [1] txId")
	}

	fmt.Println("-- start lock")

	locked, err := stub.GetState("lock_" + args[0])
	if locked != nil {
		return shim.Error("Already locked ["+ args[0]+"]")
	}

	//txId := stub.GetTxID()
	err = stub.PutState("lock_" + args[0], /*[]byte(txId)*/[]byte(args[1]))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("-- end lock")
	return shim.Success(nil)

}
