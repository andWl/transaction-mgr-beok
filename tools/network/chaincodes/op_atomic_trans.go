package main

import (
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
	"strconv"
)

type transferChaincode struct {
}

func main() {
	err := shim.Start(new(transferChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init initializes chaincode
// ===========================
func (t *transferChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *transferChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "setMessage" {			//save
		return t.setMessage(stub, args)
	} else if function == "getMessage" {	//query
		return t.getMessage(stub, args)
	} else if function == "transferFrom" {	    //transfer from
		return t.transferFrom(stub, args)
	} else if function == "transferTo" {	    //transfer to
		return t.transferTo(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")

}

func (t *transferChaincode) setMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] value")
	}

	fmt.Println("-- start setMessage =>")
	fmt.Println(" key: " + args[0]+ "\n value: " + args[1])

	// === Save data set to state DB ===
	err := stub.PutState(args[0], []byte(args[1]))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("-- end setMessage")
	return shim.Success(nil)

}

func (t *transferChaincode) getMessage(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1: [0] key")
	}

	fmt.Println("-- start getMessage")

	queryResults, err := stub.GetState(args[0])
	if err != nil {
		if err.Error() == "no such key" {
			return shim.Success(queryResults)
		}else {
			return shim.Error(err.Error())
		}
	}

	//fmt.Println(string(queryResults))

	//fmt.Println("-- end getMessage")
	return shim.Success(queryResults)

}

func (t *transferChaincode) transferFrom(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// args[0] = key, args[1] = amount
	var val, amount int
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] amount")
	}

	fmt.Println("-- start transfer from")

	queryResults, err := stub.GetState(args[0])
	fmt.Println(" key: " + args[0]+ "\n amount: " + args[1])

	if err != nil {
		return shim.Error(err.Error())
	}
	if queryResults == nil {
		return shim.Error("Entity not found")
	}
	fmt.Println("key = " + string(queryResults))
	val, _ = strconv.Atoi(string(queryResults))

	amount, err = strconv.Atoi(args[1])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}

	if val < amount {
		return shim.Error("Transfer amount is larger than the current value ")
	} else {
		val = val - amount

		err = stub.PutState(args[0], []byte(strconv.Itoa(val)))
		if err != nil {
			return shim.Error(err.Error())
		}

		fmt.Printf("%d = val - (%d)\n", val, amount)

		fmt.Println("-- end transfer from")
		return shim.Success(nil)
	}
}

func (t *transferChaincode) transferTo(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// args[0] = key, args[1] = amount
	var val, amount int
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2: [0] key, [1] amount")
	}

	fmt.Println("-- start transfer to ")

	queryResults, err := stub.GetState(args[0])
	fmt.Println(" key: " + args[0]+ "\n amount: " + args[1])

	if err != nil {
		return shim.Error(err.Error())
	}
	if queryResults == nil {
		return shim.Error("Entity not found")
	}
	fmt.Println("key = " + string(queryResults))
	val, _ = strconv.Atoi(string(queryResults))

	amount, err = strconv.Atoi(args[1])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}

	val = val + amount

	err = stub.PutState(args[0], []byte(strconv.Itoa(val)))
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Printf("%d = val + (%d)\n", val, amount)

	fmt.Println("-- end transfer to")
	return shim.Success(nil)
}
