/*

INVOKE:
peer chaincode invoke -C __CHANNEL__ -n __CHAINCODE_ID__ -c '{"Args":["func","arg"]}'

QUERY:
peer chaincode query -C __CHANNEL__ -n __CHAINCODE_ID__ -c '{"Args":["func","arg"]}'

*/

package main

import (
	"bytes"
	"encoding/json"
	"net"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

const IpTrackerETHABIData = "[{\"constant\":false,\"inputs\":[{\"name\":\"ID\",\"type\":\"string\"}],\"name\":\"IPdeleteRelation\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"IP\",\"type\":\"string\"},{\"name\":\"ID\",\"type\":\"string\"}],\"name\":\"modifyID\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"IPgetAllState\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"IP\",\"type\":\"string\"}],\"name\":\"getID\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"ID\",\"type\":\"string\"}],\"name\":\"getIP\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"ID\",\"type\":\"string\"},{\"name\":\"IP\",\"type\":\"string\"}],\"name\":\"IPsetRelation\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"ID\",\"type\":\"string\"},{\"name\":\"IP\",\"type\":\"string\"}],\"name\":\"modifyIP\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

const (
	// IPTrackerString is a name of this chaincode
	IPTrackerString string = "IPTracker"

	// IndexIPtoMID is an index name for Policy
	IndexIPtoMID string = "docType~ip"

	// IndexMIDtoIP is an index name for Policy
	IndexMIDtoIP string = "docType~mid"
)

type IPdeleteRelation struct {
	ID	string
}
type modifyID struct {
	IP	string
	ID	string
}
type getID struct {
	IP	string
}
type getIP struct {
	ID	string
}
type IPsetRelation struct {
	ID	string
	IP	string
}
type modifyIP struct {
	ID	string
	IP	string
}

var logger1 = shim.NewLogger("IP Tracker")

func (t* TNS) fromIDtoKey(id string) string {
	Type := "id"
	Value := id
	return t.toKey(Type, Value)
}

func (t* TNS) fromIPtoKey(ip string) string {
	Type := "ip"
	Value := ip
	return t.toKey(Type, Value)
}

func (t* TNS) toKey(Type, Value string) string {
	key := "{\"type\":\"" + Type + "\", \"value\":\"" + Value + "\"}"
	return key
}

func (t* TNS) checkIPFormat(ipAsString string) bool {
	ip := net.ParseIP(ipAsString)
	if ip == nil {
		return false
	}
	return true
}

func (t* TNS) checkIDFormat(id string) bool {
	// TODO: Check ID format
	return true
}

func (t* TNS) IPsetRelation(stub shim.ChaincodeStubInterface, param IPsetRelation) pb.Response {
	var msg string
	var err error

	id := param.ID
	ip := param.IP
	if t.checkIDFormat(id) == false {
		msg = "Invalid ID format"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if t.checkIPFormat(ip) == false {
		msg = "Invalid IP format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Check a relation */
	// idAsKey := t.fromIDtoKey(id)
	idAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, id})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipAsValue, err := stub.GetState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if ipAsValue != nil {
		msg = "Already mapped ID"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// ipAsKey := t.fromIPtoKey(ip)
	ipAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, ip})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idAsValue, err := stub.GetState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if idAsValue != nil {
		msg = "Already mapped IP"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Set the relation */
	err = stub.PutState(idAsKey, []byte(ip))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.PutState(ipAsKey, []byte(id))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}

	logger1.Info("ID:" + id + ", IP:" + ip)
	return shim.Success(nil)
}

func (t* TNS) IPdeleteRelation(stub shim.ChaincodeStubInterface, param IPdeleteRelation) pb.Response {
	var msg string
	var err error

	id := param.ID
	if t.checkIDFormat(id) == false {
		msg = "Invalid ID format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Check a relation */
	// idAsKey := t.fromIDtoKey(id)
	idAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, id})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipAsValue, err := stub.GetState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if ipAsValue == nil {
		msg = "Not mapped ID"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// ipAsKey := t.fromIPtoKey(string(ipAsValue))
	ipAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, string(ipAsValue)})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idAsValue, err := stub.GetState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if idAsValue == nil {
		msg = "Not mapped IP"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if bytes.Compare([]byte(id), idAsValue) != 0 {
		msg = "Broken relation"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Delete the relation */
	err = stub.DelState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.DelState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}

	logger1.Info("ID:" + id + ", IP:" + string(ipAsValue))
	return shim.Success(nil)
}

func (t* TNS) IPgetAllState(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error
	var result string

	resultIterator, err := stub.GetStateByPartialCompositeKey(IndexMIDtoIP, []string{IPTrackerString})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	defer resultIterator.Close()

	result = "["
	var i int
	for i = 0; resultIterator.HasNext(); i++ {
		response, err := resultIterator.Next()
		if err != nil {
			logger1.Error(err)
			return shim.Error(err.Error())
		}
		key := response.Key
		value := response.Value
		_, ckey, err := stub.SplitCompositeKey(key)
		if err != nil {
			logger1.Error(err)
			return shim.Error(err.Error())
		}
		MID := ckey[1]

		msg := "{\"key\":{\"mid\":\"" + MID + "\"},\"value\":{\"ip\":\"" + string(value) + "\"}}"
		logger1.Info(msg)

		if i > 0 {
			result += ","
		}
		result += msg
	}

	resultIterator2, err := stub.GetStateByPartialCompositeKey(IndexIPtoMID, []string{IPTrackerString})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	defer resultIterator2.Close()

	var j int
	for j = 0; resultIterator2.HasNext(); j++ {
		response, err := resultIterator2.Next()
		if err != nil {
			logger1.Error(err)
			return shim.Error(err.Error())
		}
		key := response.Key
		value := response.Value
		_, ckey, err := stub.SplitCompositeKey(key)
		if err != nil {
			logger1.Error(err)
			return shim.Error(err.Error())
		}
		IP := ckey[1]

		msg := "{\"key\":{\"ip\":\"" + IP + "\"},\"value\":{\"mid\":\"" + string(value) + "\"}}"
		logger1.Info(msg)

		if i > 0 || j > 0 {
			result += ","
		}
		result += msg
	}

	result += "]"
	logger1.Info("Result: " + result)

	resultStringifiedAsBytes, err := json.Marshal(result)
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	resultStringified := string(resultStringifiedAsBytes)

	return shim.Success([]byte(resultStringified))
}

func (t* TNS) getID(stub shim.ChaincodeStubInterface, param getID) pb.Response {
	var msg string
	var err error

	ip := param.IP
	if t.checkIPFormat(ip) == false {
		msg = "Invalid IP format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Get ID */
	// ipAsKey := t.fromIPtoKey(ip)
	ipAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, ip})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idAsValue, err := stub.GetState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if idAsValue == nil {
		msg = "Not mapped IP"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	logger1.Info("ID:" + string(idAsValue))

	return shim.Success(idAsValue)
}

func (t* TNS) getIP(stub shim.ChaincodeStubInterface, param getIP) pb.Response {
	var msg string
	var err error

	id := param.ID
	if t.checkIDFormat(id) == false {
		msg = "Invalid IP format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Get IP */
	// idAsKey := t.fromIDtoKey(id)
	idAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, id})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipAsValue, err := stub.GetState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if ipAsValue == nil {
		msg = "Not mapped ID"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	logger1.Info("IP:" + string(ipAsValue))

	return shim.Success(ipAsValue)
}

func (t* TNS) modifyID(stub shim.ChaincodeStubInterface, param modifyID) pb.Response {
	var msg string
	var err error

	ip := param.IP
	id := param.ID
	if t.checkIPFormat(ip) == false {
		msg = "Invalid IP format"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if t.checkIDFormat(id) == false {
		msg = "Invalid ID format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Check a relation */
	// ipAsKey := t.fromIPtoKey(ip)
	ipAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, ip})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idAsValue, err := stub.GetState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if idAsValue == nil {
		msg = "Not mapped IP"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// idAsKey := t.fromIDtoKey(string(idAsValue))
	idAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, string(idAsValue)})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipAsValue, err := stub.GetState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if bytes.Compare([]byte(ip), ipAsValue) != 0 {
		msg = "Broken relation"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// idNewAsKey := t.fromIDtoKey(id)
	idNewAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, id})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipNewAsValue, err := stub.GetState(idNewAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if ipNewAsValue != nil {
		msg = "Already mapped ID"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Modify the relation */
	err = stub.DelState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.PutState(ipAsKey, []byte(id))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.PutState(idNewAsKey, []byte(ip))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}

	logger1.Info("IP:" + ip + ", ID:" + id)
	return shim.Success(nil)
}

func (t* TNS) modifyIP(stub shim.ChaincodeStubInterface, param modifyIP) pb.Response {
	var msg string
	var err error

	id := param.ID
	ip := param.IP
	if t.checkIDFormat(id) == false {
		msg = "Invalid ID format"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if t.checkIPFormat(ip) == false {
		msg = "Invalid IP format"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Check a relation */
	// idAsKey := t.fromIDtoKey(id)
	idAsKey, err := stub.CreateCompositeKey(IndexMIDtoIP, []string{IPTrackerString, id})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	ipAsValue, err := stub.GetState(idAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if ipAsValue == nil {
		msg = "Not mapped ID"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// ipAsKey := t.fromIPtoKey(string(ipAsValue))
	ipAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, string(ipAsValue)})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idAsValue, err := stub.GetState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if bytes.Compare([]byte(id), idAsValue) != 0 {
		msg = "Broken relation"
		logger1.Error(msg)
		return shim.Error(msg)
	}
	// ipNewAsKey := t.fromIPtoKey(ip)
	ipNewAsKey, err := stub.CreateCompositeKey(IndexIPtoMID, []string{IPTrackerString, ip})
	if err != nil {
		logger1.Error(err)
		return shim.Error(err.Error())
	}
	idNewAsValue, err := stub.GetState(ipNewAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	if idNewAsValue != nil {
		msg = "Already mapped IP"
		logger1.Error(msg)
		return shim.Error(msg)
	}

	/* Modify the relation */
	err = stub.DelState(ipAsKey)
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.PutState(idAsKey, []byte(ip))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}
	err = stub.PutState(ipNewAsKey, []byte(id))
	if err != nil {
		msg = err.Error()
		logger1.Error(msg)
		return shim.Error(msg)
	}

	logger1.Info("ID:" + id + ", IP:" + ip)
	return shim.Success(nil)
}
