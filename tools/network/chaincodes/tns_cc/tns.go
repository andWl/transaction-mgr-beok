package main
//GiGAstealth와 소스 병합
//MVCC read conflict 방어를 위한 버전향상(High-Throughput Network 예제)
import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"kt.com/bns/blockchain"
	"math"

	"log"
	"strings"
	"encoding/gob"
	"crypto/rand"
	"encoding/base64"
)
// - 잘못된 형식의 ethereum address가 들어가는 경우 에러가 아닌 0x0 주소가 들어가는 오류 수정해야 함(190312)
// user 기준 master, manager가 해당하는 Targetlist 관리, Domain 권한에 대한 Targetlist 관리 함수 구현필요(180315)
// invoke의 구성함수: TNS가 호출하는 함수, AppGW sendTransaction, AppGW callTransaction, AppGW getNonce
const (
	NonceKeyName = "_NONCE"
	UserKeyName = "_USER"
	TNSETHABIData = "[{\"constant\":false,\"inputs\":[{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"originHex\",\"type\":\"string\"}],\"name\":\"initializeTicket\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"}],\"name\":\"GetMaster\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"domainRR\",\"type\":\"string\"}],\"name\":\"addDomain\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"}],\"name\":\"deleteTarget\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"A\",\"type\":\"string\"},{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getReverseResourceRecords\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"string\"},{\"name\":\"\",\"type\":\"string\"},{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"manager\",\"type\":\"address\"},{\"name\":\"managerRR\",\"type\":\"string\"}],\"name\":\"addManager\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domain\",\"type\":\"string\"}],\"name\":\"deleteDomain\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getUserDomainList\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"targetRR\",\"type\":\"string\"},{\"name\":\"masterRR\",\"type\":\"string\"}],\"name\":\"addTarget\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"},{\"name\":\"accessToken\",\"type\":\"string\"}],\"name\":\"generateAccessToken\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"manager\",\"type\":\"address\"}],\"name\":\"deleteManager\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getManagerList\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"},{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getTargetDomainList\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domains\",\"type\":\"bytes32[]\"}],\"name\":\"deleteBulkDomain\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"user\",\"type\":\"address\"},{\"name\":\"userRR\",\"type\":\"string\"}],\"name\":\"addUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domains\",\"type\":\"bytes32[]\"},{\"name\":\"domainRR\",\"type\":\"bytes32[]\"}],\"name\":\"addBulkDomain\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"user\",\"type\":\"address\"}],\"name\":\"deleteUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"newMaster\",\"type\":\"address\"},{\"name\":\"targetRR\",\"type\":\"string\"},{\"name\":\"masterRR\",\"type\":\"string\"}],\"name\":\"updateTarget\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getResourceRecords\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"string\"},{\"name\":\"\",\"type\":\"string\"},{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"},{\"name\":\"accessToken\",\"type\":\"string\"}],\"name\":\"verifyAccessToken\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"target\",\"type\":\"address\"},{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"newdomain\",\"type\":\"string\"},{\"name\":\"domainRR\",\"type\":\"string\"}],\"name\":\"updateDomain\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"domain\",\"type\":\"string\"},{\"name\":\"plainMsg\",\"type\":\"bytes29\"},{\"name\":\"userSig\",\"type\":\"string\"}],\"name\":\"getUserList\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"Error\",\"type\":\"string\"},{\"indexed\":false,\"name\":\"code\",\"type\":\"uint256\"}],\"name\":\"EventError\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"eventName\",\"type\":\"string\"},{\"indexed\":false,\"name\":\"code\",\"type\":\"uint256\"}],\"name\":\"EventResult\",\"type\":\"event\"}]"
	TicketIndexString = "userAddress~TNSAddress"
	TokenIndexString = "targetAddress~userAddress"
	)

type Target struct {
	TargetAddress common.Address
	Master User
	Permission map[common.Address]User
	Domain map[string]Domain
	RR map[string]string
}

type Domain struct {
	Name string
	Permission map[common.Address]User
	RR map[string]string
}

type User struct {
	Address common.Address
	//Ticket uint64
	//TicketSlot []uint64
	RR map[string]interface{}
}

type TicketSlot struct {
	Ticket uint64
	Slot []uint64
}

type getTargetDomainListParam struct {
	PlainMsg [29]byte
	UserSig string
}

type verifyAccessTokenParam struct {
	Domain string
	PlainMsg [29]byte
	UserSig string
	AccessToken string
}

type AddDomainParam struct {
	Target common.Address
	Domain string
	DomainRR string
}

//type AddBulkDomainParam struct {
//	Target common.Address
//	Domains [][]byte
//	DomainRR [][]byte
//}

type UpdateDomainParam struct {
	Target common.Address
	Domain string
	Newdomain string
	DomainRR string
}

type DeleteDomainParam struct {
	Target common.Address
	Domain string
}

//type DeleteBulkDomainParam struct {
//	Target common.Address
//	Domains [][]byte
//}

type updateTargetParam struct {
	Target common.Address
	NewMaster common.Address
	TargetRR string
	MasterRR string
}

type addTargetParam struct {
	Target common.Address
	TargetRR string
	MasterRR string
}

type NonceParam struct {
	Nonce uint64
}

type ResultParam struct {
	Result string
}

type AddUserParam struct {
	Target common.Address
	Domain string
	User common.Address
	UserRR string
}

type DeleteUserParam struct {
	Target common.Address
	Domain string
	User common.Address
}

type AddManagerParam struct {
	Target common.Address
	Manager common.Address
	ManagerRR string
}

type DeleteManagerParam struct {
	Target common.Address
	Manager common.Address
}

type TokenMap map[string]TokenStruct

type TokenStruct struct {
	FirstUsedTime uint64
	UsedTimeSlot []uint64
}

type TNS struct {
}

func (t *TNS) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *TNS) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	if fn == "getNonce" {
		return t.GetNonce(stub, args)
	} else if fn ==  "getMaster" {
		return t.getMaster(stub, args)
	} else if fn ==  "testInvoke" {
		return t.testInvoke(stub, args)
	} else if fn ==  "testQuery" {
		return t.testQuery(stub, args)
	} else if fn ==  "getResourceRecords" {
		return t.getResourceRecords(stub, args)
	} else if fn ==  "initializeTicket" {	//TNS에서 getResourceRecords 후 호출
		return t.initializeTicket(stub, args)
	} else if fn == "verifyAccessToken" {
		return t.verifyAccessToken(stub, args)
	} else if fn ==  "callTransaction" {
		return t.callTransaction(stub, args)
	} else if fn ==  "sendTransaction" {
		return t.sendTransaction(stub, args)
		//AppGW 내부 호출 함수
	} else if fn ==  "increNonce" {	//callTransaction 후 호출
		_, tx, userAddress, err := t.parseRawTx(stub, args)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to invoke setNonce. Incorrect arguments: %s",err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return shim.Success(nil)
	} else if fn == "generateAccessToken" { //verifyAccessToken 후 호출
		return t.generateAccessToken(stub, args)
	} else {
		return shim.Error(fmt.Sprintf("Failed to invoke. unknown function: %s", fn))
	}
}
func (t* TNS) testInvoke(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke testInvoke. Incorrect arguments."))
	}
	num := args[0]

	err := stub.PutState("testNum", []byte(num))
	if err != nil {
		return shim.Error(err.Error())
	}

	var msg string
	msg = fmt.Sprintf("{\"%s\":\"%s\"}",
		"num",num)

	return shim.Success([]byte(msg))
}
func (t* TNS) testQuery(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke testQuery. Incorrect arguments."))
	}

	value, err := stub.GetState("testNum")
	if err != nil {
		shim.Error(fmt.Sprintf("failed to GetState[%s]", err.Error()))
	}

	if value == nil {
		shim.Error(fmt.Sprintf("failed to testInvoke"))
	}

	return shim.Success(value)
}

func (t* TNS) callTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fn, tx, _, err := t.parseRawTx(stub, args)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to invoke sendTransaction. Incorrect arguments: %s",err.Error()))
	}
	log.Println("CallTransaction FunctionHashes: ", fn)

	//TNS
	if fn == "a24829a0" {
		return t.getTargetDomainList(stub, tx)
	} else if fn == "a0daf8f5" {
		return t.getManagerList(stub, tx)
	//ip-tracker
	} else if fn == "70aca6c8" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := getID{}
		err = myAbi.Methods["getID"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		return t.getID(stub, param)
	} else if fn == "8b5c01cd" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := getIP{}
		err = myAbi.Methods["getIP"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		return t.getIP(stub, param)
	} else if fn == "42964878" {
		return t.IPgetAllState(stub, []string{"", ""})
		//policy-manager
	} else if fn == "2423835b" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := checkPolicy{}
		err = myAbi.Methods["checkPolicy"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		return t.checkPolicy(stub, param)
	} else if fn == "d8b8df20" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := getSeed{}
		err = myAbi.Methods["getSeed"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		return t.getSeed(stub, param)
	} else if fn == "07211896" {
		return t.getAllState(stub, []string{"", ""})
	} else if fn == "273c56a4" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := checkRelation{}
		err = myAbi.Methods["checkRelation"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		result, err := t.checkRelation(stub, param)
		if err != nil {
			return shim.Error(err.Error())
		}
		var msg string
		msg = "{\\\"result\\\":"
		if result == true {
			msg += "true"
		} else {
			msg += "false"
		}
		msg += "}"
		logger.Info("Result: " + msg)
		return shim.Success([]byte(msg))
	} else {
		return shim.Error(fmt.Sprintf("Failed to invoke callTransaction. unknown function: %s", fn))
	}
}

func (t* TNS) sendTransaction(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fn, tx, userAddress, err := t.parseRawTx(stub, args)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to invoke sendTransaction. Incorrect arguments: %s",err.Error()))
	}
	log.Println("sendTransaction FunctionHashes: ", fn)

	//tns
	if fn == "58224a08" {
		return t.addTarget(stub, tx, userAddress)
	} else if fn == "c0163fcd" {
		return t.updateTarget(stub, tx, userAddress)	//Master 변경
	} else if fn == "ab812bed" {
		return t.addAccToDomain(stub, tx, userAddress)
	} else if fn == "ba90dea2" {
		return t.deleteAccToDomain(stub, tx, userAddress)
	} else if fn == "30e303dc" {
		return t.addAccToTarget(stub, tx, userAddress)
	} else if fn == "7158358d" {
		return t.deleteAccToTarget(stub, tx, userAddress)
	} else if fn == "2a073af1"{
		return t.addDomain(stub, tx, userAddress)
		//} else if fn == "b12f3612"{
		//	return t.addBulkDomain(stub, tx, userAddress)
	} else if fn == "ecd8b98d" {
		return t.updateDomain(stub, tx, userAddress)
	} else if fn == "3ce8e78f" {
		return t.deleteDomain(stub, tx, userAddress)
		//} else if fn == "a5f220a1" {
		//	return t.deleteBulkDomain(stub, tx, userAddress)
		//ip-tracker
	} else if fn == "b56112d0" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := IPsetRelation{}
		err = myAbi.Methods["IPsetRelation"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.IPsetRelation(stub, param)
	} else if fn == "2565315b" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := IPdeleteRelation{}
		err = myAbi.Methods["IPdeleteRelation"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.IPdeleteRelation(stub, param)
	} else if fn == "2f461f91" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := modifyID{}
		err = myAbi.Methods["modifyID"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.modifyID(stub, param)
	} else if fn == "c0197d1f" {
		myAbi, err := abi.JSON(strings.NewReader(IpTrackerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := modifyIP{}
		err = myAbi.Methods["modifyIP"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.modifyIP(stub, param)
		//policy-manager
	} else if fn == "0f48a984" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := deleteRelation{}
		err = myAbi.Methods["deleteRelation"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.deleteRelation(stub, param)
		if err != nil {
			return shim.Error(err.Error())
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return shim.Success(nil)
	} else if fn == "5ecdd116" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := deletePolicy{}
		err = myAbi.Methods["deletePolicy"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.deletePolicy(stub, param)
	} else if fn == "c55de040" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := setSeed{}
		err = myAbi.Methods["setSeed"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.setSeed(stub, param)
	} else if fn == "e3042d62" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := setRelation{}
		err = myAbi.Methods["setRelation"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.setRelation(stub, param)
		if err != nil {
			return shim.Error(err.Error())
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return shim.Success(nil)
	} else if fn == "f9a42441" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := deleteSeed{}
		err = myAbi.Methods["deleteSeed"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.deleteSeed(stub, param)
	} else if fn == "fd7425b1" {
		myAbi, err := abi.JSON(strings.NewReader(PolicyManagerETHABIData))
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
		}
		param := setPolicy{}
		err = myAbi.Methods["setPolicy"].Inputs.Unpack(&param, tx.Data()[4:])
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
		}
		err = t.SetNonce(stub, tx, userAddress.Hex())
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
		}
		return t.setPolicy(stub, param)
	} else {
		return shim.Error(fmt.Sprintf("Failed to invoke sendTransaction. unknown function: %s", fn))
	}

}

func (t* TNS) GetNonce(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke getNonce. Incorrect arguments."))
	}

	userAddress := args[0]

	nonce, err := t.getNonce(stub, userAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to invoke getNonce. [%s]", err.Error()))
	}
	nonceParam := NonceParam{}
	nonceParam.Nonce = nonce

	b, err := json.Marshal(nonceParam)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
	}
	return shim.Success(b)
}

func (t* TNS) SetNonce(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress string) error {
	nonce, err := t.getNonce(stub, userAddress)
	if tx.Nonce() >= nonce + 1 {
		err = t.setNonce(stub, userAddress, tx.Nonce() + 1)
	} else {
		err = t.setNonce(stub, userAddress, nonce + 1)
	}

	if err != nil {
		return fmt.Errorf(err.Error())
	}
	return nil
}

func (t* TNS) getMaster(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke getMaster. Incorrect arguments."))
	}

	targetAddress := args[0]

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	result := ResultParam{}
	result.Result = target.Master.Address.Hex()

	b, err := json.Marshal(result)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
	}
	return shim.Success(b)
}

func (t* TNS) addTarget(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := addTargetParam{}
	err = myAbi.Methods["addTarget"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	var target Target

	if value != nil {
		target = Target{}
		err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
		}
		if target.Master.Address.Hex() != userAddress.Hex() {
			return shim.Error(fmt.Sprintf("Failed to addTarget. there is no auth to this target [%s]", param.Target.Hex()))
		}
	}

	target = Target{}
	target.RR = make(map[string]string)
	target.TargetAddress = param.Target
	target.Master = User{}
	target.Master.Address = userAddress
	target.Master.RR = make(map[string]interface{})
	err = json.Unmarshal([]byte(param.MasterRR), &target.Master.RR)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unmarshal master RR [%s]", err.Error()))
	}

	err = json.Unmarshal([]byte(param.TargetRR), &target.RR)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unmarshal target RR [%s]", err.Error()))
	}

	err = t.setTarget(stub, &target)

	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.addTargetToUser(stub, userAddress.Hex(), target.TargetAddress.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) updateTarget(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := updateTargetParam{}
	err = myAbi.Methods["updateTarget"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	var target Target
	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to change master. there is no master to this target [%s]", param.Target.Hex()))
	} else {
		target = Target{}
		err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
		}

		if target.Master.Address.Hex() != userAddress.Hex() {
			return shim.Error(fmt.Sprintf("Failed to updateTarget. there is no auth to this target [%s]", param.Target.Hex()))
		}

		_, managerFlag := target.Permission[param.NewMaster]
		var userFlag = false
		var domain string
		for k, v := range target.Domain {
			_, flag := v.Permission[param.NewMaster]
			if flag {
				userFlag = true
				domain = k
			}
		}
		if managerFlag {
			delete(target.Permission, param.NewMaster)
			err = t.removeTargetToUser(stub, param.NewMaster.Hex(), target.TargetAddress.Hex())
			if err != nil {
				return shim.Error(err.Error())
			}
		} else if userFlag {
			delete(target.Domain[domain].Permission, param.NewMaster)
			err = t.removeTargetToUser(stub, param.NewMaster.Hex(), target.TargetAddress.Hex())
			if err != nil {
				return shim.Error(err.Error())
			}
		}

		target.Master = User{}
		target.Master.Address = param.NewMaster
		target.Master.RR = make(map[string]interface{})
		err = json.Unmarshal([]byte(param.MasterRR), &target.Master.RR)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
		}

		target.RR = make(map[string]string)
		err = json.Unmarshal([]byte(param.TargetRR), &target.RR)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unmarshal target RR [%s]", err.Error()))
		}
	}

	err = t.setTarget(stub, &target)

	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.removeTargetToUser(stub, userAddress.Hex(), target.TargetAddress.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.addTargetToUser(stub, param.NewMaster.Hex(), target.TargetAddress.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) getTargetDomainList(stub shim.ChaincodeStubInterface, tx *types.Transaction) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := getTargetDomainListParam{}
	err = myAbi.Methods["getTargetDomainList"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	userAddress, targetAddress, ticket, err := t.parseHexFromSplitData(param.PlainMsg, param.UserSig)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	if target.Master.Address.Hex() == userAddress {
		flag, err :=t.verifyTicket(stub, userAddress, ticket)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to verifyTicket [%s]", err.Error()))
		}

		if flag {
			b, err := json.Marshal(target.Domain)
			if err != nil {
				return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
			}
			return shim.Success(b)
		} else {
			return shim.Error(fmt.Sprintf("Failed to check ticket. already used ticket [%d]", ticket))
		}
	} else {
		return shim.Error(fmt.Sprintf("Failed to getTargetDomainList. there is no auth to this target [%s]", targetAddress))
	}
}

func (t* TNS) addAccToDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := AddUserParam{}
	err = myAbi.Methods["addUser"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}
	_, error := t.getTarget(stub, param.Target.Hex())
	if error != nil {
		return shim.Error(fmt.Sprintf("Failed to addAccToDomain. there is no Target [%s]", error.Error()))
	}

	log.Println("data : ", param)
	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get target from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	target := Target{}

	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from target Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to addAccToDomain. there is no auth to this target [%s]", param.Target.Hex()))
	}

	_, managerFlag := target.Permission[param.User]
	if managerFlag || target.Master.Address.Hex() == param.User.Hex() {
		return shim.Error(fmt.Sprintf("Failed to addAccToDomain. [%s] have auth to this target [%s] already", param.User.Hex(), param.Target.Hex()))
	}

	_, ok := target.Domain[param.Domain]

	if ok {
		_, flag := target.Domain[param.Domain].Permission[param.User]

		if flag {
			requestUserRR := make(map[string]interface{})
			err = json.Unmarshal([]byte(param.UserRR), &requestUserRR)
			if err != nil {
				return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
			}
			for k, v := range requestUserRR {
				target.Domain[param.Domain].Permission[param.User].RR[k] = v
			}
		} else {
			domain := target.Domain[param.Domain]

			if len(target.Domain[param.Domain].Permission) == 0 {
				domain.Permission = make(map[common.Address]User)
			}
			user := User{}
			user.Address = param.User
			user.RR = make(map[string]interface{})

			domain.Permission[param.User] = user
			target.Domain[param.Domain] = domain

			requestUserRR := make(map[string]string)
			err = json.Unmarshal([]byte(param.UserRR), &requestUserRR)
			if err != nil {
				return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
			}

			for k, v := range requestUserRR {
				target.Domain[param.Domain].Permission[param.User].RR[k] = v
			}
		}
	} else {
		return shim.Error(fmt.Sprintf("Failed to addAccToDomain. there is no domain [%s] to this target", param.Domain))
	}

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.addTargetToUser(stub, param.User.Hex(), param.Target.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) deleteAccToDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := DeleteUserParam{}
	err = myAbi.Methods["deleteUser"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}
	_, error := t.getTarget(stub, param.Target.Hex())
	if error != nil {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToDomain. there is no Target [%s]", error.Error()))
	}

	log.Println("data : ", param)
	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get target from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	target := Target{}

	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from target Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToDomain. there is no auth to this target [%s]", param.Target.Hex()))
	}

	_, ok := target.Domain[param.Domain]

	if ok {
		_, flag := target.Domain[param.Domain].Permission[param.User]

		if flag {
			delete(target.Domain[param.Domain].Permission, param.User)
		} else {
			return shim.Error(fmt.Sprintf("Failed to deleteAccToDomain. there is no domain [%s] to this target", param.Domain))
		}
	} else {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToDomain. there is no domain [%s] to this target", param.Domain))
	}

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.removeTargetToUser(stub, param.User.Hex(), param.Target.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) addAccToTarget(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := AddManagerParam{}
	err = myAbi.Methods["addManager"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}
	_, error := t.getTarget(stub, param.Target.Hex())
	if error != nil {
		return shim.Error(fmt.Sprintf("Failed to addAccToTarget. there is no Target [%s]", error.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get target from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	target := Target{}

	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from target Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to addAccToTarget. there is no auth to this target [%s]", param.Target.Hex()))
	}

	if target.Master.Address.Hex() == param.Manager.Hex() {
		return shim.Error(fmt.Sprintf("Failed to addAccToTarget. [%s] already have auth to this target [%s]", param.Manager.Hex(), param.Target.Hex()))
	}
	var userFlag = false
	var domain string
	for k, v := range target.Domain {
		_, flag := v.Permission[param.Manager]
		if flag {
			userFlag = true
			domain = k
		}
	}
	if userFlag {
		delete(target.Domain[domain].Permission, param.Manager)
		err = t.removeTargetToUser(stub, param.Manager.Hex(), target.TargetAddress.Hex())
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	_, ok := target.Permission[param.Manager]

	if ok {
		requestUserRR := make(map[string]interface{})
		err = json.Unmarshal([]byte(param.ManagerRR), &requestUserRR)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
		}
		for k, v := range requestUserRR {
			target.Permission[param.Manager].RR[k] = v
		}
	} else {
		if len(target.Permission) == 0 {
			target.Permission = make(map[common.Address]User)
		}
		user := User{}
		user.Address = param.Manager
		user.RR = make(map[string]interface{})
		target.Permission[param.Manager] = user
		requestUserRR := make(map[string]string)
		err = json.Unmarshal([]byte(param.ManagerRR), &requestUserRR)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
		}

		for k, v := range requestUserRR {
			target.Permission[param.Manager].RR[k] = v
		}
	}

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.addTargetToUser(stub, param.Manager.Hex(), target.TargetAddress.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) deleteAccToTarget(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}
	param := DeleteManagerParam{}
	err = myAbi.Methods["deleteManager"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}
	_, error := t.getTarget(stub, param.Target.Hex())
	if error != nil {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToTarget. there is no Target [%s]", error.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get target from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	target := Target{}

	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from target Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToTarget. there is no auth to this target [%s]", param.Target.Hex()))
	}
	_, ok := target.Permission[param.Manager]

	if ok {
		delete(target.Permission, param.Manager)
	} else {
		return shim.Error(fmt.Sprintf("Failed to deleteAccToTarget. there is no auth for user [%s]", param.Manager.Hex()))
	}

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	err = t.removeTargetToUser(stub, userAddress.Hex(), target.TargetAddress.Hex())
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t* TNS) getManagerList(stub shim.ChaincodeStubInterface, tx *types.Transaction) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := getTargetDomainListParam{}
	err = myAbi.Methods["getManagerList"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	userAddress, targetAddress, ticket, err := t.parseHexFromSplitData(param.PlainMsg, param.UserSig)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	if target.Master.Address.Hex() == userAddress {
		flag, err :=t.verifyTicket(stub, userAddress, ticket)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to verifyTicket [%s]", err.Error()))
		}

		if flag {
			b, err := json.Marshal(target.Permission)
			if err != nil {
				return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
			}
			return shim.Success(b)
		} else {
			return shim.Error(fmt.Sprintf("Failed to check ticket. already used ticket [%d]", ticket))
		}
	} else {
		return shim.Error(fmt.Sprintf("Failed to getManagerList. there is no auth to this target [%s]", targetAddress))
	}
}

func (t* TNS) addDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := AddDomainParam{}
	err = myAbi.Methods["addDomain"].Inputs.Unpack(&param, tx.Data()[4:])

	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	var target Target
	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to addDomain. there is no target [%s]. please do the addTarget first", param.Target.Hex()))
	} else {
		target = Target{}
		err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
		}

		if target.Master.Address.Hex() != userAddress.Hex() {
			return shim.Error(fmt.Sprintf("Failed to addDomain. there is no auth to this target [%s]", param.Target.Hex()))
		}

		if len(target.Domain) == 0 {
			target.Domain = make(map[string]Domain)
		}

		_, ok := target.Domain[param.Domain]
		if ok {
			return shim.Error(fmt.Sprintf("Failed to add domain. already exist domain [%s]", param.Domain))
		}

		domain := Domain{}
		domain.Name = param.Domain
		domain.RR = make(map[string]string)

		requestDomainRR := make(map[string]string)
		err = json.Unmarshal([]byte(param.DomainRR), &requestDomainRR)
		if err != nil {
			return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
		}

		for k, v := range requestDomainRR {
			domain.RR[k] = v
		}

		target.Domain[param.Domain] = domain
	}

	err = t.setTarget(stub, &target)

	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	return shim.Success(nil)
}

//func (t* TNS) addBulkDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
//	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
//	}
//
//	param := AddBulkDomainParam{}
//	err = myAbi.Methods["addBulkDomain"].Inputs.Unpack(&param, tx.Data()[4:])
//
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
//	}
//
//	log.Println("data : ", param)
//
//	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
//	}
//
//	var target Target
//	if value == nil {
//		return shim.Error(fmt.Sprintf("Failed to addDomain. there is no target [%s]. please do the addTarget first", param.Target.Hex()))
//	} else {
//		target = Target{}
//		err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
//		if err != nil {
//			return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
//		}
//
//		if target.Master.Address.Hex() != userAddress.Hex() {
//			return shim.Error(fmt.Sprintf("Failed to addDomain. there is no auth to this target [%s]", param.Target.Hex()))
//		}
//
//		if len(target.Domain) == 0 {
//			target.Domain = make(map[string]Domain)
//		}
//
//		domainArr := util.BytesToStrings(param.Domains)
//		domainRRArr := util.BytesToStrings(param.DomainRR)
//
//		log.Println("domainArr: ", domainArr)
//		log.Println("domainRRArr: ", domainRRArr)
//
//		for i := range domainArr {
//			_, ok := target.Domain[domainArr[i]]
//			if ok {
//				return shim.Error(fmt.Sprintf("Failed to add domain. already exist domain [%s]", domainArr[i]))
//			}
//			domain := Domain{}
//			domain.Name = domainArr[i]
//			domain.RR = make(map[string]string)
//
//			requestDomainRR := make(map[string]string)
//			err = json.Unmarshal([]byte(domainRRArr[i]), &requestDomainRR)
//			if err != nil {
//				return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
//			}
//
//			for k, v := range requestDomainRR {
//				domain.RR[k] = v
//			}
//
//			target.Domain[domainArr[i]] = domain
//		}
//	}
//
//	err = t.setTarget(stub, &target)
//
//	if err != nil {
//		return shim.Error(err.Error())
//	}
//
//	err = t.SetNonce(stub, tx, userAddress)
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
//	}
//
//	return shim.Success(nil)
//}

func (t* TNS) updateDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := UpdateDomainParam{}
	err = myAbi.Methods["updateDomain"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", param.Target.Hex()))
	}

	target := Target{}
	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to updateDomain. there is no auth to this target [%s]", param.Target.Hex()))
	}

	_, ok := target.Domain[param.Domain]
	if !ok {
		return shim.Error(fmt.Sprintf("Failed to update domain. domain [%s] is not existed", param.Domain))
	}

	domain := Domain{}
	domain.Name = param.Newdomain
	domain.RR = make(map[string]string)

	requestDomainRR := make(map[string]string)
	err = json.Unmarshal([]byte(param.DomainRR), &requestDomainRR)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unmarshal user RR [%s]", err.Error()))
	}

	for k, v := range requestDomainRR {
		domain.RR[k] = v
	}

	delete(target.Domain, param.Domain)
	target.Domain[param.Newdomain] = domain

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	return shim.Success(nil)
}

func (t* TNS) deleteDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := DeleteDomainParam{}
	err = myAbi.Methods["deleteDomain"].Inputs.Unpack(&param, tx.Data()[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if value == nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", param.Target.Hex()))
	}

	target := Target{}
	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
	}

	if target.Master.Address.Hex() != userAddress.Hex() {
		return shim.Error(fmt.Sprintf("Failed to deleteDomain. there is no auth to this target [%s]", param.Target.Hex()))
	}

	_, ok := target.Domain[param.Domain]
	if !ok {
		return shim.Error(fmt.Sprintf("Failed to delete domain. domain [%s] is not existed", param.Domain))
	}

	delete(target.Domain, param.Domain)

	err = t.setTarget(stub, &target)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = t.SetNonce(stub, tx, userAddress.Hex())
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
	}

	return shim.Success(nil)
}

//func (t* TNS) deleteBulkDomain(stub shim.ChaincodeStubInterface, tx *types.Transaction, userAddress common.Address) peer.Response {
//	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
//	}
//
//	param := DeleteBulkDomainParam{}
//	err = myAbi.Methods["deleteBulkDomain"].Inputs.Unpack(&param, tx.Data()[4:])
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
//	}
//
//	log.Println("data : ", param)
//
//	value, err := stub.GetState(strings.ToUpper(param.Target.Hex()))
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to get state from BC [%s] [%s]", err.Error(), param.Target.Hex()))
//	}
//
//	if value == nil {
//		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", param.Target.Hex()))
//	}
//
//	target := Target{}
//	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to marshal from Byte [%s] [%s]", err.Error(), param.Target.Hex()))
//	}
//
//	if target.Master.Address.Hex() != userAddress.Hex() {
//		return shim.Error(fmt.Sprintf("Failed to deleteDomain. there is no auth to this target [%s]", param.Target.Hex()))
//	}
//
//	domainArr := util.BytesToStrings(param.Domains)
//	log.Println("domainArr: ", domainArr)
//
//	for i := range domainArr {
//		_, ok := target.Domain[domainArr[i]]
//		if !ok {
//			return shim.Error(fmt.Sprintf("Failed to delete domain. domain [%s] is not existed", domainArr[i]))
//		}
//
//		delete(target.Domain, domainArr[i])
//	}
//
//	err = t.setTarget(stub, &target)
//	if err != nil {
//		return shim.Error(err.Error())
//	}
//
//	err = t.SetNonce(stub, tx, userAddress)
//	if err != nil {
//		return shim.Error(fmt.Sprintf("Failed to set nonce [%s]", err.Error()))
//	}
//
//	return shim.Success(nil)
//}

func (t* TNS) getResourceRecords(stub shim.ChaincodeStubInterface, args []string) peer.Response{
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("Failed to invoke getResourceRecords. Incorrect arguments."))
	}

	originHex := args[0]
	domainName := args[1]

	userAddress, targetAddress, ticket, err := t.parseHEX(originHex)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	domain, ok := target.Domain[domainName]
	if !ok {
		return shim.Error(fmt.Sprintf("Failed to getResourceRecords. there is no domain [%s]", domainName))
	}

	user := target.Master
	if user.Address.Hex() == userAddress {
		return t.getRR(stub, common.HexToAddress(targetAddress), &user, ticket, &target.RR, &domain.RR, &user.RR)
	}

	user, ok = target.Permission[common.HexToAddress(userAddress)]
	if ok {
		return t.getRR(stub, common.HexToAddress(targetAddress), &user, ticket, &target.RR, &domain.RR, &user.RR)
	}

	user, ok = domain.Permission[common.HexToAddress(userAddress)]
	if ok {
		return t.getRR(stub, common.HexToAddress(targetAddress), &user, ticket, &target.RR, &domain.RR, &user.RR)
	}

	return shim.Error(fmt.Sprintf("Failed to getResourceRecords. there is no auth to domain [%s]", domainName))
}

func (t* TNS) initializeTicket(stub shim.ChaincodeStubInterface, args []string) peer.Response{
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("Failed to invoke initializeTicket. Incorrect arguments."))
	}

	originHex := args[0]
	TNSAddress := args[1]

	userAddress, _, ticket, err := t.parseHEX(originHex)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	err = t.updateTicket(stub, userAddress, common.HexToAddress(TNSAddress), ticket)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to update ticket [%s]", err.Error()))
	}

	err = stub.SetEvent("initializeTicket_"+userAddress,[]byte(userAddress))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to event Error [%s]", err.Error()))
	}

	return shim.Success(nil)
}

func (t* TNS) verifyAccessToken(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke callTransaction. Incorrect arguments."))
	}

	rawETHTransaction := args[0]

	b, err := hexutil.Decode(rawETHTransaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("argument is not hex format. [%s]", err.Error()))
	}

	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := verifyAccessTokenParam{}
	err = myAbi.Methods["verifyAccessToken"].Inputs.Unpack(&param, b[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("verfify access token data : ", param)

	userAddress, targetAddress, ticket, err := t.parseHexFromSplitData(param.PlainMsg, param.UserSig)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	res := ResultParam{}
	_, managerFlag := target.Permission[common.HexToAddress(userAddress)]
	_, userFlag := target.Domain[param.Domain].Permission[common.HexToAddress(userAddress)]

	tokenMap, _, err := t.GetTokenMap(stub, common.HexToAddress(targetAddress), common.HexToAddress(userAddress))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get TokenMap [%s]", err.Error()))
	}

	if tokenMap == nil {
		shim.Error(fmt.Sprintf("there is no TokenMap state of user [%s]", userAddress))
	}

	if target.Master.Address.Hex() == userAddress || managerFlag || userFlag {
		v, flag := tokenMap[param.AccessToken]

		log.Println("verifyAccessToken")
		log.Println("ticket: ", ticket)
		log.Println("param.AccessToken: ", param.AccessToken)
		log.Println("flag: ", flag)
		log.Println("stored AccessToken")
		for k, v := range tokenMap {
			log.Println("key: ", k)
			log.Println("expiredTime: ", v.FirstUsedTime)
			for _, value := range v.UsedTimeSlot {
			log.Print("slotValue: ", value)
			log.Print(" ")
			}
			log.Println("")
			log.Println("expiredTime: ", v.UsedTimeSlot)
		}

		if flag && v.FirstUsedTime != 0 && math.Abs(float64(ticket - v.FirstUsedTime)) < 5000 {
			checkFlag := true
			for _, value := range v.UsedTimeSlot {
				if value == ticket {
					checkFlag = false
				}
			}
			if checkFlag {
				res.Result = "true"
				b, err := json.Marshal(res.Result)
				if err != nil {
					return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
				}
				return shim.Success(b)
			} else {
				return shim.Error(fmt.Sprintf("Already Used Access Token in Hex"))
			}
		} else if flag && v.FirstUsedTime == 0 {
			res.Result = "true"
			b, err := json.Marshal(res.Result)
			if err != nil {
				return shim.Error(fmt.Sprintf("Failed to marhal json. [%s]", err.Error()))
			}
			return shim.Success(b)
		} else {
			return shim.Error(fmt.Sprintf("Invalid or Expired Access Token"))
		}
	} else {
		return shim.Error(fmt.Sprintf("Failed to verifyAccessToken. there is no auth to target [%s]", targetAddress))
	}
}

func (t* TNS) generateAccessToken(stub shim.ChaincodeStubInterface, args []string) peer.Response{
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("Failed to invoke callTransaction. Incorrect arguments."))
	}

	rawETHTransaction := args[0]

	b, err := hexutil.Decode(rawETHTransaction)
	if err != nil {
		return shim.Error(fmt.Sprintf("argument is not hex format. [%s]", err.Error()))
	}

	myAbi, err := abi.JSON(strings.NewReader(TNSETHABIData))
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse abi [%s]", err.Error()))
	}

	param := verifyAccessTokenParam{}
	err = myAbi.Methods["verifyAccessToken"].Inputs.Unpack(&param, b[4:])
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to unpack data [%s]", err.Error()))
	}

	log.Println("data : ", param)

	userAddress, targetAddress, ticket, err := t.parseHexFromSplitData(param.PlainMsg, param.UserSig)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to parse HEX [%s]", err.Error()))
	}

	target, err := t.getTarget(stub, targetAddress)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to get Target [%s]", err.Error()))
	}

	_, managerFlag := target.Permission[common.HexToAddress(userAddress)]
	_, userFlag := target.Domain[param.Domain].Permission[common.HexToAddress(userAddress)]

	if target.Master.Address.Hex() == userAddress || managerFlag || userFlag {
		t.GenerateTokenMapToUser(stub, common.HexToAddress(targetAddress), common.HexToAddress(userAddress), ticket, param.AccessToken)
	} else {
		return shim.Error(fmt.Sprintf("Failed to generateAccessToken. there is no auth to target [%s]", targetAddress))
	}

	return shim.Success(nil)
}

//inner function
func (t* TNS) parseRawTx(stub shim.ChaincodeStubInterface, args []string) (string, *types.Transaction, common.Address, error) {
	if len(args) != 1 {
		return "", nil, common.Address{}, fmt.Errorf("Failed to invoke parseRawTx. Incorrect arguments.")
	}

	rawETHTransaction := args[0]

	tx, userAddress, err := t.parseETHTransaction(rawETHTransaction)
	if err != nil {
		return "", nil, common.Address{}, fmt.Errorf("Failed to invoke parseETHTransaction. [%s]", err.Error())
	}

	nonce, err := t.getNonce(stub, userAddress.Hex())
	if err != nil {
		return "", nil, common.Address{}, fmt.Errorf("Failed to get nonce. [%s]", err.Error())
	}
	log.Println("nonce: ", nonce)
	log.Println("tx.Nonce: ", tx.Nonce())
	if nonce > tx.Nonce() {
		return "", nil, common.Address{}, fmt.Errorf("Failed to check nonce. nonce too low [%s]", userAddress.Hex())
	}
	buf := tx.Data()[0:4]
	functionHashes := common.Bytes2Hex(buf)

	return functionHashes, tx, userAddress, nil
}

func (t* TNS) parseETHTransaction(rawTransaction string) (*types.Transaction, common.Address, error) {
	encodedTx, err := hexutil.Decode(rawTransaction)
	if err != nil {
		return nil, common.Address{}, fmt.Errorf("argument is not hex format. [%s]", err.Error())
	}

	tx := new(types.Transaction)
	if err := rlp.DecodeBytes(encodedTx, tx); err != nil {
		return nil, common.Address{}, fmt.Errorf("argument is not eth transaction. [%s]", err.Error())
	}

	signer := types.NewEIP155Signer(nil)
	userAddress, err := signer.Sender(tx)

	if err != nil {
		return nil, common.Address{}, fmt.Errorf("failed to ecrecover from eth transaction [%s]", err.Error())
	}

	log.Println("User Address : ", userAddress.Hex())

	log.Println("TX Data : ", hexutil.Encode(tx.Data()))
	log.Println("TX Data : ", tx.Data())

	return tx, userAddress, nil
}

func (t* TNS) parseHexFromSplitData(originalData [29]byte, signatureHex string) (string, string, uint64, error) {
	userAddress, err := blockchain.ECRecover(originalData[:], common.FromHex(signatureHex))
	if err != nil {
		return "", "", 0, fmt.Errorf("failed to ecrecover from HEX [%s]", err.Error())
	}

	var protocol [1]byte // 서비스 타입
	var ticket uint64 // 티켓
	var targetAddressBinary [20]byte // ID Wall Public key
	buf := bytes.NewBuffer(originalData[:])
	binary.Read(buf, binary.LittleEndian, &protocol)
	binary.Read(buf, binary.LittleEndian, &ticket)
	binary.Read(buf, binary.LittleEndian, &targetAddressBinary)

	targetAddress := common.BytesToAddress(targetAddressBinary[:]).Hex()

	return userAddress, targetAddress, ticket, nil
}

func (t* TNS) parseHEX(originHex string) (string, string, uint64, error) {
	unHexTx, err := hex.DecodeString(originHex)
	if err != nil {
		return "", "", 0, fmt.Errorf("HEX is not hex format. [%s]", err.Error())
	}

	var originalData [29]byte
	copy(originalData[:], unHexTx[0:29])
	signatureHex := common.ToHex(unHexTx[29:])
	return t.parseHexFromSplitData(originalData, signatureHex)
}

func (t* TNS) getNonce(stub shim.ChaincodeStubInterface, userAddress string) (uint64, error) {
	value, err := stub.GetState(strings.ToUpper(userAddress) + NonceKeyName)
	if err != nil {
		return 0,  fmt.Errorf("failed to get nonce state from BC [%s] [%s]", err.Error(), userAddress)
	}

	if value == nil {
		return 0,  nil
	}

	var currentNonce uint64
	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&currentNonce)
	if err != nil {
		return 0,  fmt.Errorf("failed to marshal from nonce byte [%s] [%s]", err.Error(), userAddress)
	}

	return currentNonce, nil
}

func (t* TNS) setNonce(stub shim.ChaincodeStubInterface, userAddress string, nonce uint64) error {
	buffer := &bytes.Buffer{}
	err := gob.NewEncoder(buffer).Encode(nonce)
	if err != nil {
		return fmt.Errorf("failed to marshal user nonce [%s] [%s]", err.Error(), userAddress)
	}
	byteSlice := buffer.Bytes()

	err = stub.PutState(strings.ToUpper(userAddress)+NonceKeyName, byteSlice)

	if err != nil {
		return fmt.Errorf("failed to put nonce state [%s] [%s]", err.Error(), userAddress)
	}

	return nil
}

func (t* TNS) getTarget(stub shim.ChaincodeStubInterface, targetAddress string) (Target, error) {
	value, err := stub.GetState(strings.ToUpper(targetAddress))
	if err != nil {
		return Target{},  fmt.Errorf("failed to get state from BC [%s] [%s]", err.Error(), targetAddress)
	}

	if value == nil {
		return Target{},  fmt.Errorf("failed to get state from BC. no target [%s]", targetAddress)
	}

	target := Target{}

	err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&target)
	if err != nil {
		return Target{},  fmt.Errorf("failed to marshal from Byte [%s] [%s]", err.Error(), targetAddress)
	}

	return target, nil
}

func (t* TNS) setTarget(stub shim.ChaincodeStubInterface, target *Target) error {
	buffer := &bytes.Buffer{}
	err := gob.NewEncoder(buffer).Encode(target)
	if err != nil {
		return fmt.Errorf("failed to marshal target [%s] [%s]", err.Error(), target.TargetAddress.Hex())
	}
	byteSlice := buffer.Bytes()

	err = stub.PutState(strings.ToUpper(target.TargetAddress.Hex()), byteSlice)

	if err != nil {
		return fmt.Errorf("failed to put target state [%s] [%s]", err.Error(), target.TargetAddress.Hex())
	}

	return nil
}

func (t* TNS) addTargetToUser(stub shim.ChaincodeStubInterface, userAddress string, targetAddress string) error {
	err := t.GenerateTokenMapToUser(stub, common.HexToAddress(targetAddress), common.HexToAddress(userAddress), 0, "")
	if err != nil {
		return fmt.Errorf("Failed to generate access token of %s: %s", userAddress, err.Error())
	}

	currentTargetList, err := t.getTargetListFromUser(stub, userAddress)

	if err != nil {
		currentTargetList = make([]string, 0)
		currentTargetList = append(currentTargetList, targetAddress)
		return t.setTargetToUser(stub, userAddress, currentTargetList)
	} else {
		currentTargetList = append(currentTargetList, targetAddress)
		return t.setTargetToUser(stub, userAddress, currentTargetList)
	}
}

func (t* TNS) removeTargetToUser(stub shim.ChaincodeStubInterface, userAddress string, targetAddress string) error {
	err := t.DeleteTokenMapToUser(stub, common.HexToAddress(targetAddress), common.HexToAddress(userAddress))
	if err != nil {
		return fmt.Errorf("Failed to delete access token of %s: %s", userAddress, err.Error())
	}

	currentTargetList, err := t.getTargetListFromUser(stub, userAddress)

	if err != nil {
		return err
	} else {
		newTargetList := make([]string, 0)
		for _, savedTargetAddress := range currentTargetList {
			if savedTargetAddress != targetAddress {
				newTargetList = append(newTargetList, savedTargetAddress)
			}
		}

		return t.setTargetToUser(stub, userAddress, newTargetList)
	}
}

func (t* TNS) setTargetToUser(stub shim.ChaincodeStubInterface, userAddress string, currentTargetList []string) error {
	buffer := &bytes.Buffer{}
	err := gob.NewEncoder(buffer).Encode(currentTargetList)
	if err != nil {
		return fmt.Errorf("failed to gob encoder target [%s] [%s]", err.Error(), userAddress)
	}
	byteSlice := buffer.Bytes()

	err = stub.PutState(strings.ToUpper(userAddress) + UserKeyName, byteSlice)

	if err != nil {
		return fmt.Errorf("failed to put user state [%s] [%s]", err.Error(), userAddress)
	}

	return nil
}

func (t* TNS) getTargetListFromUser(stub shim.ChaincodeStubInterface, userAddress string) ([]string, error) {
	value, err := stub.GetState(strings.ToUpper(userAddress) + UserKeyName)
	if err != nil {
		return nil, fmt.Errorf("failed to get user state from BC [%s] [%s]", err.Error(), userAddress)
	}

	if value == nil {
		return nil, fmt.Errorf("failed to get user state from BC. no user [%s]", userAddress)
	} else {
		var currentTargetList []string
		err = gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&currentTargetList)
		if err != nil {
			return nil, fmt.Errorf("failed to marshal from user state byte [%s] [%s]", err.Error(), userAddress)
		}

		return currentTargetList, nil
	}
}

func (t* TNS) getRR(stub shim.ChaincodeStubInterface, targetAddress common.Address, user *User, ticket uint64, tRR *map[string]string, dRR *map[string]string, uRR *map[string]interface{} ) peer.Response {
	result, err :=  t.verifyTicket(stub, user.Address.Hex(), ticket)
	log.Println("verifyTicket result: ", result)
	if err != nil{
		return shim.Error(fmt.Sprintf("Failed to verifyTicket [%s]", err.Error()))
	}

	if result {
		return t.toJSON(stub, targetAddress, user.Address, tRR, dRR, uRR)
	} else {
		return shim.Error(fmt.Sprintf("Failed to check ticket. already used ticket [%d]", ticket))
	}
}

func (t *TNS) updateTicket(stub shim.ChaincodeStubInterface, userAddress string, TNSAddress common.Address, ticket uint64) error {
	compositeKey, compositeErr := stub.CreateCompositeKey(TicketIndexString, []string{strings.ToUpper(userAddress), strings.ToUpper(TNSAddress.Hex())})
	if compositeErr != nil {
		return fmt.Errorf("Could not create a composite key for %s: %s", userAddress, compositeErr.Error())
	}

	value, compositeGetErr := stub.GetState(compositeKey)
	if compositeGetErr != nil {
		return fmt.Errorf("failed to GetState[%s]", compositeGetErr.Error())
	}

	var ticketSlot TicketSlot
	if value != nil {
		ticketSlot = TicketSlot{}
		err := gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&ticketSlot)
		if err != nil {
			return fmt.Errorf("Failed to marshal TicketSlot from Byte [%s] [%s]", err.Error(), userAddress)
		}
		if ticketSlot.Ticket == ticket {
			return  fmt.Errorf("failed to save ticket. already used ticket")
		} else if ticketSlot.Ticket < ticket {
			ticketSlot.Ticket = ticket
			newTicketSlot := make([]uint64, 0)
			if len(ticketSlot.Slot) != 0 {
				for _, savedTicket := range ticketSlot.Slot {
					if math.Abs(float64(savedTicket - ticket)) <= 5000 {
						newTicketSlot = append(newTicketSlot, savedTicket)
					}
				}
			}
			ticketSlot.Slot = newTicketSlot
		} else {
			if math.Abs(float64(ticketSlot.Ticket - ticket)) > 5000 {
				return fmt.Errorf("failed to save ticket. ticket is invalid")
			}
			ticketSlot.Slot = append(ticketSlot.Slot, ticket)
		}
	} else {
		ticketSlot = TicketSlot{}
		ticketSlot.Ticket = ticket
		ticketSlot.Slot = make([]uint64, 0)
	}
	buffer := &bytes.Buffer{}
	err := gob.NewEncoder(buffer).Encode(ticketSlot)
	if err != nil {
		return fmt.Errorf("failed to marshal TicketSlot [%s] [%s]", err.Error(), userAddress)
	}
	byteSlice := buffer.Bytes()
	err = stub.PutState(compositeKey, byteSlice)
	if err != nil {
		return fmt.Errorf("failed to put TicketSlot state [%s] [%s]", err.Error(), userAddress)
	}

	return nil
}

func (t *TNS) verifyTicket(stub shim.ChaincodeStubInterface, userAddress string, ticket uint64) (bool, error) {
	ticketResultsIterator, deltaErr := stub.GetStateByPartialCompositeKey(TicketIndexString, []string{strings.ToUpper(userAddress)})
	if deltaErr != nil {
		return false, fmt.Errorf("Could not retrieve TicketSlot of %s: %s", userAddress, deltaErr.Error())
	}
	defer ticketResultsIterator.Close()

	if !ticketResultsIterator.HasNext() {
		return true, nil
	}

	var i int
	mergeTicket := TicketSlot{}
	mergeTicket.Ticket = 0
	mergeTicket.Slot = make([]uint64, 0)
	for i = 0; ticketResultsIterator.HasNext(); i++ {
		responseRange, nextErr := ticketResultsIterator.Next()
		if nextErr != nil {
			return false, fmt.Errorf(nextErr.Error())
		}

		var ticketSlot TicketSlot
		ticketSlot = TicketSlot{}
		err := gob.NewDecoder(bytes.NewBuffer(responseRange.Value[:])).Decode(&ticketSlot)
		if err != nil {
			return false, fmt.Errorf("Failed to marshal TicketSlot from Byte [%s] [%s]", err.Error(), userAddress)
		}
		if(ticketSlot.Ticket > mergeTicket.Ticket) {
			mergeTicket.Ticket = ticketSlot.Ticket
		}

		mergeTicket.Slot = append(mergeTicket.Slot, ticketSlot.Ticket)
	}

	if mergeTicket.Ticket == ticket {
		return false, nil
	} else if mergeTicket.Ticket < ticket {
		return true, nil
	} else {
		if math.Abs(float64(mergeTicket.Ticket - ticket)) > 5000 {
			return false, nil
		}

		for _, savedTicket := range mergeTicket.Slot {
			if ticket == savedTicket {
				return false, nil
			}
		}
		return true, nil
	}
}

func (t* TNS) toJSON(stub shim.ChaincodeStubInterface, targetAddress common.Address, userAddress common.Address, tRR *map[string]string, dRR *map[string]string, uRR *map[string]interface{}) peer.Response {
	rRR := make(map[string]interface{})
	for k, v := range *tRR {
		rRR[k] = v
	}

	for k, v := range *dRR {
		rRR[k] = v
	}

	for k, v := range *uRR {
		rRR[k] = v
	}

	tokenMap, _, err := t.GetTokenMap(stub, targetAddress, userAddress)
	if err == nil && tokenMap != nil {
		for k, v := range tokenMap {
			if v.FirstUsedTime == 0 {
				rRR["accessToken"] = k
			}
		}
	} else {
		rRR["accessToken"] = ""
	}

	b, err := json.Marshal(rRR)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to make json.[%s]", err))
	}

	return shim.Success(b)
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
func (t* TNS) GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (t* TNS) GenerateRandomString(s int) (string, error) {
	b, err := t.GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func (t* TNS) GetTokenMap(stub shim.ChaincodeStubInterface, targetAddress common.Address, userAddress common.Address) (TokenMap, string, error) {
	log.Println("GetTokenMap")
	compositeKey, compositeErr := stub.CreateCompositeKey(TokenIndexString, []string{strings.ToUpper(targetAddress.Hex()), strings.ToUpper(userAddress.Hex())})
	if compositeErr != nil {
		return nil, "", fmt.Errorf("Could not create a composite key for %s: %s", userAddress.Hex(), compositeErr.Error())
	}

	value, compositeGetErr := stub.GetState(compositeKey)
	if compositeGetErr != nil {
		return nil, compositeKey, fmt.Errorf("failed to GetState[%s]", compositeGetErr.Error())
	}

	var tokenMap TokenMap
	if value != nil {
		tokenMap = TokenMap{}
		err := gob.NewDecoder(bytes.NewBuffer(value[:])).Decode(&tokenMap)
		if err != nil {
			return nil, compositeKey, fmt.Errorf("Failed to Decode TokenMap from Byte [%s] [%s]", err.Error(), userAddress.Hex())
		}
		return tokenMap, compositeKey, nil
	} else {
		return nil, compositeKey, nil
	}
}

func (t* TNS) DeleteTokenMapToUser(stub shim.ChaincodeStubInterface, targetAddress common.Address, userAddress common.Address) error {
	compositeKey, compositeErr := stub.CreateCompositeKey(TokenIndexString, []string{strings.ToUpper(targetAddress.Hex()), strings.ToUpper(userAddress.Hex())})
	if compositeErr != nil {
		return fmt.Errorf("Could not create a composite key for %s: %s", userAddress.Hex(), compositeErr.Error())
	}

	err := stub.DelState(compositeKey)
	if err != nil {
		return fmt.Errorf("Could not delete a composite key for %s: %s", userAddress.Hex(), err.Error())
	}

	return nil
}

func (t* TNS) GenerateTokenMapToUser(stub shim.ChaincodeStubInterface, targetAddress common.Address, userAddress common.Address, ticket uint64, AccessToken string) error {
	randNum, err := t.GenerateRandomString(16)
	if err != nil {
		return fmt.Errorf("Failed to generate token [%s]", err.Error())
	}

	tokenMap, compositeKey, err := t.GetTokenMap(stub, targetAddress, userAddress)
	log.Println("compositeKey: ", compositeKey)
	if err != nil {
		return fmt.Errorf("Failed to get TokenMap [%s]", err.Error())
	}

	if tokenMap != nil {
		if ticket == 0 && AccessToken == "" {
			log.Println("ticket == 0 && AccessToken no")
			for k, v := range tokenMap {
				if v.FirstUsedTime == 0 || math.Abs(float64(ticket - v.FirstUsedTime)) > 5000 {
					delete(tokenMap, k)
				}
			}
			tokenStruct := TokenStruct{}
			tokenStruct.FirstUsedTime = 0
			tokenStruct.UsedTimeSlot = make([]uint64, 0)
			tokenMap[randNum] = tokenStruct
		} else {
			v, flag := tokenMap[AccessToken]
			if flag && v.FirstUsedTime == 0 {
				log.Println("test1")
				tokenStruct := TokenStruct{}
				tokenStruct.FirstUsedTime = ticket
				tokenMap[AccessToken] = tokenStruct

				newTokenStruct := TokenStruct{}
				newTokenStruct.FirstUsedTime = 0
				newTokenStruct.UsedTimeSlot = make([]uint64, 0)
				tokenMap[randNum] = newTokenStruct
			} else if flag && math.Abs(float64(ticket - v.FirstUsedTime)) < 5000 {
				if len(v.UsedTimeSlot) != 0 {
					log.Println("test2")
					checkFlag := true
					for _, v := range v.UsedTimeSlot {
						if v == ticket {
							checkFlag = false
						}
					}
					if checkFlag {
						v.UsedTimeSlot = append(v.UsedTimeSlot, ticket)
					}
				} else {
					log.Println("test3")
					v.UsedTimeSlot = make([]uint64, 0)
					v.UsedTimeSlot = append(v.UsedTimeSlot, ticket)
				}
			}
			for k, v := range tokenMap {
				if v.FirstUsedTime != 0 && math.Abs(float64(ticket - v.FirstUsedTime)) > 5000 {
					delete(tokenMap, k)
				}
			}
		}
	} else {
		log.Println("new create tokenMap")
		tokenMap = make(map[string]TokenStruct)
		tokenStruct := TokenStruct{}
		tokenStruct.FirstUsedTime = 0
		tokenStruct.UsedTimeSlot = make([]uint64, 0)
		tokenMap[randNum] = tokenStruct
	}

	log.Println("generateAccessToken")
	log.Println("ticket: ", ticket)
	log.Println("stored AccessToken")
	for k, v := range tokenMap {
		log.Println("key: ", k)
		log.Println("expiredTime: ", v.FirstUsedTime)
		for _, value := range v.UsedTimeSlot {
			log.Print("slotValue: ", value)
			log.Print(" ")
		}
		log.Println("")
		log.Println("expiredTime: ", v.UsedTimeSlot)
	}

	buffer := &bytes.Buffer{}
	err = gob.NewEncoder(buffer).Encode(tokenMap)
	if err != nil {
		return fmt.Errorf("failed to marshal TokenMap [%s] [%s]", err.Error(), userAddress.Hex())
	}
	byteSlice := buffer.Bytes()
	err = stub.PutState(compositeKey, byteSlice)
	if err != nil {
		return fmt.Errorf("failed to put TokenMap state [%s] [%s]", err.Error(), userAddress.Hex())
	}

	return nil
}
func main() {
	logger.SetLevel(shim.LogDebug)
	logger1.SetLevel(shim.LogDebug)

	if err := shim.Start(new(TNS)); err != nil {
		fmt.Printf("Error starting chaincode: %s", err)
	}
}

