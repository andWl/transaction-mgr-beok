/*

INVOKE:
peer chaincode invoke -C __CHANNEL__ -n __CHAINCODE_ID__ -c '{"Args":["func","arg"]}'

QUERY:
peer chaincode query -C __CHANNEL__ -n __CHAINCODE_ID__ -c '{"Args":["func","arg"]}'

*/

package main

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

const PolicyManagerETHABIData = "[{\"constant\":true,\"inputs\":[],\"name\":\"getAllState\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"MID\",\"type\":\"string\"}],\"name\":\"deleteRelation\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"toMID\",\"type\":\"string\"},{\"name\":\"viaMID\",\"type\":\"string\"},{\"name\":\"fromGSID\",\"type\":\"string\"}],\"name\":\"checkPolicy\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"MID\",\"type\":\"string\"}],\"name\":\"checkRelation\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"srcGSID\",\"type\":\"string\"},{\"name\":\"srcGSBID\",\"type\":\"string\"},{\"name\":\"srcMID\",\"type\":\"string\"},{\"name\":\"dstMID\",\"type\":\"string\"},{\"name\":\"dstGSBID\",\"type\":\"string\"},{\"name\":\"dstGSID\",\"type\":\"string\"}],\"name\":\"deletePolicy\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"fromGSID\",\"type\":\"string\"},{\"name\":\"viaMID\",\"type\":\"string\"},{\"name\":\"toMID\",\"type\":\"string\"},{\"name\":\"timestampIssuedAsString\",\"type\":\"string\"}],\"name\":\"setSeed\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"fromGSID\",\"type\":\"string\"},{\"name\":\"viaMID\",\"type\":\"string\"},{\"name\":\"toMID\",\"type\":\"string\"}],\"name\":\"getSeed\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"MID\",\"type\":\"string\"}],\"name\":\"setRelation\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"GSBID\",\"type\":\"string\"},{\"name\":\"fromGSID\",\"type\":\"string\"},{\"name\":\"viaMID\",\"type\":\"string\"},{\"name\":\"toMID\",\"type\":\"string\"}],\"name\":\"deleteSeed\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"srcGSID\",\"type\":\"string\"},{\"name\":\"srcGSBID\",\"type\":\"string\"},{\"name\":\"srcMID\",\"type\":\"string\"},{\"name\":\"dstMID\",\"type\":\"string\"},{\"name\":\"dstGSBID\",\"type\":\"string\"},{\"name\":\"dstGSID\",\"type\":\"string\"}],\"name\":\"setPolicy\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]"

type deleteRelation struct {
	GSBID string
	MID   string
}
type checkPolicy struct {
	GSBID    string
	ToMID    string
	ViaMID   string
	FromGSID string
}
type checkRelation struct {
	GSBID string
	MID   string
}
type deletePolicy struct {
	SrcGSID  string
	SrcGSBID string
	SrcMID   string
	DstMID   string
	DstGSBID string
	DstGSID  string
}
type setSeed struct {
	GSBID                   string
	FromGSID                string
	ViaMID                  string
	ToMID                   string
	TimestampIssuedAsString string
}
type getSeed struct {
	GSBID    string
	FromGSID string
	ViaMID   string
	ToMID    string
}
type setRelation struct {
	GSBID string
	MID   string
}
type deleteSeed struct {
	GSBID    string
	FromGSID string
	ViaMID   string
	ToMID    string
}
type setPolicy struct {
	SrcGSID  string
	SrcGSBID string
	SrcMID   string
	DstMID   string
	DstGSBID string
	DstGSID  string
}

// PolicyKey .
type PolicyKey struct {
	SrcGSID string `json:"srcGsid"`
	SrcMID  string `json:"srcMid"`
	DstMID  string `json:"dstMid"`
}

// Policy .
type Policy struct {
	IsAllowed          bool `json:"isAllowed"`
	NumberOfReferences int  `json:"nRefs"`
}

// Seed .
type Seed struct {
	HasSeed          bool     `json:"hasSeed"`
	TimestampIssued  uint64   `json:"timestampIssued"`
	TimestampExpired uint64   `json:"timestampExpired"`
	Seed             [32]byte `json:"seed"`
}

// RelationKey .
type RelationKey struct {
	GSBID string `json:"gsb"`
	MID   string `json:"mid"`
}

// Relation .
type Relation struct {
	HasRelation bool `json:"rel"`
}

const (
	// SeedTimeout is a timeout value for 'seed'
	SeedTimeout uint64 = (60 * 60 * 24)

	// PolicyManagerString is a name of this chaincode
	PolicyManagerString string = "PolicyManager"

	// IndexPolicy is an index name for Policy
	IndexPolicy string = "docType~recordType~srcGsid~srcMid~dstMid"

	// PolicyString is a record type for Policy
	PolicyString string = "Policy"

	// IndexSeed is an index name for Seed
	IndexSeed string = "docType~recordType~srcGsid~srcMid~dstMid"

	// SeedString is a record type for Seed
	SeedString string = "Seed"

	// IndexRelationBetweenGateAndMID is an index name for Relation between Gate and MID
	IndexRelationBetweenGateAndMID string = "docType~recordType~gateAddr~mid"

	// RelationBetweenGateAndMIDString is a record type for Relation between Gate and MID
	RelationBetweenGateAndMIDString string = "RelationBetweenGateAndMID"
)

var logger = shim.NewLogger("Policy Manager")

// MarshalToByteArray is a function
func MarshalToByteArray(v interface{}) []byte {
	b, err := json.Marshal(v)
	if err != nil {
		logger.Error(err.Error())
		return nil
	}

	return b
}

// MarshalToString is a function
func MarshalToString(v interface{}) string {
	b := MarshalToByteArray(v)
	s := string(b)

	return s
}

func (t *TNS) checkMIDFormat(mid string) bool {
	// TODO: Check MID format
	if len(mid) <= 0 {
		return false
	}

	return true
}

func (t *TNS) checkGSBIDFormat(gsbid string) bool {
	// TODO: Check GSBID format
	if len(gsbid) <= 0 {
		return false
	}

	return true
}

func (t *TNS) checkGSIDFormat(gsid string) bool {
	// TODO: Check GSID format
	if len(gsid) <= 0 {
		return false
	}

	return true
}

func (t *TNS) checkTimestampFormat(timestamp string) bool {
	_, err := strconv.ParseUint(timestamp, 10, 64)
	if err != nil {
		return false
	}

	return true

}

func (t *TNS) setPolicy(stub shim.ChaincodeStubInterface, param setPolicy) pb.Response {
	var err error

	srcGSID, srcGSBID, srcMID, dstMID, dstGSBID, dstGSID := param.SrcGSID, param.SrcGSBID, param.SrcMID, param.DstMID, param.DstGSBID, param.DstGSID
	if t.checkGSIDFormat(srcGSID) == false || t.checkGSIDFormat(dstGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(srcGSBID) == false || t.checkGSBIDFormat(dstGSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(srcMID) == false || t.checkMIDFormat(dstMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Set relation */
	{
		/* For 'srcGSB' and 'srcMID' */
		args := setRelation{}
		args.GSBID = srcGSBID
		args.MID = srcMID
		err = t.setRelation(stub, args)
		if err != nil {
			return shim.Error(err.Error())
		}

		/* For 'dstGSB' and 'dstMID' */
		args.GSBID = dstGSBID
		args.MID = dstMID
		err = t.setRelation(stub, args)
		if err != nil {
			return shim.Error(err.Error())
		}
	}

	/* Set Policy */
	{
		var policy Policy

		/* For 'srcGS'-'srcMID'-'dstMID' */
		{
			/* Get Policy */
			policyKey, err := stub.CreateCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString, srcGSID, srcMID, dstMID})
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			value, err := stub.GetState(policyKey)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}

			/* Set Policy */
			if value != nil {
				err = json.Unmarshal(value, &policy)
				if err != nil {
					logger.Error(err)
					return shim.Error(err.Error())
				}
			} else {
				policy = Policy{IsAllowed: true, NumberOfReferences: 0}
			}
			policy.NumberOfReferences++
			err = stub.PutState(policyKey, MarshalToByteArray(policy))
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			logger.Info("Policy1: " + MarshalToString(policy))
		}

		/* For 'dstGS'-'dstMID'-'srcMID' */
		{
			/* Get Policy */
			policyKey, err := stub.CreateCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString, dstGSID, dstMID, srcMID})
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			value, err := stub.GetState(policyKey)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}

			/* Set Policy */
			if value != nil {
				err = json.Unmarshal(value, &policy)
				if err != nil {
					logger.Error(err)
					return shim.Error(err.Error())
				}
			} else {
				policy = Policy{IsAllowed: true, NumberOfReferences: 0}
			}
			policy.NumberOfReferences++
			err = stub.PutState(policyKey, MarshalToByteArray(policy))
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			logger.Info("Policy2: " + MarshalToString(policy))
		}
	}

	return shim.Success(nil)
}

func (t *TNS) checkPolicy(stub shim.ChaincodeStubInterface, param checkPolicy) pb.Response {
	var msg string
	var err error

	GSBID, toMID, viaMID, fromGSID := param.GSBID, param.ToMID, param.ViaMID, param.FromGSID
	if t.checkGSIDFormat(fromGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(viaMID) == false || t.checkMIDFormat(toMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Check relation between 'GSB' and ('viaMID' or 'toMID') */
	args := checkRelation{}
	args.GSBID = GSBID
	args.MID = viaMID
	result, err := t.checkRelation(stub, args)
	if err != nil {
		return shim.Error(err.Error())
	}
	if result == false {
		args := checkRelation{}
		args.GSBID = GSBID
		args.MID = toMID
		result, err = t.checkRelation(stub, args)
		if err != nil {
			return shim.Error(err.Error())
		}
		if result == false {
			err = errors.New("Not permitted relation")
			logger.Error(err)
			return shim.Error(err.Error())
		}
	}

	/* Check policy for 'fromGSID'-'viaMID'-'toMID' */
	result, err = t.checkPolicyInternal(stub, []string{fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}

	msg = "{\"result\":"
	if result == true {
		msg += "true"
	} else {
		msg += "false"
	}
	msg += "}"
	logger.Info("Result: " + msg)

	msgStringifiedAsBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	msgStringified := string(msgStringifiedAsBytes)

	return shim.Success([]byte(msgStringified))
}

func (t *TNS) checkPolicyInternal(stub shim.ChaincodeStubInterface, args []string) (bool, error) {
	var err error
	var policy Policy

	logger.Info("checkPolicyInternal(" + strings.Join(args, ", ") + ")")

	/* Check arguments */
	if len(args) != 3 {
		err = errors.New("Invalid number of arguments")
		logger.Error(err)
		return false, err
	}
	fromGSID, viaMID, toMID := args[0], args[1], args[2]
	if t.checkGSIDFormat(fromGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return false, err
	}
	if t.checkMIDFormat(viaMID) == false || t.checkMIDFormat(toMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return false, err
	}

	/* Check policy for 'fromGSID'-'viaMID'-'toMID' */
	policyKey, err := stub.CreateCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString, fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return false, err
	}
	value, err := stub.GetState(policyKey)
	if err != nil {
		logger.Error(err)
		return false, err
	}
	if value == nil {
		err = errors.New("Failed to check policy")
		logger.Error(err)
		return false, nil
	}
	err = json.Unmarshal(value, &policy)
	if err != nil {
		logger.Error(err)
		return false, err
	}
	logger.Info("Policy: " + MarshalToString(policy))
	if policy.NumberOfReferences <= 0 {
		err = errors.New("Invalid number of references")
		logger.Error(err)
		return false, nil
	}

	if policy.IsAllowed == true {
		return true, nil
	}
	return false, nil
}

func (t *TNS) deletePolicy(stub shim.ChaincodeStubInterface, param deletePolicy) pb.Response {
	var msg string
	var err error
	var policy1, policy2 Policy

	srcGSID, srcGSBID, srcMID, dstMID, dstGSBID, dstGSID := param.SrcGSID, param.SrcGSBID, param.SrcMID, param.DstMID, param.DstGSBID, param.DstGSID
	if t.checkGSIDFormat(srcGSID) == false || t.checkGSIDFormat(dstGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(srcGSBID) == false || t.checkGSBIDFormat(dstGSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(srcMID) == false || t.checkMIDFormat(dstMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* For source */
	{
		/* Check relation between 'srcGSB' and 'srcMID' */
		args := checkRelation{}
		args.GSBID = srcGSBID
		args.MID = srcMID
		result, err := t.checkRelation(stub, args)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if result == false {
			err = errors.New("Not permitted relation")
			logger.Error(err)
			return shim.Error(err.Error())
		}

		/* Delete policy for 'srcGSID'-'srcMID'-'dstMID' */
		policyKey, err := stub.CreateCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString, srcGSID, srcMID, dstMID})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		value, err := stub.GetState(policyKey)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if value == nil {
			err = errors.New("Failed to get policy")
			logger.Error(err)
			return shim.Error(err.Error())
		}
		err = json.Unmarshal(value, &policy1)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if policy1.NumberOfReferences <= 0 {
			err = errors.New("Invalid number of references")
			logger.Error(err)
			return shim.Error(err.Error())
		}
		policy1.NumberOfReferences--
		logger.Info("Policy1: " + MarshalToString(policy1))
		if policy1.NumberOfReferences == 0 {
			err = stub.DelState(policyKey)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
		} else {
			err = stub.PutState(policyKey, MarshalToByteArray(policy1))
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
		}
	}

	/* For destination */
	{
		/* Check relation between 'dstGSB' and 'dstMID' */
		args := checkRelation{}
		args.GSBID = dstGSBID
		args.MID = dstMID
		result, err := t.checkRelation(stub, args)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if result == false {
			err = errors.New("Not permitted relation")
			logger.Error(err)
			return shim.Error(err.Error())
		}

		/* Delete policy for 'dstGSID'-'dstMID'-'srcMID' */
		policyKey, err := stub.CreateCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString, dstGSID, dstMID, srcMID})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		value, err := stub.GetState(policyKey)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if value == nil {
			err = errors.New("Failed to get policy")
			logger.Error(err)
			return shim.Error(err.Error())
		}
		err = json.Unmarshal(value, &policy2)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if policy2.NumberOfReferences <= 0 {
			err = errors.New("Invalid number of references")
			logger.Error(err)
			return shim.Error(err.Error())
		}
		policy2.NumberOfReferences--
		logger.Info("Policy2: " + MarshalToString(policy2))
		if policy2.NumberOfReferences == 0 {
			err = stub.DelState(policyKey)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
		} else {
			err = stub.PutState(policyKey, MarshalToByteArray(policy2))
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
		}
	}

	msg = fmt.Sprintf("{\"%s\":%d,\"%s\":%d}",
		"srcCnt", policy1.NumberOfReferences,
		"dstCnt", policy2.NumberOfReferences)
	logger.Info("Result: " + msg)

	msgStringifiedAsBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	msgStringified := string(msgStringifiedAsBytes)

	return shim.Success([]byte(msgStringified))
}

func (t *TNS) setRelation(stub shim.ChaincodeStubInterface, param setRelation) error {
	var err error

	GSBID, MID := param.GSBID, param.MID
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return err
	}
	if t.checkMIDFormat(MID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return err
	}

	/* Set relation */
	relationKey, err := stub.CreateCompositeKey(IndexRelationBetweenGateAndMID, []string{PolicyManagerString, RelationBetweenGateAndMIDString, GSBID, MID})
	if err != nil {
		logger.Error(err)
		return err
	}
	relation := Relation{HasRelation: true}
	logger.Info("Relation: " + MarshalToString(relation))
	err = stub.PutState(relationKey, MarshalToByteArray(relation))
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (t *TNS) checkRelation(stub shim.ChaincodeStubInterface, param checkRelation) (bool, error) {
	var err error
	var relation Relation

	GSBID, MID := param.GSBID, param.MID
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return false, err
	}
	if t.checkMIDFormat(MID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return false, err
	}

	/* Get relation between 'GSB' and 'MID' */
	relationKey, err := stub.CreateCompositeKey(IndexRelationBetweenGateAndMID, []string{PolicyManagerString, RelationBetweenGateAndMIDString, GSBID, MID})
	if err != nil {
		logger.Error(err)
		return false, err
	}
	value, err := stub.GetState(relationKey)
	if err != nil {
		logger.Error(err)
		return false, err
	}
	if value == nil {
		err = errors.New("Failed to get relation")
		logger.Error(err)
		return false, nil
	}
	err = json.Unmarshal(value, &relation)
	if err != nil {
		logger.Error(err)
		return false, err
	}
	logger.Info("Relation: " + MarshalToString(relation))

	if relation.HasRelation == true {
		return true, nil
	}
	return false, nil
}

func (t *TNS) deleteRelation(stub shim.ChaincodeStubInterface, param deleteRelation) error {
	var err error

	GSBID, MID := param.GSBID, param.MID
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return err
	}
	if t.checkMIDFormat(MID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return err
	}

	/* Delete relation between 'GSB' and 'MID' */
	relationKey, err := stub.CreateCompositeKey(IndexRelationBetweenGateAndMID, []string{PolicyManagerString, RelationBetweenGateAndMIDString, GSBID, MID})
	if err != nil {
		logger.Error(err)
		return err
	}
	err = stub.DelState(relationKey)
	if err != nil {
		logger.Error(err)
		return err
	}

	return nil
}

func (t *TNS) setSeed(stub shim.ChaincodeStubInterface, param setSeed) pb.Response {
	var msg string
	var err error
	var seed Seed

	GSBID, fromGSID, viaMID, toMID, timestampIssuedAsString := param.GSBID, param.FromGSID, param.ViaMID, param.ToMID, param.TimestampIssuedAsString
	if t.checkGSIDFormat(fromGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(viaMID) == false || t.checkMIDFormat(toMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkTimestampFormat(timestampIssuedAsString) == false {
		err = errors.New("Invalid Timestamp format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Check relation between 'GSB' and 'viaMID' */
	args := checkRelation{}
	args.GSBID = GSBID
	args.MID = viaMID
	result, err := t.checkRelation(stub, args)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		err = errors.New("Not permitted relation")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Check policy for 'fromGSID'-'viaMID'-'toMID' */
	result, err = t.checkPolicyInternal(stub, []string{fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		err = errors.New("Not permitted policy")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Set seed for 'fromGSID'-'viaMID'-'toMID' */
	seedKey, err := stub.CreateCompositeKey(IndexSeed, []string{PolicyManagerString, SeedString, fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	seed.HasSeed = true
	seed.TimestampIssued, err = strconv.ParseUint(timestampIssuedAsString, 10, 64)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	seed.TimestampExpired = seed.TimestampIssued + SeedTimeout
	tokenSeed := make([]byte, 32)
	n, err := rand.Read(tokenSeed)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if n != cap(tokenSeed) {
		err = errors.New("Invalid seed length")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	copy(seed.Seed[:cap(seed.Seed)], tokenSeed)
	// logger.Info("(new)Policy: " + MarshalToString(seed))
	logger.Info("(new)Seed: " + MarshalToString(seed))
	err = stub.PutState(seedKey, MarshalToByteArray(seed))
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}

	msg = fmt.Sprintf("{\"%s\":%d,\"%s\":%d,\"%s\":\"%s\"}",
		"timestampIssued", seed.TimestampIssued,
		"timestampExpired", seed.TimestampExpired,
		"seed", hex.EncodeToString(seed.Seed[:]))
	logger.Info("Result: " + msg)

	msgStringifiedAsBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	msgStringified := string(msgStringifiedAsBytes)

	return shim.Success([]byte(msgStringified))
}

func (t *TNS) getSeed(stub shim.ChaincodeStubInterface, param getSeed) pb.Response {
	var msg string
	var err error
	var seed Seed

	GSBID, fromGSID, viaMID, toMID := param.GSBID, param.FromGSID, param.ViaMID, param.ToMID
	if t.checkGSIDFormat(fromGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(viaMID) == false || t.checkMIDFormat(toMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* If 'toMID' is 'all', do something */
	if strings.Compare(toMID, "all") == 0 {
		resultIterator, err := stub.GetStateByPartialCompositeKey(IndexSeed, []string{PolicyManagerString, SeedString, fromGSID, viaMID})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		defer resultIterator.Close()

		result := "["
		var i, cnt int
		cnt = 0
		for i = 0; resultIterator.HasNext(); i++ {
			response, err := resultIterator.Next()
			if err != nil {
				logger.Error(err)
				continue
			}
			key := response.Key
			_, compositeKey, err := stub.SplitCompositeKey(key)
			if err != nil {
				logger.Error(err)
				continue
			}
			value := response.Value
			err = json.Unmarshal(value, &seed)
			if err != nil {
				logger.Error(err)
				continue
			}
			if seed.HasSeed == false {
				err = errors.New("Not issued seed")
				logger.Error(err)
				continue
			}

			/* Remove a suffix from 'policy.DstMID */
			dstMID := compositeKey[4]
			re := regexp.MustCompile("[.]gs[.][0-9]+")
			loc := re.FindStringIndex(compositeKey[4])
			if loc != nil {
				dstMID = compositeKey[4][:(loc[0] + 3)]
			}

			/* Check relation between 'GSB' and 'dstMID' */
			args := checkRelation{}
			args.GSBID = GSBID
			args.MID = dstMID
			rr, err := t.checkRelation(stub, args)
			if err != nil {
				logger.Error(err)
				continue
			}
			if rr == false {
				err = errors.New("Not permitted relation")
				logger.Error(err)
				continue
			}

			cnt++
			msg = fmt.Sprintf("{\"%s\":\"%s\",\"%s\":%d,\"%s\":%d,\"%s\":\"%s\"}",
				"dstMid", dstMID,
				"timestampIssued", seed.TimestampIssued,
				"timestampExpired", seed.TimestampExpired,
				"seed", hex.EncodeToString(seed.Seed[:]))
			logger.Info("Result: " + msg)
			if cnt > 1 {
				msg = "," + msg
			}
			result += msg
		}
		result += "]"

		if cnt == 0 {
			err = errors.New("Empty result")
			logger.Error(err)
			return shim.Error(err.Error())
		}

		resultStringifiedAsBytes, err := json.Marshal(result)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		resultStringified := string(resultStringifiedAsBytes)

		return shim.Success([]byte(resultStringified))
	}

	/* Check relation between 'GSB' and ('viaMID' or 'toMID') */
	args := checkRelation{}
	args.GSBID = GSBID
	args.MID = viaMID
	result, err := t.checkRelation(stub, args)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		args := checkRelation{}
		args.GSBID = GSBID
		args.MID = toMID
		result, err = t.checkRelation(stub, args)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		if result == false {
			err = errors.New("Not permitted relation")
			logger.Error(err)
			return shim.Error(err.Error())
		}
	}

	/* Check policy for 'fromGSID'-'viaMID'-'toMID' */
	result, err = t.checkPolicyInternal(stub, []string{fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		err = errors.New("Not permitted policy")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Get seed for 'fromGSID'-'viaMID'-'toMID' */
	seedKey, err := stub.CreateCompositeKey(IndexSeed, []string{PolicyManagerString, SeedString, fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	value, err := stub.GetState(seedKey)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if value == nil {
		// err = errors.New("Failed to get policy")
		err = errors.New("Not issued seed")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(value, &seed)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if seed.HasSeed == false {
		err = errors.New("Not issued seed")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	msg = fmt.Sprintf("[{\"%s\":\"%s\",\"%s\":%d,\"%s\":%d,\"%s\":\"%s\"}]",
		"dstMid", toMID,
		"timestampIssued", seed.TimestampIssued,
		"timestampExpired", seed.TimestampExpired,
		"seed", hex.EncodeToString(seed.Seed[:]))
	logger.Info("Result: " + msg)

	msgStringifiedAsBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	msgStringified := string(msgStringifiedAsBytes)

	return shim.Success([]byte(msgStringified))
}

func (t *TNS) deleteSeed(stub shim.ChaincodeStubInterface, param deleteSeed) pb.Response {
	var err error
	var seed Seed

	GSBID, fromGSID, viaMID, toMID := param.GSBID, param.FromGSID, param.ViaMID, param.ToMID
	if t.checkGSIDFormat(fromGSID) == false {
		err = errors.New("Invalid GSID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkGSBIDFormat(GSBID) == false {
		err = errors.New("Invalid GSBID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if t.checkMIDFormat(viaMID) == false || t.checkMIDFormat(toMID) == false {
		err = errors.New("Invalid MID format")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Check relation between 'GSB' and 'viaMID' */
	args := checkRelation{}
	args.GSBID = GSBID
	args.MID = viaMID
	result, err := t.checkRelation(stub, args)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		err = errors.New("Not permitted relation")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Check policy for 'fromGSID'-'viaMID'-'toMID' */
	result, err = t.checkPolicyInternal(stub, []string{fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if result == false {
		err = errors.New("Not permitted policy")
		logger.Error(err)
		return shim.Error(err.Error())
	}

	/* Delete seed for 'fromGSID'-'viaMID'-'toMID' */
	seedKey, err := stub.CreateCompositeKey(IndexSeed, []string{PolicyManagerString, SeedString, fromGSID, viaMID, toMID})
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	value, err := stub.GetState(seedKey)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if value == nil {
		// err = errors.New("Failed to get policy")
		err = errors.New("Not issued seed")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	err = json.Unmarshal(value, &seed)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	if seed.HasSeed == false {
		err = errors.New("Not issued seed")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	seed.HasSeed = false
	seed.TimestampIssued = 0
	seed.TimestampExpired = 0
	tokenSeed := make([]byte, 32)
	copy(seed.Seed[:cap(seed.Seed)], tokenSeed)
	// logger.Info("(new)Policy: " + MarshalToString(seed))
	logger.Info("(new)Seed: " + MarshalToString(seed))
	err = stub.PutState(seedKey, MarshalToByteArray(seed))
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t *TNS) getAllState(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error
	var result string
	var i, j, k int

	result = "["

	/* Policy */
	{
		resultIterator1, err := stub.GetStateByPartialCompositeKey(IndexPolicy, []string{PolicyManagerString, PolicyString})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		defer resultIterator1.Close()

		for i = 0; resultIterator1.HasNext(); i++ {
			response, err := resultIterator1.Next()
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			key := response.Key
			value := response.Value
			_, ckey, err := stub.SplitCompositeKey(key)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			SrcGSID := ckey[2]
			SrcMID := ckey[3]
			DstMID := ckey[4]

			msg := "{\"key\":{\"srcGsid\":\"" + SrcGSID + "\",\"srcMid\":\"" + SrcMID + "\",\"dstMid\":\"" + DstMID + "\"},\"value\":" + string(value) + "}"
			logger.Info(msg)

			if i > 0 {
				result += ","
			}
			result += msg
		}
	}

	/* Seed */
	{
		resultIterator2, err := stub.GetStateByPartialCompositeKey(IndexSeed, []string{PolicyManagerString, SeedString})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		defer resultIterator2.Close()

		for j = 0; resultIterator2.HasNext(); j++ {
			response, err := resultIterator2.Next()
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			key := response.Key
			value := response.Value
			_, ckey, err := stub.SplitCompositeKey(key)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			SrcGSID := ckey[2]
			SrcMID := ckey[3]
			DstMID := ckey[4]

			msg := "{\"key\":{\"srcGsid\":\"" + SrcGSID + "\",\"srcMid\":\"" + SrcMID + "\",\"dstMid\":\"" + DstMID + "\"},\"value\":" + string(value) + "}"
			logger.Info(msg)

			if i > 0 || j > 0 {
				result += ","
			}
			result += msg
		}
	}

	/* Relation between Gate and MID */
	{
		resultIterator3, err := stub.GetStateByPartialCompositeKey(IndexRelationBetweenGateAndMID, []string{PolicyManagerString, RelationBetweenGateAndMIDString})
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		defer resultIterator3.Close()

		for k = 0; resultIterator3.HasNext(); k++ {
			response, err := resultIterator3.Next()
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			key := response.Key
			value := response.Value
			_, ckey, err := stub.SplitCompositeKey(key)
			if err != nil {
				logger.Error(err)
				return shim.Error(err.Error())
			}
			GSB := ckey[2]
			MID := ckey[3]

			msg := "{\"key\":{\"gsb\":\"" + GSB + "\",\"mid\":\"" + MID + "\"},\"value\":" + string(value) + "}"
			logger.Info(msg)

			if i > 0 || j > 0 || k > 0 {
				result += ","
			}
			result += msg
		}
	}

	result += "]"
	logger.Info("Result: " + result)

	resultStringifiedAsBytes, err := json.Marshal(result)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	resultStringified := string(resultStringifiedAsBytes)

	return shim.Success([]byte(resultStringified))
}

func (t *TNS) getAllStateWithPagination(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var pageSize = int32(1)
	var err error
	var result = "["
	var i, j int

	logger.Info("getAllStateWithPagination(" + strings.Join(args, ", ") + ")")

	/* Check arguments */
	if len(args) != 2 {
		err = errors.New("Invalid number of arguments")
		logger.Error(err)
		return shim.Error(err.Error())
	}
	bookmark1 := args[0]
	bookmark2 := args[1]

	resultIterator1, resultMetadata1, err := stub.GetStateByPartialCompositeKeyWithPagination(IndexPolicy, []string{PolicyManagerString}, pageSize, bookmark1)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	defer resultIterator1.Close()
	fetchedRecordsCount1 := resultMetadata1.GetFetchedRecordsCount()
	bookmark1 = resultMetadata1.GetBookmark()
	logger.Info("Fetched records count:", fetchedRecordsCount1)
	logger.Info("Bookmark: " + bookmark1)
	for i = 0; resultIterator1.HasNext(); i++ {
		response, err := resultIterator1.Next()
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		key := response.Key
		value := response.Value
		_, ckey, err := stub.SplitCompositeKey(key)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		SrcGSID := ckey[1]
		SrcMID := ckey[2]
		DstMID := ckey[3]
		msg := "{\"key\":{\"srcGsid\":\"" + SrcGSID + "\",\"srcMid\":\"" + SrcMID + "\",\"dstMid\":\"" + DstMID + "\"},\"value\":" + string(value) + "}"
		// logger.Info(msg)
		if i > 0 {
			result += ","
		}
		result += msg
	}

	resultIterator2, resultMetadata2, err := stub.GetStateByPartialCompositeKeyWithPagination(IndexRelationBetweenGateAndMID, []string{PolicyManagerString}, pageSize, bookmark2)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	defer resultIterator2.Close()
	fetchedRecordsCount2 := resultMetadata2.GetFetchedRecordsCount()
	bookmark2 = resultMetadata2.GetBookmark()
	logger.Info("Fetched records count:", fetchedRecordsCount2)
	logger.Info("Bookmark: " + bookmark2)
	for j = 0; resultIterator2.HasNext(); j++ {
		response, err := resultIterator2.Next()
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		key := response.Key
		value := response.Value
		_, ckey, err := stub.SplitCompositeKey(key)
		if err != nil {
			logger.Error(err)
			return shim.Error(err.Error())
		}
		GSB := ckey[1]
		MID := ckey[2]
		msg := "{\"key\":{\"gsb\":\"" + GSB + "\",\"mid\":\"" + MID + "\"},\"value\":" + string(value) + "}"
		// logger.Info(msg)
		if i > 0 || j > 0 {
			result += ","
		}
		result += msg
	}

	result += "]"
	result = "{\"bookmark1\":\"" + bookmark1 + "\"" +
		",\"bookmark2\":\"" + bookmark2 + "\"" +
		",\"records\":" + result + "}"
	logger.Info("Result: " + result)

	resultStringifiedAsBytes, err := json.Marshal(result)
	if err != nil {
		logger.Error(err)
		return shim.Error(err.Error())
	}
	resultStringified := string(resultStringifiedAsBytes)

	return shim.Success([]byte(resultStringified))
}
