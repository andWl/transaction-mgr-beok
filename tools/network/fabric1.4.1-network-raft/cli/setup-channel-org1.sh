#!/bin/bash

#	define
#
WAIT_TIME=10
TIMESTAMP=`date "+%Y%m%d_%H%M%S" -d "9 hour"`

#	wait func
#
function waittime_4_normalization()
{
	sleep $WAIT_TIME
}

echo 'Create Channel..'

waittime_4_normalization

peer channel create -o orderer.example.com:7050 -c mychannel -f ./channel-artifacts/channel.tx \
--tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

echo 'Done'

echo 'Update Org1 Anchor Peer..'

waittime_4_normalization

peer channel update -o orderer.example.com:7050 -c mychannel -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx  \
--tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

echo 'Done'

echo 'Join Channel..'

waittime_4_normalization

CORE_PEER_ADDRESS=peer0.org1.example.com:7051 peer channel join -b mychannel.block
CORE_PEER_ADDRESS=peer1.org1.example.com:7051 peer channel join -b mychannel.block

echo 'Done'
