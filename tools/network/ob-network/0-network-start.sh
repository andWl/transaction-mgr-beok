#!/bin/bash

docker-compose -f docker-compose.yaml down --volumes --remove-orphans

docker-compose -f docker-compose.yaml up -d
