#!/bin/bash 

########### container info ###########
#peer_name=peer0.org1.example.com
peer_name=orderer.example.com
target_dir=/var/hyperledger/production/orderer/chains/mychannel
#target_dir=/var/hyperledger/production/ledgersData/chains/chains/mychannel
backup_dir=/Users/jake/transaction-mgr/ob_script/block_bak
######################################

max_block_num=-1
min_block_num=0
min_block_num=`expr "${min_block_num}"`
#list=`find /Users/jake/sh/bl`
list=`docker exec $peer_name find $target_dir`

num_check='^[0-9]+$'

ret_value=""

get_max_block_num(){
echo $list
if [ "$list" ]; then
for input in $list
	do
		f_name=`basename "$input"`
		block_num=`expr "${f_name#*_}"`
		echo "test: $block_num"
		if [[ $block_num =~ $num_check ]] ; then
			echo "$block_num is number."
               		if [ $block_num -gt $max_block_num ]; then
                        	max_block_num=$block_num;
               		fi
		fi
		echo "max_block_num: ${max_block_num}"
	done
else
	echo "file not found"
fi
}

# 이 부분을 checksum 을 통해서 hash 값 비교로 변경해야한다 
certify_backup(){
# certify_target 인자받기
local certify_target_num=$1
echo "certify_target_num : $certify_target_num"
list_bak=`find $backup_dir`
echo "list_bak: $list_bak"
if [ "$list_bak" ]; then
for input in $list_bak
	do
		f_name=`basename "$input"`
		block_num=`expr "${f_name#*_}"`
		echo "certify: $block_num"
		if [[ $block_num -ne $certify_target_num ]] ; then
			echo "$block_num is not target."
			ret_value=0
        else
            echo "$block_num is success!!!"
            ret_value=1
            return
		fi
	done
else
	echo "file not found"
fi
}


backup_block(){
    for input_block in $list
        do
            f_name=`basename "$input_block"`
            block_num=`expr "${f_name#*_}"`
            if [[ $block_num =~ $num_check ]] ; then
                if [ $block_num -ne $max_block_num -a $block_num -ne $min_block_num ] ; then
                    echo "block backup target: $input_block;"
                    docker cp -a $peer_name:$input_block $backup_dir

                    #이전명령어가성공했을경우
                    certify_backup $block_num
                    echo "ret_value: $ret_value"
                    if [ $ret_value -ne 1 ] ; then
                        echo "cp command false."

                    else
                       echo "cp command true."
                       docker exec $peer_name rm $input_block
                    fi
                fi
            else
                echo "$block_num is not number."
            fi
            done
}

get_max_block_num
backup_block
