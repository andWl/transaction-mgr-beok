#!/bin/bash

echo
echo 'invoke api test...'
curl -X POST "http://localhost:8081/api/v1/message" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"key\": \"string\", \"value\": \"string\"}"

#sleep 3

echo
echo 'query api test...'
curl -X GET "http://localhost:8081/api/v1/message/string" -H "accept: application/json"
echo
