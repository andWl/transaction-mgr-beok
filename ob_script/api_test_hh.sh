#!/bin/bash

echo -e "\n invoke api test... \n"
curl -X POST "https://obgw.kt.co.kr:8081/api/v1/message" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"key\": \"string\", \"value\": \"string\"}"

#sleep 3

echo -e "\n query api test... \n"
curl -X GET "https://obgw.kt.co.kr:8081/api/v1/message/string" -H "accept: application/json"
echo
