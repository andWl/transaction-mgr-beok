const net = require('net');

const connect = function (servername, port) {
  return new Promise(((resolve, reject) => {
    const result = false;
    var client = net.connect(
      { host: servername, port },
      data => {
        client.end();
        resolve('connected');
      },
    );

    client.on('error', err => {
      reject(err);
    });
  }));
};

exports.connect = connect;
