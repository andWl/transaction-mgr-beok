import logger from '../logger';

const util = require('util');
const path = require('path');
const hfc = require('fabric-client');
hfc.setLogger(logger);

let file = 'network-config%s.yaml';

const env = process.env.TARGET_NETWORK;
if (env) { file = util.format(file, `-${env}`); } else { file = util.format(file, ''); }

hfc.setConfigSetting('network-connection-profile-path', path.join(__dirname, 'connection-profile', file));


if (env === 'anchoring_network' || env === 'anchoring_network_swarm') {
  hfc.setConfigSetting('Org0-connection-profile-path', path.join(__dirname, 'connection-profile', `org0-${env}.yaml`));
  hfc.setConfigSetting('Org1-connection-profile-path', path.join(__dirname, 'connection-profile', `org1-${env}.yaml`));
  hfc.setConfigSetting('Org2-connection-profile-path', path.join(__dirname, 'connection-profile', `org2-${env}.yaml`));
  hfc.setConfigSetting('Org3-connection-profile-path', path.join(__dirname, 'connection-profile', `org3-${env}.yaml`));
  hfc.setConfigSetting('Org4-connection-profile-path', path.join(__dirname, 'connection-profile', `org4-${env}.yaml`));
} else if (env === 'first_network' || env === 'ob_network' || env === 'ktbcp_network' || env === 'raft_network' || env === 'fabric1.4.1_network' || env === 'raft_test_network' || env === 'fabric1.4.1_beok_network') {
  hfc.setConfigSetting('Org1-connection-profile-path', path.join(__dirname, 'connection-profile', `org1-${env}.yaml`));
  hfc.setConfigSetting('Org2-connection-profile-path', path.join(__dirname, 'connection-profile', `org2-${env}.yaml`));
} else if (env === 'basic_network') {
  hfc.setConfigSetting('Org1-connection-profile-path', path.join(__dirname, 'connection-profile', `org1-${env}.yaml`));
} else if (env === 'raft_test_network_docker') {
  hfc.setConfigSetting('Org1-connection-profile-path', path.join(__dirname, 'connection-profile', 'org1-raft_test_network.yaml'));
  hfc.setConfigSetting('Org2-connection-profile-path', path.join(__dirname, 'connection-profile', 'org2-raft_test_network.yaml'));
}

hfc.addConfigFile(path.join(__dirname, 'config.json'));
