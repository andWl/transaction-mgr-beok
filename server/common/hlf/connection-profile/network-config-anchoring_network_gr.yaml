---
#
# The network connection profile provides client applications the information about the target
# blockchain network that are necessary for the applications to interact with it. These are all
# knowledge that must be acquired from out-of-band sources. This file provides such a source.
#
name: "Anchoring Service"

#
# Any properties with an "x-" prefix will be treated as application-specific, exactly like how naming
# in HTTP headers or swagger properties work. The SDK will simply ignore these fields and leave
# them for the applications to process. This is a mechanism for different components of an application
# to exchange information that are not part of the standard schema described below. In particular,
# the "x-type" property with the "hlfv1" value example below is used by Hyperledger Composer to
# determine the type of Fabric networks (v0.6 vs. v1.0) it needs to work with.
#
x-type: "hlfv1"

#
# Describe what the target network is/does.
#
description: "Anchoring Network (Raft)"

#
# Schema version of the content. Used by the SDK to apply the corresponding parsing rules.
#
version: "1.0"

#
# The client section will be added on a per org basis see org1.yaml and org2.yaml
#
#client:

#
# [Optional]. But most apps would have this section so that channel objects can be constructed
# based on the content below. If an app is creating channels, then it likely will not need this
# section.
#
channels:
  # name of the channel
  anchorchannel:
    # Required. list of orderers designated by the application to use for transactions on this
    # channel. This list can be a result of access control ("org1" can only access "ordererA"), or
    # operational decisions to share loads from applications among the orderers.  The values must
    # be "names" of orgs defined under "organizations/peers"
    orderers:
      - orderer0.hh.kt.com
      - orderer1.gr.kt.com
      - orderer2.dg.kt.com
      - orderer3.bs.kt.com
      - orderer4.kj.kt.com

    # Required. list of peers from participating orgs
    peers:
      peer0.hh.kt.com:
        # [Optional]. will this peer be sent transaction proposals for endorsement? The peer must
        # have the chaincode installed. The app can also use this property to decide which peers
        # to send the chaincode install request. Default: true
        endorsingPeer: true

        # [Optional]. will this peer be sent query proposals? The peer must have the chaincode
        # installed. The app can also use this property to decide which peers to send the
        # chaincode install request. Default: true
        chaincodeQuery: true

        # [Optional]. will this peer be sent query proposals that do not require chaincodes, like
        # queryBlock(), queryTransaction(), etc. Default: true
        ledgerQuery: true

        # [Optional]. will this peer be the target of the SDK's listener registration? All peers can
        # produce events but the app typically only needs to connect to one to listen to events.
        # Default: true
        eventSource: true

        discover: true

      peer1.hh.kt.com:
        endorsingPeer: false
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: false

      peer0.gr.kt.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: true

      peer1.gr.kt.com:
        endorsingPeer: false
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: false

      peer0.dg.kt.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: true

      peer1.dg.kt.com:
        endorsingPeer: false
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: false

      peer0.bs.kt.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: true

      peer1.bs.kt.com:
        endorsingPeer: false
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: false

      peer0.kj.kt.com:
        endorsingPeer: true
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: true

      peer1.kj.kt.com:
        endorsingPeer: false
        chaincodeQuery: true
        ledgerQuery: true
        eventSource: true
        discover: false

    # [Optional]. what chaincodes are expected to exist on this channel? The application can use
    # this information to validate that the target peers are in the expected state by comparing
    # this list with the query results of getInstalledChaincodes() and getInstantiatedChaincodes()
    chaincodes:
      # the format follows the "cannonical name" of chaincodes by fabric code
      - anchor_cc:v0

#
# list of participating organizations in this network
#
organizations:
  Org0:
    mspid: Org0MSP

    peers:
      - peer0.hh.kt.com
      - peer1.hh.kt.com

    # [Optional]. Certificate Authorities issue certificates for identification purposes in a Fabric based
    # network. Typically certificates provisioning is done in a separate process outside of the
    # runtime network. Fabric-CA is a special certificate authority that provides a REST APIs for
    # dynamic certificate management (enroll, revoke, re-enroll). The following section is only for
    # Fabric-CA servers.
    certificateAuthorities:
      - ca-org0

    # [Optional]. If the application is going to make requests that are reserved to organization
    # administrators, including creating/updating channels, installing/instantiating chaincodes, it
    # must have access to the admin identity represented by the private key and signing certificate.
    # Both properties can be the PEM string or local path to the PEM file. Note that this is mainly for
    # convenience in development mode, production systems should not expose sensitive information
    # this way. The SDK should allow applications to set the org admin identity via APIs, and only use
    # this route as an alternative when it exists.
    adminPrivateKey:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/hh.kt.com/users/Admin@hh.kt.com/msp/keystore/697494da6b93ab6781f239870f6dca6b48ad1728162ba4922df375d88030ce1f_sk
    signedCert:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/hh.kt.com/users/Admin@hh.kt.com/msp/signcerts/Admin@hh.kt.com-cert.pem

  # the profile will contain public information about organizations other than the one it belongs to.
  # These are necessary information to make transaction lifecycles work, including MSP IDs and
  # peers with a public URL to send transaction proposals. The file will not contain private
  # information reserved for members of the organization, such as admin key and certificate,
  # fabric-ca registrar enroll ID and secret, etc.
  Org1:
    mspid: Org1MSP
    peers:
      - peer0.gr.kt.com
      - peer1.gr.kt.com
    certificateAuthorities:
      - ca-org1
    adminPrivateKey:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/gr.kt.com/users/Admin@gr.kt.com/msp/keystore/cdccbdd1bb4a8903df1e4aadc44d70923e43ef3ed596521cb5a8997f3d557cc3_sk
    signedCert:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/gr.kt.com/users/Admin@gr.kt.com/msp/signcerts/Admin@gr.kt.com-cert.pem

  Org2:
    mspid: Org2MSP
    peers:
      - peer0.dg.kt.com
      - peer1.dg.kt.com
    certificateAuthorities:
      - ca-org2
    adminPrivateKey:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/dg.kt.com/users/Admin@dg.kt.com/msp/keystore/2c9f207cb94b9e68a9016ef584e59d6833854a55b97d47171885f2d6e7f7666d_sk
    signedCert:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/dg.kt.com/users/Admin@dg.kt.com/msp/signcerts/Admin@dg.kt.com-cert.pem

  Org3:
    mspid: Org3MSP
    peers:
      - peer0.bs.kt.com
      - peer1.bs.kt.com
    certificateAuthorities:
      - ca-org3
    adminPrivateKey:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/bs.kt.com/users/Admin@bs.kt.com/msp/keystore/58b43eb91cf2806d449957aa4c4d2e3741ffab324667c8fab69ad68fc4e03208_sk
    signedCert:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/bs.kt.com/users/Admin@bs.kt.com/msp/signcerts/Admin@bs.kt.com-cert.pem

  Org4:
    mspid: Org4MSP
    peers:
      - peer0.kj.kt.com
      - peer1.kj.kt.com
    certificateAuthorities:
      - ca-org4
    adminPrivateKey:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/kj.kt.com/users/Admin@kj.kt.com/msp/keystore/8fd465c30e666072cbf09358760f7d63b01df89073d2e01b7e911aea0ada74f4_sk
    signedCert:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/kj.kt.com/users/Admin@kj.kt.com/msp/signcerts/Admin@kj.kt.com-cert.pem

#
# List of orderers to send transaction and channel create/update requests to. For the time
# being only one orderer is needed. If more than one is defined, which one get used by the
# SDK is implementation specific. Consult each SDK's documentation for its handling of orderers.
#
orderers:
  orderer1.gr.kt.com:
    url: grpcs://218.145.40.146:7050
    grpcOptions:
      ssl-target-name-override: orderer1.gr.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/ordererOrganizations/kt.com/orderers/orderer1.gr.kt.com/tls/ca.crt

  orderer0.hh.kt.com:
    url: grpcs://218.145.40.79:7050

    # these are standard properties defined by the gRPC library
    # they will be passed in as-is to gRPC client constructor
    grpcOptions:
      ssl-target-name-override: orderer0.hh.kt.com

    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/ordererOrganizations/kt.com/orderers/orderer0.hh.kt.com/tls/ca.crt

  orderer2.dg.kt.com:
    url: grpcs://59.23.120.15:7050
    grpcOptions:
      ssl-target-name-override: orderer2.dg.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/ordererOrganizations/kt.com/orderers/orderer2.dg.kt.com/tls/ca.crt

  orderer3.bs.kt.com:
    url: grpcs://59.20.98.15:7050

    # these are standard properties defined by the gRPC library
    # they will be passed sin as-is to gRPC client constructor
    grpcOptions:
      ssl-target-name-override: orderer3.bs.kt.com

    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/ordererOrganizations/kt.com/orderers/orderer3.bs.kt.com/tls/ca.crt

  orderer4.kj.kt.com:
    url: grpcs://59.0.100.175:7050

    # these are standard properties defined by the gRPC library
    # they will be passed sin as-is to gRPC client constructor
    grpcOptions:
      ssl-target-name-override: orderer4.kj.kt.com

    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/ordererOrganizations/kt.com/orderers/orderer4.kj.kt.com/tls/ca.crt

#
# List of peers to send various requests to, including endorsement, query
# and event listener registration.
#
peers:
  peer0.gr.kt.com:
    url: grpcs://218.145.40.134:7051
    grpcOptions:
      ssl-target-name-override: peer0.gr.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/gr.kt.com/peers/peer0.gr.kt.com/tls/ca.crt
    orgName: Org1

  peer1.gr.kt.com:
    url: grpcs://218.145.40.138:7051
    grpcOptions:
      ssl-target-name-override: peer1.gr.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/gr.kt.com/peers/peer1.gr.kt.com/tls/ca.crt
    orgName: Org1

  peer0.hh.kt.com:
    # this URL is used to send endorsement and query requests
    url: grpcs://218.145.40.71:7051

    grpcOptions:
      ssl-target-name-override: peer0.hh.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/hh.kt.com/peers/peer0.hh.kt.com/tls/ca.crt
    orgName: Org0

  peer1.hh.kt.com:
    url: grpcs://218.145.40.75:7051
    grpcOptions:
      ssl-target-name-override: peer1.hh.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/hh.kt.com/peers/peer1.hh.kt.com/tls/ca.crt
    orgName: Org0

  peer0.dg.kt.com:
    url: grpcs://59.23.120.7:7051
    grpcOptions:
      ssl-target-name-override: peer0.dg.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/dg.kt.com/peers/peer0.dg.kt.com/tls/ca.crt
    orgName: Org2

  peer1.dg.kt.com:
    url: grpcs://59.23.120.11:7051
    grpcOptions:
      ssl-target-name-override: peer1.dg.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/dg.kt.com/peers/peer1.dg.kt.com/tls/ca.crt
    orgName: Org2

  peer0.bs.kt.com:
    url: grpcs://59.20.98.7:7051
    grpcOptions:
      ssl-target-name-override: peer0.bs.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/bs.kt.com/peers/peer0.bs.kt.com/tls/ca.crt
    orgName: Org3

  peer1.bs.kt.com:
    url: grpcs://59.20.98.11:7051
    grpcOptions:
      ssl-target-name-override: peer1.bs.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/bs.kt.com/peers/peer1.bs.kt.com/tls/ca.crt
    orgName: Org3

  peer0.kj.kt.com:
    url: grpcs://59.0.100.167:7051
    grpcOptions:
      ssl-target-name-override: peer0.kj.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/kj.kt.com/peers/peer0.kj.kt.com/tls/ca.crt
    orgName: Org4

  peer1.kj.kt.com:
    url: grpcs://59.0.100.171:7051
    grpcOptions:
      ssl-target-name-override: peer1.kj.kt.com
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/kj.kt.com/peers/peer1.kj.kt.com/tls/ca.crt
    orgName: Org4
#
# Fabric-CA is a special kind of Certificate Authority provided by Hyperledger Fabric which allows
# certificate management to be done via REST APIs. Application may choose to use a standard
# Certificate Authority instead of Fabric-CA, in which case this section would not be specified.
#
certificateAuthorities:
  ca.org0.kt.com:
    url: https://218.145.40.85:7054
    # the properties specified under this object are passed to the 'http' client verbatim when
    # making the request to the Fabric-CA server
    httpOptions:
      verify: false
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/hh.kt.com/ca/ca.hh.kt.com-cert.pem

    # Fabric-CA supports dynamic user enrollment via REST APIs. A "root" user, a.k.a registrar, is
    # needed to enroll and invoke new users.
    registrar:
      - enrollId: admin
        enrollSecret: adminpw
    # [Optional] The optional name of the CA.
    caName: ca-org0

  ca.org1.kt.com:
    url: https://218.145.40.154:7054
    httpOptions:
      verify: false
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/gr.kt.com/ca/ca.gr.kt.com-cert.pem
    registrar:
      - enrollId: admin
        enrollSecret: adminpw
    # [Optional] The optional name of the CA.
    caName: ca-org1

  ca.org2.kt.com:
    url: https://59.23.120.23:7054
    httpOptions:
      verify: false
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/dg.kt.com/ca/ca.dg.kt.com-cert.pem
    registrar:
      - enrollId: admin
        enrollSecret: adminpw
    # [Optional] The optional name of the CA.
    caName: ca-org2

  ca.org3.kt.com:
    url: https://59.20.98.23:7054
    httpOptions:
      verify: false
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/bs.kt.com/ca/ca.bs.kt.com-cert.pem
    registrar:
      - enrollId: admin
        enrollSecret: adminpw
    # [Optional] The optional name of the CA.
    caName: ca-org3

  ca.org4.kt.com:
    url: https://59.0.100.183:7054
    httpOptions:
      verify: false
    tlsCACerts:
      path: server/common/hlf/connection-profile/anchoring-network/crypto-config/peerOrganizations/kj.kt.com/ca/ca.kj.kt.com-cert.pem
    registrar:
      - enrollId: admin
        enrollSecret: adminpw
    # [Optional] The optional name of the CA.
    caName: ca-org4
