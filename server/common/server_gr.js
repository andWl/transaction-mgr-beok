import Express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as http from 'http';
import * as os from 'os';
import cookieParser from 'cookie-parser';
import swaggerify from './swagger';
import logger from './logger';


var fs   = require('fs');
var https = require('https');
const hlf = require('../blockchain/hlf-client');
require('./hlf/config.js');

const app = new Express();

//jake
var cert_key=fs.readFileSync('/home/obchain/transaction-mgr/ca/obgw2_kt_co_kr_key.pem')
var cert_cert=fs.readFileSync('/home/obchain/transaction-mgr/ca/obgw2_kt_co_kr_cert.pem')
var cert_chain1=fs.readFileSync('/home/obchain/transaction-mgr/ca/ChainCA1.crt')
var cert_chain2=fs.readFileSync('/home/obchain/transaction-mgr/ca/ChainCA2.crt')
var cert_root=fs.readFileSync('/home/obchain/transaction-mgr/ca/RootCA.crt')

var options = {
 	key : cert_key,
	cert : cert_cert,
	ca:[
		 cert_chain1,
		 cert_chain2,
		 cert_root
	 ],
	passphrase : '2z44c608'
};
//~jake

export default class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.set('appPath', `${root}client`);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(Express.static(`${root}/public`));

    hlf.setClient();
  }

  router(routes) {
    swaggerify(app, routes);
    return this;
  }

  listen(port = process.env.PORT) {
    const welcome = p => () => logger.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname()} on port: ${p}}`);
    // http.createServer(app).listen(port, welcome(port));
    https.createServer(options, app).listen(port, welcome(port));   
// var server = https.createServer(options, app); 
    return app;
  }
}
