import logger from '../../../common/logger';

const transaction = require('../../../blockchain/transaction/transaction');

class TnsService {
  invokeTns(req, res) {
    logger.info(`${this.constructor.name}.invokeTns(${req})`);

    const chainCodeName = req.params.chaincodeName;
    const functionName = req.params.functionName;
    const args = req.body.args;

    // logger.debug('chainCodeName:' + chainCodeName);
    // logger.debug('functionName:' + functionName);

    return Promise.resolve(transaction.invokeChainCode(null,'mychannel', chainCodeName, functionName, args, 'admin', 'Org1'));
  }

  queryTns(req, res) {
    logger.info(`${this.constructor.name}.queryTns(${req})`);

    const chainCodeName = req.params.chaincodeName;
    const functionName = req.params.functionName;
    const args = req.body.args;

    // logger.debug('chainCodeName:' + chainCodeName);
    // logger.debug('functionName:' + functionName);

    return Promise.resolve(transaction.queryChainCode(null,'mychannel', chainCodeName, functionName, args, 'admin', 'Org1'));
  }
}

export default new TnsService();
