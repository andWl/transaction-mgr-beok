/**
 *
 * Copyright 2018 KT All Rights Reserved.
 *
 */

import logger from '../../../common/logger';
import management from '../../../blockchain/management/management';

class ManagementService {
  getNetworkInfo(req, res) {
    logger.info(`${this.constructor.name}.getNetworkInfo()`);
    return Promise.resolve(management.getNetworkInfo());
  }

  getChannels(req, res) {
    logger.info(`${this.constructor.name}.getChannels()`);

    if (req.query.orgName && req.query.peerName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.peerName = req.query.peerName;
      return Promise.resolve(management.getChannels(params));
    } else if (!req.query.orgName && !req.query.peerName) {
      return Promise.resolve(management.getChannels(''));
    }
    return Promise.resolve('Error: Input both orgName and peerName!');
  }

  getChannelsInfo(req, res) {
    logger.info(`${this.constructor.name}.getChannelsInfo()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;

    return Promise.resolve(management.getChannelsInfo(params));
  }

  getBlockByNo(req, res) {
    logger.info(`${this.constructor.name}.getBlockByNo()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.no = req.query.no;

    return Promise.resolve(management.getBlock(params));
  }

  getTxCntByBlock(req, res) {
    logger.info(`${this.constructor.name}.getTxCntByBlock()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.no = req.query.no;

    return Promise.resolve(management.getTxCntbyBlock(params));
  }

  getBlockByHash(req, res) {
    logger.info(`${this.constructor.name}.getBlockByHash()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;
    params.hash = req.query.hash;

    return Promise.resolve(management.getBlockByHash(params));
  }

  getInstalledChaincodes(req, res) {
    logger.info(`${this.constructor.name}.getInstalledChaincodes()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;

    return Promise.resolve(management.getInstalledChaincodes(params));
  }

  getInstantiatedChaincodes(req, res) {
    logger.info(`${this.constructor.name}.getInstantiatedChaincodes()`);
    const params = [];

    params.orgName = req.query.orgName;
    params.channelName = req.query.channelName;
    params.peerName = req.query.peerName;

    return Promise.resolve(management.getInstantiatedChaincodes(params));
  }

  getQueryPeers(req, res) {
    logger.info(`${this.constructor.name}.getQueryPeers()`);
    const peerRole = 'chaincodeQuery';

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(management.getPeers(params, peerRole));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(management.getPeers('', peerRole));
    }
    return Promise.resolve('Error: Input both orgName and channelName!');
  }

  getEndorsingPeers(req, res) {
    logger.info(`${this.constructor.name}.getEndorsingPeers()`);
    const peerRole = 'endorsingPeer';

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(management.getPeers(params, peerRole));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(management.getPeers('', peerRole));
    }
    return Promise.resolve('Error: Input both orgName and channelName!');
  }

  getDiscoveryInfo(req, res) {
    logger.info(`${this.constructor.name}.getDiscoveryInfo()`);

    if (req.query.orgName && req.query.channelName) {
      const params = [];
      params.orgName = req.query.orgName;
      params.channelName = req.query.channelName;
      return Promise.resolve(management.getDiscoveryInfo(params));
    } else if (!req.query.orgName && !req.query.channelName) {
      return Promise.resolve(management.getDiscoveryInfo(''));
    }
    return Promise.resolve('Error: Input both orgName and channelName!');
  }
}

export default new ManagementService();
