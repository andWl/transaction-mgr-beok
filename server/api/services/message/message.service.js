import logger from '../../../common/logger';

const hlf = require('../../../blockchain/hlf-client');
const transaction = require('../../../blockchain/transaction/transaction');

class MessageService {
  _getChannelInfo() {
    const config = hlf.getNetworkBasicInfo();
    const tmp = config.channelsObj[config.channelName[0]].chaincodes[0].split(':');

    const names = {
      channel: config.channelName[0],
      chaincode: tmp[0],
      org: config.orgName[0],
    };

    return names;
  }

  getMessage(req, res) {
    logger.info(`${this.constructor.name}.getMessage(${req})`);
    const args = [];
    args.push(req.params.key);

    const names = this._getChannelInfo();

    return Promise.resolve(transaction.queryChainCodeForMessage(null, names.channel, names.chaincode, 'getMessage', args, 'admin', names.org));
  }

  setMessage(req, res) {
    logger.info(`${this.constructor.name}.setMessage(${req})`);
    const args = [];
    args.push(req.body.key);
    args.push(req.body.value);

    const names = this._getChannelInfo();

    return Promise.resolve(transaction.invokeChainCodeForMessage(null, names.channel, names.chaincode, 'setMessage', args, 'admin', names.org));
  }
}

export default new MessageService();
