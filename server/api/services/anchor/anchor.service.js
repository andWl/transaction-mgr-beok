import logger from '../../../common/logger';

const transaction = require('../../../blockchain/transaction/transaction');

class AnchorService {
  setmessage(req, res) {
    const value = req.body.args;
    // console.log("jake success :)")
    // return Promise.resolve(value);
    return Promise.resolve(transaction.invokeChainCodeForAnchor('anchorchannel', 'anchor_cc', 'setMessage', value, 'Admin'));
  }

  getmessage(req, res) {
    // logger.info(`${this.constructor.name}.byId(${req})`);
    const value = [];
    value.push(req.body.args);
    return Promise.resolve(transaction.queryChainCodeForAnchor('anchorchannel', 'anchor_cc', 'getMessage', value, 'Admin'));
  }

  async healthcheck(req, res) {
    logger.debug(`${this.constructor.name}.byId(${req.url})`);
    const message = 'success';
    return Promise.resolve(message);
  }

  async switch(req, res) {
    logger.debug(`${this.constructor.name}.byId(${req.url})`);
    const message = 'success';
    return Promise.resolve(message);
  }
}
export default new AnchorService();
