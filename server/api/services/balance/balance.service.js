import logger from '../../../common/logger';

const transaction = require('../../../blockchain/transaction/transaction');

class BalanceService {
  getBalance(req, res) {
    const args = [];
    args.push(req.params.id);
    return Promise.resolve(transaction.queryChainCode(null, 'mychannel', 'balance',
      'query', args, 'admin', 'Org1'));
  }

  move(req, res) {
    logger.info(`${this.constructor.name}.byId(${req})`);
    const args = [];

    args.push(req.body.from);
    args.push(req.body.to);
    args.push(req.body.amount.toString());

    return Promise.resolve(transaction.invokeChainCode(null, 'mychannel', 'balance',
      'move', args, 'admin', 'Org1'));
  }

  addUser(req, res) {
    logger.info(`${this.constructor.name}.byId(${req})`);
    const args = [];

    args.push(req.body.name);
    args.push(req.body.balance.toString());

    return Promise.resolve(transaction.invokeChainCode(null, 'mychannel', 'balance',
      'add', args, 'admin', 'Org1'));
  }

  moveOption(req, res) {
    logger.info(`${this.constructor.name}.moveOption(${req})`);
    const args = [];

    args.push(req.body.from);
    args.push(req.body.to);
    args.push(req.body.amount.toString());

    if (req.body.option === '' || req.body.option === 'targeted' || req.body.option === 'non_target') {
      return Promise.resolve(transaction.invokeChainCode(null, 'mychannel', 'balance', 'move', args, 'admin', 'Org1'));
    } else if (req.body.option === 'load_info' || req.body.option === 'discover') {
      return Promise.resolve(transaction.invokeChainCodeDiscover(null, 'mychannel', 'balance', 'move', args, 'admin', 'Org1', req.body.option));
    }
    return Promise.resolve('Error: Invalid option parameter!');
  }
}

export default new BalanceService();
