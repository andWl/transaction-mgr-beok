import managementService from '../../services/management/management.service';

export class Controller {
  getOrganizations(req, res) {
    managementService.getNetworkInfo(req, res)
      .then(r => res.json(r));
  }

  getChannels(req, res) {
    managementService.getChannels(req, res)
      .then(r => res.json(r));
  }

  getChannelsInfo(req, res) {
    managementService
      .getChannelsInfo(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getBlockByNo(req, res) {
    managementService
      .getBlockByNo(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }

      });
  }

  getTxCntByBlock(req, res) {
    managementService
      .getTxCntByBlock(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getBlockByHash(req, res) {
    managementService
      .getBlockByHash(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getInstalledChaincodes(req, res) {
    managementService
      .getInstalledChaincodes(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getInstantiatedChaincodes(req, res) {
    managementService
      .getInstantiatedChaincodes(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getQueryPeers(req, res) {
    managementService
      .getQueryPeers(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getEndorsingPeers(req, res) {
    managementService
      .getEndorsingPeers(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }

  getDiscoveryLoadInfo(req, res) {
    managementService
      .getDiscoveryInfo(req, res)
      .then(r => {
        if (r) {
          res.json(r);
        } else {
          res.status(404)
            .end();
        }
      });
  }
}

export default new Controller();
