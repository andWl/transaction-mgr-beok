import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  .get('/network', controller.getOrganizations)
  .get('/channel', controller.getChannels)
  .get('/channel/info', controller.getChannelsInfo)
  .get('/blocks/no', controller.getBlockByNo)
  .get('/blocks/txcnt', controller.getTxCntByBlock)
  .get('/blocks/hash', controller.getBlockByHash)
  .get('/chaincode/install', controller.getInstalledChaincodes)
  .get('/chaincode/instantiation', controller.getInstantiatedChaincodes)
  .get('/query/peer', controller.getQueryPeers)
  .get('/endorse/peer', controller.getEndorsingPeers)
  .get('/discovery/peer', controller.getDiscoveryLoadInfo);
