import AnchorService from '../../services/anchor/anchor.service';

export class Controller {
  setMessage(req, res) {
    AnchorService
      .setmessage(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }
  getMessage(req, res) {
    AnchorService
      .getmessage(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }
  healthCheck(req, res) {
    AnchorService
      .healthcheck(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }

  switch(req, res) {
    AnchorService
      .switch(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }
}

export default new Controller();
