import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  .post('/oscc', controller.setMessage)
  .post('/query/oscc', controller.getMessage)
  .get('/oscc/healthcheck', controller.healthCheck);
