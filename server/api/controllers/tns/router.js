import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  .post('/:chaincodeName/invoke/:functionName', controller.invokeTns)
  .post('/:chaincodeName/query/:functionName', controller.queryTns);
