import TnsService from '../../services/tns/tns.service';

export class Controller {
  invokeTns(req, res) {
    TnsService
      .invokeTns(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }

  queryTns(req, res) {
    TnsService
      .queryTns(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }
}

export default new Controller();
