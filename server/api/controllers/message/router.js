import * as express from 'express';
import controller from './controller';

export default express
  .Router()
  .get('/:key', controller.getMessage)
  .post('/', controller.setMessage);
