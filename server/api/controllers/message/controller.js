import MessageService from '../../services/message/message.service';

export class Controller {
  getMessage(req, res) {
    MessageService
      .getMessage(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }

  setMessage(req, res) {
    MessageService
      .setMessage(req, res)
      .then(r => {
        if (r) res.json(r);
        else res.status(404).end();
      });
  }
}
export default new Controller();
