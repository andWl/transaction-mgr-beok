const conf = require('dotenv').config();
const util = require('util');
const path = require('path');
const log4js = require('log4js');

const logger = log4js.getLogger('HealthCheckApp');
logger.setLevel('info');

const Shared = require('mmap-object');

const sharedObject = new Shared.Create(path.join(__dirname, '../peer-orderer-default'));

const yaml = require('js-yaml');
const fs = require('fs');
const cron = require('node-cron');

const grpcConnect = require(path.join(__dirname, '/app/grpc-connect.js'));

const nwCfg = new Object();
// nwCfg.channels = new Array();
nwCfg.peers = new Array();
nwCfg.orderers = new Array();


try {
  let file = 'network-config%s.yaml';
  const env = process.env.TARGET_NETWORK;

  if (env) {
    file = util.format(file, `-${env}`);
  } else {
    file = util.format(file, '');
  }

  // var config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '/common/hlf/connection-profile/network-config-raft_test_network.yaml'), 'utf8'));
  var config = yaml.safeLoad(fs.readFileSync(path.join(__dirname, '/common/hlf/connection-profile', file), 'utf8'));
  console.log('Loaded network config from', file, '...');
} catch (e) {
  throw new Error(e);
}

// const channels = Object.keys(config.channels);
// for (const idx in channels) {
//   nwCfg.channels.push(channels[idx]);
// }

const orgs = Object.keys(config.organizations);
const peers = Object.keys(config.peers);
const orderers = Object.keys(config.orderers);
const organizations = Object.keys(config.organizations);

getPeerInfo();
getOrdererInfo();

console.log('Network config : \n', JSON.stringify(nwCfg, null, 2));

const sharedOrg = orgs[0];
let sharedPeer = peers[0];
let sharedOrderer = orderers[0];

sharedObject.checked_org = sharedOrg;
sharedObject.checked_peer = sharedPeer;
sharedObject.checked_orderer = sharedOrderer;


cron.schedule('*/5 * * * * *', async () => {
  logger.info('*** cron scheduler');
  sharedOrderer = await grpcConnectCheck('orderers', sharedOrderer, 'checked_orderer');
  sharedPeer = await grpcConnectCheck('peers', sharedPeer, 'checked_peer');

  // grpcConnectCheck('orderers', sharedOrderer, 'checked_orderer')
  //   .then(result => {
  //     sharedOrderer = result;
  //     console.log('result : ', result);
  //     console.log('sharedOrderer : ', sharedOrderer);
  //   });
  //
  // grpcConnectCheck('peers', sharedPeer, 'checked_peer')
  //   .then(result => {
  //     sharedPeer = result;
  //     console.log('result : ', result);
  //     console.log('sharedPeer : ', sharedPeer);
  //   });
});


function splitURL(url) {
  let tmp = url.split('//');
  tmp = tmp[1].split(':');

  const host = tmp[0];
  const port = tmp[1];

  return [host, port];
}

function getOrgName(peerName) {
  for (const i in orgs) {
    for (const j in config.organizations[orgs[i]].peers) {
      if (peerName === config.organizations[orgs[i]].peers[j]) {
        return orgs[i];
      }
    }
  }
}


function getPeerInfo() {
  for (const idx in peers) {
    const segURL = splitURL(config.peers[peers[idx]].url);
    // console.log(segURL[0], segURL[1]);
    const org = getOrgName(peers[idx]);

    nwCfg.peers.push({ name: peers[idx], address: { url: config.peers[peers[idx]].url, host: segURL[0], port: segURL[1] }, org });
    // console.log('getPeerInfo #', idx, ': ', nwCfg.peers[idx]);
  }
}


function getOrdererInfo() {
  for (const idx in orderers) {
    const segURL = splitURL(config.orderers[orderers[idx]].url);
    // console.log(segURL[0], segURL[1]);

    nwCfg.orderers.push({ name: orderers[idx], address: { url: config.orderers[orderers[idx]].url, host: segURL[0], port: segURL[1] } });
    // console.log('getOrdererInfo #', idx, ': ', nwCfg.orderers[idx]);
  }
}


async function grpcConnectCheck(target, shared_value, object_key) {
  console.log('\n========== grpcConnectCheck fnc : ', target, '\n');
  logger.info(' shared value : ', shared_value);
  for (let idx = 0; idx < nwCfg[target].length; idx++) {
    // console.log('\n idx #', idx);
    console.log(nwCfg[target][idx].name);
    try {
      const result = await grpcConnect.connect(nwCfg[target][idx].address.host, nwCfg[target][idx].address.port);
      console.log(result);

      if (nwCfg[target][idx].name != shared_value) {
        shared_value = nwCfg[target][idx].name;
        sharedObject[object_key] = shared_value;

        logger.info('Changed the shared value...', sharedObject[object_key]);

        if (target === 'peers') {
          sharedObject.checked_org = nwCfg[target][idx].org;
          // console.log(sharedObject.checked_org, '\n');
        }

        // const read_only_sharedObject = new Shared.Open('./app/peer-orderer-default
        // logger.debug('shared value : ' + read_only_sharedObject[object_key]);
        // read_only_sharedObject.close();
      }
      break;
    } catch (error) {
      console.log('failed!');
      logger.debug(error);
    }
  }
  return shared_value;
}
