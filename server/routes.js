import examplesRouter from './api/controllers/examples/router';
import managementRouter from './api/controllers/management/router';
import balanceRouter from './api/controllers/balance/router';
import messageRouter from './api/controllers/message/router';
import anchoringRouter from './api/controllers/anchor/router';
import tnsRouter from './api/controllers/tns/router';


export default function routes(app) {
  app.use('/api/v1/examples', examplesRouter);
  app.use('/api/v1/mgmt', managementRouter);
  app.use('/api/v1/balance', balanceRouter);
  app.use('/api/v1/message', messageRouter);
  app.use('/api/v1/anchor', anchoringRouter);
  app.use('/api/v1/tns', tnsRouter);
}
