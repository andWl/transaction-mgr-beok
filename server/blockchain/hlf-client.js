/**
 *
 * Copyright 2018 KT All Rights Reserved.
 *
 */

const hfc = require('./ob/ClientExtends');
const member = require('./common/member.js');

class HlfClient {
  constructor() {
    this._hfc = new Map();
    this._orgs = {};
    this._orgArr = [];
    this._stateStore = {};
    this._cryptoSuite = {};
    this._channels = [];
    this._chaincodes = new Map();
    this._eventhubs = new Map();
  }

  async setClient() {
    const connectionProfile = hfc.loadFromConfig(hfc.getConfigSetting('network-connection-profile-path'));
    this._loadConfig = connectionProfile;
    this._orgs = connectionProfile._network_config._network_config.organizations;
    this._orgArr = Object.keys(this._orgs);
    this._setClientByOrgs(this._orgs);
    this._setFSKeyValueStore(this._orgs);
    this._checkAdminClient(this._orgs);
    this._setChannelFor10K(this._orgs);
    this._setEventHubFor10K(this._orgs);

    return new Promise(async (resolve, reject) => {
      if (this) {
        resolve(this);
      } else {
        reject(new Error('Failed to Get HFC Client'));
      }
    });
  }

  _setClientByOrgs(orgs) {
    const orgArr = Object.keys(orgs);
    orgArr.forEach(orgName => {
      const client = hfc.loadFromConfig(hfc.getConfigSetting('network-connection-profile-path'));
      client.loadFromConfig(hfc.getConfigSetting(`${orgName}-connection-profile-path`));
      this._hfc.set(orgName, client);
    });
  }

  _setFSKeyValueStore(orgs) {
    const orgArr = Object.keys(orgs);
    orgArr.forEach(async orgName => {
      const orgClient = this._hfc.get(orgName);
      await this._initCredentialStores(orgClient, orgName);
    });
  }

  async _initCredentialStores(client, orgName) {
    const clientConfig = client._network_config.getClientConfig();
    return hfc.newDefaultKeyValueStore(clientConfig.credentialStore)
      .then(store => {
        client.setStateStore(store);
        this._stateStore[orgName] = store;
        return hfc.newCryptoKeyStore(clientConfig.credentialStore.cryptoStore);
      })
      .then(cryptoKeyStore => {
        const cryptoSuite = hfc.newCryptoSuite();
        cryptoSuite.setCryptoKeyStore(cryptoKeyStore);
        client.setCryptoSuite(cryptoSuite);
        this._cryptoSuite[orgName] = cryptoSuite;
        return Promise.resolve();
      });
  }

  _checkAdminClient(orgs) {
    const isAdmin = hfc.getConfigSetting('isAdminClient');

    if (isAdmin === 'true') {
      const orgArr = Object.keys(orgs);
      orgArr.forEach(async orgName => {
        await this._setupAdminAccount(orgName);
      });
    }
  }

  async _setupAdminAccount(orgName) {
    const client = this._hfc.get(orgName);
    await client.initCredentialStores();

    const organizationConfig = client._network_config.getOrganization(orgName, true);
    const mspId = organizationConfig.getMspid();
    const adminKey = organizationConfig.getAdminPrivateKey();
    const adminCert = organizationConfig.getAdminCert();

    await member.createAdmin(client, mspId, adminKey, adminCert);
  }

  _setChannelFor10K(orgs) {
    const orgArr = Object.keys(orgs);
    const orgClient = this._hfc.get(orgArr[0]);
    const channelDefinitions = orgClient._network_config._network_config.channels;
    const channelArr = Object.keys(channelDefinitions);

    channelArr.forEach(async channelName => {
      this._chaincodes.set(channelName, channelDefinitions[channelName].chaincodes);
      await this._channels.push(
        // Refactor-Me
        this._hfc.get(channelDefinitions[channelName].Orgs[0])
          ._network_config.getChannel(channelName));
    });
  }

  _setEventHub(organizations) {
    const orgArr = Object.keys(organizations);
    this._channels.forEach(async channel => {
      await orgArr.forEach(async orgName => {
        const channelEventHubs = channel.getChannelEventHubsForOrg(organizations[orgName].mspid);
        if (channelEventHubs.length !== 0 && !this._eventhubs.get(orgName)) {
          channelEventHubs.forEach(eh => {
            if (!eh.isconnected()) {
              eh.connect();
            }
          });
          this._eventhubs.set(orgName, channelEventHubs);
        }
      });
    });
  }

  _setEventHubFor10K(organizations) {
    const orgArr = Object.keys(organizations);
    this._channels.forEach(async channel => {
      await orgArr.forEach(async orgName => {
        const channelEventHubs = channel.getChannelEventHubsForOrg(organizations[orgName].mspid);
        if (channelEventHubs.length !== 0) {
          channelEventHubs.forEach(eh => {
            if (!eh.isconnected()) {
              eh.connect();
            }
          });
          this._eventhubs.set(channel._name, channelEventHubs);
        }
      });
    });
  }

  getEventHubsForOrg(orgName) {
    return this._eventhubs.get(orgName);
  }

  getAdminClientForOrg(orgName) {
    return new Promise(async (resolve, reject) => {
      /* Refactor-Me (Check Empty) */
      if (this._hfc) {
        const client = this._hfc.get(orgName);
        const organizationConfig = client._network_config.getOrganization(orgName, true);
        const mspId = organizationConfig.getMspid();
        await member.getAdminContext(client, mspId);

        // const eventHubs = this.getEventHubsForOrg(orgName);
        // eventHubs.forEach(eh => {
        //   if (!eh.isconnected()) {
        //     eh.connect();
        //   }
        // });

        resolve(client);
      } else {
        reject(new Error('Failed to Get Admin Client'));
      }
    });
  }

  getNetworkInfo() {
    return this._orgs;
  }

  getNetworkConfig() {
    return this._loadConfig;
  }

  getNetworkBasicInfo() {
    const channels = Object.keys(this._loadConfig._network_config._network_config.channels);
    const channelsObject = this._loadConfig._network_config._network_config.channels;
    const orgs = Object.keys(this._loadConfig._network_config._network_config.organizations);
    const orgsObject = this._loadConfig._network_config._network_config.organizations;
    const peers = Object.keys(this._loadConfig._network_config._network_config.peers);
    const basicConfig = {
      channelName: channels,
      channelsObj: channelsObject,
      orgName: orgs,
      orgsObj: orgsObject,
      peerName: peers,
    };
    return basicConfig;
  }
}

const hlfClient = new HlfClient();
module.exports = hlfClient;
