/* eslint-disable prefer-destructuring */
/**
 *
 * Copyright 2018 KT All Rights Reserved
 *
 */
import logger from '../../common/logger';

const util = require('util');
const hlf = require('../hlf-client');

class MgmtClient {
  async getNetworkInfo() {
    return Promise.resolve(hlf.getNetworkInfo());
  }

  async getChannels(params) {
    if (params) {
      this._peerName = params.peerName;
      this._orgName = params.orgName;
    } else {
      const config = hlf.getNetworkBasicInfo();
      this._orgName = config.orgName[0];
      this._peerName = config.orgsObj[this._orgName].peers[0];
    }

    try {
      const client = await hlf.getAdminClientForOrg(this._orgName);
      logger.debug(`Successfully got the fabric client for the organization ${this._orgName}`);

      const response = await client.queryChannels(this._peerName);
      if (response) {
        logger.debug('<<< channels >>>');
        const channelNames = [];
        for (let i = 0; i < response.channels.length; i++) {
          channelNames.push(`channel id: ${response.channels[i].channel_id}`);
        }
        logger.debug(channelNames);
        return response;
      }
      logger.error('response_payloads is null');
      return 'response_payloads is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getChannelsInfo(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);
      const channel = client.getChannel(params.channelName);
      if (!channel) {
        logger.debug('Channel %s was not defined in the connection profile', params.channelName);
      }

      const responsePayload = await channel.queryInfo(params.peerName);
      if (responsePayload) {
        logger.debug('Successfully got queryInfo from "%s"', params.peerName);
        logger.debug(responsePayload);
        return responsePayload;
      }
      logger.error('response_payload is null');
      return 'response_payload is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getBlock(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);
      const channel = client.getChannel(params.channelName);
      if (!channel) {
        logger.debug('Channel %s was not defined in the connection profile', params.channelName);
      }

      const responsePayload = await channel.queryBlock(params.no, params.peerName);
      if (responsePayload) {
        logger.debug(responsePayload);
        return responsePayload;
      }
      logger.error('responsePayload is null');
      return 'responsePayload is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getTxCntbyBlock(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);
      const channel = client.getChannel(params.channelName);
      if (!channel) {
        logger.debug('Channel %s was not defined in the connection profile', params.channelName);
      }

      const responsePayload = await channel.queryBlock(params.no, params.peerName);
      responsePayload.transactionCnts = responsePayload.data.data.length;
      if (responsePayload.transactionCnts) {
        logger.debug(responsePayload.transactionCnts);
        return responsePayload.transactionCnts;
      }
      logger.error('transaction count is null');
      return 'transaction count is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getBlockByHash(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);
      const channel = client.getChannel(params.channelName);
      if (!channel) {
        logger.debug('Channel %s was not defined in the connection profile', params.channelName);
      }

      const responsePayload = await channel.queryBlockByHash(Buffer.from(params.hash, 'hex'), params.peerName);
      if (responsePayload) {
        logger.debug(responsePayload);
        return responsePayload;
      }
      logger.error('response_payload is null');
      return 'response_payload is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getInstalledChaincodes(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);

      let response = null;
      response = await client.queryInstalledChaincodes(params.peerName, true);

      if (response) {
        logger.debug('<<< Installed Chaincodes >>>');

        const details = [];
        for (let i = 0; i < response.chaincodes.length; i++) {
          logger.debug(`name: ${response.chaincodes[i].name}, version: ${
            response.chaincodes[i].version}, path: ${response.chaincodes[i].path}`,
          );
          details.push(`name: ${response.chaincodes[i].name}, version: ${
            response.chaincodes[i].version}, path: ${response.chaincodes[i].path}`,
          );
        }
        return details;
      }
      logger.error('response is null');
      return 'response is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getInstantiatedChaincodes(params) {
    try {
      const client = await hlf.getAdminClientForOrg(params.orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', params.orgName);

      let response = null;
      const channel = client.getChannel(params.channelName);
      if (!channel) {
        logger.error('Channel %s was not defined in the connection profile', params.channelName);
      }
      response = await channel.queryInstantiatedChaincodes(params.peerName, true);

      if (response) {
        logger.debug('<<< Instantiated Chaincodes >>>');

        const details = [];
        for (let i = 0; i < response.chaincodes.length; i++) {
          logger.debug(`name: ${response.chaincodes[i].name}, version: ${
            response.chaincodes[i].version}, path: ${response.chaincodes[i].path}`,
          );
          details.push(`name: ${response.chaincodes[i].name}, version: ${
            response.chaincodes[i].version}, path: ${response.chaincodes[i].path}`,
          );
        }
        return details;
      }
      logger.error('response is null');
      return 'response is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getPeers(params, role) {
    if (params) {
      this._channelName = params.channelName;
      this._orgName = params.orgName;
    } else {
      const config = hlf.getNetworkBasicInfo();
      this._channelName = config.channelName[0];
      this._orgName = config.orgName[0];
    }

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(this._orgName);
      logger.debug(`Successfully got the fabric client for the organization ${this._orgName}`);

      const channel = client.getChannel(this._channelName);
      if (!channel) {
        const message = util.format('Channel "%s" was not defined in the connection profile', this._channelName);
        logger.error(message);
        throw new Error(message);
      }

      const peersObj = channel._getTargets('', role);
      if (peersObj) {
        logger.debug('Successfully got peer list');

        const peers = [];
        for (const i in peersObj) {
          peers.push(peersObj[i]._name);
        }
        return peers;
      }
      logger.error('response is null');
      return 'response is null';
    } catch (error) {
      logger.error(`Failed due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async getDiscoveryInfo(params) {
    if (params) {
      this._channelName = params.channelName;
      this._orgName = params.orgName;
    } else {
      const config = hlf.getNetworkBasicInfo();
      this._channelName = config.channelName[0];
      this._orgName = config.orgName[0];
    }

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(this._orgName);
      logger.debug(`Successfully got the fabric client for the organization ${this._orgName}`);
      const channel = client.getChannel(this._channelName);
      if (!channel) {
        const message = util.format('Channel "%s" was not defined in the connection profile', this._channelName);
        logger.error(message);
        throw new Error(message);
      }

      const response = await channel.initialize({
        discover: true,
        asLocalhost: true,
      });

      if (response.peers_by_org) {
        logger.debug('Successfully got discovery info');
        return response.peers_by_org;
      }
      logger.error('response is null');
      return 'response is null';
    } catch (error) {
      logger.error(`Failed to service discovery due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }
}

const mgmtClient = new MgmtClient();
module.exports = mgmtClient;
