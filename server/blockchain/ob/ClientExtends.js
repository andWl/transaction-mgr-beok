/*
 Copyright 2018 KT All Rights Reserved.
*/
/* eslint-disable */
import logger from '../../common/logger';

const Client = require('fabric-client/lib/Client');
const ChannelExtends = require('./ChannelExtends');

const util = require('util');
const fs = require('fs-extra');
const path = require('path');
const yaml = require('js-yaml');

const ClientExtends = class extends Client {
  constructor() {
    super();
    this._ob_channels = new Map();
  }

  static loadFromConfig(config) {
    const client = new ClientExtends();
    // SDK에서 internal function 으로 정의 되어 있으므로 COPY 불가피. any idea.?
    client._network_config = _getNetworkConfig(config, client);
    if (client._network_config.hasClient()) {
      client._setAdminFromConfig();
    }

    return client;
  }

  getOBChannel(name, throwError = true) {
    let channel;
    // Refactor-Me
    let obChannel = new ChannelExtends(name, this);

    if (name) {
      channel = this._ob_channels.get(name);
    } else if (this._ob_channels.size > 0) {
      // not sure it's deterministic which channel would be returned if more than 1.
      channel = this._ob_channels.values().next().value;
    }

    if (channel) {
      return channel;
    } else {
      // maybe it is defined in the network config
      if (this._network_config) {
        if (!name) {
          const channel_names = Object.keys(this._network_config._network_config.channels);
          name = channel_names[0];
        }
        if (name) {
          channel = this._network_config.getChannel(name);
          // Refactor-Me
          obChannel._channel_peers = channel._channel_peers;
          obChannel._orderers = channel._orderers;
        }
      }
      if (obChannel) {
        this._ob_channels.set(name, obChannel);
        return obChannel;
      }

      logger.error(`Channel not found for name ${name}`);

      if (throwError) {
        throw new Error(`Channel not found for name ${name}.`);
      } else {
        return null;
      }
    }
  }
};

function _getNetworkConfig(config, client) {
  let network_config = null;
  let network_data = null;
  if (typeof config === 'string') {
    const config_loc = path.resolve(config);
    logger.debug('%s - looking at absolute path of ==>%s<==', '_getNetworkConfig', config_loc);
    const file_data = fs.readFileSync(config_loc);
    const file_ext = path.extname(config_loc);
    // maybe the file is yaml else has to be JSON
    if (file_ext.indexOf('y') > -1) {
      network_data = yaml.safeLoad(file_data);
    } else {
      network_data = JSON.parse(file_data);
    }
  } else {
    network_data = config;
  }

  let error_msg = null;
  if (network_data) {
    if (network_data.version) {
      const parsing = ClientExtends.getConfigSetting('network-config-schema');
      if (parsing) {
        const pieces = network_data.version.toString().split('.');
        const version = `${pieces[0]}.${pieces[1]}`;
        if (parsing[version]) {
          const NetworkConfig = require(`fabric-client/lib/${parsing[version]}`);
          network_config = new NetworkConfig(network_data, client);
        } else {
          error_msg = 'network configuration has an unknown "version"';
        }
      } else {
        error_msg = 'missing "network-config-schema" configuration setting';
      }
    } else {
      error_msg = '"version" is missing';
    }
  } else {
    error_msg = 'missing configuration data';
  }

  if (error_msg) {
    throw new Error(util.format('Invalid network configuration due to %s', error_msg));
  }

  return network_config;
}

module.exports = ClientExtends;
