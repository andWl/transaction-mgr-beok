/*
 Copyright 2018 KT All Rights Reserved.
*/

/* eslint-disable */
import logger from '../../common/logger';

const baseChannel = require('fabric-client/lib/Channel');

const utils = require('fabric-client/lib/utils.js');
const clientUtils = require('./ClientUtilsExtends');
const Constants = require('fabric-client/lib/Constants.js');

const grpc = require('grpc');
const Long = require('long');
const path = require('path');

const _ccProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/chaincode.proto')).protos;
const _transProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/transaction.proto')).protos;
const _proposalProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/peer/proposal.proto')).protos;
const _commonProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/common/common.proto')).common;

const _identityProto = grpc.load(path.join(__dirname, '/protos/msp/identities.proto')).msp;
const _gossipProto = grpc.load(path.join(__dirname, '/protos/gossip/message.proto')).gossip;
const _discoveryProto = grpc.load(path.join(__dirname, '/protos/discovery/protocol.proto')).discovery;

const ChannelExtends = class extends baseChannel {
    constructor(name, clientContext) {
        super(name, clientContext);
    }

    sendTransactionProposalOB(request, timeout) {
        logger.debug('sendTransactionProposal - start');

        if (!request) {
            throw new Error('Missing request object for this transaction proposal');
        }
        request.targets = this._getTargets(request.targets, Constants.NetworkConfig.ENDORSING_PEER_ROLE);

        return ChannelExtends.sendTransactionProposalOB(request, this._name, this._clientContext, timeout);
    }

    static sendTransactionProposalOB(request, channelId, clientContext, timeout) {
        // Verify that a Peer has been added
        var errorMsg = clientUtils.checkProposalRequest(request);

        if (errorMsg) {
            // do nothing so we skip the rest of the checks
        } else if (!request.args) {
            // args is not optional because we need for transaction to execute
            errorMsg = 'Missing "args" in Transaction proposal request';
        } else if (!request.targets || request.targets.length < 1) {
            errorMsg = 'Missing peer objects in Transaction proposal';
        }

        if (errorMsg) {
            logger.error('sendTransactionProposal error ' + errorMsg);
            throw new Error(errorMsg);
        }

        var args = [];
        args.push(Buffer.from(request.fcn ? request.fcn : 'invoke', 'utf8'));
        logger.debug('sendTransactionProposal - adding function arg:%s', request.fcn ? request.fcn : 'invoke');

        for (let i = 0; i < request.args.length; i++) {
            logger.debug('sendTransactionProposal - adding arg:%s', request.args[i]);
            args.push(Buffer.from(request.args[i], 'utf8'));
        }
        //special case to support the bytes argument of the query by hash
        if (request.argbytes) {
            logger.debug('sendTransactionProposal - adding the argument :: argbytes');
            args.push(request.argbytes);
        } else {
            logger.debug('sendTransactionProposal - not adding the argument :: argbytes');
        }
        let invokeSpec = {
            type: _ccProto.ChaincodeSpec.Type.GOLANG,
            chaincode_id: {
                name: request.chaincodeId
            },
            input: {
                args: args
            }
        };

        var proposal, header;
        var signer = null;
        if (request.signer) {
            signer = request.signer;
        } else {
            signer = clientContext._getSigningIdentity(request.txId.isAdmin());
        }
        var channelHeader = clientUtils.buildChannelHeader(
            _commonProto.HeaderType.ENDORSER_TRANSACTION,
            channelId,
            request.txId.getTransactionID(),
            null,
            request.chaincodeId,
            clientUtils.buildCurrentTimestamp(),
            request.targets[0].getClientCertHash()
        );
        header = clientUtils.buildHeaderOB(signer, channelHeader, request.txId.getNonce());
        proposal = clientUtils.buildProposal(invokeSpec, header, request.transientMap);
        let signed_proposal = clientUtils.signProposalOB(signer, proposal);

        return clientUtils.sendPeersProposal(request.targets, signed_proposal, timeout)
            .then(
                function (responses) {
                    return Promise.resolve([responses, proposal]);
                }
            ).catch(
                function (err) {
                    logger.error('Failed Proposal. Error: %s', err.stack ? err.stack : err);
                    return Promise.reject(err);
                }
            );
    }

    sendTransactionOB(request) {
        logger.debug('sendTransaction - start :: channel %s', this);
        var errorMsg = null;

        if (request) {
            // Verify that data is being passed in
            if (!request.proposalResponses) {
                errorMsg = 'Missing "proposalResponses" parameter in transaction request';
            }
            if (!request.proposal) {
                errorMsg = 'Missing "proposal" parameter in transaction request';
            }
        } else {
            errorMsg = 'Missing input request object on the transaction request';
        }

        if (errorMsg) {
            logger.error('sendTransaction error ' + errorMsg);
            throw new Error(errorMsg);
        }

        let proposalResponses = request.proposalResponses;
        let chaincodeProposal = request.proposal;

        var endorsements = [];
        let proposalResponse = proposalResponses;
        if (Array.isArray(proposalResponses)) {
            for (let i = 0; i < proposalResponses.length; i++) {
                // make sure only take the valid responses to set on the consolidated response object
                // to use in the transaction object
                if (proposalResponses[i].response && proposalResponses[i].response.status === 200) {
                    proposalResponse = proposalResponses[i];
                    endorsements.push(proposalResponse.endorsement);
                }
            }
        } else {
            if (proposalResponse && proposalResponse.response && proposalResponse.response.status === 200) {
                endorsements.push(proposalResponse.endorsement);
            }
        }

        if (endorsements.length < 1) {
            logger.error('sendTransaction - no valid endorsements found');
            throw new Error('no valid endorsements found');
        }

        // verify that we have an orderer configured
        var orderer = this._clientContext.getTargetOrderer(request.orderer, this._orderers, this._name);

        var use_admin_signer = false;
        if (request.txId) {
            use_admin_signer = request.txId.isAdmin();
        }

        let header = _commonProto.Header.decode(chaincodeProposal.getHeader());

        var chaincodeEndorsedAction = new _transProto.ChaincodeEndorsedAction();
        chaincodeEndorsedAction.setProposalResponsePayload(proposalResponse.payload);
        chaincodeEndorsedAction.setEndorsements(endorsements);

        var chaincodeActionPayload = new _transProto.ChaincodeActionPayload();
        chaincodeActionPayload.setAction(chaincodeEndorsedAction);

        // the TransientMap field inside the original proposal payload is only meant for the
        // endorsers to use from inside the chaincode. This must be taken out before sending
        // to the orderer, otherwise the transaction will be rejected by the validators when
        // it compares the proposal hash calculated by the endorsers and returned in the
        // proposal response, which was calculated without the TransientMap
        var originalChaincodeProposalPayload = _proposalProto.ChaincodeProposalPayload.decode(chaincodeProposal.payload);
        var chaincodeProposalPayloadNoTrans = new _proposalProto.ChaincodeProposalPayload();
        chaincodeProposalPayloadNoTrans.setInput(originalChaincodeProposalPayload.input); // only set the input field, skipping the TransientMap
        chaincodeActionPayload.setChaincodeProposalPayload(chaincodeProposalPayloadNoTrans.toBuffer());

        var transactionAction = new _transProto.TransactionAction();
        transactionAction.setHeader(header.getSignatureHeader());
        transactionAction.setPayload(chaincodeActionPayload.toBuffer());

        var actions = [];
        actions.push(transactionAction);

        var transaction = new _transProto.Transaction();
        transaction.setActions(actions);


        var payload = new _commonProto.Payload();
        payload.setHeader(header);
        payload.setData(transaction.toBuffer());

        let payload_bytes = payload.toBuffer();

        var signer = this._clientContext._getSigningIdentity(use_admin_signer);

        let signature = "0";

        // building manually or will get protobuf errors on send
        var envelope = {
            signature: signature,
            payload: payload_bytes
        };

        return orderer.sendBroadcast(envelope);
        //orderer.sendBroadcastOB(envelope);
    }

    async initializeOB(request) {
        const method = 'initialize';
        logger.debug('%s - start', method);

        let endorsement_handler_path = null;
        let commit_handler_path = null;

        if (request) {
            if (request.configUpate) {
                logger.debug('%s - have a configupdate', method);
                this.loadConfigUpdate(request.configUpate);

                return true;
            } else {
                if (typeof request.discover !== 'undefined') {
                    if (typeof request.discover === 'boolean') {
                        logger.debug('%s - user requested discover %s', method, request.discover);
                        this._use_discovery = request.discover;
                    } else {
                        throw new Error('Request parameter "discover" must be boolean');
                    }
                }
                if (request.endorsementHandler) {
                    logger.debug('%s - user requested endorsementHandler %s', method, request.endorsementHandler);
                    endorsement_handler_path = request.endorsementHandler;
                }
                if (request.commitHandler) {
                    logger.debug('%s - user requested commitHandler %s', method, request.commitHandler);
                    commit_handler_path = request.commitHandler;
                }
            }
        }

        // setup the endorsement handler
        if (!endorsement_handler_path && this._use_discovery) {
            endorsement_handler_path = utils.getConfigSetting('endorsement-handler');
            logger.debug('%s - using config setting for endorsement handler ::%s', method, endorsement_handler_path);
        }
        if (endorsement_handler_path) {
            this._endorsement_handler = require(endorsement_handler_path).create(this);
            await this._endorsement_handler.initialize();
        }

        // setup the commit handler
        if (!commit_handler_path) {
            commit_handler_path = utils.getConfigSetting('commit-handler');
            logger.debug('%s - using config setting for commit handler ::%s', method, commit_handler_path);
        }
        if (commit_handler_path) {
            this._commit_handler = require(commit_handler_path).create(this);
            await this._commit_handler.initialize();
        }

        const results = await this._initializeOB(request);

        return results;
    }

    async _initializeOB(request) {
        const method = '_initialize';
        logger.debug('%s - start', method);

        this._discovery_results = null;
        this._last_discover_timestamp = null;
        this._last_refresh_request = Object.assign({}, request);
        let target_peer = this._discovery_peer;

        if (request && request.target) {
            target_peer = request.target;
        }

        if (this._use_discovery) {
            logger.debug('%s - starting discovery', method);
            target_peer = this._getTargetForDiscovery(target_peer);
            if (!target_peer) {
                throw new Error('No target provided for discovery services');
            }

            try {
                const discover_request = {
                    target: target_peer,
                    config: true
                };

                const discovery_results = await this._discoverOB(discover_request);
                if (discovery_results) {
                    if (discovery_results.msps) {
                        this._buildDiscoveryMSPs(discovery_results);
                    } else {
                        throw Error('No MSP information found');
                    }
                    if (discovery_results.orderers) {
                        this._buildDiscoveryOrderers(discovery_results, discovery_results.msps, request);
                    }
                    if (discovery_results.peers_by_org) {
                        this._buildDiscoveryPeers(discovery_results, discovery_results.msps, request);
                    }
                }

                discovery_results.endorsement_plans = [];

                const interests = [];
                const plan_ids = [];
                this._discovery_interests.forEach((interest, plan_id) =>{
                    logger.debug('%s - have interest of:%s', method, plan_id);
                    plan_ids.push(plan_id);
                    interests.push(interest);
                });

                for(const i in plan_ids) {
                    const plan_id = plan_ids[i];
                    const interest = interests[i];

                    const discover_request = {
                        target: target_peer,
                        interests: [interest]
                    };

                    let discover_interest_results = null;
                    try {
                        discover_interest_results = await this._discoverOB(discover_request);
                    } catch(error) {
                        logger.error('Not able to get an endorsement plan for %s', plan_id);
                    }

                    if(discover_interest_results && discover_interest_results.endorsement_plans && discover_interest_results.endorsement_plans[0]) {
                        const plan = this._buildDiscoveryEndorsementPlan(discover_interest_results, plan_id, discovery_results.msps, request);
                        discovery_results.endorsement_plans.push(plan);
                        logger.debug('Added an endorsement plan for %s', plan_id);
                    } else {
                        logger.debug('Not adding an endorsement plan for %s', plan_id);
                    }
                }

                discovery_results.timestamp = Date.now();
                this._discovery_results = discovery_results;
                this._discovery_peer = target_peer;
                this._last_discover_timestamp = discovery_results.timestamp;

                return discovery_results;
            } catch(error) {
                logger.error(error);
                throw Error('Failed to discover ::'+ error.toString());
            }
        } else {
            target_peer = this._getFirstAvailableTarget(target_peer);
            if (!target_peer) {
                throw new Error('No target provided for non-discovery initialization');
            }
            const config_envelope = await this.getChannelConfig(target_peer);
            logger.debug('initialize - got config envelope from getChannelConfig :: %j', config_envelope);
            const config_items = this.loadConfigEnvelope(config_envelope);

            return config_items;

        }
    }

    async _discoverOB(request) {
        const method = 'discover';
        const self = this;
        logger.debug('%s - start', method);
        const results = {};
        if (!request) {
            request = {};
        }

        let useAdmin = true; //default
        if(typeof request.useAdmin === 'boolean') {
            useAdmin = request.useAdmin;
        }
        const target_peer = this._getTargetForDiscovery(request.target);
        const signer = this._clientContext._getSigningIdentity(useAdmin); //use the admin if assigned
        const discovery_request = new _discoveryProto.Request();

        const authentication = new _discoveryProto.AuthInfo();
        authentication.setClientIdentity(signer.serialize());
        const cert_hash = this._clientContext.getClientCertHash(true);
        if (cert_hash) {
            authentication.setClientTlsCertHash(cert_hash);
        }
        discovery_request.setAuthentication(authentication);

        // be sure to add all entries to this array before setting into the
        // grpc object
        const queries = [];

        // if doing local it will be index 0 of the results
        if (request.local) {
            const query = new _discoveryProto.Query();
            queries.push(query);

            const local_peers = new _discoveryProto.LocalPeerQuery();
            query.setLocalPeers(local_peers);
            logger.debug('%s - adding local peers query', method);
        }

        if (request.config) {
            let query = new _discoveryProto.Query();
            queries.push(query);
            query.setChannel(this.getName());

            const config_query = new _discoveryProto.ConfigQuery();
            query.setConfigQuery(config_query);
            logger.debug('%s - adding config query', method);

            query = new _discoveryProto.Query();
            queries.push(query);
            query.setChannel(this.getName());

            const peer_query = new _discoveryProto.PeerMembershipQuery();
            query.setPeerQuery(peer_query);
            logger.debug('%s - adding channel peers query', method);
        }

        // add a chaincode query to get endorsement plans
        if (request.interests && request.interests.length > 0) {
            const query = new _discoveryProto.Query();
            queries.push(query);
            query.setChannel(this.getName());

            const interests = [];
            for(const interest of request.interests) {
                const proto_interest = this._buildProtoChaincodeInterest(interest);
                interests.push(proto_interest);
            }

            const cc_query = new _discoveryProto.ChaincodeQuery();
            cc_query.setInterests(interests);
            query.setCcQuery(cc_query);
            logger.debug('%s - adding chaincodes/collection query', method);
        }

        // be sure to set the array after completely building it
        discovery_request.setQueries(queries);

        // build up the outbound request object
        const signed_request = clientUtils.toEnvelope(clientUtils.signProposal(signer, discovery_request));

        const response = await target_peer.sendDiscovery(signed_request);
        logger.debug('%s - processing discovery response', method);
        if (response && response.results) {
            let error_msg = null;
            logger.debug('%s - parse discovery response', method);
            for (const index in response.results) {
                const result = response.results[index];
                if (!result) {
                    error_msg = 'Discover results are missing';
                    break;
                } else if (result.result === 'error') {
                    logger.error('Channel:%s received discovery error:%s', self.getName(), result.error.content);
                    error_msg = result.error.content;
                    break;
                } else {
                    logger.debug('%s - process results', method);
                    if (result.config_result) {
                        const config = self._processDiscoveryConfigResults(result.config_result);
                        results.msps = config.msps;
                        results.orderers = config.orderers;
                    }
                    if (result.members) {
                        if (request.local && index === 0) {
                            results.local_peers = self._processDiscoveryMembershipResultsOB(result.members);
                        } else {
                            results.peers_by_org = self._processDiscoveryMembershipResultsOB(result.members);
                        }
                    }
                    if (result.cc_query_res) {
                        results.endorsement_plans = self._processDiscoveryChaincodeResultsOB(result.cc_query_res);
                    }
                    logger.debug('%s - completed processing results', method);
                }
            }

            if (error_msg) {
                throw Error('Channel:' + self.getName() + ' Discovery error:' + error_msg);
            } else {

                return results;
            }
        } else {
            throw new Error('Discovery has failed to return results');
        }
    }

    _processDiscoveryChaincodeResultsOB(q_chaincodes) {
        const method = '_processDiscoveryChaincodeResults';
        logger.debug('%s - start', method);
        const endorsement_plans = [];
        if (q_chaincodes && q_chaincodes.content) {
            if (Array.isArray(q_chaincodes.content)) {
                for (const index in q_chaincodes.content) {
                    const q_endors_desc = q_chaincodes.content[index];
                    const endorsement_plan = {};
                    endorsement_plan.chaincode = q_endors_desc.chaincode;
                    endorsement_plans.push(endorsement_plan);

                    // GROUPS
                    endorsement_plan.groups = {};
                    for (const group_name in q_endors_desc.endorsers_by_groups) {
                        logger.debug('%s - found group: %s', method, group_name);
                        const group = {};
                        group.peers = this._processPeersOB(q_endors_desc.endorsers_by_groups[group_name].peers);
                        //all done with this group
                        endorsement_plan.groups[group_name] = group;
                    }

                    // LAYOUTS
                    endorsement_plan.layouts = [];
                    for (const index in q_endors_desc.layouts) {
                        const q_layout = q_endors_desc.layouts[index];
                        const layout = {};
                        for (const group_name in q_layout.quantities_by_group) {
                            layout[group_name] = q_layout.quantities_by_group[group_name];
                        }
                        logger.debug('%s - layout :%j', method, layout);
                        endorsement_plan.layouts.push(layout);
                    }
                }
            }
        }

        return endorsement_plans;
    }

    _processDiscoveryMembershipResultsOB(q_members) {
        const method = '_processDiscoveryChannelMembershipResults';
        logger.debug('%s - start', method);
        const peers_by_org = {};
        if (q_members && q_members.peers_by_org) {
            for (const mspid in q_members.peers_by_org) {
                logger.debug('%s - found org:%s', method, mspid);
                peers_by_org[mspid] = {};
                peers_by_org[mspid].peers = this._processPeersOB(q_members.peers_by_org[mspid].peers);
            }
        }
        return peers_by_org;
    }

    _processPeersOB(q_peers) {
        const method = '_processPeersOB';
        const peers = [];
        q_peers.forEach((q_peer) => {
            const peer = {};
            // IDENTITY
            const q_identity = _identityProto.SerializedIdentity.decode(q_peer.identity);
            peer.mspid = q_identity.mspid;

            // MEMBERSHIP
            const q_membership_message = _gossipProto.GossipMessage.decode(q_peer.membership_info.payload);
            peer.endpoint = q_membership_message.alive_msg.membership.endpoint;
            logger.debug('%s - found peer :%s', method, peer.endpoint);

            // STATE
            if (q_peer.state_info) {
                const message_s = _gossipProto.GossipMessage.decode(q_peer.state_info.payload);
                if (message_s && message_s.state_info && message_s.state_info.properties && message_s.state_info.properties.ledger_height) {
                    peer.ledger_height = Long.fromValue(message_s.state_info.properties.ledger_height);
                } else {
                    logger.debug('%s - did not find ledger_height', method);
                    peer.ledger_height = Long.fromValue(0);
                }
                logger.debug('%s - found ledger_height :%s', method, peer.ledger_height);

                if(message_s && message_s.state_info && message_s.state_info.properties && message_s.state_info.properties.load_info) {
                    peer.load_info = message_s.state_info.properties.load_info;
                } else {
                    logger.debug('%s - did not find ledger_height', method);
                    peer.load_info = 0;
                }
                logger.debug('%s - found ledger_height :%s', method, peer.load_info);
                peer.chaincodes = [];
                for (const index in message_s.state_info.properties.chaincodes) {
                    const q_chaincode = message_s.state_info.properties.chaincodes[index];
                    const chaincode = {};
                    chaincode.name = q_chaincode.getName();
                    chaincode.version = q_chaincode.getVersion();
                    //TODO metadata ?
                    logger.debug('%s - found chaincode :%j', method, chaincode);
                    peer.chaincodes.push(chaincode);
                }
            }

            //all done with this peer
            peers.push(peer);
        });

        return peers;
    }

};
module.exports = ChannelExtends;
