/*
 Copyright 2018 KT All Rights Reserved.
*/
/* eslint-disable */
const clientUtils = require('fabric-client/lib/client-utils.js');

const grpc = require('grpc');
const path = require('path');

const _commonProto = grpc.load(path.join(__dirname, '../../..', 'node_modules/fabric-client/lib/protos/common/common.proto')).common;

clientUtils.signProposalOB = function (signingIdentity, proposal) {
  const proposal_bytes = proposal.toBuffer();
  // sign the proposal
  const signature = '0';

  // build manually for now
  const signedProposal = {
    signature,
    proposal_bytes,
  };
  return signedProposal;
};

clientUtils.buildHeaderOB = function (creator, channelHeader, nonce) {
  const signatureHeader = new _commonProto.SignatureHeader();
  signatureHeader.setCreator(new ArrayBuffer(1));
  signatureHeader.setNonce(nonce);

  const header = new _commonProto.Header();
  header.setSignatureHeader(signatureHeader.toBuffer());
  header.setChannelHeader(channelHeader.toBuffer());

  return header;
};

// re-export the module, for the changes to take effect
module.exports = clientUtils;
