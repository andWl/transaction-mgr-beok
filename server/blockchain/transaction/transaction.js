/* eslint-disable prefer-destructuring */

/**
 *
 * Copyright 2018 KT All Rights Reserved
 *
 */
import logger from '../../common/logger';

const Shared = require('mmap-object');
const path = require('path');

const util = require('util');
const hlf = require('../hlf-client');

class TrxClient {
  async queryChainCode(peer, channelName, chaincodeName, fcn, args, username, orgName) {
    try {
      const client = await hlf.getAdminClientForOrg(orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }

      // send query
      const request = {
        // targets: [peer], // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args,
      };
      const responsePayLoads = await channel.queryByChaincode(request);
      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          logger.info(`${args[0]} now has ${responsePayLoads[i].toString('utf8')} after the move`);
        }
        const txInfo = JSON.stringify({
          Tx_msg_query: 'QUERY TRANSACTION',
          Tx_fcn: fcn,
        });
        logger.debug(txInfo);
        // return `${args[0]} now has ${responsePayLoads[0].toString('utf8')}`;
        const responseValue = responsePayLoads[0].toString('utf8');
        return {
          value: responseValue,
        };
      }
      logger.error('responsePayLoads is null');
      return 'responsePayLoads is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async queryChainCodeForAnchor(channelName, chaincodeName, fcn, args, username) {
    let peer = null;
    let orgName = null;

    try {
      const read_only_shared_object = new Shared.Open(path.join(__dirname, '../../../', 'peer-orderer-default'));
      peer = read_only_shared_object.checked_peer;
      orgName = read_only_shared_object.checked_org;
      read_only_shared_object.close();
    } catch (err) {
      logger.info('grpc health check error :', err);
      // peer = 'peer0.org2.example.com';
      // orgName = 'Org2';
    }

    try {
      const client = await hlf.getAdminClientForOrg(orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }

      // send query
      const request = {
        targets: peer, // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args,
      };
      if (peer) {
        request.targets = [peer];
      }

      const responsePayLoads = await channel.queryByChaincode(request);
      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          if (responsePayLoads[i][0]) {
            logger.info(`value of ${args[0]} is ${responsePayLoads[i].toString('utf8')} `);
            return `${responsePayLoads[i].toString('utf8')}`;
          }
        }
      }
      logger.error('responsePayLoads is null');
      return 'responsePayLoads is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async queryChainCodeForMessage(peer, channelName, chaincodeName, fcn, args, username, orgName) {
    try {
      const client = await hlf.getAdminClientForOrg(orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }

      // send query
      const request = {
        // targets: peer, // queryByChaincode allows for multiple targets
        chaincodeId: chaincodeName,
        fcn,
        args,
      };
      if (peer) {
        request.targets = [peer];
      }

      const responsePayLoads = await channel.queryByChaincode(request);
      if (responsePayLoads) {
        for (let i = 0; i < responsePayLoads.length; i++) {
          if (responsePayLoads[i][0]) {
            logger.info(`value of ${args[0]} is ${responsePayLoads[i].toString('utf8')} `);
            return `${responsePayLoads[i].toString('utf8')}`;
          }
        }
      }
      logger.error('responsePayLoads is null');
      return 'responsePayLoads is null';
    } catch (error) {
      logger.error(`Failed to query due to error: ${error.stack}` ? error.stack : error);
      return error.toString();
    }
  }

  async invokeChainCode(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    logger.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));
    let errorMsg = null;
    let txIdString = null;
    let blockHeight = null;
    let responsePayload = null;
    let responseStatus = null;
    let responseMessage = null;

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }
      const txId = client.newTransactionID();
      // will need the transaction ID string for the event registration later
      txIdString = txId.getTransactionID();

      // send proposal to endorser
      const request = {
        targets: 'peer0.org1.example.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId,
      };
      logger.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request);

      // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer
      const proposalResponses = results[0];
      const proposal = results[1];

      // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed
      let allGood = true;
      for (const i in proposalResponses) {
        let oneGood = false;
        if (proposalResponses && proposalResponses[i].response &&
            proposalResponses[i].response.status === 200) {
          oneGood = true;
          logger.info('invoke chaincode proposal was good');
        } else {
          responseStatus = proposalResponses[i].status;
          responseMessage = proposalResponses[i].message;
          logger.error(JSON.stringify(proposalResponses));
          logger.error('invoke chaincode proposal was bad');
        }
        allGood &= oneGood;
      }

      if (allGood) {
        responseStatus = proposalResponses[0].response.status;
        responsePayload = proposalResponses[0].response.payload;
        logger.info(util.format(
          'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
          proposalResponses[0].response.status, proposalResponses[0].response.message,
          proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization
        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          logger.debug('invokeEventPromise - setting up event');
          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;
              logger.error(message);
              eh.disconnect();
            }, 3000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              logger.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());
              logger.info('Transaction %s has status of %s in block %s', tx, code, blockNum);
              clearTimeout(ehTimeOut);
              blockHeight = blockNum;

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);
                logger.error(message);
                reject(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';
                logger.info(message);
                resolve(message);
              }
            }, err => {
              clearTimeout(ehTimeOut);
              logger.error(err);
              reject(err);
            },
              // the default for 'unregister' is true for transaction listeners
              // so no real need to set here, however for 'disconnect'
              // the default is false as most event hubs are long running
              // in this use case we are using it only once
            { unregister: true, disconnect: true },
            );
            eh.connect();
          });
          promises.push(invokeEventPromise);
        });

        const ordererReq = {
          txId,
          proposalResponses,
          proposal,
        };
        const sendPromise = channel.sendTransaction(ordererReq);
        // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing
        promises.push(sendPromise);
        const results = await Promise.all(promises);
        logger.debug(util.format('------->>> R E S P O N S E : %j', results));
        const response = results.pop(); //  orderer results are last in the results
        if (response.status === 'SUCCESS') {
          logger.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);
          logger.debug(errorMsg);
        }

        // now see what each of the event hubs reported
        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];
          logger.debug('Event results for event hub :%s', eh.getPeerAddr());
          if (typeof ehResult === 'string') {
            logger.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();
            logger.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');
        logger.debug(errorMsg);
      }
    } catch (error) {
      logger.error(`
              }Failed to invoke due to error: ${error.stack}` ? error.stack : error);
      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format(
        'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        orgName, channelName, txIdString);
      logger.info(message);

      const txInfo = JSON.stringify({
        Tx_msg_invoke: 'INVOKE TRANSACTION',
        Tx_ID: txIdString,
        Tx_blockNum: blockHeight,
        Tx_channel: channelName,
        Tx_org: orgName,
        Tx_fcn: fcn,
      });
      logger.debug(txInfo);
      logger.debug('responsePayload is :%s', responsePayload);

      const payloadString = responsePayload.toString();
      return {
        status: responseStatus,
        txId: txIdString,
        message: payloadString,
      };
    }
    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);
    logger.error(message);
    // throw new Error(message);
    return {
      status: responseStatus,
      message: responseMessage,
    };
  }

  async invokeChainCodeForAnchor(channelName, chaincodeName, fcn, value, username) {
    logger.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));
    let errorMsg = null;
    let txIdString = null;
    const args = [];

    let orderer = null; // location.orderer;
    let peer = null; // location.peer;
    let orgName = null; // location.orgName;
    let target_changed = False; // location.target_changed;

    try {
      const read_only_shared_object = new Shared.Open(path.join(__dirname, '../../../', 'peer-orderer-default'));
      orderer = read_only_shared_object.checked_orderer;
      peer = read_only_shared_object.checked_peer;
      orgName = read_only_shared_object.checked_org;
      target_changed = read_only_shared_object.checked_target_changed;



      read_only_shared_object.close();
    } catch (err) {
      logger.info('grpc health check error :', err);
      // orderer = 'orderer2.example.com';
      // peer = 'peer0.org2.example.com';
      // orgName = 'Org2';
    }

    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }
      const txId = client.newTransactionID();
      // will need the transaction ID string for the event registration later
      txIdString = txId.getTransactionID();

      // input txId to key
      args.push(txIdString);
      args.push(value);

      logger.info(`peer_name : ${peer}`);
      logger.info(`orderer_name : ${orderer}`);
      logger.info(`orgName : ${orgName}`);

      // send proposal to endorser
      const request = {
        targets: peer,
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId,
      };
      logger.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request);

      // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer
      const proposalResponses = results[0];
      const proposal = results[1];

      // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed
      let allGood = true;
      for (const i in proposalResponses) {
        let oneGood = false;
        if (proposalResponses && proposalResponses[i].response &&
            proposalResponses[i].response.status === 200) {
          oneGood = true;
          logger.info('invoke chaincode proposal was good');
        } else {
          logger.error(JSON.stringify(proposalResponses));
          logger.error('invoke chaincode proposal was bad');
        }
        allGood &= oneGood;
      }

      if (allGood) {
        logger.info(util.format(
          'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
          proposalResponses[0].response.status, proposalResponses[0].response.message,
          proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization
        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          logger.debug('invokeEventPromise - setting up event');

          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;
              logger.info(message);
              eh.disconnect();
            }, 3000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              logger.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());
              logger.info('Transaction %s has status of %s in block %s', tx, code, blockNum);
              clearTimeout(ehTimeOut);

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);
                logger.error(message);
                reject(new Error(message));
              } else {
                const message = util.format('The invoke chaincode transaction was valid. -', eh.getPeerAddr());
                logger.info(message);
                resolve(message);
                promises.push(invokeEventPromise);
              }
            }, err => {
              clearTimeout(ehTimeOut);
              logger.info(err, 'The invoke event hub was timeout on', eh.getPeerAddr());
              resolve(err);
            },
              // the default for 'unregister' is true for transaction listeners
              // so no real need to set here, however for 'disconnect'
              // the default is false as most event hubs are long running
              // in this use case we are using it only once
            { unregister: true, disconnect: true },
            );
            eh.connect();
          });
          // promises.push(invokeEventPromise);
        });

        const ordererReq = {
          orderer,
          txId,
          proposalResponses,
          proposal,
        };
        const sendPromise = channel.sendTransaction(ordererReq);
        // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing
        promises.push(sendPromise);
        const results = await Promise.all(promises);
        logger.debug(util.format('------->>> R E S P O N S E : %j', results));
        const response = results.pop(); //  orderer results are last in the results
        if (response.status === 'SUCCESS') {
          logger.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);
          logger.debug(errorMsg);
        }

        // now see what each of the event hubs reported
        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];
          logger.debug('Event results for event hub :%s', eh.getPeerAddr());
          if (typeof ehResult === 'string') {
            logger.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();
            logger.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');
        logger.debug(errorMsg);
      }
    } catch (error) {
      logger.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);
      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format(
        'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        orgName, channelName, txIdString);
      logger.info(message);

      return txIdString;
    }
    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);
    logger.error(message);
    throw new Error(message);
  }

  async invokeChainCodeForMessage(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    logger.debug(util.format('\n============ invokeChainCode : invoke transaction on channel %s ============\n', channelName));
    let errorMsg = null;
    let txIdString = null;
    let responseStatus = null;
    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg(orgName);
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }
      const txId = client.newTransactionID();
      // will need the transaction ID string for the event registration later
      txIdString = txId.getTransactionID();

      // const response = await channel.initialize({
      //   discover: true,
      //   asLocalhost: true,
      // });
      // logger.debug('Service discovery init : ');
      // logger.debug(response);

      // send proposal to endorser
      const request = {
        // targets: 'peer1.gr.kt.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId,
      };
      if (peerNames) {
        request.targets = peerNames;
      }

      logger.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request);

      // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer
      const proposalResponses = results[0];
      const proposal = results[1];

      // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed
      let allGood = true;
      for (const i in proposalResponses) {
        let oneGood = false;
        if (proposalResponses && proposalResponses[i].response &&
          proposalResponses[i].response.status === 200) {
          oneGood = true;
          logger.info('invoke chaincode proposal was good on peer', proposalResponses[i].peer.name);
        } else {
          logger.error(JSON.stringify(proposalResponses));
          logger.error('invoke chaincode proposal was bad on peer', proposalResponses[i].peer.name);
        }
        allGood &= oneGood;
      }

      if (allGood) {
        responseStatus = proposalResponses[0].response.status;
        logger.info(util.format(
          'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
          proposalResponses[0].response.status, proposalResponses[0].response.message,
          proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization
        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          logger.debug('invokeEventPromise - setting up event');

          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;
              logger.info(message);
              eh.disconnect();
            }, 3000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              logger.info('The invoke chaincode transaction has been committed on peer', eh.getPeerAddr());
              logger.info('Transaction %s has status of %s in block %s', tx, code, blockNum);
              clearTimeout(ehTimeOut);

              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:', code);
                logger.error(message);
                reject(new Error(message));
              } else {
                const message = util.format('The invoke chaincode transaction was valid. -', eh.getPeerAddr());
                logger.info(message);
                resolve(message);
                promises.push(invokeEventPromise);
              }
            }, err => {
              clearTimeout(ehTimeOut);
              logger.info(err, 'The invoke event hub was timeout on', eh.getPeerAddr());
              resolve(err);
            },
              // the default for 'unregister' is true for transaction listeners
              // so no real need to set here, however for 'disconnect'
              // the default is false as most event hubs are long running
              // in this use case we are using it only once
            { unregister: true, disconnect: true },
            );
            eh.connect();
          });
            // promises.push(invokeEventPromise);
        });

        const ordererReq = {
          txId,
          proposalResponses,
          proposal,
          // orderer: 'orderer1.gr.kt.com',
        };
        const sendPromise = channel.sendTransaction(ordererReq);
        // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing
        promises.push(sendPromise);
        const results = await Promise.all(promises);
        logger.debug(util.format('------->>> R E S P O N S E : %j', results));
        const response = results.pop(); //  orderer results are last in the results
        if (response.status === 'SUCCESS') {
          logger.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);
          logger.debug(errorMsg);
        }

        // now see what each of the event hubs reported
        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];
          logger.debug('Event results for event hub :%s', eh.getPeerAddr());
          if (typeof ehResult === 'string') {
            logger.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();
            logger.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');
        logger.debug(errorMsg);
      }
    } catch (error) {
      logger.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);
      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format(
        'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        orgName, channelName, txIdString);
      logger.info(message);

      return {
        status: responseStatus,
        txID: txIdString,
      };
    }
    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);
    logger.error(message);
    throw new Error(message);
  }

  async invokeChainCodeDiscover(peerNames, channelName, chaincodeName, fcn, args, username, orgName, invokeOption) {
    logger.debug(util.format('\n============ invokeChainCodeDiscover : invoke transaction on channel %s ============\n', channelName));
    let errorMsg = null;
    let txIdString = null;
    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getOBChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }
      const txId = client.newTransactionID();
      // will need the transaction ID string for the event registration later
      txIdString = txId.getTransactionID();

      // send proposal to endorser
      const request = {
        // targets: 'peer0.org2.example.com',
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId,
      };

      if (invokeOption === 'load_info') {
        // endorsing role peers
        const peersObj = channel._getTargets('', 'endorsingPeer');
        this._endorsingPeers = [];
        if (peersObj) {
          logger.debug('Successfully got peer list');
          for (const i in peersObj) {
            this._endorsingPeers.push(peersObj[i]._name);
          }
        }

        // initialize service discovery to get load_info
        const response = await channel.initializeOB({ discover: true, asLocalhost: true });
        if (response.peers_by_org) {
          logger.debug('Successfully got discovery info');
          const orgMSPName = Object.keys(response.peers_by_org);
          const targetPeer = { name: null, load_info: 100 };
          const discoveryPeer = {};
          for (const i in orgMSPName) {
            logger.debug(orgMSPName[i]);
            for (const j in response.peers_by_org[orgMSPName[i]].peers) {
              discoveryPeer.name = response.peers_by_org[orgMSPName[i]].peers[j].endpoint.split(':')[0];
              discoveryPeer.load_info = response.peers_by_org[orgMSPName[i]].peers[j].load_info;
              logger.debug('========= discovery info');
              logger.debug('name : %s, load_info : %s', discoveryPeer.name, discoveryPeer.load_info);

              this._endorsingPeers.forEach(peerName => {
                if (peerName === discoveryPeer.name && targetPeer.load_info > discoveryPeer.load_info) {
                  targetPeer.name = discoveryPeer.name;
                  targetPeer.load_info = discoveryPeer.load_info;
                }
              });
            }
          }
          logger.debug('target peer:%s, load_info:%s', targetPeer.name, targetPeer.load_info);
          request.targets = targetPeer.name;
        } else {
          const message = util.format('Failed to get load_info');
          logger.error(message);
        }
      } else if (invokeOption === 'discover') {
        await channel.initializeOB({ discover: true, asLocalhost: true });
      }
      // else if (invokeOption === 'targeted') {
      //   request.targets = 'peer0.org1.example.com';
      // } else if (invokeOption == '' || 'non_target')

      logger.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposal(request);

      // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer
      const proposalResponses = results[0];
      const proposal = results[1];

      // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed
      let allGood = true;
      for (const i in proposalResponses) {
        let oneGood = false;
        if (proposalResponses && proposalResponses[i].response &&
          proposalResponses[i].response.status === 200) {
          oneGood = true;
          logger.info('invoke chaincode proposal was good');
        } else {
          logger.error(JSON.stringify(proposalResponses));
          logger.error('invoke chaincode proposal was bad');
        }
        allGood &= oneGood;
      }

      if (allGood) {
        logger.info(util.format(
          'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
          proposalResponses[0].response.status, proposalResponses[0].response.message,
          proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        // wait for the channel-based event hub to tell us
        // that the commit was good or bad on each peer in our organization
        const promises = [];
        const eventHubs = channel.getChannelEventHubsForOrg();
        eventHubs.forEach(eh => {
          logger.debug('invokeEventPromise - setting up event');
          const invokeEventPromise = new Promise((resolve, reject) => {
            const ehTimeOut = setTimeout(() => {
              const message = `REQUEST_TIMEOUT:${eh.getPeerAddr()}`;
              logger.error(message);
              eh.disconnect();
            }, 30000);
            eh.registerTxEvent(txIdString, (tx, code, blockNum) => {
              logger.info('The chaincode invoke chaincode transaction has been committed on peer %s', eh.getPeerAddr());
              logger.info('Transaction %s has status of %s in blocl %s', tx, code, blockNum);
              clearTimeout(ehTimeOut);


              if (code !== 'VALID') {
                const message = util.format('The invoke chaincode transaction was invalid, code:%s', code);
                logger.error(message);
                reject(new Error(message));
              } else {
                const message = 'The invoke chaincode transaction was valid.';
                logger.info(message);
                resolve(message);
              }
            }, err => {
              clearTimeout(ehTimeOut);
              logger.error(err);
              reject(err);
            },
            // the default for 'unregister' is true for transaction listeners
            // so no real need to set here, however for 'disconnect'
            // the default is false as most event hubs are long running
            // in this use case we are using it only once
            { unregister: true, disconnect: true },
            );
            eh.connect();
          });
          promises.push(invokeEventPromise);
        });

        const ordererReq = {
          txId,
          proposalResponses,
          proposal,
        };
        const sendPromise = channel.sendTransaction(ordererReq);
        // put the send to the orderer last so that the events get registered and
        // are ready for the orderering and committing
        promises.push(sendPromise);
        const results = await Promise.all(promises);
        logger.debug(util.format('------->>> R E S P O N S E : %j', results));
        const response = results.pop(); //  orderer results are last in the results
        if (response.status === 'SUCCESS') {
          logger.info('Successfully sent transaction to the orderer.');
        } else {
          errorMsg = util.format('Failed to order the transaction. Error code: %s', response.status);
          logger.debug(errorMsg);
        }

        // now see what each of the event hubs reported
        for (const i in results) {
          const ehResult = results[i];
          const eh = eventHubs[i];
          logger.debug('Event results for event hub :%s', eh.getPeerAddr());
          if (typeof ehResult === 'string') {
            logger.debug(ehResult);
          } else {
            if (!errorMsg) errorMsg = ehResult.toString();
            logger.debug(ehResult.toString());
          }
        }
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');
        logger.debug(errorMsg);
      }
    } catch (error) {
      logger.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);
      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format(
        'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        orgName, channelName, txIdString);
      logger.info(message);

      return txIdString;
    }
    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);
    logger.error(message);
    throw new Error(message);
  }

  async invokeChainCodeOB(peerNames, channelName, chaincodeName, fcn, args, username, orgName) {
    logger.debug(util.format('\n============ invoke transaction on channel %s ============\n', channelName));
    let errorMsg = null;
    let txIdString = null;
    try {
      // first setup the client for this org
      const client = await hlf.getAdminClientForOrg('Org1');
      logger.debug('Successfully got the fabric client for the organization "%s"', orgName);
      const channel = client.getOBChannel(channelName);
      if (!channel) {
        const message = util.format('Channel %s was not defined in the connection profile', channelName);
        logger.error(message);
        throw new Error(message);
      }
      const txId = client.newTransactionID();
      // will need the transaction ID string for the event registration later
      txIdString = txId.getTransactionID();

      // send proposal to endorser
      const request = {
        targets: peerNames,
        chaincodeId: chaincodeName,
        fcn,
        args,
        chainId: channelName,
        txId,
      };
      logger.debug(`request:${JSON.stringify(request)}`);

      const results = await channel.sendTransactionProposalOB(request);

      // the returned object has both the endorsement results
      // and the actual proposal, the proposal will be needed
      // later when we send a transaction to the orderer
      const proposalResponses = results[0];
      const proposal = results[1];

      // lets have a look at the responses to see if they are
      // all good, if good they will also include signatures
      // required to be committed
      let allGood = true;
      for (const i in proposalResponses) {
        let oneGood = false;
        if (proposalResponses && proposalResponses[i].response &&
          proposalResponses[i].response.status === 200) {
          oneGood = true;
          logger.info('invoke chaincode proposal was good');
        } else {
          logger.error(JSON.stringify(proposalResponses));
          logger.error('invoke chaincode proposal was bad');
        }
        allGood &= oneGood;
      }

      if (allGood) {
        logger.info(util.format(
          'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
          proposalResponses[0].response.status, proposalResponses[0].response.message,
          proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));

        const ordererReq = {
          txId,
          proposalResponses,
          proposal,
        };

        channel.sendTransactionOB(ordererReq);
      } else {
        errorMsg = util.format('Failed to send Proposal and receive all good ProposalResponse');
        logger.debug(errorMsg);
      }
    } catch (error) {
      logger.error(`Failed to invoke due to error: ${error.stack}` ? error.stack : error);
      errorMsg = error.toString();
    }

    if (!errorMsg) {
      const message = util.format(
        'Successfully invoked the chaincode %s to the channel \'%s\' for transaction ID: %s',
        orgName, channelName, txIdString);
      logger.info(message);

      return txIdString;
    }
    const message = util.format('Failed to invoke chaincode. cause:%s', errorMsg);
    logger.error(message);
    throw new Error(message);
  }
}

const trxClient = new TrxClient();
module.exports = trxClient;
