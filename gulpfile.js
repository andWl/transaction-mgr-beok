/**
 *
 * Copyright 2018 KT All Rights Reserved
 *
 */
const requireDir = require('require-dir');
const gulp = require('gulp');
const shell = require('gulp-shell');

// Require all tasks in gulp/tasks, including subfolders
requireDir('./build/tasks', { recurse: true });

gulp.task('default', ['lint', 'check_license'], () => {
  // This will only run if the lint task is successful...
});

// First network
gulp.task('first-network', ['setup-chaincode-first_network', 'set-project-env-first_network_1', 'set-project-env-first_network_2', 'set-project-env-first_network_3'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.first_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-first_network', ['setup-channel-first_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org1.sh balance 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org2.sh balance 0.1',
]));

gulp.task('setup-channel-first_network', ['first_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-first_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-first_network_2', () => process.env.DOCKER_IMG_TAG = ':1.3.0');
gulp.task('set-project-env-first_network_2', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.13');
gulp.task('set-project-env-first_network_3', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('first_network', ['clean-up',
  'docker-clean-first_network'], shell.task([
  'docker-compose -f tools/network/first-network/docker-compose.yaml up -d',
]));

gulp.task('docker-clean-first_network', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-balance-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/first-network/docker-compose.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));

// Basic network
gulp.task('basic-network', ['setup-chaincode-basic_network'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.basic_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-basic_network', ['setup-channel-basic_network', 'set-project-env-basic_network_1', 'set-project-env-basic_network_2', 'set-project-env-basic_network_3', 'set-project-env-basic_network_4'], shell.task([
  'docker exec cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode.sh balance 0.1',
]));

gulp.task('setup-channel-basic_network', ['basic_network'], shell.task([
  'docker exec cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel.sh',
]));

gulp.task('set-project-env-basic_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-basic_network_2', () => process.env.DOCKER_IMG_TAG = ':1.3.0');
gulp.task('set-project-env-basic_network_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.13');
gulp.task('set-project-env-basic_network_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('basic_network', ['clean-up', 'docker-clean-basic_network'], shell.task([
  'docker-compose -f tools/network/basic-network/docker-compose.yaml up -d',
]));

gulp.task('docker-clean-basic_network', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-balance-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/basic-network/docker-compose.yaml down',
], {
  verbose: true,
  ignoreErrors: true,
}));

// OB network
gulp.task('ob-network', ['setup-chaincode-ob_network', 'set-project-env-ob_network_1', 'set-project-env-ob_network_2'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.ob_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-ob_network', ['setup-channel-ob_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org1.sh balance 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org2.sh balance 0.1',
]));

gulp.task('setup-channel-ob_network', ['ob_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-ob_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-ob_network_2', () => process.env.DOCKER_IMG_TAG = ':ob-amd64-1.3.1-snapshot-a6df81e04');

gulp.task('ob_network', ['clean-up', 'docker-clean-ob_network', 'set-project-env-ob_network_1', 'set-project-env-ob_network_2'], shell.task([
  'docker-compose -f tools/network/ob-network/docker-compose.yaml up -d',
]));

gulp.task('docker-clean-ob_network', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-balance-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/ob-network/docker-compose.yaml down --volumes --remove-orphans',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));

// Fabric 1.4.1 network - zookeeper, kafka
gulp.task('fabric1.4.1-network', ['setup-chaincode-fabric1.4.1_network', 'set-project-env-fabric1.4.1_network_1', 'set-project-env-fabric1.4.1_network_2', 'set-project-env-fabric1.4.1_network_3', 'set-project-env-fabric1.4.1_network_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.fabric1.4.1_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-fabric1.4.1_network', ['setup-channel-fabric1.4.1_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org1.sh balance 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org2.sh balance 0.1',
]));

gulp.task('setup-channel-fabric1.4.1_network', ['fabric1.4.1_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-fabric1.4.1_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-fabric1.4.1_network_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-fabric1.4.1_network_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-fabric1.4.1_network_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('fabric1.4.1_network', ['clean-up', 'docker-clean-fabric1.4.1_network'], shell.task([
  'docker-compose -f tools/network/fabric1.4.1-network-kafka/docker-compose.yaml up -d',
]));

gulp.task('docker-clean-fabric1.4.1_network', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-balance-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/fabric1.4.1-network-kafka/docker-compose.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));


// Fabric 1.4.1 network - zookeeper, kafka (op_cc)
gulp.task('fabric1.4.1-network-op', ['setup-chaincode-fabric1.4.1_network_op', 'set-project-env-fabric1.4.1_network_op_1', 'set-project-env-fabric1.4.1_network_op_2', 'set-project-env-fabric1.4.1_network_op_3', 'set-project-env-fabric1.4.1_network_op_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.fabric1.4.1_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-fabric1.4.1_network_op', ['setup-channel-fabric1.4.1_network_op'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-op-org1.sh op_cc 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-op-org2.sh op_cc 0.1',
]));

gulp.task('setup-channel-fabric1.4.1_network_op', ['fabric1.4.1_network_op'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-fabric1.4.1_network_op_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-fabric1.4.1_network_op_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-fabric1.4.1_network_op_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-fabric1.4.1_network_op_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('fabric1.4.1_network_op', ['clean-up', 'docker-clean-fabric1.4.1_network_op'], shell.task([
  'docker-compose -f tools/network/fabric1.4.1-network-kafka/docker-compose.yaml up -d',
]));

gulp.task('docker-clean-fabric1.4.1_network_op', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-op_cc-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/fabric1.4.1-network-kafka/docker-compose.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));


// Fabric 1.4.1 network - raft
gulp.task('raft-network', ['setup-chaincode-raft_network', 'set-project-env-raft_network_1', 'set-project-env-raft_network_2', 'set-project-env-raft_network_3', 'set-project-env-raft_network_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.raft_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-raft_network', ['setup-channel-raft_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org1.sh balance 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-org2.sh balance 0.1',
]));

gulp.task('setup-channel-raft_network', ['raft_network'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-raft_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-raft_network_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-raft_network_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-raft_network_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('raft_network', ['clean-up', 'docker-clean-raft_network'], shell.task([
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml up -d',
]));

gulp.task('docker-clean-raft_network', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-balance-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));

// Fabric 1.4.1 network - raft (op_cc)
gulp.task('raft-network-op', ['setup-chaincode-raft_network_op', 'set-project-env-raft_network_1', 'set-project-env-raft_network_2', 'set-project-env-raft_network_3', 'set-project-env-raft_network_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.raft_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-raft_network_op', ['setup-channel-raft_network_op'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-op-org1.sh op_cc 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-op-org2.sh op_cc 0.1',
]));

gulp.task('setup-channel-raft_network_op', ['raft_network_op'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-raft_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-raft_network_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-raft_network_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-raft_network_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('raft_network_op', ['clean-up', 'docker-clean-raft_network_op'], shell.task([
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml up -d',
]));

gulp.task('docker-clean-raft_network_op', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-op_cc-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));

// Fabric 1.4.1 network - raft (tns_cc)
gulp.task('raft-network-tns', ['setup-chaincode-raft_network_tns', 'set-project-env-raft_network_1', 'set-project-env-raft_network_2', 'set-project-env-raft_network_3', 'set-project-env-raft_network_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.raft_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-raft_network_tns', ['setup-channel-raft_network_tns'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-tns-org1.sh tns_cc 0.1',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-tns-org2.sh tns_cc 0.1',
]));

gulp.task('setup-channel-raft_network_tns', ['raft_network_tns'], shell.task([
  'docker exec org1-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org1.sh',
  'docker exec org2-cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel-org2.sh',
]));

gulp.task('set-project-env-raft_network_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-raft_network_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-raft_network_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-raft_network_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('raft_network_tns', ['clean-up', 'docker-clean-raft_network_tns'], shell.task([
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml up -d',
]));

gulp.task('docker-clean-raft_network_tns', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-tns-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/fabric1.4.1-network-raft/docker-compose-etcdraft.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));


// Raft test network - fabric 1.4.1 (op_cc)
gulp.task('raft-test', ['setup-chaincode-raft_test', 'set-project-env-raft_test_1', 'set-project-env-raft_test_2', 'set-project-env-raft_test_3', 'set-project-env-raft_test_4'], shell.task([
  'nodemon server --exec babel-node --config .nodemonrc.raft_test_network.json | pino-pretty',
]));

gulp.task('setup-chaincode-raft_test', ['setup-channel-raft_test'], shell.task([
  'docker exec cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-chaincode-op.sh op_cc 0.1',
]));

gulp.task('setup-channel-raft_test', ['raft_test'], shell.task([
  'docker exec cli /opt/gopath/src/github.com/hyperledger/fabric/peer/cli/setup-channel.sh',
]));

gulp.task('set-project-env-raft_test_1', () => process.env.COMPOSE_PROJECT_NAME = 'net');
gulp.task('set-project-env-raft_test_2', () => process.env.DOCKER_IMG_TAG = ':1.4.1');
gulp.task('set-project-env-raft_test_3', () => process.env.THIRDPARTY_IMG_TAG = ':0.4.15');
gulp.task('set-project-env-raft_test_4', () => process.env.HFC_LOGGING = '{"debug":"console", "info":"console"}');

gulp.task('raft_test', ['clean-up',
  'docker-clean-raft_test'], shell.task([
  'docker-compose -f tools/network/raft-test-network/docker-compose-etcdraft2.yaml up -d',
]));

gulp.task('docker-clean-raft_test', shell.task([
  'docker rm -f $(docker ps -aq)',
  'docker rmi $(docker images | grep "^dev-peer[0-9].org[0-9].example.com-op_cc-" | awk \'{print $3}\')',
  'docker-compose -f tools/network/raft-test-network/docker-compose-etcdraft2.yaml down',
], {
  verbose: true, // so we can see the docker command output
  ignoreErrors: true, // kill and rm may fail because the containers may have been cleaned up
}));
